import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {
    getQueryParam,
    fireEvent,
    objectDiff,
    getCookie,
    setCookie,
    array_merge_distinct,
    multiStringReplace
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {ContactManager} from "../../contact-manager/js/contact-manager.js?v=[cs_version]"
import {PbDocuments, PbDocument} from "./documents.js?v=[cs_version]"
import {PbSelect2Tags} from "../../csf-lib/pb-select2-tags.js?v=[cs_version]"
import {PbEditor} from "../../csf-lib/pb-editor.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbGuide} from "../../csf-lib/pb-guide.js?v=[cs_version]"
import {pbEventEntries} from "../../easy2schedule/js/pb-event-entries.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTag} from "../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"
import {CallcenterQuestionaire} from "./questionaire.js?v=[cs_version]"
import {PbBrowserDb} from "../../csf-lib/pb/indexeddb/PbBrowserDb.js?v=[cs_version]"
import {PbApiRequest} from "../../csf-lib/pbapi/pb-api-request.js?v=[cs_version]"

export class RenderCallcenter {
    constructor(socket, DocumentationModuleInstance) {
        if (!RenderCallcenter.instance) {
            let nj = nunjucks.configure(CONFIG.APP_BASE_URL, {autoescape: false})
            nj.addFilter('date', (data) => {
                let createtime = moment(data)
                return createtime.format(CONFIG.DATETIMEFORMAT)
            })
            RenderCallcenter.instance = this
            RenderCallcenter.instance.socket = socket
            RenderCallcenter.instance.displayProperties = getCookie("dcp") == "true"
            RenderCallcenter.instance.source_id = getQueryParam("sid")
            RenderCallcenter.instance.initViewListener()
            RenderCallcenter.instance.documentationModuleInstance = DocumentationModuleInstance
            RenderCallcenter.instance.notifications = new PbNotifications()
            RenderCallcenter.instance.tagListModel = new PbTagsList()
            RenderCallcenter.instance.questionaireDirty = true
            RenderCallcenter.instance.activeDocument = undefined
            RenderCallcenter.instance.documentDirty = false
            let dbStores = {
                questionaire: "[questionaireID+questionID+answerID],[questionaireID+questionID], [questionaireID+answerID], data"
            }
            RenderCallcenter.instance.db = new PbBrowserDb("questionaire", dbStores, 2)
            RenderCallcenter.instance.handledTaskIds = []

            let today = new Date()
            var year = today.getFullYear();
            var month = String(today.getMonth() + 1).padStart(2, "0");
            var day = String(today.getDate()).padStart(2, "0");

            $("#header_search").val(`${day}.${month}.${year} Telefondienst`)
            document.addEventListener("pbTagsListChanged", function _(event) {
                if (!RenderCallcenter.instance.handledTaskIds.includes(event.data.args.task_id)) {
                    RenderCallcenter.instance.handledTaskIds.push(event.data.args.task_id)
                    RenderCallcenter.instance.renderTagList(event.data.args.tags)
                }
            })

            document.addEventListener("command", function _(event) {
                // if (!RenderCallcenter.instance.handledTaskIds.includes(event.data.data.args.task_id)) {
                //     RenderCallcenter.instance.handledTaskIds.push(event.data.data.args.task_id)
                // } else {
                //     return
                // }

                if (event.data.data.event == "questionaireAnswersAdded") {
                    RenderCallcenter.instance.handleNewTagCommand()
                    RenderCallcenter.instance.resetQuestionaireForm()
                    RenderCallcenter.instance.notifications.showHint("Saved")
                } else if (event.data.data.event == "questionaireAnswersAddedFailed") {
                    RenderCallcenter.instance.notifications.showHint("Saving failed, please try again", "error")
                }
            })

        }

        return RenderCallcenter.instance
    }

    init() {
        let is_configured = false
        let is_admin = true
        if (!is_configured) {
            if (is_admin) {
                new PbGuide("./tours/other_tour.js")
            } else {
                new PbGuide("./tours/test.js")
            }
        }

        let renderer = new RenderCallcenter(
            getQueryParam("apikey"),
            CONFIG,
            RenderCallcenter.instance.documentationModuleInstance.socket,
            RenderCallcenter.instance.documentationModuleInstance.docs
        )

        let contact = new ContactManager()
        let eventEntries = new pbEventEntries()

        let promises = [contact.init(), eventEntries.init(), RenderCallcenter.instance.db.init()]
        Promise.allSettled(promises).then(async function () {

        }).catch(function (e) {
            console.error("ERROR during init of renderCallcenter", e)
            $("#event_form_action_buttons button").attr("disabled", true)
            $("#event_form_action_buttons button").attr("title", new pbI18n().translate("Disabled to prevent data-loss due to an error while page load. Please reload the page and try again."))
        }).finally(function () {
            RenderCallcenter.instance.contacts_list = contact.contacts_list
            let fetchFunction = RenderCallcenter.instance.documentationModuleInstance.docs.fetchDocumentsBySourceId
            if (!RenderCallcenter.instance.documentationModuleInstance.source_id) {
                fetchFunction = RenderCallcenter.instance.documentationModuleInstance.docs.fetchAllDocuments
            }

            fetchFunction(RenderCallcenter.instance.documentationModuleInstance.source_id).then((docs) => {
                let eventEntries = new pbEventEntries()
                let sorted_groups = eventEntries.possible_groups;
                sorted_groups.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);

                let sorted_members = eventEntries.group_members;
                sorted_members.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1);
                let groupsandmembers = sorted_groups.concat(sorted_members);

                RenderCallcenter.instance.groupsandmembers = groupsandmembers
                RenderCallcenter.instance.documentationModuleInstance.docs.filter($("#documents_search_input").val())
                RenderCallcenter.instance.renderDocumentList(docs)
                RenderCallcenter.instance.renderContactsList()
            })
            RenderCallcenter.instance.renderQuestionaire("firstCallQuestionaire_2024")

            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }
        })


    }

    renderAttachments() {
        PbTpl.instance.renderInto('documentation/templates/sub-attachments.tpl', {document: RenderCallcenter.instance.activeDocument}, "#attachments_container")
    }

    renderQuestionaire(questionaireID) {
        RenderCallcenter.instance.notifications.blockForLoading("#contentRight")
        new PbApiRequest("/get_questionaire_config_by_id/" + questionaireID, "GET").execute().then(function (questionaireConfig) {
            RenderCallcenter.instance.questionaireConfig = questionaireConfig
            RenderCallcenter.instance.notifications.unblock("#contentRight")
            if (!questionaireConfig || questionaireConfig?.error) {
                RenderCallcenter.instance.notifications.showHint(questionaireConfig?.error, "warning")
                $("#contentRight").removeClass("show")
            } else {
                new CallcenterQuestionaire(PbTpl.instance, questionaireConfig).renderInto("#contentRight",
                    () => this.postProcessQuestionaire(questionaireConfig),
                    (question) => {
                        for (let answerInput of question.answer.data.inputs) {
                            RenderCallcenter.instance.db.delete("questionaire", {
                                "questionaireID": questionaireConfig.id,
                                "answerID": "answer_value_" + answerInput.id
                            })
                        }
                    })
                    .then(function () {
                        this.postProcessQuestionaire(questionaireConfig)
                    }.bind(this))
            }
        }.bind(this)).catch(function () {
            window.setTimeout(function () {
                this.renderQuestionaire(questionaireID)
            }.bind(this), 1500)
        }.bind(this))
    }

    postProcessQuestionaire(questionaireConfig) {
        if (questionaireConfig.id == "firstCallQuestionaire_2024") {
            $("#firstCallButton").addClass("btn-indigo").removeClass("btn-outline-light")
            $("#followUpCallButton").removeClass("btn-indigo").addClass("btn-outline-light")
        } else {
            $("#firstCallButton").removeClass("btn-indigo").addClass("btn-outline-light")
            $("#followUpCallButton").addClass("btn-indigo").removeClass("btn-outline-light")
        }


        $('.content-right-resize').on('click', function (e) {
            e.preventDefault();
            let width = "25%"
            if ($('.content-right-content').css('flex-basis') == width) {
                width = '45%'
                if (!$(".az-content-left").hasClass("content-left-colapse")) {
                    toggleLeftColSize()
                }
            }
            $('.content-right-content').css({'flex-basis': `${width}`})

        })

        $('.content-right-colapse').on('click', function (e) {
            e.preventDefault();
            hideRightCol()
        })

        $("#firstCallButton").click(function () {
            RenderCallcenter.instance.resetQuestionaireForm()
            this.renderQuestionaire("firstCallQuestionaire_2024")
        }.bind(this))
        $("#followUpCallButton").click(function () {
            RenderCallcenter.instance.resetQuestionaireForm()
            this.renderQuestionaire("followUpCallQuestionaire_2024")
        }.bind(this))

        $('[data-answer-value-element]').click(async function (event) {
            if (event.currentTarget.type == "radio" | event.currentTarget.type == "checkbox") {
                $("#question_" + event.currentTarget.dataset.question_id).removeClass("missing")
            }
        })

        $('[data-answer-value-element]').change(async function (event) {
            let val = $(event.currentTarget).val()
            if (event.currentTarget.type == "radio") {
                RenderCallcenter.instance.db.delete("questionaire", {
                    "questionaireID": questionaireConfig.id,
                    "questionID": event.currentTarget.dataset.question_id
                }).then(function (data) {
                    RenderCallcenter.instance.db.bulkPut("questionaire", [
                        {
                            "questionaireID": questionaireConfig.id,
                            "questionID": event.currentTarget.dataset.question_id,
                            "answerID": event.currentTarget.id,
                            "data": val
                        }
                    ])
                })
            } else if (event.currentTarget.type == "checkbox") {
                if (event.currentTarget.checked) {
                    RenderCallcenter.instance.db.bulkPut("questionaire", [
                        {
                            "questionaireID": questionaireConfig.id,
                            "questionID": event.currentTarget.dataset.question_id,
                            "answerID": event.currentTarget.id,
                            "data": val
                        }
                    ])
                } else {
                    RenderCallcenter.instance.db.delete("questionaire", {
                        "questionaireID": questionaireConfig.id,
                        "answerID": event.currentTarget.id
                    })
                }
            } else {
                RenderCallcenter.instance.db.bulkPut("questionaire", [
                    {
                        "questionaireID": questionaireConfig.id,
                        "questionID": event.currentTarget.dataset.question_id,
                        "answerID": event.currentTarget.id,
                        "data": val
                    }
                ])
            }
        })

        RenderCallcenter.instance.db.read("questionaire", {"questionaireID": questionaireConfig.id}).then((data) => {
            RenderCallcenter.instance.questionaireDirty = data.length > 0
            for (let row of data) {
                if ($("#" + row.answerID).attr('type') == "checkbox") {
                    if (!$(`#${row.answerID}`).is(":checked"))
                        $(`#${row.answerID}`).click()
                } else if ($("#" + row.answerID).attr('type') == "radio") {
                    if (!$(`input[type=radio][data-question_id=${row.questionID}][value=${row.data}]`).is(":checked")) {
                        $(`input[type=radio][data-question_id=${row.questionID}][value=${row.data}]`).click()
                    }
                } else {
                    $("#" + row.answerID).val(row.data)
                }
                $("#question_" + row.questionID).removeClass("missing")

            }
        })

        $("#new_questionaire_button").unbind()
        $("#new_questionaire_button").click(function (event) {
            event.preventDefault()
            RenderCallcenter.instance.resetQuestionaireForm()
        })
        $("#save_questionaire_button").unbind()
        $("#save_questionaire_button").click(function (event) {
            event.preventDefault()
            RenderCallcenter.instance.db.read("questionaire", {"questionaireID": questionaireConfig.id}).then((data) => {
                let answerList = []
                for (let entry of data) {
                    let answerListEntry = {
                        "persistent_id": `${entry.questionaireID}|${entry.questionID}|${entry.answerID}`,
                        "value": entry
                    }
                    answerList.push(answerListEntry)
                }

                let payload = {
                    "data": answerList,
                    "data_owners": []
                }

                return new PbApiRequest("/es/cmd/addQuestionaireAnswers", "POST", payload).execute().then(() => {

                })
            })

        })
    }

    isQuestionaireDirty() {
        return new Promise((resolve, reject) => {
            if (RenderCallcenter.instance.questionaireConfig) {
                RenderCallcenter.instance.db.read("questionaire", {"questionaireID": RenderCallcenter.instance.questionaireConfig.id}).then((data) => {
                    resolve(RenderCallcenter.instance.questionaireDirty = data.length > 0)
                })
            } else {
                resolve(RenderCallcenter.instance.questionaireDirty = false)
            }
        })
    }

    resetQuestionaireForm() {
        $("#questionaire_form")[0].reset()
        $('input').prop('checked', false)
        RenderCallcenter.instance.db.clear("questionaire")
        $("[data-questionli]").addClass("missing")
    }

    renderTagList(selectedTags = []) {
        RenderCallcenter.instance.notifications.blockForLoading("#tag_container")
        let self = this
        let tags = array_merge_distinct(selectedTags || [], $("#tags").val() || [])
        return new Promise((resolve, reject) => {
            RenderCallcenter.instance.tagListModel.load(false, "documentation").then(() => {
                let entries = RenderCallcenter.instance.documentsInstance.documentations
                for (let entry of entries) {
                    if (entry.data?.tags) {
                        for (let localTag of entry.data.tags) {
                            let myTag = new PbTag().fromShort(localTag)
                            RenderCallcenter.instance.tagListModel.addTagToList(myTag)
                        }
                    }
                }

                for (let tag of tags) {
                    if (!tag) {
                        continue
                    }
                    if (typeof tag == "string") {
                        tag = new PbTag().fromShort(tag).getData()
                    }

                    tag = new PbTag({"name": tag.id || tag.name, "value": tag.text || tag.value})
                    if (!selectedTags.includes(tag.short())) {
                        selectedTags.push(tag.short())
                    }
                    RenderCallcenter.instance.tagListModel.addTagToList(tag)
                }

                let tagOptions = RenderCallcenter.instance.tagListModel.getSelectOptions(selectedTags)

                PbTpl.instance.renderIntoAsync('csf-lib/pbapi/tagmanager/templates/tag-select2.tpl', {
                    "multiple": true,
                    "addButton": true,
                    "propertyPath": "tags",
                    "options": tagOptions
                }, "#tag_container").then(() => {
                    $('[data-propertyname="tags"]').select2({
                        tags: true,
                        tokenSeparators: [',', ' '],
                        selectOnClose: false
                    })
                    $("#tags").attr("disabled", false)
                    $("#addGlobalTagButton").click(() => {
                        PbNotifications.instance.ask(
                            '<div><input type="text" id="newGlobalTag" placeholder="" class="form-control"/></div>',
                            'New global tag',
                            () => {
                                RenderCallcenter.instance.tagListModel.addTagToList(new PbTag().fromShort($("#newGlobalTag").val()).setUsableFor(["callcenter"]), true)
                            })
                        $("#newGlobalTag").focus()
                    })
                    RenderCallcenter.instance.notifications.unblock("#tag_container")
                    resolve()
                })
            })
        })
    }

    renderDocumentList(DocumentsInstance) {
        RenderCallcenter.instance.documentsInstance = DocumentsInstance
        RenderCallcenter.instance.documentationModuleInstance.docs.filter($("#header_search").val())
        $("#document_list").html('')
        if (RenderCallcenter.instance.source_id) {
            let sourceObject = {
                'module': RenderCallcenter.instance.source_id.split('|')[0],
                'id': RenderCallcenter.instance.source_id.split('|')[2]
            }

            $('#source_module_pill').html(`<i class="fa fa-filter mr-1"> ${sourceObject.module}`)
            $('#source_id_pill').html(`<i class="fa fa-filter mr-1"> ${sourceObject.id}`)
        }

        PbTpl.instance.renderInto('documentation/templates/documents-list.tpl', {documents: DocumentsInstance}, "#document_list")

        let self = this
        $("[data-listentry]").click((e) => {
            $("[data-listentry]").removeClass("selected")
            $(e.currentTarget).addClass("selected")
            self.renderDetailView(DocumentsInstance.getDocumentById(e.currentTarget.dataset.listentry), DocumentsInstance)

            $("#page_menu_button_2").removeClass("hidden")
            $("#page_menu_button_3").removeClass("hidden")
        })

        $("#button-new-document, [data-id='button-new-document']").click(function () {
            self.renderDetailView({}, DocumentsInstance)
        })

    }

    renderDetailView(selected_document = {}, DocumentsInstance = undefined, skipAutosaveCheck = false) {
        RenderCallcenter.instance.documentDirty = false
        selected_document = new PbDocument(structuredClone(selected_document))
        RenderCallcenter.instance.activeDocument = selected_document
        if (RenderCallcenter.instance.autosave_interval) {
            window.clearInterval(RenderCallcenter.instance.autosave_interval)
        }
        RenderCallcenter.instance.autosave_interval = window.setInterval(() => {
                if (RenderCallcenter.instance.activeDocument && RenderCallcenter.instance.documentDirty) {
                    RenderCallcenter.instance.updateDocument(RenderCallcenter.instance.activeDocument)
                    RenderCallcenter.instance.documentationModuleInstance.docs.autosave(RenderCallcenter.instance.activeDocument)
                }
            }
            , 1000)
        DocumentsInstance.getAutosavedDocumentById(selected_document.document_id).then(function (data) {
            let document = undefined
            if (data) {
                document = new PbDocument(data)
                if (skipAutosaveCheck == false) {
                    PbNotifications.instance.ask(
                        'An autosaved version was found and loaded. Do you want to keep or delete the Autosaved version?',
                        'Autosaved version found',
                        () => {

                        },
                        () => {
                            DocumentsInstance.deleteAutosave(document.document_id).then(function () {
                                RenderCallcenter.instance.documentDirty = false
                                window.clearInterval(RenderCallcenter.instance.autosave_interval)
                                RenderCallcenter.instance.renderDetailView(selected_document, DocumentsInstance)
                            }.bind(this))
                        },
                        "warning",
                        "Keep",
                        "Delete")
                } else {
                    DocumentsInstance.deleteAutosave(document.document_id).then(function () {
                        RenderCallcenter.instance.documentDirty = false
                        window.clearInterval(RenderCallcenter.instance.autosave_interval)
                        RenderCallcenter.instance.renderDetailView(selected_document, DocumentsInstance)
                    }.bind(this))
                }
            } else {
                document = selected_document
            }

            let tags = ["Telefondienst"]
            if (document.data) {

                RenderCallcenter.instance.activeDocument = document
                PbTpl.instance.renderInto('documentation/templates/detail-view-callcenter.tpl', {document: document}, "#azDocBody")
                if (document.data.type) {
                    $('#documentation_type').val(document.data.type.toLowerCase())
                }

                if (document.data && document.data.tags) {
                    tags = document.data.tags
                }

                new PbEditor(CONFIG.DOCUMENTATION.EDITOR, "#content", this.onEditorUploadStart, this.onEditorUploadFinished).then((pbeditor) => {
                    pbeditor.init().then((initialisedEditor) => {
                        RenderCallcenter.instance.editor = initialisedEditor
                        if (document.data && document.data.content)
                            RenderCallcenter.instance.editor.setContent(document.data.content)

                        RenderCallcenter.instance.renderTagList(tags).then(() => {
                            RenderCallcenter.instance.setDetailViewListener(document, DocumentsInstance)
                            RenderCallcenter.instance.renderDocumentDetailPills(document)
                        })
                        RenderCallcenter.instance.addContactsToContactList()
                        $("#contacts").select2()
                        RenderCallcenter.instance.selectAttachedContacts(document)
                    })
                })
            } else {
                let newDocument = new PbDocument()
                RenderCallcenter.instance.activeDocument = newDocument
                PbTpl.instance.renderInto('documentation/templates/detail-view-callcenter.tpl', {document: newDocument}, "#azDocBody")
                new PbEditor(CONFIG.DOCUMENTATION.EDITOR, "#content", this.onEditorUploadStart, this.onEditorUploadFinished).then((pbeditor) => {
                    pbeditor.init().then((initialisedEditor) => {
                        RenderCallcenter.instance.editor = initialisedEditor
                        RenderCallcenter.instance.renderTagList(tags).then(() => {
                            RenderCallcenter.instance.setDetailViewListener(newDocument, DocumentsInstance)
                        })
                        RenderCallcenter.instance.addContactsToContactList(true)
                    })
                })
            }

            RenderCallcenter.instance.addEntriesToAccessrightsDropdown()
            RenderCallcenter.instance.addEntriesToObserverDropdown()

            RenderCallcenter.instance.selectAttachedContacts(document)
            RenderCallcenter.instance.selectDataOwners(document)
            RenderCallcenter.instance.preselectObservers(document)
            $("#save_document_button").prop('disabled', true)
            $("#document_title_input").keyup(function () {
                $("#save_document_button").prop('disabled', false)
            })

            $(document).ready(function () {
                //$('#contacts').dropdown({placeholder: 'Kein Kontakt ausgewählt'})
                $('#permissions').dropdown({placeholder: 'Für alle sichtbar'})
                $('#observers').dropdown({placeholder: 'Keine'})
            })

            if (RenderCallcenter.instance.displayProperties) {
                $("#collapseDocumentProperties").collapse('show')
            } else {
                $("#collapseDocumentProperties").collapse('hide')
            }
        }.bind(this))
    }

    updateDocument(docu) {
        let links = []

        if (document.getElementById("contacts"))
            for (let attachedContact of document.getElementById("contacts").selectedOptions) {
                let id = attachedContact.getAttribute("data-contact-id")
                let link_object = {
                    "resolver": "contact-manager",
                    "type": "contact",
                    "external_id": id
                }
                links.push(link_object)
            }

        let data_owners = []

        if (document.getElementById("permissions")) {
            for (let attachedAccessright of document.getElementById("permissions").selectedOptions) {
                data_owners.push(attachedAccessright.getAttribute("data-accessrightid"))
            }
        }
        let observers = []
        if (document.getElementById("observers")) {
            for (let selected_observers of document.getElementById("observers").selectedOptions) {
                observers.push(selected_observers.value)
            }
        }
        let tags = []
        let options = document.getElementById("tags")?.selectedOptions
        if (options) {
            for (let tag of options) {
                tags.push(tag.text)
            }
        }

        docu.setSourceId(RenderCallcenter.instance.source_id)
        docu.setTags(tags)
        docu.setLinks(links)
        docu.setDataOwners(data_owners)
        docu.setObservers(observers)
        RenderCallcenter.instance.documentationModuleInstance.docs.autosave(docu)

    }

    renderContactsList() {
        //gather list of contacts by H1 configuration from ContactManager and return them as titles
        let tmp_titles = {}

        for (let configEntry of ContactManager.instance.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
            }
        }
        // reset contactTitles in case of rerendering if contacts are refreshed
        RenderCallcenter.instance.contactTitles = []
        let contactObject = {}
        for (let entry of RenderCallcenter.instance.contacts_list) {
            let contactId = entry.contact_id
            let contactTitleString = ""
            for (let titlefield_key of Object.keys(tmp_titles)) {
                if (entry.data[tmp_titles[titlefield_key]]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                }
                else if (entry.data[tmp_titles[titlefield_key].toLowerCase()]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key].toLowerCase()] + " "
                }
            }
            contactObject = {
                contact_id: contactId,
                title: contactTitleString
            }
            RenderCallcenter.instance.contactTitles.push(contactObject)
        }

        let tmp = RenderCallcenter.instance.contactTitles;

        tmp.sort(function (a, b) {
                a = a.title.toLowerCase()
                b = b.title.toLowerCase()
                return a < b ? -1 : 1
            }
        );

        RenderCallcenter.instance.contacts_list_sorted = tmp || [];
        RenderCallcenter.instance.addContactsToContactList()
    }


    addContactsToContactList = function (preselectSidId = false) {
        RenderCallcenter.instance.resetContactsList()
        if (document.getElementById("contacts")) {
            let dropdown = $('#contacts')
            let option = document.createElement('option')
            option.text = new pbI18n().translate("No contact linked")
            option.value = 'default' //JSON.stringify(item)
            option.setAttribute('data-contact-id', '')
            dropdown.append(option)
            for (let item of RenderCallcenter.instance.contacts_list_sorted) {
                let option = document.createElement("option");
                option.text = item.title
                if (preselectSidId && getQueryParam("sid")?.split("|").pop() == item.contact_id) {
                    option.selected = true
                }
                option.value = item.contact_id//JSON.stringify(item)
                option.setAttribute("data-contact-id", item.contact_id)
                dropdown.append(option)
            }
        }
    }

    resetContactsList() {
        var dropdown = $('#contacts');
        dropdown.find('option').remove()
    }

    selectAttachedContacts(docu) {
        let selectedIds = []
        if (docu.links && docu.links.length > 0) {
            for (let link of docu.links) {
                if (link.type == "contact") {
                    selectedIds.push(link.external_id)
                }
            }
            $('#contacts').val(selectedIds).trigger('change')
        }
    }

    addEntriesToObserverDropdown = function () {
        if ($("#observers")) {
            let dropdown = document.getElementById("observers");
            for (let item of RenderCallcenter.instance.groupsandmembers) {
                if (!item || item.includes("group.")) {
                    continue
                }
                let option = document.createElement("option");
                option.text = item.replace("user.", "")
                option.value = item.replace("user.", "")
                dropdown.append(option);
            }
        }
    }
    addEntriesToAccessrightsDropdown = function () {
        if ($("#permissions")) {
            let dropdown = document.getElementById("permissions");
            for (let item of RenderCallcenter.instance.groupsandmembers) {
                let option = document.createElement("option");
                option.text = item.replace("group.", new pbI18n().translate("Group") + ": ").replace("user.", new pbI18n().translate("User") + ": ")
                option.value = item
                option.setAttribute("data-accessrightid", item)
                dropdown.append(option);
            }
        }
    }

    preselectObservers(docu) {
        if (!docu.document_id) {
            for (let option of document.getElementById("observers")) {
                option.selected = false
            }
        } else {
            if (docu.data?.observers && docu.data?.observers.length > 0) {
                for (let option of document.getElementById("observers")) {
                    docu.data?.observers.forEach(item => {
                        if (item == option.value) {
                            option.selected = true
                        }
                    })
                }
            }
        }
    }

    selectDataOwners(docu) {
        if (docu.data_owners && docu.data_owners.length > 0) {
            for (let option of document.getElementById("permissions")) {
                docu.data_owners.forEach(item => {
                    if (item == option.value) {
                        option.selected = true
                    }
                })
            }
        }
    }

    setDetailViewListener(docu, DocumentsInstance) {
        //TODO: teach editor class to accept callback-function for change since
        // this version only works with quill, but wouldn't with future froala
        let socket = new socketio()
        RenderCallcenter.instance.editor.onTextChanged(function (delta, old, source) {
            RenderCallcenter.instance.documentDirty = true
            if (!delta && !old && !source) {
                //Froala is the editor here
                // Update the internal Content on user-input
                docu.setContent(RenderCallcenter.instance.editor.getContent())
            } else {
                //Quill is the editor here
                if (source == 'api') {
                    // Nothing to do here since the content from internal was just set into the editor by api
                } else if (source == 'user') {
                    // Update the internal Content on user-input
                    docu.setContent(RenderCallcenter.instance.editor.getContent())
                }
            }
        })


        $("*[data-key]").on('change', event => {
            RenderCallcenter.instance.documentDirty = true
            let target = $(event.target)
            let key = target.attr('data-key')
            let value = target.val()

            docu.setDataProperty(key, value)
        })

        $("select").on('change', event => {
            RenderCallcenter.instance.documentDirty = true
        })

        $('#page_menu_button_3').unbind('click')
        $("#page_menu_button_3").on('click', event => {
            let links = []

            if (document.getElementById("contacts"))
                for (let attachedContact of document.getElementById("contacts").selectedOptions) {
                    let id = attachedContact.getAttribute("data-contact-id")
                    let link_object = {
                        "resolver": "contact-manager",
                        "type": "contact",
                        "external_id": id
                    }
                    links.push(link_object)
                }

            let data_owners = []

            if (document.getElementById("permissions")) {
                for (let attachedAccessright of document.getElementById("permissions").selectedOptions) {
                    data_owners.push(attachedAccessright.getAttribute("data-accessrightid"))
                }
            }

            let observers = []
            if (document.getElementById("observers")) {
                for (let selected_observers of document.getElementById("observers").selectedOptions) {
                    observers.push(selected_observers.value)
                }
            }

            let tags = []
            let options = document.getElementById("tags").selectedOptions
            if (document.getElementById("tags"))
                for (let tag of document.getElementById("tags").selectedOptions) {
                    tags.push(tag.text)
                }


            if (docu.data.content) {
                docu.setSourceId(RenderCallcenter.instance.source_id)
                docu.setTags(tags)
                docu.setLinks(links)
                docu.setDataOwners(data_owners)
                docu.setObservers(observers)
                $('#save_icon').hide()
                $('#save_spinner').show()
                $("#save_document_button").off("click")
                DocumentsInstance.save(docu)
                RenderCallcenter.instance.documentDirty = false
                RenderCallcenter.instance.isQuestionaireDirty().then((dirtyFlag) => {
                    if (dirtyFlag === true) {
                        RenderCallcenter.instance.notifications.ask(
                            new pbI18n().translate("Es ist noch ein angefangener Fragebogen vorhanden. Soll dieser gespeichert oder verworfen werden?"),
                            new pbI18n().translate("Attention..."),
                            () => {
//                                document.addEventListener("command", function (event) {
//                                    if (event?.data?.data?.event == "questionaireAnswersAdded") {
//                                        RenderCallcenter.instance.handleNewTagCommand()
//                                    }
//                                }, {once : true})

                                $("#save_questionaire_button").click()
                            },
                            () => {
                                $("#new_questionaire_button").click()
                            },
                            "warning",
                            "Speichern",
                            "Verwerfen"
                        )
                    }
                })
            } else {
                Fnon.Alert.Danger({
                    title: new pbI18n().translate('Fehlender Dokumentationstext'),
                    message: new pbI18n().translate('Trage einen Wert in das Textfeld ein, um deine Dokumentation speichern zu können.'),
                    callback: () => {
                    }
                });
            }

        })

        $('#page_menu_button_2').unbind('click')
        $("#page_menu_button_2").on('click', event => {
            let deleteTitle = new pbI18n().translate("Löschen")
            let deleteQuestion = new pbI18n().translate("Möchten Sie das Dokument wirklich löschen?")

            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    DocumentsInstance.delete(docu).then(function () {
                        RenderCallcenter.instance.documentDirty = false
                        RenderCallcenter.instance.renderDetailView({}, DocumentsInstance)
                    })
                }
            })
        })

        $('#advancedOptions').on('mousedown', function (ev) {
            if (ev.target.nodeName == 'A') {
                for (let c of RenderCallcenter.instance.contacts_list) {
                    if (c.contact_id == ev.target.dataset.value) {
                        window.open(
                            '/contact-manager/?ci=' + ev.target.dataset.value,
                            '_blank'
                        );
                        break;
                    }
                }
            }
        })

        $(document).on("showHistoryData", (data) => {
            new RenderCallcenter().showEntryHistory(data)
        })

        $(document).on("updatePBHistory", args => {
            let payload = args.originalEvent.data
            $('#save_icon').show()
            $('#save_spinner').hide()
            if ($('#history').children().attr('data-selector') == 'on') {
                let doc = DocumentsInstance.getDocumentById(args.document_id)
                if (docu.document_id == payload.document_id) {
                    docu.fetchDocumentHistoryFromApi(CONFIG.API_BASE_URL, getCookie('apikey')).then((response) => {
                        fireEvent("showHistoryData", response)
                    })
                }
            }
        })

        $(document).on("newPBHistory", args => {
            $('#save_icon').show()
            $('#save_spinner').hide()
        })
    }

    handleNewTagCommand() {
        let tag = RenderCallcenter.instance.questionaireConfig?.title.replace(" ", "_").replace(/\W+/g, "_")
        var data = {
            id: tag,
            text: tag
        };

        var newOption = new Option(data.text, data.id, true, true);
        if ($('#tags').find("option[value='" + data.id + "']").length) {
            $('#tags').find("option[value='" + data.id + "']").prop("selected", true).trigger('change');
        } else {
            // Create a DOM Option and pre-select by default
            var newOption = new Option(data.text, data.id, true, true);
            // Append it to the select
            $('#tags').append(newOption).trigger('change');
        }

        RenderCallcenter.instance.notifications.ask(
            new pbI18n().translate(`Das Dokument wurde automatisch mit "${tag}" getaggt. Soll diese Änderung jetzt gespeichert werden?`),
            new pbI18n().translate("Dokument wurde getaggt..."),
            () => {
                $("#page_menu_button_3").click()
            })
    }

    renderDocumentDetailPills(document) {
        $("#history-list-container").html("")
        let params = {
            "headline": pbI18n.instance.translate("Activity"),
            "pills": [
                {
                    id: "history",
                    name: pbI18n.instance.translate("History"),
                    icon: "fa fa-history",
                    permission: "frontend.documentation",
                    toggle: "true",
                    onclick: function () {
                        if ($("#history_container").is(":visible")) {
                            $("#history_container").hide()
                        } else {
                            Fnon.Wait.Circle('', {
                                svgSize: {w: '50px', h: '50px'},
                                svgColor: '#3a22fa',
                            })
                            $(document).on("showHistoryData", (data) => {
                                new RenderCallcenter().showEntryHistory(data)
                            })
                            //TODO create new function to get data for docs
                            document.fetchDocumentHistoryFromApi(CONFIG.API_BASE_URL, getCookie('apikey')).then((response) => {
                                fireEvent("showHistoryData", response)
                            })
                            return false
                        }
                    }
                }
            ]
        }

        PbTpl.instance.renderInto('snippets/pills.tpl', params, "#interactions")

        for (let param of params.pills) {
            $(`#${param.id}`).click(param.onclick)
        }
    }

    showEntryHistory(data) {
        if (data.originalEvent.data.history && Array.isArray(data.originalEvent.data.history)) {
            $("#history_container").show()
            $("#history-list-container").html("")
            let previous_data = {}
            let newHistoryItem = $("#history-item").html()

            for (let {payload, type} of data.originalEvent.data.history) {
                RenderCallcenter.instance.currentlyShownHistoryId = payload.contact_id
                let {creator, create_time} = payload

                let localized_create_time = moment.utc(create_time).local().format();
                let local_create_time = new Date(localized_create_time).toLocaleString()

                let historyDetail = new pbI18n().translate("No details available")
                if (payload != null && previous_data != null) {
                    historyDetail = ""
                    let old_values = objectDiff(previous_data, payload, [previous_data.correlation_id, previous_data.create_time])
                    let new_values = objectDiff(payload, previous_data, [payload.correlation_id, payload.create_time])
                    if (old_values.data == undefined) {
                        old_values.data = {}
                    }
                    if (new_values.data == undefined) {
                        new_values.data = {}
                    }
                    let intersect_keys = array_merge_distinct(Object.keys(old_values.data), Object.keys(new_values.data))
                    for (let key of intersect_keys) {
                        if (key == "order") {
                            continue
                        }
                        let key_translated = new pbI18n().translate(key)
                        let old_value = old_values.data[key] ? old_values.data[key] : ""
                        let new_value = new_values.data[key] ? new_values.data[key] : false
                        if (new_value !== false && old_value != new_value) {
                            historyDetail += `<code><xmp> ${key_translated} : --> ${JSON.stringify(new_value)} </xmp></code>`
                        }
                    }
                }

                let details = {
                    'history-creator': creator,
                    'history-time': local_create_time,
                    'history-type': new pbI18n().translate(type),
                    'history-detail': historyDetail
                }

                previous_data = payload

                let historyItemContent = multiStringReplace(details, newHistoryItem)
                $("#history-list-container").prepend(historyItemContent)
            }
            Fnon.Wait.Remove(500)
        }
    }

    initViewListener() {
        let self = this

        document.addEventListener('froala.remoteFileDeleted', function (event) {
            console.log('Custom event remoteFileDeleted received:', event.detail.url);

            RenderCallcenter.instance.activeDocument.data.attachments = RenderCallcenter.instance.activeDocument.data.attachments.filter(item => item && item != event.detail.url)
            RenderCallcenter.instance.renderAttachments()
            RenderCallcenter.instance.notifications.unblockAll("body")
        });


        $(document).on('click', '#documentPropertiesToggle', function (e) {
            self.displayProperties = !$("#documentPropertiesToggle").hasClass("collapsed")
            setCookie("dcp", self.displayProperties)
        })

        $("#page_menu_button_1, [data-id='button-new-document']").click(function () {
            $('body').addClass('az-content-body-show')
            self.renderDetailView({}, RenderCallcenter.instance.documentationModuleInstance.docs)
            hideRightCol()
            if (window.matchMedia('(min-width: 992px)').matches) {
                showRightCol()
            }
            $("#page_menu_button_2").removeClass("hidden")
            $("#page_menu_button_3").removeClass("hidden")
        })

        $(document).on('keyup', '#header_search', function (e) {
            RenderCallcenter.instance.documentationModuleInstance.docs.filter($("#header_search").val())
            self.renderDocumentList(RenderCallcenter.instance.documentationModuleInstance.docs)
        })
    }

    onEditorUploadStart() {
        RenderCallcenter.instance.notifications.blockForLoading("#detail_view_tpl")
    }

    onEditorUploadFinished(link = undefined) {
        link = JSON.parse(link)
        if (!RenderCallcenter.instance.activeDocument.data.attachments) {
            RenderCallcenter.instance.activeDocument.data.attachments = []
        }
        if (link["link"]) {
            RenderCallcenter.instance.activeDocument.data.attachments.push(link["link"])
        }
        RenderCallcenter.instance.renderAttachments()
        RenderCallcenter.instance.notifications.unblock("#detail_view_tpl")
    }
}

//# sourceMappingURL=renderCallcenter.js.map
