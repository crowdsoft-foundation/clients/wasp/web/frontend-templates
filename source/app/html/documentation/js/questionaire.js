import {PbQuestionaire} from "../../csf-lib/pb/questionaire/classes.js";

export class CallcenterQuestionaire extends PbQuestionaire {
    PbTpl;

    constructor(PbTplInstance, questionaireConfig) {
        super(questionaireConfig);
        this.PbTpl = PbTplInstance;
    }

    renderInto(selector, postProcessingCallback, onSubQuestionDelete) {
        return this.PbTpl.renderIntoAsync('csf-lib/pb/questionaire/tpl/questionaire.html', {
            "title": this.title,
            "questions": this.questions
        }, selector).then(function () {
            for (let question of this.questions) {
                if (question.hidden === true) {
                    if (question.options.subquestion_to_id) {
                        $(`[data-answercontainerforid=${question.options.subquestion_to_id}] input`).click(function (event) {
                            if ($(event.target).is(":checked")) {
                                question.hidden = false
                                $(`#question_${question.id}`).remove()
                                this.PbTpl.renderIntoAsync('csf-lib/pb/questionaire/tpl/question.html', {
                                    "question": question,
                                    "class": "subquestion_depth_1"
                                }, `[data-answercontainerforid=${question.options.subquestion_to_id}]`, "append").then(function () {
                                    postProcessingCallback()
                                })
                            } else {
                                onSubQuestionDelete(question)
                                $(`#question_${question.id}`).remove()
                            }
                        }.bind(this))
                    }
                }
            }
        }.bind(this));
    }
}

//# sourceMappingURL=questionaire.js.map
