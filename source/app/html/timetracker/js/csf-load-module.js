import {PbAccount} from "/csf-lib/pb-account.js?v=[cs_version]"
import {socketio} from "/csf-lib/pb-socketio.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {Controller} from "./controller.js?v=[cs_version]"

export class TimetrackerModule {
    actions = {}
    config = []

    constructor() {
        if (!TimetrackerModule.instance) {
            TimetrackerModule.instance = this
            TimetrackerModule.instance.socket = new socketio()
            TimetrackerModule.instance.initListeners()
            TimetrackerModule.instance.initActions()
            TimetrackerModule.instance.acc = new PbAccount()
        }

        return TimetrackerModule.instance
    }

    initActions() {
        let self = this
        this.actions["timetrackerModuleInitialised"] = function (myevent, callback = () => "") {
            self.acc.init().then(() => {
                if (!self.acc.hasPermission("frontend.timetracker")) {
                    self.acc.logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new Controller(TimetrackerModule.instance.socket).init(TimetrackerModule.instance)
                    callback()
                }
            })
        }
    }

    initListeners() {
        $(document).on('click', '.az-contact-item, #button-new-document, [data-id="button-new-document"]', function () {
            $('body').addClass('az-content-body-show')
        })
    }
}
