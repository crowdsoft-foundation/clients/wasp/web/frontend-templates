import {PbTpl} from "/csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "/csf-lib/pb-notifications.js?v=[cs_version]"
import {makeId, getNextParentElement, getCookie} from "/csf-lib/pb-functions.js?v=[cs_version]"
import {PbTimer} from "./timer.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]"
import {pbI18n} from "/csf-lib/pb-i18n.js?v=[cs_version]"

export class Render {
    constructor() {
        if (!Render.instance) {
            Render.instance = this
            Render.instance.notifications = new PbNotifications()
            Render.instance.handledTaskIds = []
            Render.instance.i18n = new pbI18n()
            Render.instance.tpl = new PbTpl()
            Render.instance.timers = {}
            this.initListeners()
            this.timerFunctions()

            setInterval(() => {

            })
        }

        return Render.instance
    }

    init(ModuleInstance) {
        console.log("THIS", this)
        this.moduleInstance = ModuleInstance
        this.renderStart().then(() => {
            $("#loader-overlay").hide()
        })
    }

    addTimer(timer = undefined, state = "running") {
        if (state == "running") {
            timer.startTicking(false)
        } else if (state == "pause") {
            timer.pauseTicking(false)
        } else if (state == "stopped") {
            timer.finishTimer(false)
        }
        this.timers[timer.getData().id] = timer
        this.updateTimer(timer)
    }

    updateTimer(timer) {
        $(`*[data-timer="${timer.getData().id}"]`).html(timer.getDurationAsString())
    }

    deleteTimer(timer) {
        return timer
    }

    updateAllTimersFromApi() {
        this.timers = {}
        return new Promise((resolve, reject) => {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_my_trackers",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                },
                "success": function (response) {
                    for (let timer of response) {
                        console.log("RESPONSE", timer.state)
                        this.addTimer(new PbTimer(
                            getCookie("apikey"),
                            CONFIG,
                            this.moduleInstance.socket,
                            () => () => {
                            },
                            () => "",
                            () => "",
                            this.updateTimer.bind(this)
                        ).setData(timer), timer.state)
                    }
                    resolve()
                }.bind(this)
            };

            $.ajax(settings)
        })
    }

    renderAllTimerss() {
        $(".timercard").remove()
        this.timers = {}
        let promises = []
        this.updateAllTimersFromApi().then(function () {
            for (let timer of Object.values(this.timers)) {
                promises.push(this.addTimer(timer, timer.getData().state))
            }

            Promise.all(promises).then(function () {
                setTimeout(function () {
                    let data = []
                    for (let timer of Object.values(this.timers)) {
                        if (timer.getData().state !== "finished") {
                            continue
                        }
                        let tmp = {}
                        tmp["start"] = timer.getData().times[0].start
                        tmp["duration"] = timer.getDurationAsString()
                        tmp["title"] = timer.getData().title
                        data.push(tmp)
                    }
                    Render.instance.tpl.renderIntoAsync('timetracker/templates/timetable.html', {"data": data}, "#timetable").then(function () {
                        this.notifications.unblock("#main_body_content")
                    }.bind(this))
                }.bind(this), 100)
            }.bind(this))
        }.bind(this))
    }

    renderStart() {
        return new Promise((resolve, reject) => {
            this.updateAllTimersFromApi().then(function () {
                let open_timers = []
                let closed_timers = []
                for (let timer of Object.values(this.timers)) {

                    if (timer.getData().state === "finished") {
                        closed_timers.push(timer)
                    } else {
                        open_timers.push(timer)
                    }
                }

                Render.instance.tpl.renderIntoAsync('timetracker/templates/timerboard.html', {
                    "open_tasks": open_timers,
                    "closed_tasks": closed_timers
                }, "#main_body_content").then(() => {
                    this.timerFunctions();

                    for (let timer of Object.values(this.timers)) {
                        if (timer.getData().state == "running") {
                            timer.startTicking(false)
                        } else if (timer.getData() == "pause") {
                            timer.pauseTicking(false)
                        } else if (timer.getData() == "stopped") {
                            timer.finishTimer(false)
                        }
                        this.timers[timer.getData().id] = timer
                        this.updateTimer(timer)
                    }

                    resolve()
                })
            }.bind(this))
        })
    }

    initListeners() {
        document.addEventListener("event", function _(e) {
            if (e.data && e.data.data && e.data.data.event == "timetrackerUpdated") {
                this.renderStart()
            }
        }.bind(this))
    }

    attachFormFunctions() {
        $("#saveTaskButton").click(function (event) {
            let task_id = $('[data-timer-id]').data('timer-id');
            let task = this.timers[task_id]
            var times = []

            $("[data-startdate]").each(function (index, element) {
                let counter = $(element).data("startdate")

                let startData = $(`[data-startdate=${counter}]`)[0].value
                let endData = $(`[data-enddate=${counter}]`)[0].value

                let parsedStartDate = moment(startData, CONFIG.DATETIMEFORMAT);
                let parsedEndDate = moment(endData, CONFIG.DATETIMEFORMAT);

                times.push({"start": parsedStartDate, "end": parsedEndDate})
            })

            let data = {
                "id": "tmp_" + makeId(30),
                "title": $("#editTaskTitle").val(),
                "notice": $("#editTaskNote").val(),
                "state": task ? task.getData().state : "running",
                "times": times
            }

            if (task && task.getData().uuid) {
                data["id"] = task.getData().id
                data["uuid"] = task.getData().uuid
            } else {
                task = new PbTimer()
                data["state"] = "finished"
            }

            task.setData(data)
            task.store()

            $('#editTaskModal').modal('hide');
            $('.modal-backdrop').hide();
        }.bind(this))
        $('#editTaskModal').modal('show');


        // Functionality to add new time entries.
        $(document).ready(function () {
            var taskTimerGroups = $('#taskTimerGroups');

            // Get the count of .task-timer-group elements.
            var currentIndex = $('.task-timer-group').length;

            $("#addTimerButton").click(function (event) {
                var firstGroup = $('.task-timer-group').first();

                if (firstGroup.length) {
                    let newGroup = firstGroup.clone();

                    // Now we add the data-attr with the current index and add 1 for a new entry.
                    newGroup.find('input[data-startdate]').attr('data-startdate', currentIndex + 1);
                    newGroup.find('input[data-enddate]').attr('data-enddate', currentIndex + 1);

                    taskTimerGroups.append(newGroup);

                    currentIndex++;
                } else {
                    console.error("No elements with class 'task-timer-group' found.");
                }
            });
        });
    }

    timerFunctions() {
        // START
        $('[data-play-task]').click(function (event) {
            let task_id = $(event.currentTarget).data('play-task');
            this.timers[task_id].startTicking(true, true);
        }.bind(this));

        // PAUSE
        $('[data-paused-task]').click(function (event) {
            let task_id = $(event.currentTarget).data('paused-task');
            this.timers[task_id].pauseTicking();

        }.bind(this));

        // STOP
        $('[data-stop-task]').click(function (event) {
            let task_id = $(event.currentTarget).data('stop-task');

            new PbNotifications().blockForLoading('#timetracker')
            new PbNotifications().ask(new pbI18n().translate("Möchtest du die Aufgabe beenden?"),
                new pbI18n().translate("Achtung..."),
                () => {
                    this.timers[task_id].finishTimer();
                },
                () => {
                    new PbNotifications().unblock('#timetracker')
                })
        }.bind(this));

        // EDIT
        $('[data-edit-task]').click(function (event) {
            let task_id = $(event.currentTarget).data('edit-task');
            this.tpl.renderIntoAsync('timetracker/templates/edit_modal.html', {"task": this.timers[task_id]}, "#editTaskModalContainer").then(function () {
                this.attachFormFunctions()


            }.bind(this))
        }.bind(this));

        // CREATE
        $('#createTimer').click(function (event) {
            let timer = new PbTimer(
                getCookie("apikey"),
                CONFIG,
                this.moduleInstance.socket,
                () => () => {
                },
                () => "",
                () => "",
                this.updateTimer.bind(this)
            ).setData({
                "id": "tmp_" + makeId(30),
                "title": $("#taskTitle").val(),
                "notice": $("#taskNotice").val(),
                "state": "running",
                "time": 0,
                "times": [{"start": moment().utc(), "end": moment().utc()}]
            })

            timer.startTicking(true)

            $('#createTimerModal').modal('hide');
        }.bind(this));

        // ADD TIMER
        $("#addTimeEntry").click(function (event) {
            let task = new PbTimer()
            task.setData({
                "id": "tmp_" + makeId(30),
                "title": "Neue Aufgabe",
                "notice": "",
                "state": "finished",
                "time": 0,
                "times": [{"start": moment().utc(), "end": moment().utc()}]
            })

            this.tpl.renderIntoAsync('timetracker/templates/edit_modal.html', {"task": task}, "#editTaskModalContainer").then(function () {
                this.attachFormFunctions()
            }.bind(this))
        }.bind(this))
    }
}
