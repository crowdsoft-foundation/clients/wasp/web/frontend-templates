import {BASECONFIG} from "./csf-lib/pb-config.js?v=[cs_version]"
import {mobileCheck} from "./csf-lib/pb-functions.js?v=[cs_version]"

export const CONFIG = BASECONFIG

CONFIG.NEEDED_JS_FILES = [
    {name: "jquery.cookie", path: "/csf-lib/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
    {name: "jquery.blockUI", path: "/csf-lib/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
    {name: "socket.io", path: "/csf-lib/vendor/socket.io.js?v=[cs_version]", loaded: null},
    {name: "aes_js", path: "/csf-lib/vendor/aes.js?v=[cs_version]", loaded: null},
]

if (document.location.href.indexOf("http://localhost") == 0) {
    console.log("Using DEV-Config")
    CONFIG.ISMOBILE = mobileCheck() //mobileORTabletCheck() || true || false
    CONFIG.JS_CACHE_ACTIVE = false
    CONFIG.CIRCUIT_BASE_URL = "http://localhost:7070/circuits/"
    CONFIG.APP_BASE_URL = "http://localhost:7070"
    CONFIG.REGISTER_URL = "http://localhost:7070/registration/"
    CONFIG.API_BASE_URL = "http://localhost:8000"
    CONFIG.SOCKET_SERVER = "http://localhost:8000"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
  //  CONFIG.LANGUAGEFILE_PREFIX = "familyplaner_"
  //  CONFIG.HELPFILE_PREFIX = "familyplaner_"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.ALWAYS_REDIRECT_TO_WWW = false
    CONFIG.NEWS = [
        // {
        //     news_id: "service_interruption_221124",
        //     content_uri: "../news/index.html?221124",
        //     content_id: "service_interruption_221124",
        //     show_in_apps: ["easy2schedule", "dashboard"],
        //     force_display: true
        // }
    ]
    CONFIG.EDITOR = "froala"
    CONFIG.DOCUMENTATION.EDITOR = "froala"
    CONFIG.FROALA_KEY = "YNB3fH3C8A6C5E6B3G3C-8TMIBDIa1NTMNZFFPFZc1d1Ib2a1E1fA4A3G3E3F2C6D5C4C3E2=="
    CONFIG.LOGIN_URL = "http://localhost:7070/login/"
    if(CONFIG.ISMOBILE) {
        CONFIG.LOGIN_URL = "http://localhost:7070/login/persistent_login.html"
    }

} else if (document.location.href.indexOf("testblick.de") != -1) {
    console.log("Using STAGING-Config")
    CONFIG.CIRCUIT_BASE_URL = "https://testblick.de/circuits/"
    CONFIG.APP_BASE_URL = "https://www.testblick.de"
    CONFIG.REGISTER_URL = "https://www.planblick.com/cloud-anwendungen-preisliste.html" //"https://testblick.de/registration/"
    CONFIG.API_BASE_URL = "https://odin.planblick.com"
    CONFIG.LOGIN_URL = "https://testblick.de/login/"
    CONFIG.SOCKET_SERVER = "https://odin.planblick.com"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
    CONFIG.LANGUAGEFILE_PREFIX = ""
    CONFIG.HELPFILE_PREFIX = ""
    CONFIG.NEWS = [
        // {
        //     news_id: "service_interruption_221124",
        //     content_uri: "../news/index.html?221124",
        //     content_id: "service_interruption_221124",
        //     show_in_apps: ["easy2schedule", "dashboard"],
        //     force_display: true
        // }
    ]
    CONFIG.EDITOR = "froala"
    CONFIG.DOCUMENTATION.EDITOR = "froala"
    CONFIG.FROALA_KEY = "YNB3fH3C8A6C5E6B3G3C-8TMIBDIa1NTMNZFFPFZc1d1Ib2a1E1fA4A3G3E3F2C6D5C4C3E2=="
    if(CONFIG.ISMOBILE) {
        CONFIG.LOGIN_URL = "https://testblick.de/login/persistent_login.html"
    }

} else if (document.location.href.indexOf("stagingapp.familienplaner-online") != -1) {
    console.log("Using STAGING-FAMILY-Config")
    CONFIG.ALWAYS_REDIRECT_TO_WWW = false
    CONFIG.CIRCUIT_BASE_URL = "https://stagingapp.familienplaner-online.de/circuits/"
    CONFIG.APP_BASE_URL = "https://stagingapp.familienplaner-online.de"
    CONFIG.REGISTER_URL = "https://stagingapp.familienplaner-online.de/pricing-and-more.html#ActionRegistration"
    CONFIG.API_BASE_URL = "https://odin.planblick.com"
    CONFIG.LOGIN_URL = "https://stagingapp.familienplaner-online.de/login/"
    CONFIG.SOCKET_SERVER = "https://odin.planblick.com"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
    CONFIG.LANGUAGEFILE_PREFIX = "familyplaner_"
    CONFIG.HELPFILE_PREFIX = "familyplaner_"
    CONFIG.EDITOR = "froala"
    CONFIG.DOCUMENTATION.EDITOR = "froala"
    CONFIG.FROALA_KEY = "YNB3fH3C8A6C5E6B3G3C-8TMIBDIa1NTMNZFFPFZc1d1Ib2a1E1fA4A3G3E3F2C6D5C4C3E2=="
    CONFIG.NEWS = [
    //     {
    //         news_id: "service_interruption_221122",
    //         content_uri: "../news/index.html?221123",
    //         content_id: "service_interruption_221122",
    //         show_in_apps: ["easy2schedule", "dashboard"],
    //         force_display: true
    //     }
    ]
    if(CONFIG.ISMOBILE) {
        CONFIG.LOGIN_URL = "https://stagingapp.familienplaner-online.de/persistent_login.html"
    }
} else if (document.location.href.indexOf("app.familienplaner-online") != -1) {
    console.log("Using PRODUCTION-FAMILY-Config")
    CONFIG.ALWAYS_REDIRECT_TO_WWW = false
    CONFIG.CIRCUIT_BASE_URL = "https://app.familienplaner-online.de/circuits/"
    CONFIG.APP_BASE_URL = "https://app.familienplaner-online.de"
    CONFIG.REGISTER_URL = "https://familienplaner-online.de/pricing-and-more.html#ActionRegistration"
    CONFIG.API_BASE_URL = "https://thor.planblick.com"
    CONFIG.LOGIN_URL = "https://app.familienplaner-online.de/login/"
    CONFIG.SOCKET_SERVER = "https://thor.planblick.com"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
    CONFIG.LANGUAGEFILE_PREFIX = "familyplaner_"
    CONFIG.HELPFILE_PREFIX = "familyplaner_"
    CONFIG.EDITOR = "froala"
    CONFIG.DOCUMENTATION.EDITOR = "froala"
    CONFIG.FROALA_KEY = "YNB3fH3C8A6C5E6B3G3C-8TMIBDIa1NTMNZFFPFZc1d1Ib2a1E1fA4A3G3E3F2C6D5C4C3E2=="
    CONFIG.NEWS = [
    //     {
    //         news_id: "service_interruption_221122",
    //         content_uri: "../news/index.html?221123",
    //         content_id: "service_interruption_221122",
    //         show_in_apps: ["easy2schedule", "dashboard"],
    //         force_display: true
    //     }
    ]
    if(CONFIG.ISMOBILE) {
        CONFIG.LOGIN_URL = "https://app.familienplaner-online.de/login/persistent_login.html"
    }
} else {
    console.log("Using LIVE-Config")
    CONFIG.CIRCUIT_BASE_URL = "https://www.serviceblick.com/circuits/"
    CONFIG.APP_BASE_URL = "https://www.serviceblick.com"
    CONFIG.REGISTER_URL = "https://www.planblick.com/cloud-anwendungen-preisliste.html"
    CONFIG.API_BASE_URL = "https://thor.planblick.com"
    CONFIG.LOGIN_URL = "https://www.serviceblick.com/login/"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
    CONFIG.EDITOR = "froala"
    CONFIG.DOCUMENTATION.EDITOR = "froala"
    CONFIG.FROALA_KEY = "YNB3fH3C8A6C5E6B3G3C-8TMIBDIa1NTMNZFFPFZc1d1Ib2a1E1fA4A3G3E3F2C6D5C4C3E2=="
    CONFIG.NEWS = [
        // {
        //     news_id: "service_interruption_221124",
        //     content_uri: "../news/index.html?221124",
        //     content_id: "service_interruption_221124",
        //     show_in_apps: ["easy2schedule", "dashboard"],
        //     force_display: true
        // }
    ]
    if(CONFIG.ISMOBILE) {
        CONFIG.LOGIN_URL = "https://www.serviceblick.com/login/persistent_login.html"
    }
}



