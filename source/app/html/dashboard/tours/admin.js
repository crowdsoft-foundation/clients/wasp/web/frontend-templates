window.tour_config = {
    header: "Lerne unser System kennen",
    intro: "Überblick aller Module im Dashboard",
    tours: {
        firstStepsAdmin:
        {
        id: "firstStepsAdmin",
        title: "Erweiterte Funktionen",
        desc: "Lerne erweiterte Funktionen für die Benutzerverwaltung kennen",
        startPage: "/dashboard/account.html",
        startStep: 11,
        prevTour: "firstStepsUser",
        nextTour: "firstStepsAdminRoles",
        permission: "dashboard.management.usermanagement"
        },
        firstStepsAdminRoles: {
            id: "firstStepsAdminRoles",
            title: "Rollenverwaltung",
            desc: "Lerne erweiterte Funktionen für die Rollenverwaltung kennen",
            startPage: "/dashboard/roles.html",
            startStep: 15,
            prevTour: "firstStepsAdmin",
            nextTour: "firstStepsAdminGroups",
            permission: "dashboard.management"
        },
        firstStepsAdminGroups: {
            id: "firstStepsAdminGroups",
            title: "Gruppenverwaltung",
            desc: "Lerne erweiterte Funktionen für die Gruppenverwaltung kennen",
            startPage: "/dashboard/groups.html",
            startStep: 18,
            prevTour: "firstStepsAdminRoles",
            nextTour: null,
            permission: "dashboard.management"
        }
    },
    tourSteps: [
        {//1
            title: "Dashboard",
            intro: "Dein Dashboard ermöglicht den Zugriff auf deine Apps und kontorelevante Informationen. Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen."
        },
        {//2
            element: "#board-portlet-container",
            intro: "Hier sind alle f\u00fcr dich verf\u00fcgbaren Anwendungen aufgelistet."
        },
        {//3
            element: "#header_apps_menu",
            intro: "Auch hier findest du nochmal Links zu allen Anwendungen. Dieser Men\u00fcpunkt steht in allen Anwendungen zur Verf\u00fcgung. So kannst du schnell zwischen Anwendungen hin und her wechseln."
        },
        {//4
            element: "#easy2schedule-card",
            intro: "Easy2Schedule ist unsere einfache Kalender-Verwaltung."
        },
        {//5
            element: "#easy2track-card",
            intro: "Easy2Track ist unsere einfache Kontakt-Nachverfolgung."
        },
        {//6
            element: "#simplycollect-card",
            intro: "Simplycollect ist dein einfacher Helfer f\u00fcr Click&Collect Gesch\u00e4fte ohne Webshop."
        },
        {//7
            element: "#easy2order-card",
            intro: "Easy2Order ist unser einfacher Helfer um Gast-Bestellungen schnell und einfach ausf\u00fchren zu k\u00f6nnen."
        },
        {//8
            element: "#contactmanager-card",
            intro: "Kontaktmanager ist unser einfacher Helfer um Kontakte zu verwalten."
        },
        {//9
            element: "#taskmanager-card",
            intro: "Der Aufgabenmanager ermöglicht die digitale Erstellung, Nachverfolgung und Abarbeitung von Aufgaben."
        },
        {//10   
            title: 'Einführung abgeschlossen',
            disableInteraction: true,
            intro: `Prima, damit hast du die Einführung abgeschlossen.<br/>Wenn Du möchtest, machen wir gleich weiter und geben dir eine Einführung in die Kontoverwaltung. Du kannst diese aber auch später starten indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong> `,
            'myBeforeChangeFunction': function (PbGuide) {
            PbGuide.setPrevButton("Beenden", function () {
                PbGuide.introguide.exit()
            })

            PbGuide.setNextButton("Weiter", function () {
                let nextTour = PbGuide.tourConfig.tours.firstStepsUser.nextTour
                let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                PbGuide.introguide.goToStepNumber(nextStartStep)
            })
            }
        },
        {//11
            title: 'Kontoverwaltung Übersicht',
            disableInteraction: true,
            element: '#create-login-wrapper',
            intro: 'Eine Einführung in die Konto- und Benutzerverwaltung',
            'myBeforeChangeFunction': function () {

            }
        },
        {//12
            title: 'Kontoverwaltung',
            element: '[data-tour-id="admin-menu"]',
            intro: 'Hier findest du die verschiedenen Kategorien der Konto- und Benutzerverwaltung.',
            'myBeforeChangeFunction': function () {

            }
        },
        {//13
            title: 'Kontoverwaltung',
            element: '#create-login-wrapper',
            intro: 'Hier kannst du zusätzliche Logins anlegen.',
            'myBeforeChangeFunction': function () {

            }
            
        },
        {//14
            title: 'Kontoverwaltung',
            element: '#manage-login-wrapper',
            intro: 'Hier kannst du die zusätzlich angelegten Logins/Nutzer zu deinem Konto verwalten',
            'myBeforeChangeFunction': function () {

            }
            
        },
        {//15
            title: 'Rollenverwaltung Übersicht',
            disableInteraction: true,
            element: '#use-activate-menu',
            intro: 'Eine Einführung in die Rollenverwaltung',
            'myBeforeChangeFunction': function () {
                if(!PbGuide.ensureUrl(PbGuide.tourConfig.tours.firstStepsAdminRoles.startPage, "firstStepsAdminRoles")) {
                    PbGuide.introguide.exit()
                }
            }
        },
        {//16
            title: 'Rollenverwaltung',
            element: '#create-role-wrapper',
            intro: 'Hier kannst du zusätzliche Rollen definieren.',
            'myBeforeChangeFunction': function () {
            }   
        },
        {//17
            title: 'Rollenverwaltung',
            element: '#manage-role-wrapper',
            intro: 'Hier kannst du die zusätzlich angelegten Rollen zu deinem Konto verwalten',
            'myBeforeChangeFunction': function () {
            }            
        },
        {//18
            title: 'Gruppenverwaltung Übersicht',
            disableInteraction: true,
            element: '#use-activate-menu',
            intro: 'Eine Einführung in die Gruppenverwaltung',
            'myBeforeChangeFunction': function (PbGuide) {
                if(!PbGuide.ensureUrl(PbGuide.tourConfig.tours.firstStepsAdminGroups.startPage, "firstStepsAdminGroups")) {
                    PbGuide.introguide.exit()
                }
            }
        },
        {//19
            title: 'Gruppenverwaltung',
            element: '#create-group-wrapper',
            intro: 'Hier kannst du zusätzliche Gruppen definieren.',
            'myBeforeChangeFunction': function () {
            }   
        },
        {//20
            title: 'Gruppenverwaltung',
            element: '#manage-group-wrapper',
            intro: 'Hier kannst du die zusätzlich angelegten Gruppen zu deinem Konto verwalten',
            'myBeforeChangeFunction': function () {
            }            
        },
        {//21
            title: 'Tour abgeschlossen',
            intro: 'Sehr gut! Damit hast du die Tour zur Konto- und Benutzerverwaltung bgeschlossen.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.markTourSeen("contactmanager.editContact")
                PbGuide.hidePreviousButton()

                // PbGuide.setNextButton("Weiter", function () {
                //     let nextTour = PbGuide.tourConfig.tours.firstSteps.nextTour
                //     let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep
                //     PbGuide.introguide.goToStepNumber(nextStartStep)
                // })
            },
        }
        
    ]
}
