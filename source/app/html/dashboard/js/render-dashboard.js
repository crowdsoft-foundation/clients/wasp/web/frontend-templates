import {getCookie, setCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import { PbAccount } from "../../csf-lib/pb-account.js?v=[cs_version]"
import { PbLogin } from "../../csf-lib/pb-login.js?v=[cs_version]"
import {PbGuide} from "../../csf-lib/pb-guide.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"

export class RenderDashboard {
    account_id = undefined
    userDashboardSetting = `[{"id":"taskmanager-card"},{"id":"easy2schedule-card"},{"id":"contactmanager-card"},{"id":"simplycollect-card"},{"id":"easy2order-card"},{"id":"easy2track-card"}]`
    dashboardCookieName = getCookie("username") + "_dashboard"
    dismissed = sessionStorage.getItem("dismissed");

    constructor() {
        if (!RenderDashboard.instance) {
            RenderDashboard.instance = this
            RenderDashboard.instance.account_id= getCookie("consumer_id")
            RenderDashboard.instance.acc = new PbAccount()
            RenderDashboard.instance.login = new PbLogin()
            RenderDashboard.instance.renderCardLayout()
            this.initListeners()
        }
        return RenderDashboard.instance     
    }

    
    renderCardLayout(){
        RenderDashboard.instance.buildCardOrder()
        if (RenderDashboard.instance.account_id) {
            RenderDashboard.instance.acc.fetchAccountProfileData(RenderDashboard.instance.account_id).then((response)=> {
                    RenderDashboard.instance.setAccountDashboardSettings(response["dashboard_settings"])
                    if(RenderDashboard.instance.userDashboardSetting != undefined) {
                        RenderDashboard.instance.buildCardOrder()
                    }
                })
        }
        else if (getCookie(RenderDashboard.instance.dashboardCookieName)) { //if cookie exists 
            RenderDashboard.instance.userDashboardSetting = RenderDashboard.instance.getCookieDecodedValueFor(RenderDashboard.instance.dashboardCookieName)
            RenderDashboard.instance.buildCardOrder()
        }   

    }

    buildCardOrder() {
        let defaultItemCount = $('#portlet-card-list').find('li').length
        const obj = JSON.parse(RenderDashboard.instance.userDashboardSetting);  
        let cookieItemCount = obj.length
                //make sure dashboard does not have more app cards that are in the cookie
                if (cookieItemCount >= defaultItemCount) {
                    //create temp dom node to build layout
                    const dFrag = $(document.createDocumentFragment());
                    for (let idx = 0; idx < obj.length; idx++) {
                        let portletCard = obj[idx].id
                        //clone html for cards from default layout
                        var card = RenderDashboard.instance.getPortletCardHtml(portletCard)
                        dFrag.append(card);
                    }

                //clear default layout after snippets are cloned
                $("#portlet-card-list").html("")
                //append new node objects
                $("#portlet-card-list").append(dFrag)
            }
    }

    getCookieDecodedValueFor(cookieName) {
        var cookie = getCookie(cookieName)
        return decodeURIComponent(cookie);
    }

    getPortletCardHtml (id) {
        let cardId = "#" + id
        let card = $(cardId).clone()
        return card
    }

    setAccountDashboardSettings(settings) {
        // Make sure we have every id in the dictionary only once
        RenderDashboard.instance.userDashboardSetting = this.makeCardsUnqiue(settings)
    }

    makeCardsUnqiue(settings) {
        let in_type = typeof settings
        if(in_type === "string") {
            settings = JSON.parse(settings)
        }
        let ids = []
        for(let entry of settings) {
            if(ids.includes(entry.id)) {
                settings.splice(settings.indexOf(entry), 1)
            } else {
                ids.push(entry.id)
            }
        }
        if (in_type === "string") {
            settings = JSON.stringify(settings)
        }
        return settings
    }
    getAccountDashboardSettings() {
        return this.makeCardsUnqiue(RenderDashboard.instance.userDashboardSetting)
    }

    updateAccountDashboardSettings() {
        RenderDashboard.instance.acc.accountProfileData["dashboard_settings"] = RenderDashboard.instance.getAccountDashboardSettings()
        RenderDashboard.instance.acc.saveAccountProfileData(getCookie("consumer_id")).then(function () {
            Fnon.Hint.Success('Neue Einstellungen wird gespeichert.')
        return false
        })
    }
    renderNews(event) {
        event = event.data
        $("#news_bell").addClass("active")
        let doc = new DOMParser().parseFromString(event.data, "text/xml");
        let news_content = doc.getElementById(event.config.content_id)
        $('#news_read_button').prop('hidden', false)
        $("#news_container").append(news_content.innerHTML)
        $("#news_container").append("<br/><br/><br/>")
        $('#news_read_button').data("contentId", event.config.content_id)
        if (event.config.force_display) {
            $("#news_modal").modal('show')
        }
    }

    setNewsCookieRead() {
        let news_content_id= $('#news_read_button').data("contentId")
        $("#news_bell").removeClass("active")
        RenderDashboard.instance.setNewsAccountRead(news_content_id)
    }

    setNewsAccountRead(news_content_id) {
        let data = {}
        data[news_content_id] = true
        PbLogin.instance.profileData["read_news"] = data
        PbLogin.instance.saveProfileData()
    }

    checkAccountForNews (event) {
        let temp_event = event.data
        if (RenderDashboard.instance.login?.profileData?.["read_news"] && RenderDashboard.instance.login?.profileData?.read_news?.[temp_event.config.content_id]) {
            return
        }

        RenderDashboard.instance.renderNews(event)
       
    }

    checkCountdownAlert () {
        //still needs function for is demo consumer / & has entered billing info / or has clicked the dismiss x
        // currently if has clicked the dismiss x than don't show for a day
        let show_billing_banner = false
        for(let subscription of RenderDashboard.instance.acc.subscriptions.getActive()) {
            if(subscription.package_name.includes("test")) {
                show_billing_banner = true
            }
        }

        if  (show_billing_banner && !RenderDashboard.instance.acc?.accountProfileData?.addresses?.billing) {
            $('#counter_demo').show()
          } else {
            $('#counter_demo').hide()
        }
    }

    initListeners() {

        //on switch
        $('#edit_dashboard_button').on('click', function () {
            $("#portlet-card-list").sortable(
                {
                    items: ".portlet-card",
                    update: function () {

                    }
                });
            $('[data-attr="movable"]').removeClass('hidden')
            $('.portlet-card').addClass('cursor-grab')
            $('#save_dashboard_button').removeClass('hidden')
            $('#edit_dashboard_button').addClass('hidden')
        });


        //off switch
        $('#save_dashboard_button').on('click', function () {
            $('.sortable').each(function () {
                $(this).sortable('destroy');
            });
            $('[data-attr="movable"]').addClass('hidden')
            $('.portlet-card').removeClass('cursor-grab')
            $('#save_dashboard_button').addClass('hidden')
            $('#edit_dashboard_button').removeClass('hidden')

            //set users order to a cookie
            let settings = JSON.stringify($("#portlet-card-list").sortable('serialize'))
            const newDashBoardOrder = new CustomEvent('newDashBoardOrder', {
                detail: {
                  settings: settings
                }
              })
            document.dispatchEvent(newDashBoardOrder)
            console.log("DAshboard settings", settings)
            RenderDashboard.instance.setAccountDashboardSettings(settings) 
            /* RenderDashboard.instance.account_id.accountProfileData["dashboard_settings"] = settings */
            setCookie(RenderDashboard.instance.dashboardCookieName, RenderDashboard.instance.userDashboardSetting,  {expires: 3650, path: '/'}); //set "user_dashboard" cookie, expires in 10 years
            RenderDashboard.instance.updateAccountDashboardSettings() 
            //T
        });

        $('#news_read_button').on('click', ()=> {
            RenderDashboard.instance.setNewsCookieRead()
            RenderDashboard.instance.acc.saveAccountProfileData(getCookie("consumer_id")).then(() => {
                Fnon.Hint.Success('Als gelesen gespeichert!')
                $('#news_read_button').prop('hidden', true)
                $("#news_container").html("")
                $("#news_modal").modal('hide')
            })
            
        })

        $('#firstStepsUser').on('click', (e) => {
            e.preventDefault()
            new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}getting_started.js`).init().then(instance => instance.startTourFromQueryParam("firstStepsUser"))

        })

        $('#dismiss_countdown').on("click", function(){
            let cookie_content = $.cookie("alert_countdown")
            if (!cookie_content){
                cookie_content = ""
                cookie_content += "/" + "alert_dismiss"
                $.cookie("alert_countdown", cookie_content, {expires: 1})
            }
            RenderDashboard.instance.checkCountdownAlert()
            
        })
        
    }
    


}