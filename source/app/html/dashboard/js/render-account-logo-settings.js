import {getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"

export class RenderAccountLogoSettings {
    account_id =  undefined
    edit_mode = false

    constructor(account_id) {
        if (!RenderAccountLogoSettings.instance) {
            RenderAccountLogoSettings.instance = this
            RenderAccountLogoSettings.instance.account_id = account_id
        }

        return RenderAccountLogoSettings.instance
    }

    setLogoData() {
        console.log("accountProfileData from RenderAccountLogoSettings", RenderAccountLogoSettings.instance.account_id.accountProfileData)
        if(RenderAccountLogoSettings.instance.account_id.accountProfileData["logo_data"]){
            const {url, description}= RenderAccountLogoSettings.instance.account_id.accountProfileData["logo_data"]
            $('#logo-url').val(url)
            $('#logo-description').val(description) 
            $('#logo-display').attr('src', url)
            $('#logo-display').attr('alt', description)
        }
    }

    getLogoData() {
        if($('#logo-url').val() != "") {
            let logo_data = {
                "url": $('#logo-url').val(),
                "description": $('#logo-description').val()  
            }
            RenderAccountLogoSettings.instance.account_id.accountProfileData["logo_data"] = logo_data
        }
        $('#logo-display').attr('src', $('#logo-url').val())
        $('#logo-display').attr('alt', $('#logo-description').val())
    }

    updateAccountLogoData() {
        RenderAccountLogoSettings.instance.getLogoData()
        RenderAccountLogoSettings.instance.account_id.saveAccountProfileData(getCookie("consumer_id")).then(function () {
            Fnon.Hint.Success('Neue Einstellungen wird gespeichert.')
        return false
        })
    }
    
    toggleEditMode () {
        RenderAccountLogoSettings.instance.edit_mode = !RenderAccountLogoSettings.instance.edit_mode
    }

    renderLogoForm() {
        if (RenderAccountLogoSettings.instance.edit_mode) {
            $('#logo-inputs').prop('hidden', false)
            $('#save_logo_button').prop('hidden', false)
            $('#cancel_logo_button').prop('hidden', false)
            $('#edit_logo_button').prop('hidden', true)
        }
        else {
            $('#logo-inputs').prop('hidden', true)
            $('#save_logo_button').prop('hidden', true)
            $('#cancel_logo_button').prop('hidden', true)
            $('#edit_logo_button').prop('hidden', false)
        }
    }

    updateFormState() {
        RenderAccountLogoSettings.instance.toggleEditMode()
        RenderAccountLogoSettings.instance.renderLogoForm()
    }
}