import {validateForm} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"

export class RenderProfileSettings {
    login = undefined

    constructor(login) {
        if (!RenderProfileSettings.instance) {
            RenderProfileSettings.instance = this
            RenderProfileSettings.instance.login = login
        }

        return RenderProfileSettings.instance
    }

    setInputFields() {
        $("#firstname").val(RenderProfileSettings.instance.login.profileData.firstname)
        $("#lastname").val(RenderProfileSettings.instance.login.profileData.lastname)
        $("#email").val(RenderProfileSettings.instance.login.profileData.email)
        if (RenderProfileSettings.instance.login.profileData.firstname && RenderProfileSettings.instance.login.profileData.lastname != undefined) {
            $("#avatar2").html(RenderProfileSettings.instance.login.profileData.firstname.charAt(0) + RenderProfileSettings.instance.login.profileData.lastname.charAt(0))
        }

    }

    updateProfileSettings() {

        if ($('[data-group="info"]').prop('disabled') == true) {
            $('#update_profile_button').text("speichern")
            $('[data-group="info"]').prop('disabled', function (i, v) {
                return !v;
            })
        } else {

            if (validateForm({
                'firstname': 'input',
                'lastname': 'input',
                'email': 'email'
            }) == true) {

                RenderProfileSettings.instance.login.profileData.firstname = ($("#firstname").val())
                RenderProfileSettings.instance.login.profileData.lastname = ($("#lastname").val())
                RenderProfileSettings.instance.login.profileData.email = ($("#email").val())
                RenderProfileSettings.instance.login.saveProfileData().then(function () {
                    console.log("profile-data saved")

                    $('#update_profile_button').text("bearbeiten")
                    $('[data-group="info"]').prop('disabled', function (i, v) {
                        return !v;
                    })

                    $.blockUI({
                        blockMsgClass: 'alertBox',
                        message: 'Neue Einstellungen wird gespeichert.',
                        timeout: 1000,
                        onUnblock: function() {location.reload()}
                    })


                }, function() { console.log("Cannot save profile-data")})


            }
        }

    }

}