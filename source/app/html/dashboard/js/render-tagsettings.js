import {PbTagsList} from "/csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"
import {PbTag} from "/csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"


export class RenderTagSettings {
    constructor(DashboardModule) {
        this.dashboardModule = DashboardModule
        this.taglist = new PbTagsList()
        this.initListeners()
    }

    init() {
        let promises = []

        return new Promise(function (resolve, reject) {
            promises.push(this.dashboardModule.groupsModel.loadFromApi(true))
            promises.push(this.taglist.load())
            Promise.all(promises).then(function () {
                resolve()
            }.bind(this))
        }.bind(this))
    }

    render() {
        let tagList = this.taglist.getList()
        let search = $("#search_input").val().toUpperCase()
        let renderData = []

        if (search.length > 0) {
            tagList.map(function (row) {
                let row_data = row.getData()
                if (row_data.name.toUpperCase().includes(search)
                    || row_data.value.toUpperCase().includes(search)
                    || row.getUsableFor().join("|").toUpperCase().includes(search)) {
                    renderData.push(row)
                }
            })
        } else {
            renderData = tagList
        }

        return this.dashboardModule.tpl.renderIntoAsync("dashboard/templates/tags/list-view.html", {"tags": renderData}, "#tag_element_list").then(function () {
            $('*[data-selector="btn-tag-delete"]').click(function (e) {
                $("#loader-overlay").show()
                let short = $(e.currentTarget).data("tag")
                let toBeDeletedTag = new PbTag().fromShort(short)
                this.taglist.apiDeleteTags([toBeDeletedTag]).then(function () {
                }.bind(this))
            }.bind(this))

            $('*[data-selector="btn-tag-edit"]').click(function (e) {
                let short = $(e.currentTarget).data("tag")
                let selectedTag = this.taglist.getList().filter(function (tag) {
                    if (tag.short() == short) {
                        return true
                    }
                    return false
                }).pop()

                this.dashboardModule.tpl.renderIntoAsync("dashboard/templates/tags/tag-modal.html", {"tag": selectedTag}, "#modal-tag-details").then(function () {
                    let groupOptions = this.dashboardModule.groupsModel.getSelectOptions()
                    this.dashboardModule.tpl.renderIntoAsync('dashboard/templates/tags/owner-select.html', {
                        "multiple": true,
                        "propertyPath": "data_owners",
                        "options": groupOptions
                    }, "#dataOwnersContainer").then(function () {
                        $('#tag_owners').trigger('change')
                        $('#tag_owners').select2({
                            tags: false,
                            dropdownParent: $('#tag_owners').parent()
                        });
                        $('#tag_owners').val(selectedTag.getDataOwners()).trigger('change')

                    }.bind(this))

                    $("#modal-tag-details").modal('show')
                    $('#save_tag_button').click(function (e) {
                        if (!$("form")[0].reportValidity()) {
                            return false
                        }
                        $("#loader-overlay").show()
                        let tag = new PbTag().fromShort($("#resource_name").val())
                        let usableFor = $('[name="usableFor"]:checked').map(function () {
                            return $(this).val();
                        }).get()

                        let tag_owners = $('#tag_owners').val()

                        tag.setUsableFor(usableFor)
                        this.taglist.apiDeleteTags([selectedTag]).then(function () {
                            this.taglist.apiAddTags([tag], tag_owners).then(function () {
                                $("#modal-tag-details").modal('hide')
                                $("#loader-overlay").hide()
                            })
                        }.bind(this))
                    }.bind(this))
                }.bind(this))
            }.bind(this))
        }.bind(this))
    }

    initListeners() {
        document.addEventListener("pbTagsListChanged", function (event) {
            this.taglist.load(true).then(function () {
                this.render().then(function () {
                    $("#loader-overlay").hide()
                })

            }.bind(this))
        }.bind(this))

        document.addEventListener("notification", function (event) {
            if (event.data.data.event == "tagAlreadyExists") {
                this.dashboardModule.notifications.showAlert("Tag <code>" + new PbTag().fromShort(event.data.data.args.key + ":" + event.data.data.args.value).short() + "</code> existiert bereits.", "Tag wurde nicht erstellt")
            }
        }.bind(this))

        $("#search_button").click(function () {
            this.render()
        }.bind(this))

        $("#search_input").on("keyup", function () {
            this.render()
        }.bind(this))


        $('#add_tag_button').on('click', function () {
            return this.dashboardModule.tpl.renderIntoAsync("dashboard/templates/tags/tag-modal.html", {"tag": new PbTag().fromShort("")}, "#modal-tag-details").then(function () {
                let groupOptions = this.dashboardModule.groupsModel.getSelectOptions()
                this.dashboardModule.tpl.renderIntoAsync('dashboard/templates/tags/owner-select.html', {
                    "multiple": true,
                    "propertyPath": "data_owners",
                    "options": groupOptions
                }, "#dataOwnersContainer").then(function () {
                    $('#tag_owners').trigger('change')
                    $('#tag_owners').select2({
                        tags: false,
                        dropdownParent: $('#tag_owners').parent()
                    });
                }.bind(this))

                $("#modal-tag-details").modal('show')

                $('#save_tag_button').click(function (e) {
                    if (!$("form")[0].reportValidity()) {
                        return false
                    }
                    $("#loader-overlay").show()
                    let tag = new PbTag().fromShort($("#resource_name").val())
                    let usableFor = $('[name="usableFor"]:checked').map(function () {
                        return $(this).val();
                    }).get()

                    let tag_owners = $('#tag_owners').val()

                    tag.setUsableFor(usableFor)
                    this.taglist.apiAddTags([tag], tag_owners).then(function () {
                        $("#modal-tag-details").modal('hide')
                        $("#loader-overlay").hide()
                    })
                }.bind(this))
            }.bind(this))
        }.bind(this))
    }
}