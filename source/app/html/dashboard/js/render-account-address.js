import {validateForm} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"

export class RenderAccountAddress {
    account_id = undefined
    #edit_mode = false

    constructor(pbTpl, pbAccount, pbI18n) {
        if (!RenderAccountAddress.instance) {
            this.tpl = pbTpl
            this.account = pbAccount
            this.pbI18n = pbI18n
            RenderAccountAddress.instance = this
        }

        return RenderAccountAddress.instance
    }

    initView() {
        RenderAccountAddress.instance.renderAddressForm()
        $("#loader-overlay").hide()
        let self = this
        document.addEventListener("command", function _(e) {
            if (e.data.data.command == "reloadAccountProfileData") {
                if (!self.fnonWait) {
                    Fnon.Wait.Circle(self.pbI18n.translate("Daten werden aktualisiert..."), {
                        position: 'center-top',
                        fontSize: '14px',
                        width: '300px'
                    })
                } else {
                    Fnon.Wait.Change(self.pbI18n.translate("Daten werden aktualisiert..."))
                }
                self.account.init(self.account.loaded_account_id, true).then(function () {
                    Fnon.Hint.Success(self.pbI18n.translate("billling_address_updated"))
                    RenderAccountAddress.instance.renderAddressForm()
                    Fnon.Wait.Remove()
                })
            }
        })
    }

    activateEditMode() {
        this.#edit_mode = true
        $('#save_address_button').prop('hidden', false)
        $('#cancel_address_button').prop('hidden', false)
        $('#edit_address_button').prop('hidden', true)
        $('[data-group="info"]').prop('disabled', false)
    }

    deactivateEditMode() {
        this.#edit_mode = false
        $('#save_address_button').prop('hidden', true)
        $('#cancel_address_button').prop('hidden', true)
        $('#edit_address_button').prop('hidden', false)
        $('[data-group="info"]').prop('disabled', true)
    }

    renderAddressForm() {
        let self = this
        this.tpl.renderIntoAsync('snippets/address/form.html', {}, "#address_form_container").then(function () {
            // $('#edit_address_button').click(function() {
            //     self.activateEditMode()
            // })
            $('#cancel_address_button').click(function () {
                self.deactivateEditMode()
                RenderAccountAddress.instance.renderAddressForm()
            })
            $('#save_address_button').click(function () {
                if (self.submitForm()) {
                    self.deactivateEditMode()
                }
            })
            $('input[name=address_kind]').on('click', function () {
                self.updateAddressType()
            })

            self.setAddressData()
            self.updateAddressType()
            if (self.account.hasPermission("system.account.profile.management.billingaddress.write")) {
                self.activateEditMode()
            } else {
                self.deactivateEditMode()
            }

            $('[data-group="info"]').each(function () {
                if ($(this).attr("type") == "radio") {
                    $(this).click(function () {
                        $('#save_address_button').prop('hidden', false)
                        $('#cancel_address_button').prop('hidden', false)
                    })
                } else {
                    $(this).keyup(function () {
                        $('#save_address_button').prop('hidden', false)
                        $('#cancel_address_button').prop('hidden', false)
                    })
                }

            })

        })
    }

    submitForm() {
        if (this.validateFormData()) {
            let data = this.getFormData()
            if (!this.account.accountProfileData["addresses"])
                this.account.accountProfileData["addresses"] = {}
            this.account.accountProfileData["addresses"]["billing"] = data
            this.fnonWait = true
            Fnon.Wait.Circle(this.pbI18n.translate("Daten werden gespeichert..."), {
                position: 'center-top',
                fontSize: '14px',
                width: '300px'
            })
            this.account.saveAccountProfileData("system")
            this.deactivateEditMode()
        } else {
            return false
        }

    }

    validateFormData() {
        this.updateAddressType()
        let validations = {}
        $('[data-validate-as]').each(function () {
            if (!$(this).prop("disabled")) {
                validations[this.name] = $(this).attr("data-validate-as")
            }
        })
        let result = validateForm(validations, {}, true, false)

        return result
    }

    getFormData() {
        let account_address_data = {}
        $('[data-group="info"]').each(function () {
            if (!$(this).prop("disabled")) {
                if (this.type == "radio" && this.checked) {
                    account_address_data[this.name] = this.value
                } else if (this.type == "text" || this.type == "email") {
                    account_address_data[this.name] = this.value
                }
            }
        })
        return account_address_data
    }

    setAddressData(self = this) {
        $('[data-group="info"]').each(function () {

            if (this.name == "address_kind") {
                if (this.value == self.account.accountProfileData?.addresses?.billing?.[this.name]) {
                    $(this).attr("checked", "checked")
                    $(this).prop("checked", true)
                }
            } else if (self.account.accountProfileData?.addresses?.billing?.[this.name]) {
                this.value = self.account.accountProfileData.addresses.billing[this.name]
            } else if (self.account.accountProfileData?.[this.name]) {
                this.value = self.account.accountProfileData[this.name]
            }
        })
    }

    updateAddressType() {
        if ($('#address_kind_company').is(':checked')) {
            $('#address_kind_company').prop("checked", true)
            $('#address_kind_private').prop("checked", false)
            $('#company_input_wrapper').show()
            if (this.#edit_mode) {
                $('#companyname').prop("disabled", false)
            }
            $('#companyname').prop("required", true)
            $('#firstname').prop("required", false)
            $('#lastname').prop("required", false)
            $('[data-required-for-private="true"]').hide()
        } else {
            $('#address_kind_company').prop("checked", false)
            $('#address_kind_private').prop("checked", true)
            $('#company_input_wrapper').hide()
            $('#companyname').prop("disabled", true)
            $('#companyname').prop("required", false)
            $('#firstname').prop("required", true)
            $('#lastname').prop("required", true)
            $('[data-required-for-private="true"]').show()
        }
    }
}
