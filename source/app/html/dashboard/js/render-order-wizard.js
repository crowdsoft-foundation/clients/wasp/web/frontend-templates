import {getCookie, getQueryParam, setCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbNotifications} from "/csf-lib/pb-notifications.js?v=[cs_version]"
import {Wizard} from "../../csf-lib/pb/wizard/pb-wizard.js?v=[cs_version]"
import {PbSubscriptions} from "../../csf-lib/pbapi/accountmanager/pb-subscriptions.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import { CONFIG } from '/pb-config.js?v=[cs_version]';

export class RenderOrderWizard {
    constructor(DashboardModule) {
        this.dashboardModule = DashboardModule
        this.wizard = new Wizard(this.dashboardModule.tpl, this.dashboardModule.i18n)
        this.selectedPackageData = undefined
        this.notifications = new PbNotifications()
        this.subscriptions = new PbSubscriptions()
        this.account = new PbAccount()
        this.packages = {
            "fpp": {
                "name": "Familien Planer Premium",
                "apps": "Familien Kalender, Kontakt Manager, Aufgaben, Notizen ",
                "includes": "Familien Kalender, Kontakt Manager, Aufgaben, Notizen, Werbefrei, 5 Benutzer Logins, Email-Support",
                "logins": "5",
                "pricing_options": [
                    {
                        "id": "radio-button-0",
                        "package": "free_tier",
                        "title": this.dashboardModule.i18n.translate("3 Monate Laufzeit"),
                        "price": "€49,00",
                        "price_numeric": 49.00
                    },
                    {
                        "id": "radio-button-1",
                        "package": "familyPlanerPremium_test",
                        "title": this.dashboardModule.i18n.translate("6 Monate Laufzeit"),
                        "price": "€89,00",
                        "price_numeric": 89.00
                    },
                    {
                        "id": "radio-button-2",
                        "package": "familyPlanerPremium",
                        "title": this.dashboardModule.i18n.translate("12 Monate Laufzeit"),
                        "price": "€129,00",
                        "price_numeric": 129.00
                    }
                ]
            },
            "fpb": {
                "name": "Familien Planer Basic",
                "apps": "Familien Kalender",
                "includes": "Familien Kalender, 1 Benutzer Login, Community-Support-Forum",
                "logins": "1",
                "pricing_options": [
                    {
                        "id": "radio-button-0",
                        "package": "free_tier",
                        "title": this.dashboardModule.i18n.translate("3 Monate Laufzeit"),
                        "price": "€0,00",
                        "price_numeric": 0.00
                    }
                ]
            }
        }
        document.addEventListener("command", function _(event) {
            if (event?.data?.data?.command == "reloadAccountProfileData") {
                this.updateBillingAddressBlock(true)
                this.billing_address_window?.close()
            }
        }.bind(this))
    }

    init() {
        return new Promise(async function (resolve, reject) {
            await this.account.init()
            await this.subscriptions.loadFromApi()
            resolve()
        }.bind(this))
    }

    updateBillingAddressBlock(force_reload) {
        return new Promise(async function (resolve, reject) {
            if (force_reload === true) {
                this.notifications.blockForLoading("#billing_address_formatted")
                this.account.init(getCookie("consumer_id"), true).then(function () {
                    if (this.account.hasValidBillingAddress()) {
                        $("#billing_address_formatted").html(this.account.getFormattedBillingAddressBlock())
                    } else {
                        this.renderBillingNotSetBlock()
                    }
                    this.notifications.unblock("#billing_address_formatted")
                    resolve()
                }.bind(this))
            } else {
                if (this.account.hasValidBillingAddress()) {
                    $("#billing_address_formatted").html(this.account.getFormattedBillingAddressBlock())
                } else {
                    this.renderBillingNotSetBlock()
                }
                resolve()
            }


        }.bind(this))
    }

    renderBillingNotSetBlock(targetSelector = "#billing_address_formatted") {
        $("#address_form_container").remove()
        this.dashboardModule.tpl.renderIntoAsync("dashboard/templates/orderwizard/noBillingAddress.html", {}, targetSelector, "overwrite").then(function () {
            $("#account_address_link_not_set").click(function () {
                this.billing_address_window = window.open("/dashboard/account-address.html")
            }.bind(this))
        }.bind(this))
    }

    onStepOneLoad(data) {
        console.log("On Load Step 1", data);

        $("#wizard_prev_btn").first().remove()
        $("#wizard_next_btn").first().html(this.dashboardModule.i18n.translate("Next >"))
    }

    onStepOneLeave(data) {
        console.log("On Leave Step 1", data);
        if (!$("form")[0].reportValidity()) {
            this.notifications.showAlert(this.dashboardModule.tpl.translate("Bitte wählen sie die gewünschte Laufzeit aus."))
            return false
        }
        return true
    }

    onStepTwoLoad(data) {
        console.log("On Load Step 2", data);
        $("#account_address_link").click(function () {
            this.billing_address_window = window.open("/dashboard/account-address.html")
        }.bind(this))
        this.updateBillingAddressBlock(false)
        let selectedOption = data?.formdata.filter((entry) => (entry?.id.startsWith("radio-button") && entry?.data?.checked === true) || false)[0]
        selectedOption = this.selectedPackageData.pricing_options.filter((entry) => entry.id === selectedOption.id)[0]
        $("#product_title").html(selectedOption.title)
        $("#product_net_price").html("€" + (selectedOption.price_numeric - selectedOption.price_numeric * 0.19))
        $("#product_vat").html("€" + (selectedOption.price_numeric * 0.19))
        $("#product_gross_price").html(selectedOption.price)
        $("#wizard_prev_btn").first().html(this.dashboardModule.i18n.translate("< Back"))
        $("#wizard_next_btn").first().html(this.dashboardModule.i18n.translate("Next >"))
    }

    onStepTwoLeave(data) {
        let formcheck = this.account.hasValidBillingAddress()
        if (!formcheck) {
            $("#billing_address_formatted").css("border", "1px solid red")
            $("#billing_address_formatted").fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100)
        }
        return formcheck
    }

    onStepThreeLoad(data) {
        console.log("On Load Step 3", data);
        this.updateBillingAddressBlock(false)
        let selectedOption = data?.formdata.filter((entry) => (entry?.id.startsWith("radio-button") && entry?.data?.checked === true) || false)[0]
        selectedOption = this.selectedPackageData.pricing_options.filter((entry) => entry.id === selectedOption.id)[0]
        $("#product_title").html(selectedOption.title)
        $("#product_net_price").html("€" + (selectedOption.price_numeric - selectedOption.price_numeric * 0.19))
        $("#product_vat").html("€" + (selectedOption.price_numeric * 0.19))
        $("#product_gross_price").html(selectedOption.price)
        $("#wizard_next_btn").html(this.dashboardModule.i18n.translate("Order now"))
        $("#wizard_prev_btn").html(this.dashboardModule.i18n.translate("< Back"))
        $('[data-id="order_overview_container_min"]').toggle();
        $('[data-id="order_overview_container"]').toggle();

        $('[data-id="max_link"]').click(function (e) {
            $('[data-id="order_overview_container_min"]').toggle();
            $('[data-id="order_overview_container"]').toggle();
        });

        $('[data-id="min_link"]').click(function (e) {
            $('[data-id="order_overview_container_min"]').toggle();
            $('[data-id="order_overview_container"]').toggle();
        });

        $("#account_address_link").click(function () {
            this.billing_address_window = window.open("/dashboard/account-address.html")
        }.bind(this))

        return true;
    }

    onStepThreeLeave(data) {
        console.log("On Leave Step 3", data);
        if (!$("#purchase_agb").is(':checked')) {
            $("#purchase_agb").fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100)
            this.notifications.showAlert(this.dashboardModule.tpl.translate("Bitte aktzeptieren sie unsere Nutzungsbedingungen und Widerrufsrecht"))
            return false
        }
        return true
    }

    onStepFourLoad(data) {
        console.log("On Load Step 4", data);
        this.notifications.blockForLoading("#subscription_enddate")
        this.updateBillingAddressBlock(false)
        let selectedOption = data?.formdata.filter((entry) => (entry?.id.startsWith("radio-button") && entry?.data?.checked === true) || false)[0]
        selectedOption = this.selectedPackageData.pricing_options.filter((entry) => entry.id === selectedOption.id)[0]

        document.addEventListener("command", function _(e) {
            if (e.data?.data?.command == "refreshAccessRights") {
                this.subscriptions.loadFromApi().then(function() {
                    $("#subscription_enddate").html(moment(this.subscriptions.getActive().slice(-1)[0].end_date).format(CONFIG.DATEFORMAT))
                    this.notifications.unblock("#subscription_enddate")
                }.bind(this))
            }
        }.bind(this))

        this.subscriptions.addAccountPackage(selectedOption.package)
        this.wizard.disableHeaderLinks()
        $("#wizard_prev_btn").remove()
        $("#wizard_next_btn").remove()

        return true;
    }


    onStepFourLeave(data) {
        console.log("On Leave Step 4", data);
        $("#wizard_prev_btn a").remove()
        $("#wizard_next_btn a").remove()
    }


    render() {
        return this.wizard.init().then(async function () {
            this.selectedPackageData = this.packages[getQueryParam("package") || "fpp"]

            console.log("HAS BILLING", this.account.hasValidBillingAddress())
            this.wizard.newScenario("OrderProcess", [])
            this.wizard.addStep(1, "Produktauswahl", "Bestellung", "Familien Planer Premium", "dashboard/templates/orderwizard/step_1_bodycontent.html", this.onStepOneLoad.bind(this), this.onStepOneLeave.bind(this), this.selectedPackageData)
            this.wizard.addStep(2, "Zahlung & Adresse", "Bestellung", "Zahlung & Adresse", "dashboard/templates/orderwizard/step_2_bodycontent.html", this.onStepTwoLoad.bind(this), this.onStepTwoLeave.bind(this), this.selectedPackageData)
            this.wizard.addStep(3, "Prüfen & Bestellen", "Bestellung", "Prüfen & Bestellen", "dashboard/templates/orderwizard/step_3_bodycontent.html", this.onStepThreeLoad.bind(this), this.onStepThreeLeave.bind(this), this.selectedPackageData)
            this.wizard.addStep(4, "Bestellung Abgeschlossen", "Vielen Dank für deine Bestellung", "Folgende Pakete stehen dir ab sofort bis zum Laufzeitende zur Verfügung", "dashboard/templates/orderwizard/step_4_bodycontent.html", this.onStepFourLoad.bind(this), this.onStepFourLeave.bind(this), this.selectedPackageData)

            const url = new URL(location);
            let start_step = url.searchParams.get("step") || 1;

            await this.wizard.renderStep(start_step)
        }.bind(this))

    }
}