import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {
    getCookie,
    fireEvent,
    showNotification,
    getQueryParam,
    get_news_for,
    getWelcome,
    togglePassword,
    loadNotifications
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]"
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {validateForm, fadeIn, stepValidatePassword} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {validateLetNumbUnderField, clearForm} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {GroupUsers} from "./group-users.js?v=[cs_version]"
import {PbLogin} from "../../csf-lib/pb-login.js?v=[cs_version]"
import {PbGuide} from "../../csf-lib/pb-guide.js?v=[cs_version]"
import {RenderProfileSettings} from "./render-profile-settings.js?v=[cs_version]"
import {RenderAccountSettings} from "./render-account-settings.js?v=[cs_version]"
import {RenderAccountLogoSettings} from "./render-account-logo-settings.js?v=[cs_version]"
import {RenderDashboard} from "./render-dashboard.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {RenderAccountAddress} from "./render-account-address.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {Backups} from "./backups.js?v=[cs_version]"
import {PbFroala} from "../../csf-lib/editors/pb-froala.js?v=[cs_version]"
import {PbSubscriptions} from "../../csf-lib/pbapi/accountmanager/pb-subscriptions.js?v=[cs_version]";
import {PbGroups} from "../../csf-lib/pbapi/accountmanager/pb-groups.js?v=[cs_version]"

export class DashboardModule {
    actions = {}

    constructor() {
        if (!DashboardModule.instance) {
            this.initListeners()
            this.initActions()
            $.unblockUI()
            $("#export_loading").hide()
            DashboardModule.instance = this
            DashboardModule.instance.acc = new PbAccount()
            DashboardModule.instance.tpl = new PbTpl()
            DashboardModule.instance.i18n = new pbI18n()
            DashboardModule.instance.subscriptions = new PbSubscriptions()
            DashboardModule.instance.groupsModel = new PbGroups()
            DashboardModule.instance.notifications = new PbNotifications()

        }

        return DashboardModule.instance
    }

    getCurrentlyShownRole() {
        let urlUser = getQueryParam("user")
        let urlRole = getQueryParam("role")
        let role = undefined
        if (urlUser != undefined) {
            role = "user." + urlUser
        }
        if (urlRole != undefined) {
            role = "role." + urlRole
        }
        return role
    }

    getRoleDataFromUri() {
        let urlUser = getQueryParam("user")
        let urlRole = getQueryParam("role")

        let name = ""
        let type = undefined
        if (urlUser != undefined) {
            name = urlUser
            type = "user"
        }
        if (urlRole != undefined) {
            name = urlRole
            type = "role"
        }
        let role = type + "." + name

        return {"name": name, "type": type, "role": role}
    }

    initActions() {
        this.actions["dashboardModuleInitialised"] = function (myevent, callback = () => "") {

            new PbAccount().handlePermissionAttributes()
            Render.dataExportRange()
            let account_id = getCookie("consumer_id")
            new PbAccount(account_id).init().then(() => {
                    try {
                        loadNotifications()
                    } catch (err) {

                    }
                    get_news_for("easy2schedule")
                    new RenderDashboard()

                    getWelcome("dashboard")

                    document.addEventListener("news_available", (event) => RenderDashboard.instance.checkAccountForNews(event), false);
                    RenderDashboard.instance.checkCountdownAlert()
                    new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}getting_started.js`)
                    $("#loader-overlay").hide()
                    $("#contentwrapper").show()
                }
            )
            callback(myevent)
        }

        this.actions["initSystemAccountProfileSettings"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.account.profile")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    let account_id = getCookie("consumer_id")
                    new PbAccount(account_id).init().then(
                        function () {
                            new RenderDashboard()
                            new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}admin.js`)
                            callback(myevent)
                        }
                    )
                }
            })
        }
        this.actions["systemAccountProfileSettingsInitialized"] = function (myevent, callback = () => "") {

            new RenderAccountSettings(new PbAccount()).setInputFields()
            Render.fromCookietoString("consumer_name", "account_company_name")

            callback(myevent)
            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }
        }

        this.actions["initSystemProfileSettings"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.user.own.profile")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new PbLogin().init(getCookie("username")).then(
                        function () {
                            callback(myevent)
                        }
                    )
                }
            })
        }
        this.actions["systemProfileSettingsInitialized"] = function (myevent, callback = () => "") {
            new RenderProfileSettings(new PbLogin()).setInputFields()
            callback(myevent)
            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }
            stepValidatePassword("new_password")
        }

        this.actions["easy2scheduleDataExportRequested"] = function (myevent, callback = () => "") {
            Render.dataExportLink($('#easy2schedule_data_export').attr('data-start'), $('#easy2schedule_data_export').attr('data-end'))
            callback(myevent)

        }

        this.actions["account-manager-init"] = function (myevent, callback = () => "") {

            new PbAccount().init(getCookie("consumer_id")).then(() => {
                if (!new PbAccount().hasPermission("dashboard.management.usermanagement")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    callback(myevent)
                    new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}admin.js`)
                }
            })

        }

        this.actions["role-manager-init"] = function (myevent, callback = () => "") {

            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("dashboard.management.rolemanagement")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    callback(myevent)
                    new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}admin.js`)
                }
            })

        }

        this.actions["user-manager-init"] = function (myevent, callback = () => "") {
            let urlUser = getQueryParam("user")
            let urlRole = getQueryParam("role")
            if (urlUser == undefined && urlRole == undefined) {
                alert("URL-Parameter is missing, please use the provided lists to get on this page.")
            }


            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("dashboard.management.usermanagement")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    let role = new DashboardModule().getCurrentlyShownRole()
                    new pbPermissions().init(role).then(function () {
                        new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}admin.js`)
                        callback(myevent)
                    })

                }
            })

        }


        this.actions["user-manager-initialised"] = function (myevent, callback = () => "") {

            let data = new DashboardModule().getRoleDataFromUri()

            let type = data["type"]
            let name = data["name"]
            let role = data["role"]

            // function set custome page desplay for edited user or role
            Render.customPreloadsForDataset(name, type, role)


            //render assigned roles for the user
            let import_roles = []
            if (new pbPermissions().roleset["roles"][role] != undefined) {
                import_roles = new pbPermissions().roleset["roles"][role].definition.import
            }

            if (type == "user") {
                Render.customPreloadsForDataset(name, type, role)
            }

            if (type == "role") {
                Render.customPreloadsForDataset(name, type, role)
            }

            let current_ruleset = new pbPermissions().userRuleSetTemplate
            current_ruleset.name = role
            current_ruleset.definition.import = import_roles
            current_ruleset.definition.rules = new pbPermissions().roleset["roles"][role] != undefined ? new pbPermissions().roleset["roles"][role].definition.rules : {}
            new pbPermissions().userrules[current_ruleset.name] = current_ruleset

            Render.renderRoleMembershipTagsFor(current_ruleset.name)
            Render.renderRoleListFor(current_ruleset.name)
            Render.renderUserRuleListFor(current_ruleset.name)
            Render.renderRuleListFor(current_ruleset.name)
            Render.renderUserRightsTableFor(current_ruleset.name)
            Render.fromCookietoString("consumer_name", "account_company_name")
            Fnon.Box.Remove('.az-content-body')
            callback()
        }

        this.actions["account-manager-initialised"] = function (myevent, callback = () => "") {
            Render.logins(document.getElementById("bootstraptable"))
            Render.fromCookietoString("consumer_name", "account_company_name")
            stepValidatePassword("newloginpassword")

            document.getElementById("invite_now").addEventListener("click", function () {
                let invite_email = document.getElementById("invite_email").value

                if (validateForm({'invite_email': 'input'}
                ) == true) {
                    let onsuccess = function () {
                        showNotification("Einladung wurde verschickt.")
                    }
                    let onerror = function (message) {
                        showNotification("Einladung konnte nicht verschickt werden.")
                        console.error("INVITATION EMAIL FAILED REASON", message)
                    }
                    new PbAccount().sendInvitationLink(invite_email, onsuccess, onerror)
                    clearForm('invite_email')
                }

            })

            //sendInvitationLink

            callback(myevent)
            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }
        }

        this.actions["role-manager-initialised"] = function (myevent, callback = () => "") {
            new pbPermissions().init().then(function () {
                if (!new PbAccount().hasPermission("dashboard.management.rolemanagement")) {
                } else {
                    Render.roles(document.getElementById("bootstraptable"))
                    Render.fromCookietoString("consumer_name", "account_company_name")
                }
            })
            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }
            callback()
        }

        this.actions["group-manager-init"] = function (myevent, callback = () => "") {

            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.groups.management")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    callback(myevent)
                }
            })

        }

        this.actions["group-manager-initialised"] = function (myevent, callback = () => "") {
            new pbPermissions().init().then(function () {
                if (!new PbAccount().hasPermission("system.groups.management")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    Render.groups(document.getElementById("bootstraptable"))
                    Render.fromCookietoString("consumer_name", "account_company_name")
                    new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}admin.js`)
                }
            })
            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }
            callback()
        }

        this.actions["group-users-manager-init"] = function (myevent, callback = () => "") {
            new pbPermissions().init().then(function () {
                if (!new PbAccount().hasPermission("system.groups.management")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new GroupUsers().init().then((ret) => ret.run())
                    Render.fromCookietoString("consumer_name", "account_company_name")
                }
            })
            Fnon.Box.Remove('.az-content-body')
            callback()
        }

        this.actions["initSystemAccountProfileLogoSettings"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.account.profile")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new PbAccount().init(getCookie("consumer_id")).then(
                        function () {
                            callback(myevent)
                        }
                    )
                }
            })
        }
        this.actions["systemAccountProfileLogoSettingsInitialized"] = function (myevent, callback = () => "") {
            new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}admin.js`)
            new RenderAccountLogoSettings(new PbAccount()).setLogoData()
            Render.fromCookietoString("consumer_name", "account_company_name")

            callback(myevent)
            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }
        }

        this.actions["initSystemAccountAddress"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.account.profile.management.billingaddress")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    callback(myevent)
                }
            })
        }
        this.actions["systemAccountAddressInitialized"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.account.profile.management.billingaddress")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new PbGuide(`./tours/${CONFIG.HELPFILE_PREFIX}admin.js`)
                    new RenderAccountAddress(DashboardModule.instance.tpl, PbAccount.instance, new pbI18n()).initView()
                    if ($("#loader-overlay")) {
                        $("#loader-overlay").hide()
                        $("#contentwrapper").show()
                    }
                    callback(myevent)
                }
            })
        }

        this.actions["initAccountBackup"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.account.backups")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    callback(myevent)
                }
            })
        },
            this.actions["accountBackupInitialised"] = function (myevent, callback = () => "") {
                new PbAccount().init().then(() => {
                    if (!new PbAccount().hasPermission("system.account.backups")) {
                        new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                    } else {
                        if ($("#loader-overlay")) {
                            $("#loader-overlay").hide()
                            $("#contentwrapper").show()
                            let data = new Backups().getAllDownloadUrls()
                            PbTpl.instance.renderIntoAsync('dashboard/templates/backup_url_table.tpl', {"data": data}, '#bootstraptablecontainer')
                        }
                        callback(myevent)
                    }
                })
            }

        this.actions["initFilestorage"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.account")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    document.addEventListener("command", function _(event) {
                        if (event.data.data.command == "reloadAccountProfileData") {
                            new PbFroala().testWebDav(
                                () => PbNotifications.instance.showHint("Saved data and successfully established connection"),
                                () => PbNotifications.instance.showHint("Saved connection-data but couldn't establish connection. Maybe wrong credentials?", "warning"),
                            ).finally(() =>
                                PbNotifications.instance.unblock("#save_webdav_button")
                            )
                        }
                    })
                    callback(myevent)
                }
            })

        }

        this.actions["filestorageInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("system.account")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    PbTpl.instance.renderIntoAsync('dashboard/templates/filestorage-form.tpl', {"data": ""}, '#form-container').then(() => {
                        $("#loader-overlay").hide()
                        $("#contentwrapper").show()

                        $("#hostname").val(PbAccount.instance?.accountProfileData?.filestorage_webdav?.hostname)
                        $("#login").val(PbAccount.instance?.accountProfileData?.filestorage_webdav?.login)
                        $("#data_password").val(PbAccount.instance?.accountProfileData?.filestorage_webdav?.password)

                        $("#webdav_data_form").submit(() => {
                            PbNotifications.instance.blockForLoading("#save_webdav_button")
                            let data = {}
                            data["hostname"] = $("#hostname").val()
                            data["login"] = $("#login").val()
                            data["password"] = $("#data_password").val()
                            PbAccount.instance.accountProfileData["filestorage_webdav"] = data
                            PbAccount.instance.saveAccountProfileData(PbAccount.instance.loaded_account_id)
                        })
                        callback(myevent)
                    })


                }
            })
        }

        this.actions["orderWizardInitialized"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (false && !new PbAccount().hasPermission("system.account.profile.management.billingaddress")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    import("./render-order-wizard.js?v=[cs_version]").then((Module) => {
                        let renderOrderWizard = new Module.RenderOrderWizard(DashboardModule.instance)
                        renderOrderWizard.init().then(function () {
                            renderOrderWizard.render().then(() => {
                                window.setTimeout(() => {
                                    $("#contentwrapper").show()
                                    $("#loader-overlay").hide()
                                }, 10)
                                callback(myevent)
                            })
                        })
                    })
                }
            })
        }

        this.actions["tagSettingsInitialized"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (false && !new PbAccount().hasPermission("system.account.profile.management.billingaddress")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    import("./render-tagsettings.js?v=[cs_version]").then((Module) => {
                        let tagSettings = new Module.RenderTagSettings(DashboardModule.instance)
                        tagSettings.init().then(function () {
                            tagSettings.render()
                            $("#contentwrapper").show()
                            $("#loader-overlay").hide()
                        }.bind(this))
                    })
                }
            })
        }
    }

    initListeners() {
        let socket = new socketio()
        socket.commandhandlers["refreshAccessRights"] = function () {
            if (new DashboardModule().getRoleDataFromUri()["type"] != undefined) {
                Render.refreshContent(new DashboardModule().getRoleDataFromUri()["role"])
            } else {
                new PbAccount().refreshPermissions()
                if (document.getElementById("newgroupname") && document.getElementById("bootstraptable")) {
                    Render.groups(document.getElementById("bootstraptable"), true)
                }
            }

            if (document.getElementById("newrolename") && document.getElementById("bootstraptable")) {
                Fnon.Hint.Success('Rolle wurde angelegt.')
                Render.roles(document.getElementById("bootstraptable"), true)
                Fnon.Box.Remove('.az-content-body')
            }
        }

        document.addEventListener("event", function _(e) {
            if (e.data && e.data.data && e.data.data.event == "rightRoleCreationFailed") {
                DashboardModule.instance.notifications.showHint(e.data.data.args.reason, "error")
                Fnon.Box.Remove('.az-content-body')
            }
        })

        document.addEventListener("notification", function _(event) {
            if (event.data.data.event == "loginCreated") {
                Fnon.Hint.Success("Login " + event.data.data.args.username + " wurde erstellt.")
                Render.logins(document.getElementById("bootstraptable"), true)
                try {
                    Fnon.Box.Remove('.az-content-body', 500)
                } catch (err) {
                }
            }
            if (event.data.data.event == "loginDeleted") {
                Render.logins(document.getElementById("bootstraptable"), true)
                Fnon.Hint.Success("Login wurde gelöscht.")
                try {
                    Fnon.Box.Remove('.az-content-body', 500)
                } catch (err) {
                }
            }
            if (event.data.data.event == "roleDeleted") {
                Fnon.Hint.Success('Rolle wurde gelöscht.')
                Render.roles(document.getElementById("bootstraptable"), true)
                try {
                    Fnon.Box.Remove('.az-content-body', 500)
                } catch (err) {
                }
            }

        })

        socket.commandhandlers["reloadLoginProfileData"] = function () {
            setTimeout(function () {
                location.reload()
            }, 1000)
        }


        // $("#easy2schedule_data_export").prop("onclick", null).off("click")
        $("#easy2schedule_data_export").click(function () {
            fireEvent("easy2scheduleDataExportRequested")
        })


        $("#createLoginButton").click(function () {
            if (validateForm({
                    'newloginemail': 'input',
                    'newloginpassword': 'password'
                }
            ) == true) {
                Fnon.Box.Circle('.az-content-body', '', {
                    svgSize: {w: '50px', h: '50px'},
                    svgColor: '#3a22fa',
                })
                let roles = undefined
                if ($("#family-role-select").is(":visible")) {
                    let role = $("#family-role-select").val()
                    if (PbAccount.instance.hasPermission("frontend.dashboard.familyplannerpremium")) {
                        role = role.replace("Planer", "PlanerPremium")
                    }
                    roles = [role]
                }

                new PbAccount().createLogin(
                    document.getElementById("newloginemail").value,
                    document.getElementById("newloginpassword").value,
                    roles,
                    function () {
                        document.getElementById("newloginemail").value = ""
                        document.getElementById("newloginpassword").value = ""
                    },
                    function () {
                        showNotification("Login konnte nicht angelegt werden.")
                    }
                )
            }
        })

        $("#pw-toggle").click(function () {
            togglePassword("newloginpassword", "pw-toggle")
        })
        $("#pw-toggle-old").click(function () {
            togglePassword("old_password", "pw-toggle-old")
        })
        $("#pw-toggle-new").click(function () {
            togglePassword("new_password", "pw-toggle-new")
        })
        $("#pw-toggle-new-confirm").click(function () {
            togglePassword("new_password_confirm", "pw-toggle-new-confirm")
        })


        //add or remove user role from role checkbox list and update user role display area and user ruleset table
        $(document).on('click', 'input.role-check-box', function (e) {
            let clicked_role = $(this).attr("id")
            let role = new DashboardModule().getCurrentlyShownRole()

            if (e.target.checked == true && !new pbPermissions().userrules[role].definition.import.includes(clicked_role)) {
                new pbPermissions().userrules[role].definition.import.push(clicked_role)
                Render.addRoleMembershipTags(clicked_role)
            } else {
                if (new pbPermissions().userrules[role].definition.import.includes(clicked_role)) {
                    new pbPermissions().userrules[role].definition.import.splice(
                        new pbPermissions().userrules[role].definition.import.indexOf(clicked_role)
                        , 1
                    )
                }
                Render.removeRoleMembershipTags(clicked_role)
            }

            pbPermissions.instance.setRole(role)
        })

        //remove role from user role display area, update role checkbox list and user ruleset table
        $(document).on('click', 'button[data-selector="btn-role"]', function () {
            let id = $(this).attr('data-roleid')
            let role = new DashboardModule().getCurrentlyShownRole()
            let current_user = "user." + getCookie("username")

            let confirmed = false

            if (current_user == role && (id == "role.admin" || id == "role.sys_admin")) {
                confirmed = confirm("Sind sie sicher das sie die Admin-Rolle von ihrem Zugang entfernen wollen? Sofern kein anderer Benutzer die Admin-Rolle hat, wird das System möglicherweise unbenutzbar. ")
            } else {
                confirmed = true
            }

            if (confirmed) {
                $(document.getElementById(id)).prop({checked: false})

                if (new pbPermissions().userrules[role].definition.import.includes(id)) {
                    new pbPermissions().userrules[role].definition.import.splice(
                        new pbPermissions().userrules[role].definition.import.indexOf(id)
                        , 1
                    )
                }
                Render.removeRoleMembershipTags(id)
                pbPermissions.instance.setRole(role)

                $(this).remove()
            }
        })


        //add or remove rule from rule checkbox list, update user rule display area and user ruleset table#
        $(document).on('click', 'input.rule-check-box', function (e) {
            let clicked_rule = $(this).attr("id")
            let role = new DashboardModule().getCurrentlyShownRole()

            if (e.target.checked == true && !new pbPermissions().userrules[role].definition.rules.hasOwnProperty(clicked_rule)) {

                let action = document.getElementById(clicked_rule).getAttribute('data-rule-action');
                let weight = document.getElementById(clicked_rule).getAttribute('data-rule-weight');

                let new_rule = {
                    "action": action,
                    "weight": parseInt(weight)
                }

                new pbPermissions().userrules[role].definition.rules[clicked_rule] = new_rule
                Render.addUserRuleListItem(clicked_rule)

            } else {
                delete new pbPermissions().userrules[role].definition.rules[clicked_rule]
                Render.removeUserRuleListItem(clicked_rule)
            }

            pbPermissions.instance.setRole(role)
        })

        //remove rule from user rule display area and update rule checkbox list and user ruleset table
        $(document).on('click', 'div[data-selector="assigned-rule"]', function () {
            let deleteQuestion = new pbI18n().translate("Do you really want to delete this rule?")
            let role = new DashboardModule().getCurrentlyShownRole()
            if (confirm(deleteQuestion)) {
                let clicked_rule = $(this).closest('li').attr('data-ruleid')
                $(document.getElementById(clicked_rule)).prop({checked: false})
                $(this).closest('li').remove()

                delete new pbPermissions().userrules[role].definition.rules[clicked_rule]
                pbPermissions.instance.setRole(role)
                Render.enableRuleEditButtons()
            }
        })

        //edit rule in user rule display area
        $(document).on('click', 'a[data-selector="assigned-rule-edit"]', function () {
            let id = $(this).closest('li').attr('id')
            let clicked_button = $(this)
            Render.editUserRules(clicked_button, id)
        })

        //save changes to rule in user rule display area
        $(document).on('click', 'a[data-selector="assigned-rule-save"]', function () {
            let clicked_button = $(this)
            let id = $(this).closest('li').attr('id')
            let rule = $(this).closest('li').attr('data-ruleid')
            let role = new DashboardModule().getCurrentlyShownRole()
            Render.saveUserRules(clicked_button, id, rule, role)
        })

        //edit role description in detail display area
        $(document).on('click', 'a[data-selector="role-details-edit"]', function () {
            let clicked_button = $(this)
            let id = "description"
            Render.editRoleDetails(clicked_button, id)
        })

        //save role descrition in detail display area
        $(document).on('click', 'a[data-selector="role-details-save"]', function () {
            let clicked_button = $(this)
            let id = "description"
            //let rule = $(this).closest('li').attr('data-ruleid')
            let role = new DashboardModule().getCurrentlyShownRole()
            Render.saveRoleDetails(clicked_button, id, role)
        })


        $("#createRoleButton").click(function () {
            let current_ruleset = new pbPermissions().userRuleSetTemplate
            let role

            let roledoesnotexist_form_check_function = function () {

                let result = new pbPermissions().roleset["roles"][role] == undefined
                if (result == false) {

                    document.getElementById('role-error-msg').style.opacity = 0
                    document.getElementById('role-error-msg').innerHTML = "<div class=''>*Rollenname ist bereits vorhanden, bitte verwende einen anderen Namen.*</div>"
                    fadeIn("role-error-msg")

                }
                return new pbPermissions().roleset["roles"][role] == undefined
            }
            // first we check name field format
            if (validateLetNumbUnderField("newrolename") == true) {

                role = Render.validateRoleName("newrolename")
                current_ruleset.name = role
                current_ruleset.description = document.getElementById("newroledescription").value

                // set name and check for all other
                if (validateForm({
                        'newroledescription': 'textarea',
                        'newrolename': 'roledoesnotexist'
                    },
                    {"roledoesnotexist": roledoesnotexist_form_check_function}
                ) == true) {
                    Fnon.Box.Circle('.az-content-body', '', {
                        svgSize: {w: '50px', h: '50px'},
                        svgColor: '#3a22fa',
                    })
                    new pbPermissions().userrules[role] = current_ruleset
                    pbPermissions.instance.setRole(role)
                }

            } else {
                Render.validateRoleName("newrolename")
            }

        })

        $("#createGroupButton").click(function () {
            let newgroup = new pbPermissions().userRuleSetTemplate
            let group

            let groupdoesnotexist_form_check_function = function () {

                let result = new pbPermissions().groups[group] == undefined
                if (result == false) {

                    $.blockUI({
                        blockMsgClass: 'alertBox',
                        message: new pbI18n().translate('Gruppenname ist bereits vorhanden, bitte verwende einen anderen Namen.')
                    });
                    setTimeout(function () {
                        $.unblockUI();
                    }, 2000) //showNotification(new pbI18n().translate("Rollenname ist bereits vorhanden, bitte verwende einen anderen Namen."))

                }
                return new pbPermissions().groups[group] == undefined
            }
            // first we check name field format
            if (validateLetNumbUnderField("newgroupname") == true) {
                group = Render.validateGroupName("newgroupname")
                newgroup.name = group
                newgroup.description = document.getElementById("newgroupdescription").value

                // set name and check for all other
                if (validateForm({
                        'newgroupdescription': 'textarea',
                        'newgroupname': 'groupdoesnotexist'
                    },
                    {"groupdoesnotexist": groupdoesnotexist_form_check_function}
                ) == true) {
                    new pbPermissions().groups[group] = newgroup
                    pbPermissions.instance.setGroup(group).then(function () {
                        $("#newgroupdescription").html("")
                        $("#newgroupdescription").val("")
                        $("#newgroupname").val("")
                        $.blockUI({blockMsgClass: 'alertBox', message: 'Gruppe ' + group + ' wurde angelegt.'});
                        setTimeout(function () {
                            $.unblockUI();
                        }, 2000)

                    })
                }

            } else {
                Render.validateGroupName("newgroupname")
            }

        })

        $('#update_profile_button').on('click', function () {
            RenderProfileSettings.instance.updateProfileSettings()
        })

        $('#edit_address_button').on('click', function () {
            RenderAccountAddress.instance.toggleState()
        })
        $('#save_address_button').on('click', function () {
            RenderAccountAddress.instance.updateAddressData()
        })

        $('#update_signature_button').on('click', function () {
            RenderAccountSettings.instance.updateMessagingAccountSettings()
        })

        $('#edit_logo_button').on('click', function () {
            RenderAccountLogoSettings.instance.updateFormState()
        })
        $('#cancel_logo_button').on('click', () => {
            let deleteTitle = new pbI18n().translate("Cancel")
            let deleteQuestion = new pbI18n().translate("All your changes for the logo will be lost! Do you want to continue?")
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    RenderAccountLogoSettings.instance.updateFormState()
                }
            })
        })

        $('#save_logo_button').on('click', function () {
            RenderAccountLogoSettings.instance.updateAccountLogoData()
            RenderAccountLogoSettings.instance.updateFormState()
        })

        $('#logo-url').on('change', () => {
            let value = $('#logo-url').val()
            try {
                let url = new URL(value)
                $('#logo-display').attr('src', url)
            } catch (error) {
                console.log("Invalid url", error)
            }
        })


        $('#button_dismiss').on('click', () => {
            let guide = new PbGuide()
            let tour = CONFIG.HELPFILE_PREFIX + "dashboard.json"
            guide.markTourSeen(tour)
            console.log("Marked seen:", tour)
        })

    }
}