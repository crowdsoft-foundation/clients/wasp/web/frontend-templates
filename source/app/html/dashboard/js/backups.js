import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"

export class Backups {
    static endpoints = {
        "CS:Dokumentation": {"csv": "/documents/backup/csv", "json": "/documents/backup/json"},
        "CS:Kontakte": {"csv": "/contacts/backup/csv", "json": "/contacts/backup/json"},
        "CS:Aufgaben": {"csv": "/queues/backup/csv", "json": "/queues/backup/json"},
        "CS:Kalender": {"csv": "/cb/backup/csv", "json": "/cb/backup/json"},
    }

    getAllDownloadUrls() {
        let apiBaseUrl = CONFIG.API_BASE_URL
        let apikey = getCookie("apikey")

        let retVal = {}
        for (let [key, value] of Object.entries(Backups.endpoints)) {
            retVal[key] = {
                "csv": apiBaseUrl + value.csv + "?apikey=" + apikey,
                "json": apiBaseUrl + value.json + "?apikey=" + apikey
            }
        }

        return retVal
    }
}