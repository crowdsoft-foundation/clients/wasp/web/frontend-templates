import {CONFIG} from "../../pb-config.js?v=[cs_version]"

export class SystemHelp {

    constructor() {
        if (!SystemHelp.instance) {
            SystemHelp.instance = this
            this.initListeners()
        }
        return SystemHelp.instance
    }

    startTour(steps) {
        console.log("TOURSTEPS", steps)
        let tour_steps = steps.filter(function (obj) {
            //filter elements hidden by permissions
            let list = $(obj.element).hasClass("hidden") === false
            console.log("List", list)
            return $(obj.element).hasClass("hidden") === false || obj.element == ".introjsFloatingElement"
        })
        console.log("STEPS", tour_steps)
        let introguide = introJs()
        introguide.setOptions({
            showBullets: false,
            nextLabel: ">",
            prevLabel: "<",
            showProgress: true,
            hidePrev: true, //turn off keyboard when using onchange calls, otherwise there are errors
            keyboardNavigation: false,
            tooltipClass: 'customTooltip',
            steps: tour_steps,
        })
        introguide.start()


    }

    //  wrap introJs setup in a function so it is called when the page elements are ready
    getStartedTour() {
        var introguide = introJs();

        // add the options and steps to the new intro object
        introguide.setOptions({
            showBullets: false,
            nextLabel: ">",
            prevLabel: "<",
            showProgress: true,
            hidePrev: true, //turn off keyboard when using onchange calls, otherwise there are errors
            keyboardNavigation: false,
            tooltipClass: 'customTooltip',

            steps: [{
                'element': '#app_content',
                'intro': ' This <b>STEP</b> use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. '
            }, {
                'element': '#edit_dashboard_button',
                'intro': 'This <b>STEP</b> use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. '
            }, {
                'element': '#account_content',
                'intro': 'This <b>STEP</b> use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. ',
                'myBeforeChangeFunction': function () {
                    alert('this is a before change loaded function');
                },
                'myChangeFunction': function () {
                    alert('this is a change loaded function');
                },
            }, {
                'element': '#document_content',
                'intro': 'This <b>STEP</b> use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. '
            }, {
                'element': '#header_apps_menu',
                'intro': 'This <b>STEP</b> use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text.'
            }, {
                'element': '#header_profile_menu',
                'intro': 'This <b>STEP</b> use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. '
            }, {
                'element': '#system_message',
                'intro': 'This <b>STEP</b> use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. '
            }, {
                'element': '#help',
                'intro': 'This <b>STEP</b> is the last one. Now we will open the help menu so you can start another tour '
            }].filter(function (obj) {
                //filter elements hidden by permissions
                let list = $(obj.element).hasClass("hidden") === false
                console.log("List", list)
                return $(obj.element).hasClass("hidden") === false || obj.element == ".introjsFloatingElement"
            })
        })


        introguide.start()

        // use built in introjs functions to define what happens before after and between steps
        introguide.onbeforechange(function () {
            // the 'steps' array you can pass to setOptions can be accessed in "this._introItems" when Intro is executing
            // check to see if there is a function on this step
            if (this._introItems[this._currentStep].myBeforeChangeFunction) {
                //if so, execute it.
                this._introItems[this._currentStep].myBeforeChangeFunction();
            }
        }).onchange(function () {  //intro.js built in onchange function
            if (this._introItems[this._currentStep].myChangeFunction) {
                this._introItems[this._currentStep].myChangeFunction();
            }
        })


        introguide.onbeforeexit(function () {
            introguide.addHints().setOptions({
                hintButtonLabel: "Alles klar"
            })
        })

        introguide.oncomplete(function () {
            $('#modal_help').modal('toggle');
        })
    }

    initListeners() {


        $('#help_tour_get_started, #tour_get_started').on('click', function (e) {
            e.preventDefault();
            let tour = CONFIG.HELPFILE_PREFIX + "dashboard.json"
            var settings = {
                "url": CONFIG.APP_BASE_URL + "/tours/" + tour,
                "method": "GET",
                "timeout": 0,
                "success": function (response) {
                    let tour_steps = JSON.parse(response)
                    SystemHelp.instance.startTour(tour_steps)
                   
            if( $('.modal').hasClass('show')){ $('#modal_help').modal('toggle');}
                   

                },
                "error": () => alert("Cannot load tour-data")
            };

            $.ajax(settings)


        });


    }

}