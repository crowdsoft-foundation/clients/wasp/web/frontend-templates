<form id="webdav_data_form" onsubmit="return false;" class="mg-b-20">
    <div class="row row-sm mg-t-10 align-items-start">
        <div class="col-lg-4">
            <label for="hostname" class="form-label"><span data-i18n="Webdav-URL">Webdav-URL:</span>
                </label>
            <input id="hostname" name="hostname" data-group="info" data-validate-as="input" type="text" class="form-control">
            <span role="alert" id="hostnameError" class="error" aria-hidden="true">
                <span data-i18n="Please specify the webdav-URL">Please specify the webdav-URL</span>
            </span>
        </div><!-- col -->
        <div class="col-lg-4 mg-t-10 mg-lg-t-0">
            <label for="login" class="form-label">
                <span data-i18n="Login">Login: </span>
            </label>
            <input id="login" name="login" data-group="info" data-validate-as="input" type="text" class="form-control">
            <span role="alert" id="loginError" class="error" aria-hidden="true">
                <span data-i18n="loginError">Enter your Login</span>
            </span>
        </div><!-- col -->
        <div class="col-lg-4 mg-t-10 mg-lg-t-0">
            <label for="data_password" class="form-label">
                <span data-i18n="Password">Password:</span>
            </label>
            <div class="password-combo">
                <input id="data_password" name="data_password" data-group="info" type="password" class="form-control">
                <span class="btn" id="pw-toggle-nextcloud"><i class="far fa-eye"></i></span>
            </div>
            <span role="alert" id="passwordError" class="error" aria-hidden="true">
                <span data-i18n="passwordError">Enter yourPassword</span>
            </span>
        </div><!-- col -->
    </div>
    <div class="row row-sm mg-t-20 align-items-end">
        <div class="col-lg-4">
        </div><!-- col -->
        <div class="col-lg-8 mg-t-10 mg-lg-t-0 tx-right">
            <button id="cancel_nextcloud_button" class="btn btn-outline-light text-nowrap">
                <i class="typcn typcn-times-outline tx-18 pr-1"></i> <span data-i18n="cancel">abbrechen</span>
            </button>
            <button id="save_webdav_button" class="btn btn-indigo text-nowrap ml-2"><i
                    class="typcn typcn-tick-outline tx-18 pr-1"></i> <span data-i18n="save">speichern</span></button>
        </div><!-- col -->
    </div>
</form><!-- form id wrapper -->