import {render} from "sass";

export class DtWizardScenario {
    name: string;
    steps: DtWizardStep[];

    constructor(name: string, steps: DtWizardStep[]) {
            this.name = name;
            this.steps = steps;
            return this;
    }

    addStep(step: DtWizardStep) {
        this.steps.push(step);
    }

    getStep(stepNumber: bigint) {
        return this.steps.filter((step) => {
            return step.step_number == stepNumber
        })[0]
    }
}

export class DtWizardStep {
    step_number: bigint;
    shortname: string;
    title: string;
    subtitle: string;
    bodytemplate: string;
    onload: (data: string) => any
    onleave: (data: string) => any
    renderData: object

    constructor(step_number:bigint, shortname:string, title: string, subtitle: string, bodytemplate: string, onload: (data: string) => any, onleave: (data: string) => any, renderData: object) {
            this.step_number = step_number;
            this.shortname = shortname;
            this.title = title;
            this.subtitle = subtitle;
            this.bodytemplate = bodytemplate;
            this.onload = onload;
            this.onleave = onleave;
            this.renderData = renderData;
            return this;
    }
}

