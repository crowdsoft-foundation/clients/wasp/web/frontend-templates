export type DtAnswerResponse = {
    id: string
    value: string | string[]
}

export class DtQuestion {
    id: string
    title: string
    text: string
    next: string
    previous: string
    hidden: boolean
    options: object
}



export type DtSelectOption = {
    id: string
    value: string
}

export type DtSelectAnswer = {
    id: string
    type: string & "select" | "multiselect"
    selectOptions: DtSelectOption[]
    text: string
    next: string
    previous: string
}

export type DtInput = {
    id: string
    placeholder: string
    nextOverride: string
}

export type DtInputAnswer = {
    id: string
    type: string & "input" | "multiinput"
    input: DtInput
    text: string
    next: string
    previous: string
}
