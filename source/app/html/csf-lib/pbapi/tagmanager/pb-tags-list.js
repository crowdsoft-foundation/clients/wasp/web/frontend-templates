import {PbTag} from "./pb-tag.js?v=[cs_version]"
import {fireEvent} from "../../pb-functions.js?v=[cs_version]"
import {PbApiRequest} from "../pb-api-request.js?v=[cs_version]"

export class PbTagsList {
    #list = []

    constructor(socket, onBackendUpdateFunc = () => "") {
        this.init_listeners()
        this.onBackendUpdateFunc = onBackendUpdateFunc
    }

    load(force_reload = false, usable_for = undefined) {
        let self = this
        self.#list = []
        if (self.#list.length == 0 || force_reload) {
            return new Promise((resolve, reject) => {
                new PbApiRequest(`/get_tag_list`).execute().then((response) => {
                    for (let tagEntry of response) {
                        if (!usable_for || (usable_for && tagEntry.usable_for.includes(usable_for))) {
                            let singleTag = new PbTag(tagEntry)
                            self.addTagToList(singleTag)
                        }
                    }
                    resolve(self)
                }).catch(() => {
                    reject()
                })
            })
        } else {
            return new Promise((resolve, reject) => {
                resolve(self)
            })
        }
    }

    getList() {
        return this.#list
    }

    getListUsableFor(for_value) {
        let usable_for_list = []
        for (let entry of this.#list) {
            if (entry.usable_for.includes(for_value)) {
                usable_for_list.push(entry)
            }
        }
        return usable_for_list
    }

    getSelectOptions(selectedIds = [], format = "dict") {
        let tag_options = []
        this.#list.sort(function (a, b) {
            var textA = a.short().toUpperCase();
            var textB = b.short().toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        })

        if (format == "dict") {
            for (let entry of this.#list) {
                let option = {
                    "id": entry.short(),
                    "name": entry.short(),
                    "selected": selectedIds.includes(entry.short()) ? "selected" : ""
                }
                tag_options.push(option)
            }
        }

        if (format == "array") {
            for (let entry of this.#list) {
                tag_options.push(entry.short())
            }
        }

        return tag_options
    }

    addTagToList(tag, updateApi = false) {
        for (let entry of this.#list) {
            if (entry.short() == tag.short()) {
                return this
            }
        }

        this.#list.push(tag)
        if (updateApi) {
            this.apiAddTags([tag])
        }
        return this
    }

    //
    // deleteTagFromList(tag, updateApi = true) {
    //     // del this.list[tag]
    //     fireEvent("pbTagsListChanged")
    //     if (updateApi) {
    //         this.apiDeleteTags([tag])
    //     }
    //     return this
    // }

    apiAddTags(tagsList = undefined, data_owners = []) {
        if (!tagsList) {
            tagsList = this.#list
        }
        let jsonData = []
        for (let tag of tagsList) {
            jsonData.push(tag.long())
        }

        let payload = {
            "data": jsonData,
            "data_owners": data_owners
        }

        return new PbApiRequest("/es/cmd/addTags", "POST", payload).execute().then(() => {

        })
    }

    apiDeleteTags(tagsList) {
        let data = []
        tagsList.map((x) => data.push(x.getData()))
        let payload = {
            "data": data
        }
        return new PbApiRequest("/es/cmd/deleteTags", "POST", payload).execute()
    }

    init_listeners() {
        let self = this
        document.addEventListener("command", function _(e) {
            if (e.data && e.data.data && e.data.data.command == "newTags") {
                fireEvent("pbTagsListChanged", e.data.data)
                self.load(true).then(() => {
                    self.onBackendUpdateFunc(e.data.data)
                })
            }
            if (e.data && e.data.data && e.data.data.command == "deletedTags") {
                fireEvent("pbTagsListChanged", e.data.data)
                self.load(true).then(() => {
                    self.onBackendUpdateFunc(e.data.data)
                })
            }
        })
    }
}