import {PbTagData} from "./pb-tag-data.js"

export class PbTag {
    #data // private
    constructor(data = undefined) {
        if(data) {
            this.#data = new PbTagData(data.name, data.value, data.create_time, data.creator, data?.usable_for, data.data_owners)
        }
    }
    getData() {
        return this.#data
    }

    getDataOwners() {
        return this.#data.data_owners
    }

    short() {
        if(this.#data.value?.length > 0 && this.#data.value != this.#data.name) {
            return `${this.#data.name}:${this.#data.value}`
        } else {
            return this.#data.name
        }
    }

    setUsableFor(list) {
        this.#data.usable_for = list
        return this
    }

    getUsableFor() {
        return this.#data.usable_for
    }

    isUsableFor(searchValue) {
        return this.#data.usable_for.includes(searchValue)
    }

    long() {
        return {"name": this.#data.name, "value": this.#data.value, "usable_for": this.#data.usable_for}
    }

    fromShort(short) {
        let parts = String(short).split(":")
        if(parts.length > 1) {
            this.#data = new PbTagData(parts[0], parts[1])
        } else {
            this.#data = new PbTagData(parts[0], parts[0])
        }
        return this
    }
}
