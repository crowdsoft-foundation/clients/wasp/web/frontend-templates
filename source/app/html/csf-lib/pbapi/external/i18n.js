export class i18n {
    constructor(config, apikey) {
        if (!i18n.instance) {
            i18n.instance = this
            i18n.instance.apikey = apikey
            i18n.instance.config = config
            i18n.instance.i18n = {}
            document.addEventListener("i18nLanguageLoaded", function _(event) {
                i18n.instance.replaceI18n()
            })
        }
        return i18n.instance;
    }

    async init() {
        await i18n.instance.getTranslations()
        return i18n.instance
    }

    translate(vocab) {
        if (i18n.instance.i18n[vocab]) {
            return i18n.instance.i18n[vocab]
        } else {
            //console.warn("Not found translation for vocab: ", vocab)
            return vocab
        }
    }

    getAvailableBrowserLanguage(available_languages = i18n.instance.config.AVAILABLE_LANGUAGES) {
        let lang = navigator.languages ? navigator.languages[0] : navigator.language;
        let ISOA2 = lang.substr(0, 2);

        if (available_languages.includes(ISOA2)) {
            return ISOA2
        } else {
            return "de"
        }
    }

    async getTranslations(language = null) {
        if (language == null || language == undefined) {
            language = i18n.instance.getAvailableBrowserLanguage()
        }

        let coreurl = language + ".json?v=[cs_version]"
        if (i18n.instance.config.LANGUAGEFILES_FOLDER) {
            coreurl = i18n.instance.config.LANGUAGEFILES_FOLDER + coreurl
        }
        let promises = []
        await i18n.instance.loadLanguageFile(coreurl, i18n.instance.setI18n, true)
    }

    async setI18n(response) {
        let promise = new Promise(function (resolve, reject) {
            try {
                let data = JSON.parse(response)
                i18n.instance.i18n = [i18n.instance.i18n, data].reduce(function (r, o) {
                    Object.keys(o).forEach(function (k) {
                        r[k] = o[k];
                    });
                    return r;
                }, {});
                resolve()
            } catch (err) {
                resolve()
            }
        })
        return promise
    }

    loadLanguageFile =  async function (url, callback, ignoreErrors) {
        let promise = new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.withCredentials = true;
            xhr.ignoreErrors = ignoreErrors

            xhr.addEventListener("readystatechange", function () {
                if (this.readyState === 4) {
                    if (this.status == 200 || this.status == 204 || ignoreErrors) {
                        callback(this.responseText);
                        resolve()
                    } else {
                        resolve()
                        //alert("Beim senden der Anfrage ist etwas schief gelaufen. Bitte prüfen Sie ob der Vorgang erfolgreich war und versuchen Sie es ggf. erneut.");
                    }
                }
            });
            if (!url.includes("_undefined")) {
                xhr.open("GET", url);
                xhr.setRequestHeader("apikey", i18n.instance.apikey);
                xhr.send();
            } else {
                resolve()
            }

        })
        await promise
    }

    replaceI18n = function () {
        for (let vocab of Object.keys(i18n.instance.i18n)) {
            try {
                if (vocab.startsWith("#") || vocab.startsWith(".")) {
                    $(vocab).html(i18n.instance.translate(vocab))
                }
            } catch (err) {

            }
        }

        let data_attr_selector = "*[data-i18n]"
        $(data_attr_selector).each((i, e) => {
            $(e).html(i18n.instance.translate($(e).data("i18n")))
        })

        data_attr_selector = "*[placeholder]"
        $(data_attr_selector).each((i, e) => {
            $(e).attr("placeholder", i18n.instance.translate($(e).attr("placeholder")))
        })

        data_attr_selector = "*[data-original-title]"
        $(data_attr_selector).each((i, e) => {
            $(e).attr("data-original-title", i18n.instance.translate($(e).attr("data-original-title")))
        })

        data_attr_selector = "*[data-hint]"
        $(data_attr_selector).each((i, e) => {
            $(e).attr("data-hint", i18n.instance.translate($(e).attr("data-hint")))
        })

        data_attr_selector = "*[title]"
        $(data_attr_selector).each((i, e) => {
            $(e).attr("title", i18n.instance.translate($(e).attr("title")))
        })
    }
}
