import {i18n} from "./i18n.js?v=[cs_version]"
import "/csf-lib/vendor/mozilla/nunjucks.min.js?v=[cs_version]"

export class Tpl {
    constructor(config, nunjuckOptions = undefined) {
        this.config = config
        if (!nunjuckOptions) {
            nunjuckOptions = {autoescape: true, web: {useCache: true, async: true}}
        }
        if (!Tpl.instance) {
            this.i18n = new i18n(this.config)
            Tpl.instance = this
            Tpl.instance.nunjuckOptions = nunjuckOptions

        }
        return Tpl.instance
    }

    translate(vocab) {
        return  this.i18n.translate(vocab)
    }

    async init() {
        await this.i18n.getTranslations()
        this.nunjucks = nunjucks.configure(this.config.TEMPLATES_BASE_URL, this.nunjuckOptions)

        this.nunjucks.addFilter('date', (data) => {
            let createtime = moment(data)
            return createtime.format(this.config.DATETIMEFORMAT)
        })

        this.nunjucks.addFilter('is_array', function (obj) {
            return Array.isArray(obj);
        });

        this.nunjucks.addFilter('datenotime', (data) => {
            let createtime = moment(data)
            return createtime.format(this.config.DATEFORMAT)
        })

        this.nunjucks.addFilter('time', (data) => {
            let createtime = moment(data)
            return createtime.format(this.config.TIMEDISPLAYFORMAT)
        })

        this.nunjucks.addFilter('weekday', (data) => {
            let createtime = moment(data)
            return createtime.format(this.config.DATEFORMAT_WEEKDAY)
        })
        return this
    }

    renderAsync(tpl, params, callback = () => "") {
        this.nunjucks.render(tpl, params, callback)
    }

    render(tpl, params) {
        let data = this.nunjucks.render(tpl, params)
        return data
    }

    renderIntoAsync(tpl, params, targetSelector, behaviour = "overwrite") {
        return new Promise(function (resolve, reject) {
            this.renderAsync(tpl, params, function (err, body) {
                let data = body
                if (behaviour == "append") {
                    data = $(targetSelector).append(data)
                }
                if (behaviour == "prepend") {
                    data = $(targetSelector).prepend($(data))
                }
                if(behaviour == "overwrite") {
                    $(targetSelector).html(data)
                }

                this.i18n.replaceI18n()
                resolve()
            }.bind(this))
        }.bind(this))
    }
}