import {CONFIG} from "./config.js?v=[cs_version]"
import {Tpl} from "./tpl.js?v=[cs_version]"


export class Pbs {
    constructor(options) {
        this.options = options
        this.apikey = this.options?.apikey
        this.target_selector = this.options?.target_selector
        this.tpl = new Tpl(CONFIG)
        if (!this.apikey) {
            throw new Error('Apikey is not set.');
        }
        if (!this.apikey) {
            throw new Error('Target-selector is not set.');
        }

        this.init()
    }

    async init() {
        if (!window.jQuery) {
            await import("./jquery-3.6.0.js?v=[cs_version]")
        }
        await import("./jquery-ui.js?v=[cs_version]")
        await import("/style-global/lib/datatables.net/js/jquery.dataTables.min.js?v=[cs_version]")

        $.datepicker.regional['de'] = {
            clearText: 'löschen', clearStatus: 'aktuelles Datum löschen',
            closeText: 'schließen', closeStatus: 'ohne Änderungen schließen',
            prevText: '<zurück', prevStatus: 'letzten Monat zeigen',
            nextText: 'Vor>', nextStatus: 'nächsten Monat zeigen',
            currentText: 'heute', currentStatus: '',
            monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
                'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
            monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun',
                'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
            monthStatus: 'anderen Monat anzeigen', yearStatus: 'anderes Jahr anzeigen',
            weekHeader: 'Wo', weekStatus: 'Woche des Monats',
            dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
            dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
            dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
            dayStatus: 'Setze DD als ersten Wochentag', dateStatus: 'Wähle D, M d',
            dateFormat: 'dd.mm.yy', firstDay: 1,
            initStatus: 'Wähle ein Datum', isRTL: false
        };
        $.datepicker.setDefaults($.datepicker.regional['de']);

        let css = ["http://serviceblick.com/style-global/lib/datatables.net-dt/css/jquery.dataTables.min.css?v=[cs_version]",
            "http://serviceblick.com/style-global/lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css?v=[cs_version]",
            "http://serviceblick.com/csf-lib/pbapi/external/css/jquery-ui.css"
        ]

        css.push("http://serviceblick.com/csf-lib/pbapi/external/css/form.css")

        for (let l of css) {
            const link = document.createElement('link');
            link.rel = 'stylesheet';
            link.href = l;
            document.head.appendChild(link);
        }
        await this.tpl.init()

        $(this.target_selector).html(this.tpl.i18n.translate("Loading..."))

        if (this.options.module && this[this.options.module]) {
            this[this.options.module]()
        }
    }

    guest_booking() {
        import("./booking.js?v=[cs_version]").then((function (m) {
            new m.Booking(CONFIG.API_BASE_URL, this.apikey, this.tpl, this.options?.target_selector).renderTable()
        }.bind(this)))
    }
}
