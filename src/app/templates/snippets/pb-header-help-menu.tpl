                <!-- snippet start header-help-menu -->
                <div {% if help_icon_permission %} data-permission="{{help_icon_permission}}" {% endif %} class="az-header-notification mg-l-10 {% if help_icon_permission %}hidden{% endif %}" data-hint="Start a tour at anytime from the help menu." data-hint-position="middle-top">
                    <a href="#" data-toggle="modal" data-target="#modal_help" id="help">
                        <i class="icon ion-ios-help-circle-outline" data-toggle="tooltip" title="" data-original-title="Help" ></i></a>
                </div>

