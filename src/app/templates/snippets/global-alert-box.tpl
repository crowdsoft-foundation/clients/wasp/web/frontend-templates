<div class="alert alert-info" style="margin-top: 50px;">
    <div class="card-body" id="info" >
        <h5><i class="la la-exclamation-circle" data-toggle="tooltip" title="" data-original-title="Hinweis" data-i18n="{{prio}}"></i>:</h5>
        <p data-i18n="{{id}} info alert"></p>
        <strong data-i18n="Example"></strong>: <p><span data-i18n="{{id}} info example"></span></p>
    </div>
</div>

