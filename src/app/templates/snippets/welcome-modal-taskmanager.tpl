<!-- snippet start welcome-dialog -->

<div data-permission="frontend.taskmanager" class="alert alert-secondary hidden" role="alert" id="taskmanager_welcome" style="display:none; border: 1px solid #cdd4e0;
border-radius: 4px;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close" id="taskmanager_welcome-close">
        <span aria-hidden="true">×</span>
      </button>
    <div class="pl-0 pt-0 ">
        <div class="d-block align-items-center d-sm-flex">
            <div class="p-2 text-center">
                <div class="large_icon" style="color: #7275d2; font-size: 60px; line-height: 80px; margin: 0 10px;">
                    <i class="fa fa-rocket"></i>
                </div>
            </div>
            <div class="p-2 text-center text-sm-left">
                <h5 class="page-title">Erste Schritte mit Aufgabenmanager</h5>
                <p> Möchten Sie eine Tour durch die Hauptfunktionen Ihres Task-Managers machen? Touren stehen Ihnen jederzeit über das Hilfe-Symbol in der Kopfzeile zur Verfügung.</p> 
                <a href="#" id="button_dismiss">Schließen und nicht mehr anzeigen.</a>
            </div>
            <div class="ml-auto p-2  text-center" style="white-space: nowrap;">
                <button id="tour_get_started" class="btn btn-outline-indigo"> Tour starten <i class="typcn icon typcn-chevron-right-outline"></i></button>
            </div>
        </div>
        <!-- <div class="text-center"><a href="#" id="button_dismiss">Schließen und nicht mehr anzeigen.</a></div> -->
    </div>
</div>

<!-- <div class="modal-box modal fade" id="taskmanager_welcome" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">×</span></button>
            <div class="modal-body">
                <h3 class="title">Get started with TaskManager</h3>
                <p> Would you like to take a tour of the main features of your Task Manager? Select a path to begin.</p>
                <div class="d-block d-flex-sm justify-content-center">
                    <a href="#" id="tour_get_started" class="box-content">
                        <div class="icon">
                            <i class="fa fa-rocket"></i>
                        </div>
                        <h4 class="title">Quick Start Tour</h4>
                        <p class="description">A quick start for using Taskmanager for the first time.</p>
                    </a>
                    <a href="#" id="tour_manage_tasks" class="box-content">
                        <div class="icon">
                            <i class="fa fa-user"></i>
                        </div>
                        <h4 class="title">Create and maintain tasks</h4>
                        <p class="description">Learn about your user settings.</p>
                    </a>
                    <a href="#" id="tour_manage_lists" class="box-content">
                        <div class="icon">
                            <i class="fa fa-book"></i>
                        </div>
                        <h4 class="title">Create and maintain lists</h4>
                        <p class="description">Learn about your Serviceblick account.</p>
                    </a>

                </div>
                <button id="button_dismiss" type="button" data-dismiss="modal" class="btn btn-indigo">Close and don't show again</button>
                <p>Your tours are always available from the help icon in the header </p>
            </div>

        </div>
    </div>
</div> -->
<!-- snippet end  -->