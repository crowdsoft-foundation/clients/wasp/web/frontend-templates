                <div class="az-header-notification mg-l-4">
                    <a href="{{url}}" data-toggle="tooltip" title=""
                       data-original-title="{{tooltip}}" data-permission='{{permission}}' class="hidden">
                        <i class="la la-cog tx-24 tx-gray-900"></i>
                    </a>
                </div>