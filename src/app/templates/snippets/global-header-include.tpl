<!-- snippet start global-header-include -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="google" value="notranslate">
<meta name="description" content="{{meta_description}}">
{% include 'snippets/tracking/matomo.tpl' %}
<link rel="stylesheet" href="/style-global/lib/fontawesome-free/css/all.min.css?v=[cs_version]">
<link rel="stylesheet" href="/style-global/lib/ionicons/css/ionicons.min.css?v=[cs_version]">
<link rel="stylesheet" href="/style-global/lib/typicons.font/typicons.css?v=[cs_version]">

{% if select2 %}
    <link rel="stylesheet" href="/style-global/lib/select2/css/select2.min.css?v=[cs_version]">
{% endif %}
{% if semantic %}
    <link rel="stylesheet" href="/style-global/lib/semantic-ui-css/semantic.min.css?v=[cs_version]">
{% endif %}

<link rel="stylesheet" href="/style-global/lib/fnon-notification/fnon.min.css?v=[cs_version]">
<link rel="stylesheet" href="/style-global/lib/introjs/introjs.min.css?v=[cs_version]">

{% if quill %}
<link rel="stylesheet" href="/style-global/lib/quill/quill.snow.css?v=[cs_version]">
{% endif %}

{% if datatables %}
<link rel="stylesheet" href="/style-global/lib/datatables.net-dt/css/jquery.dataTables.min.css?v=[cs_version]" >
<link rel="stylesheet" href="/style-global/lib/datatables.net-dt/css/buttons.dataTables.min.css?v=[cs_version]" >
{% endif %}

<link rel="stylesheet" href="/style-global/css/azia-global.min.css?v=[cs_version]">
<link rel="stylesheet" href="/style-global/css/serviceblick-global.css?v=[cs_version]">

{% if custom_css %}
<link rel="stylesheet" href="{{custom_css}}?v=[cs_version]">
{% endif %}
{% if title %}
<title data-i18n="{{title}}">{{title}}</title>
{% endif %}
<!-- snippet end  -->