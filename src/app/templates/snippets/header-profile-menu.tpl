<!-- snippet start header-profile-menu -->
<link href="/style-global/lib/fnon-notification/fnon.min.css?v=[cs_version]" rel="stylesheet">

<div class="dropdown az-profile-menu" id="header_profile_menu">
    <a href="" class="az-img-user" data-toggle="tooltip" title="" data-original-title="My User Account">
        <div class="az-avatar-no-status avatar-sm" id="nav_dropdown_avatar_menu"><i class="fas fa-user fa-1x"></i><!-- initials generated here on page load --></div>
    </a>
    <div class="dropdown-menu">
        <div class="az-dropdown-header d-sm-none">
            <a href="" class="az-header-arrow"><i class="icon ion-md-arrow-back"></i></a>
        </div>
        <div class="az-header-profile px-0">
            <div class="az-img-user">
                <div class="az-avatar-no-status avatar-xl" id="nav_dropdown_avatar"><i class="fas fa-user fa-1x"></i><!-- initials generated here on page load --></div>
            </div><!-- az-img-user -->
            <h6 class="force-wrap" id="nav_dropdown_name">Profilname <a data-permission="system.user.own.profle.management" href="/dashboard/profile-settings.html">setzen</a><!-- profile name generated here on page load -->
            </h6>
            <p id="account_name" class="az-profile-name-text">Benutzerkonto</p>
        </div><!-- az-header-profile -->
        <a data-permission="frontend.timetracker" href="/timetracker/" class="dropdown-item">
            <i class="typcn typcn-time"></i>Zeiterfassung
        </a>
        <a data-permission="system.user.own.profile" href="/dashboard/profile-settings.html" class="dropdown-item">
            <i class="typcn typcn-cog-outline"></i>Profil verwalten
        </a>
        <a data-permission="dashboard.management.change_own_password" href="/dashboard/profile-password.html"  class="dropdown-item">
            <i class="typcn typcn-edit"></i>Passwort ändern
        </a>
        <a data-permission="dashboard.management.usermanagement" href="/dashboard/account.html" class="dropdown-item">
            <i class="typcn typcn-book"></i>Konto verwalten
        </a>
        <a href="#" id="logout_button" class="dropdown-item logout_button">
            <i class="typcn typcn-power-outline"></i>Abmelden
        </a>
    </div><!-- dropdown-menu -->
</div>


<script src="/style-global/lib/fnon-notification/fnon.min.js?v=[cs_version]"></script>

<script defer type="module">
    import {PbLogin} from '/csf-lib/pb-login.js?v=[cs_version]';
    import {getCookie} from "/csf-lib/pb-functions.js?v=[cs_version]";
    new PbLogin().init(getCookie("username")).then(function(login) {
        if (login.profileData.firstname && login.profileData.firstname != undefined) {
            let initials = login.profileData.firstname.charAt(0) + login.profileData.lastname.charAt(0)
            $('#nav_dropdown_avatar_menu').html(initials)
            $('#nav_dropdown_avatar').html(initials)
            $('#nav_dropdown_name').html(login.profileData.firstname + " " + login.profileData.lastname)

        }
    })
</script>
<!-- snippet end-->