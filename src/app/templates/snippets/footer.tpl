<!-- snippet start footer.tpl -->
<div class="az-footer">
    <div class="container-fluid row py-3 ht-100p">
        <div class="col-sm-6 text-center">
            <h6>Quick Links</h6>

                <a class="tx-dark" href="https://www.planblick.com" target="_blank">planBLICK</a><br>
                <a class="tx-dark" href="https://www.crowdsoft.net" target="_blank">CROWDSOFT</a>

        </div>
        <div class="col-sm-6 text-center">
            <h6>Legal</h6>

                <a class="tx-dark" href="https://impressum.planblick.com" target="_blank">Impressum</a><br>
                <a class="tx-dark" href="https://datenschutz.planblick.com" target="_blank">Datenschutz</a>

        </div>
    </div>
    {% include 'snippets/CopyRight.tpl' %}
</div>