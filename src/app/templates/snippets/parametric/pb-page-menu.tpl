<div id="dialogNav">
    <div class="d-md-flex d-block justify-content-between align-items-center container pd-t-20 w-100">
        <div class="d-flex mr-md-auto mr-0">
            {% if title %}
            <h6 class="tx-16" data-i18n="{{ title }}">{{ title }}</h6>
            {% endif %}
        </div>
        <div class="d-flex align-items-center justify-content-end">
            <!-- right -->
            {% if show_search_input %}
                <div class="listview-search flex-fill" tabindex="100" id="search">
                    <input id="header_search" type="search" class="form-control mb-0 mb-md-2" placeholder="{{search_placeholer|safe}}">
                    <button class="btn"><i class="fas fa-search"></i></button>
                </div>
            {% endif %}
            
            <div id="event_form_action_buttons" class="d-flex">
                <input type="hidden" id="event_id" name="event_id" /><input type="hidden" id="allday" name="allday" />
                {% if button_1_text %}
                <button class="btn {{button_1_type}} btn-with-icon no-wrap ml-2 mb-0 mb-md-2 {% if not button_1_display %}hidden{% endif %}" type="submit" id="page_menu_button_1">
                    <span>{{button_1_icon}}</span>
                    <span data-i18n="{{button_1_text|safe}}" class="d-none d-md-block">{{button_1_text|safe}}</span>
                </button>
                {% endif %}
                {% if button_2_text %}
                <button class="btn {{button_2_type}} btn-with-icon no-wrap ml-2 mb-0 mb-md-2 {% if not button_2_display %}hidden{% endif %}" type="submit" id="page_menu_button_2">
                    <span>{{button_2_icon}}</span>
                    <span data-i18n="{{button_2_text|safe}}" class="d-none d-md-block">{{button_2_text|safe}}</span>
                </button>
                {% endif %}
                {% if button_3_text %}
                <button class="btn {{button_3_type}} btn-with-icon no-wrap ml-2 mb-0 mb-md-2 {% if not button_3_display %}hidden{% endif %}" type="submit" id="page_menu_button_3">
                    <span>{{button_3_icon}}</span>
                    <span data-i18n="{{button_3_text|safe}}" class="d-none d-md-block">{{button_3_text|safe}}</span>
                </button>
                {% endif %}
                {% if button_4_text %}
                <button class="btn {{button_4_type}} btn-with-icon no-wrap ml-2 mb-0 mb-md-2 {% if not button_4_display %}hidden{% endif %}" type="submit" id="page_menu_button_4">
                    <span>{{button_4_icon}}</span>
                    <span data-i18n="{{button_4_text|safe}}" class="d-none d-md-block">{{button_4_text|safe}}</span>
                </button>
                {% endif %}
            </div>
        </div>
    </div>
</div>