<!-- snippet start content-left-easy2schedule-settings-nav -->
<div class="component-item">
<label data-i18n="Settings">Einstellungen</label>
    <label class="hidden" data-permission='easy2schedule.ressources' data-i18n="General">Allgemeine</label>
    <nav data-permission='easy2schedule.ressources' class="nav flex-column" id="use-activate-menu">
        <a data-permission='easy2schedule.ressources' href="./resource-settings.html" class="nav-link hidden" data-i18n="Calendar Resources">Kalenderressourcen</a>
        <a data-permission='easy2schedule.columns' href="./column-settings.html" class="nav-link hidden" data-i18n="Calender Columns">Kalenderspalten</a>
    </nav>
    <label class="hidden" data-permission='easy2schedule.types' data-i18n="Appointment Entry Form">TerminDialog
    </label>
    <nav data-permission='easy2schedule.types' class="nav flex-column" id="use-activate-menu">
        <a data-permission='easy2schedule.types' href="./field-settings.html" class="nav-link hidden" data-i18n="Appointmenttypes">Appointmenttypes</a>
    </nav>

    <nav data-permission='easy2schedule.type_status' class="nav flex-column" id="use-activate-menu">
        <a data-permission='easy2schedule.type_status' href="./status-settings.html" class="nav-link hidden" data-i18n="Appointmentstatus">Appointmentstatus</a>
    </nav>
    <label data-permission='easy2schedule.user.settings' class="hidden" data-i18n="My Calendar">Für meine Kalender</label>
    <nav class="nav flex-column" id="use-activate-menu">
        <a data-permission='easy2schedule.user.settings' href="./user-settings.html" class="nav-link hidden" data-i18n="Messaging">Benachrichtigungen</a>
    </nav>
</div><!-- component-item -->
<!-- snippet end  -->

