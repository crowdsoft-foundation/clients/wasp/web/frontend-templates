<!-- snippet start help-dialog -->
<div id="modal_help" class="modal fixed-right fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-aside" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hilfe-Center</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Hilfe und Touren</h5>
                <p data-i18n="quickstart help subtitle">Schnellstarthilfe für jeden Bereich Ihres Serviceblick-Kontos und Ihrer Anwendung.</p>

                <ul id="column_element_list" class="column-element-list pl-0">
                    <li class="column-element tickets-card row hidden" data-permission='dashboard'>
                        <div class="col-1 pd-0">
                            <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                        </div>
                        <div class="col-11">
                            <a id="help_tour_get_started" href="#" class="tx-dark-blue" >
                                <span data-i18n="welcome in your dashboard">Willkommen in Ihrem Dashboard</span>
                                <br>
                                <span class="tx-12 tx-gray-700" data-i18n="help_new_here">
                                    Sind Sie neu hier? Dies ist ein guter Ausgangspunkt.
                                </span>
                            </a>
                        </div>
                    </li>
                    <!-- <li class="column-element tickets-card row hidden" data-permission='dashboard'>
                        <div class="col-1 pd-0">
                            <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                        </div>
                        <div class="col-11">
                            <a id="" href="#" class="tx-dark-blue">
                                Profile Management
                                <br>
                                <span class="tx-12 tx-gray-700">Learn about your user settings.</span>
                            </a>
                        </div>
                    </li>
                    <li class="column-element tickets-card row hidden" data-permission='dashboard'>
                        <div class="col-1 pd-0">
                            <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                        </div>
                        <div class="col-11">
                            <a id="" href="#" class="tx-dark-blue">
                                Accout Management
                                <br>
                                <span class="tx-12 tx-gray-700">Learn about your Serviceblick account.</span>
                            </a>
                        </div>
                    </li>
                    <li class="column-element tickets-card row hidden" data-permission='dashboard'>
                        <div class="col-1 pd-0">
                            <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
                        </div>
                        <div class="col-11">
                            <a id="simplycollect_customers_view_link" href="../simplycollect/customer.html?mid=demo" class="tx-dark-blue">
                                User Management
                                <br>
                                <span class="tx-12 tx-gray-700">Learn about your account users.</span>
                            </a>
                        </div>
                    </li> -->
                </ul>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- snippet end  -->