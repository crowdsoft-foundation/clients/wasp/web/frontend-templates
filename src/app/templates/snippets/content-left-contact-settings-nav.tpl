<!-- snippet start content-left-contact-nav -->
<div class="component-item">

    <label data-permission='frontend.contactmanager.views.settings'>Einstellungen</label>
    <nav data-permission='frontend.contactmanager.views.settings' class="nav flex-column" id="use-activate-menu">
        <a data-permission='frontend.contactmanager.settings.list_order' href="./field-settings.html" class="nav-link" >Eingabefelder</a>
        <a data-permission='frontend.contactmanager.settings.entries' href="./list-settings.html" class="nav-link" >Listenansicht</a>
        <a data-permission='frontend.contactmanager.settings.guest' href="./guest-settings.html" class="nav-link" >Gast-Zugriff</a>
    </nav>
   
</div><!-- component-item -->
<!-- snippet end  -->

