<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>{{title}}</title>

<link rel="stylesheet" href="/style-global/lib/fontawesome-free/css/all.min.css?v=[cs_version]">
<link rel="stylesheet" href="/style-global/lib/ionicons/css/ionicons.min.css?v=[cs_version]">
<link rel="stylesheet" href="/style-global/lib/typicons.font/typicons.css?v=[cs_version]">

<link rel="stylesheet" href="/style-global/lib/select2/css/select2.min.css?v=[cs_version]" >
<link rel="stylesheet" href="/style-global/lib/semantic-ui-css/semantic.min.css?v=[cs_version]">

<link href="/style-global/lib/fnon-notification/fnon.min.css?v=[cs_version]" rel="stylesheet">
<link href="/style-global/lib/introjs/introjs.min.css?v=[cs_version]" rel="stylesheet">
<link rel="stylesheet" href="/style-global/lib/quill/quill.snow.css?v=[cs_version]">

<link rel="stylesheet" href="/style-global/css/azia-vendor.css?v=[cs_version]">
<link rel="stylesheet" href="/style-global/css/azia-global.min.css?v=[cs_version]">
<link rel="stylesheet" href="/style-global/css/serviceblick-global.css?v=[cs_version]">
