<!-- snippet start alert-countdown -->
<div data-permission="beta_access">
<div class="alert alert-thin" role="alert" id="counter_demo" style="background-image: linear-gradient(90deg,#5c5cff,#8960ff); border-radius: 4px; display: none;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close" id="dismiss_countdown">
        <span aria-hidden="true">×</span>
      </button>
    <div class="pl-0 pt-0 ">
        <div class="d-block align-items-center d-sm-flex">
            <div class="p-0 text-center" style="white-space: nowrap; display: none;">
                <div>
                    <span data-countdown="count-pos-1" class="countdown-badge">1</span>
                    <span data-countdown="count-pos-2" class="countdown-badge">0</span>
                </div>
                <span  data-i18n="TAGE NOCH" data-countdown="count-day" class="countdown label tx-white">DAYS TO GO</span>
            </div>
            <div class="p-2 text-center text-sm-left">
                <h5 data-i18n="" class="tx-white">Hast du schon vieles ausprobiert?</h5>
                <span data-i18n="Please add your billing information to continue using your access after 30 days." class="tx-white"></span>
            </div>
            <div class="ml-auto p-2  text-center" style="white-space: nowrap;">
                <div class="col-11" id="firstStepsUser">
                    <a href="/dashboard/account-address.html" class="btn btn-outline-indigo-white" data-i18n="Billing Address">
                    <i class="typcn icon typcn-chevron-right-outline"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- snippet end  -->