<div class="az-header">
    <div class="container"> <!-- change to container-fluid for full browser width-->
        {% include 'snippets/page-segments/header/left.tpl' %}
        {% include 'snippets/page-segments/header/right.tpl' %}
    </div> <!-- container -->
</div><!-- az-header -->