<!-- collaped view -->
<div id="rightActionMenu" class="content-right-action-menu-wrapper az-content-body az-content-body-contacts d-none">
    {% if nav_1_id %}
    <div class="text-right">
        <span class="btn {{nav_1_button_class}} slide-button" data-toggle="true" id="{{nav_1_id}}">
            <span class="slide-button-info">{{nav_1_text}}</span> {{nav_1_icon}}
        </span>
    </div>
    {% endif %}
</div>
<!-- expanded view -->
<div id="contentRight" class="content-right-content show">

</div>

