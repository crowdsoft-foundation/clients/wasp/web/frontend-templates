<div class="contacts-list-header border-bottom">
    <div class="mb-3 ml-2 ml-md-0 mr-2 d-flex">
            <button class="btn btn-indigo btn-block mb-2 mb-sm-0 " id="button-new-document">
                <i class="typcn typcn-plus-outline tx-16"></i><span data-i18n="new Document">New Document</span>
            </button>
    </div>
    <div class="mb-3 ml-2 ml-md-0 mr-2 d-flex">
        <div class="listview-search flex-fill" tabindex="100" id="search">
            <input id="documents_search_input" type="search" class="form-control" placeholder="Suche...">
            <button class="btn"><i class="fas fa-search"></i></button>
        </div>
        <!--
            <button type="button" class="btn btn-icons btn-light ml-2" id="button-toggle-filter" data-selector="off">
            <i class="typcn icon typcn-filter"></i>
        </button> -->
    </div>
    <div class="mb-3 ml-2 ml-md-0 mr-2 ">
        <span id="source_module_pill" class="badge badge-light ml-2"></span>
        <span id="source_id_pill" class="badge badge-light ml-2"></span>
    </div>
</div>
<div id="document_list" class="az-contacts-list">
</div>
