from flask import Flask, render_template, send_from_directory, send_file, redirect, request, jsonify
from urllib.parse import urlparse, urlunparse
import json
import os
import socket

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 300


@app.route("/")
def path_root(filepath=None):
    return redirect('/login/')


@app.route("/upload_public_json/<path:filename>", methods=["POST"])
def upload_json(filename):
    payload = request.get_json()
    resolved_host, _, _ = socket.gethostbyaddr(request.remote_addr)
    print("HOST: ", resolved_host)
    if "planblick.svc.cluster.local" in resolved_host or "localhost" in resolved_host:
        print(f'IP is writing JSON "{filename}.json": {request.remote_addr}')
        with open(f"/src/app/html/uploads/{filename}.json", "w") as f:
            f.write(json.dumps(payload))

    return jsonify(payload)


@app.route("/<path:filepath>")
def path_login(filepath=None):
    if ("." not in filepath and filepath[-1] != "/"):
        return redirect("/" + filepath + '/')

    url_params = dict(request.args)
    url_params["host"] = request.host
    if "." not in filepath:
        filepath = f"{filepath}/" if filepath[-1] != "/" else filepath
        filepath += "index.html"

    if os.path.isfile(f"./html/{filepath}"):
        return send_file(f"./html/{filepath}")
    elif os.path.isfile(f"./templates/{filepath}"):
        return render_template(f"/{filepath}", **url_params)

    filepath_guess = filepath.replace("index.html", "")

    if filepath_guess.find("index.html") == -1 and filepath_guess.find(".") == -1:
        max_trys = 7
        current_trys = 0
        while len(filepath_guess.split("/")) > 0 and current_trys <= max_trys:
            current_trys += 1
            filepath_guess = filepath_guess.split("/")[:-1]
            filepath_guess = "/".join(filepath_guess)
            if os.path.isfile(f"./templates/{filepath_guess}/index.html"):
                return render_template(f"/{filepath_guess}/index.html", **url_params)

    return f"not found: {filepath}"


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=8000)
