<h5>{{ tour_config.header }}</h5>
<p>{{ tour_config.intro }}</p>
<br>
<ul id="column_element_list" class="column-element-list pl-0">
    {% for tour_id, tour_data in tour_config.tours %}
     <li class="column-element tickets-card row" data-permission='{{ tour_data.permission }}'>
        <div class="col-1 pd-0">
            <i class="fa fa-shoe-prints tx-22 tx-gray-700"></i>
        </div>
        <div data-dismiss="modal" data-tour="{{ tour_id }}" class="col-11" id="{{ tour_id }}">
            <!--  -->
            <a href="javascript:void(0)" class="tx-dark-blue">
                <!-- tour name --><strong>{{ tour_data.title }}</strong></a>
            <br>
            <span class="tx-12 tx-gray-700">{{ tour_data.desc }}</span>
            </a>
        </div>
    </li>
    {% endfor %}
</ul>
