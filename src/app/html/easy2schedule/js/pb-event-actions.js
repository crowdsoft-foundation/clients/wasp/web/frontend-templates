import {PbBaseEventActions} from "./pbjs/pb-base-event-actions.js?v=[cs_version]"
import {
    speech,
    fireEvent,
    getCookie,
    enterFullscreen,
    showNotification,
    isEmail,
    checkPasswordStrength
} from "./pbjs/pb-functions.js?v=[cs_version]"
import {pbApp} from "./pb-app.js?v=[cs_version]";
import {PbAccount} from "./pbjs/pb-account.js?v=[cs_version]";
import {CONFIG} from "./pb-config.js?v=[cs_version]";
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"


export class PbEventActions extends PbBaseEventActions {
    constructor() {
        super();

        this.initActions()
        if (!PbEventActions.instance) {
            PbEventActions.instance = this;
            self = PbEventActions.instance
        }
        new pbApp()

        return PbEventActions.instance;
    }

    initActions() {
        this.actions["init"] = function (myevent, callback = () => "") {
            console.log("INIT")
            new pbI18n().init()
            callback(myevent)
        }
        this.actions["newContactData"] = function (myevent, callback = () => "") {
            callback(myevent)
        }
        this.actions["calendarRessourcesFetched"] = function (myevent, callback = () => "") {
            let ressources = myevent.data.sort(function (a, b) {
                if (a.display_name < b.display_name) {
                    return -1;
                }
                if (a.display_name > b.display_name) {
                    return 1;
                }
                return 0;
            })
            new pbApp().ressources = ressources
            for (let ressource of new pbApp().ressources) {
                if (ressource.type == "person") {
                    $('#todo_callback_assignee').append(new Option(ressource.display_name, ressource.id));
                    $('#todo_firstcontact_assignee').append(new Option(ressource.display_name, ressource.id));
                }
            }
            callback(myevent)
        }

        this.actions["newTaskCreated"] = function (myevent, callback = () => "") {
            new pbApp().addNewContact(myevent.data)
            callback(myevent)
        }
    }
}


