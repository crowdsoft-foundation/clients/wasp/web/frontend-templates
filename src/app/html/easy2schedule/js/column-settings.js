import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]"
import {getCookie, makeId} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"

export class ColumnSettings {
    columns = {}
    active_view = undefined
    active_view_columns = []
    views = []
    column_order = []
    
    constructor() {
        if (!ColumnSettings.instance) {
            ColumnSettings.instance = this
        }

        return ColumnSettings.instance
    }

    init() {
        return new Promise(function (resolve, reject) {
            ColumnSettings.instance.fetchColumnsFromAPI().then(function (res) {
                    let columns = res
                    ColumnSettings.instance.setColumns(columns)
                    ColumnSettings.instance.addTempIDs()
                    /*  ColumnSettings.instance.setViews()
                    console.log("SET VIEWS", ColumnSettings.instance.views) */
                    /* ColumnSettings.instance.removeTempIDs()
                    console.log("IDS removed", ColumnSettings.instance.columns) */
                    resolve(ColumnSettings.instance)
                })
                .catch(function (err) {
                    console.log("ERROR", err)
                    reject()
                }) // This is executed
        })
    }

    fetchColumnsFromAPI () {
        return new Promise (function(resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/cb/columns",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "error": reject,
                "success": (response) => resolve(response),
            };

            $.ajax(settings)
        })
    }

    saveUpdatedColumns () {
        return new Promise (function(resolve, reject) {
            let data = ColumnSettings.instance.columns
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateCalendarColumns",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "data": data
                }),
                "error" : reject,
                "success": response => resolve(response)
            };
            $.ajax(settings)
        })
    }

    setColumns(value) {
        ColumnSettings.instance.columns= value
    }

    getColumns() {
        return ColumnSettings.instance.columns
    }

    setActiveView(view) {
        ColumnSettings.instance.active_view = view
        ColumnSettings.instance.setActiveViewColumns()
    }

    setActiveViewColumns() {
        let view = ColumnSettings.instance.active_view
        let active_view_columns = ColumnSettings.instance.columns[view]
        ColumnSettings.instance.active_view_columns = active_view_columns
        console.log("Active view columns", ColumnSettings.instance.active_view_columns)
    }

    setViews() {
        let columns = ColumnSettings.instance.columns
        for (const[key, value] of Object.entries(columns)) {
            ColumnSettings.instance.views.push(key)
            
        }
        console.log("Set views", ColumnSettings.instance.views)
    }
    getViews() {
        return ColumnSettings.instance.views
    }

    addTempIDs() {
        let columns = ColumnSettings.instance.columns
        for (const [key, value] of Object.entries(columns)) {
            if(!value) {
                continue;
            }
            value.forEach(column => {
                column.temp_id = makeId(8)
            })
        }
        ColumnSettings.instance.columns = columns
    }
    
    removeTempIDs() {
        let columns = ColumnSettings.instance.columns
        for (const [key, value] of Object.entries(columns)){
            value.forEach(column => { 
            delete column.temp_id                   
        })
    }
        ColumnSettings.instance.columns = columns
    }
}