import {isEmail, handleGUIbyidError} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import { NotificationsFilter } from "./notifications-filter.js?v=[cs_version]"

export class RenderUserSettings {
    constructor(usersetting) {
        if (!RenderUserSettings.instance) {
            RenderUserSettings.instance = this
        }
        RenderUserSettings.instance.usersetting = usersetting
        RenderUserSettings.instance.nf = new NotificationsFilter()
        return RenderUserSettings.instance
    }

    init() {
        $("#opening").val(RenderUserSettings.instance.usersetting.getEmailConfigHeader())
        $("#closing").val(RenderUserSettings.instance.usersetting.getEmailConfigFooter())
        let emailString = ""
        if(RenderUserSettings.instance.usersetting.getEmailConfigRecipients()) {
            emailString = RenderUserSettings.instance.usersetting.getEmailConfigRecipients().join()
        }
        $("#recipient").val(emailString) 
        $("#filter-owned-events").prop("checked", RenderUserSettings.instance.nf.getFilterOwnedEvents())
        console.log("FILTER", RenderUserSettings.instance.nf.getReminderEmailFilter())
        $("#filter-assigned-events").prop("checked", RenderUserSettings.instance.nf.getFilterAssignedEvents())
        $("#filter-other-events").prop("checked", RenderUserSettings.instance.nf.getFilterOtherEvents())
    }

    updateRecipients(emails) {

        if (emails != ""){
            let emailList = emails.split(",")
            let checkResult = true

            for (let email of emailList) {
                if (isEmail(email) == false) {
                    checkResult = false
                    break
                }
            }
            if (checkResult == true) {
                handleGUIbyidError("recipient")
                RenderUserSettings.instance.usersetting.setEmailConfigRecipients(emailList)
                return true
            } else {
                handleGUIbyidError("recipient", false)
            }
        }  
        if (emails == ""){
            handleGUIbyidError("recipient")
            RenderUserSettings.instance.usersetting.setEmailConfigRecipients([])
            return true
        }
    }

    updateReminderEmailConfig(){
        
        if( $('[data-group="weekly"]').prop('disabled') == true){               
            $('#update_weekly_mailer_button').text("speichern")
            $('[data-group="weekly"]').prop('disabled', function (i, v) {
                return !v;
                }) 
        } else {

            if (RenderUserSettings.instance.updateRecipients($("#recipient").val()) == true){
                RenderUserSettings.instance.usersetting.setEmailConfigHeader($("#opening").val())
                RenderUserSettings.instance.usersetting.setEmailConfigFooter($("#closing").val())

                NotificationsFilter.instance.setFilterOwnedEvents($("#filter-owned-events").prop("checked"))
                NotificationsFilter.instance.setFilterAssignedEvents($("#filter-assigned-events").prop("checked"))
                NotificationsFilter.instance.setFilterOtherEvents($("#filter-other-events").prop("checked"))

                RenderUserSettings.instance.usersetting.saveEmailConfig()

                $('#update_weekly_mailer_button').text("bearbeiten")
                $('[data-group="weekly"]').prop('disabled', function (i, v) {
                    return !v;
                }) 

                $.blockUI({
                    blockMsgClass: 'alertBox',
                    message: 'Neue Einstellung wird gespeichert.'
                });
                setTimeout(function () {
                    $.unblockUI();
                }, 2000) 

            }     
        }

    }
   
}
