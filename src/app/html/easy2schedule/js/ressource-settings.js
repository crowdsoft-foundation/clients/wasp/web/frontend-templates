import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]"
import {getQueryParam, getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"

export class RessourceSettings  {
    ressources = []
    active_ressource = {}
    resource_toupdate = undefined

    constructor() {
        if (!RessourceSettings.instance) {
            RessourceSettings.instance = this
        }

        return RessourceSettings.instance
    }

    init() {
        return new Promise(function (resolve, reject) {
            var arr = [RessourceSettings.instance.fetchRessourcesFromApi()]

            Promise.all(arr)
                .then(function (res) {
                    let ressources = res
                    RessourceSettings.instance.setRessources(ressources)
                    resolve(RessourceSettings.instance)
                })
                .catch(function (err) {
                    console.log("ERROR", err)
                    reject()
                }) // This is executed
        })
    }

    fetchRessourcesFromApi () {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/cb/ressources",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "error": reject,
                "success": (response) => resolve(response),
            };

            $.ajax(settings)
        })
    }
    
    setRessources(value) {
        RessourceSettings.instance.ressources= value
    }
    
    getRessources() {
        return RessourceSettings.instance.ressources
    }

    setActiveRessource(value) {
        if (!value) {
            RessourceSettings.instance.active_ressource = undefined
        } else {
            for (let ressource of RessourceSettings.instance.ressources[0]) {
                if (ressource.ressource_id == value) {
                    RessourceSettings.instance.active_ressource = ressource
                    break
                }
            }
        }
        }
 
    
    getActiveRessource() {
        return RessourceSettings.instance.active_ressource
    }
   
    updateActiveRessource(value_displayname, value_contactid, value_backgroundcolor, value_fontcolor, value_loginid, value_ressourceid, value_type) {
        var payloadjsontmp ={
            "ressource_id": value_ressourceid,
            "display_name": value_displayname,
            "props": {
              "eventBackgroundColor": value_backgroundcolor,
              "eventFontColor": value_fontcolor,
              "login_id": value_loginid,
              "contact_id": value_contactid
            },
            "type": value_type
          }
          RessourceSettings.instance.active_ressource = payloadjsontmp
    }

    saveActiveRessource() {
        let payload = RessourceSettings.instance.active_ressource

        let url = ""
        if(!RessourceSettings.instance.active_ressource.ressource_id) {
            url = CONFIG.API_BASE_URL + "/es/cmd/createCalendarRessource"
        } else {
            url = CONFIG.API_BASE_URL + "/es/cmd/updateCalendarRessource"
        }
            
        return new Promise(function (resolve, reject) {
            var settings = {
                "url":  url,
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "error": reject,
                "success": (response) => resolve(response),
            };

            $.ajax(settings)
        })
    }

    deleteRessource(value) {
        return new Promise(function (resolve, reject) {
           let ressourceID_todelete = value
           
           var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/deleteCalendarRessource",
            "method": "POST",
            "timeout": 0,
            "headers": {
              "apikey": getCookie("apikey"),
              "Content-Type": "application/json"
            },
            "data": JSON.stringify({
              "ressource_id": ressourceID_todelete
            }),
            "error": reject,
            "success": (response) => resolve(response),
            };

            $.ajax(settings)
        })
    }
}