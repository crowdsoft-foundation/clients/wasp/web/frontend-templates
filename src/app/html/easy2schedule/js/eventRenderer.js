export class PbEventRenderer {
    static getRenderer(renderer) {
        switch (renderer) {
            case 'title_participants':
                return PbEventRenderer.title_participants()
            case 'participants_title':
                return PbEventRenderer.participants_title()
            default:
                return PbEventRenderer.participants_title()
        }
    }

    static title_participants(event) {
        return (event) => {
            let all_day_head = $(event.el).find(".fc-content")
            let day_head = $(event.el).find(".fc-time").find("span")
            let day_title = $(event.el).find(".fc-title")
            let participants = []
            if (event.event._def.extendedProps.attachedRessources != null) {
                for (let res of event.event._def.extendedProps.attachedRessources) {
                    res = JSON.parse(res)
                    participants.push(res.name)
                }
            }

            if (event.event._def.rendering == 'background') {
                event.el.append(event.event._def.title);
            } else {
                var br = document.createElement("br");
                if (event.event._def.allDay && event.event._def.allDay == true) {
                    all_day_head.html(all_day_head.html() + "<br>" + participants.join(", "))
                } else {
                    day_title.html(participants.join(", "))
                    day_head.html(day_head.html() + " " + event.event._def.title)
                }
            }
        }
    }

    static participants_title(event) {
        return (event) => {
            let all_day_head = $(event.el).find(".fc-content")
            let day_head = $(event.el).find(".fc-time").find("span")
            let day_title = $(event.el).find(".fc-title")
            let participants = []
            if (event.event._def.extendedProps.attachedRessources != null) {
                for (let res of event.event._def.extendedProps.attachedRessources) {
                    res = JSON.parse(res)
                    participants.push(res.name)
                }
            }

            if (event.event._def.rendering == 'background') {
                event.el.append(event.event._def.title);
            } else {
                var br = document.createElement("br");
                if (event.event._def.allDay && event.event._def.allDay == true) {
                    all_day_head.html(participants.join(", ") + "<br>" + all_day_head.html())
                } else {
                    day_head.html(day_head.html() + " " + participants.join(", "))
                    day_title.html(event.event._def.title)
                }
            }
        }
    }
}