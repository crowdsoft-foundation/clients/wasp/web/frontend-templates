import { ColumnSettings } from "./column-settings.js?v=[cs_version]"
import { PbAccount } from "../../csf-lib/pb-account.js?v=[cs_version]"
import { makeId } from '../../csf-lib/pb-functions.js?v=[cs_version]';
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"


export class RenderColumnSettings {

  constructor() {
    if (!RenderColumnSettings.instance) {
      RenderColumnSettings.instance = this
    }
    RenderColumnSettings.instance.cs = new ColumnSettings()

    return RenderColumnSettings.instance
  }


  init() {
    RenderColumnSettings.instance.initListeners();
    RenderColumnSettings.instance.cs.setViews()
    RenderColumnSettings.instance.renderViewDropdown()
    RenderColumnSettings.instance.getActiveView()
    RenderColumnSettings.instance.renderColumnsList()

  }
  
  renderViewDropdown() {
    $("#calendar_view_selection").html("")
    RenderColumnSettings.instance.cs.views.forEach( view => {
      if(
          (view == "resourceTimeGridMultiDay" && new PbAccount().hasPermission("easy2schedule.views.rooms_week")) ||
          (view == "resourceTimeGridMultiDay3" && new PbAccount().hasPermission("easy2schedule.views.categories_week")) ||
          (view == "resourceTimeGridDay" && new PbAccount().hasPermission("easy2schedule.views.rooms_day")) ||
          (view == "resourceTimeGridDay2" && new PbAccount().hasPermission("easy2schedule.views.categories_day"))
      ) {
        $('#calendar_view_selection').append($('<option>',
            {
              value: view,
              text: new pbI18n().translate(view)
            }))
      }
    })
    

  }

  renderColumnsList(forceReload = false) {
    $('#column_element_list').html("")
    let list = $('#column_element_list')   
    let list_entry = $('#column_item').html()
    let columns = RenderColumnSettings.instance.cs.active_view_columns
    console.log("Active view columns", columns)
    columns.forEach(column => {
        const new_entry = list_entry.replaceAll("[{temp_id}]", column.temp_id).replace("[{column-id-value}]", column.id).replace("[{column-label-value}]", column.title)
        list.append(new_entry)
        if(column.id.toLowerCase() == "others" || column.id.toLowerCase() == "sonstiges"){
          $(`#check_${column.temp_id}`).prop('checked', true)
          $(`#column_id_${column.temp_id}`).prop("readonly", true)
        } 
    })
  }

  getActiveView() {
    let selected_view = $('#calendar_view_selection option').filter(':selected').val()
    console.log("Selected View", selected_view)
    RenderColumnSettings.instance.cs.setActiveView(selected_view)
  }
  


  removeColumn(temp_id) {
    //buttonClick removes column
    RenderColumnSettings.instance.cs.active_view_columns.filter(
      (element) => element.temp_id != temp_id
    )
    let entry_to_delete = document.getElementById(`${temp_id}`)
    entry_to_delete.parentElement.removeChild(entry_to_delete)
  }

  updateColumn(temp_id) {
    //read values from input field and update column
    RenderColumnSettings.instance.cs.activeview_columns.forEach((element) => {
      if (element.temp_id === temp_id) {
        //update values
      }
    })
  }

  saveChanges() {
    Fnon.Box.Circle('#list-view','', {
      svgSize: { w: '50px', h: '50px' },
      svgColor: '#3a22fa',
  })
    let order = RenderColumnSettings.instance.cs.column_order
    order = Array.from(order)
    RenderColumnSettings.instance.cs.active_view_columns = []
    order.forEach(element => {
      let title = $(`#column_label_${element.id}`).val()
      let id = $(`#column_id_${element.id}`).val()
      RenderColumnSettings.instance.cs.active_view_columns.push({
          "id": id,
          "title": title
      })
    })
    console.log("Active view columns:", RenderColumnSettings.instance.cs.active_view_columns)
    if(RenderColumnSettings.instance.cs.active_view_columns && RenderColumnSettings.instance.cs.active_view_columns.length > 0){
      RenderColumnSettings.instance.cs.columns[RenderColumnSettings.instance.cs.active_view] = RenderColumnSettings.instance.cs.active_view_columns
      RenderColumnSettings.instance.cs.removeTempIDs()
      RenderColumnSettings.instance.cs.saveUpdatedColumns()
    }   
    

    Fnon.Hint.Success('Die Änderung wird in den Kalender übertragen.')
    Fnon.Box.Remove('#list-view', 1000)
  }

  editMode() {
    RenderColumnSettings.instance.cs.active_view_columns.forEach(element => 
      RenderColumnSettings.instance.cs.column_order.push({"id": element.temp_id})
    )
    $("#column_element_list").sortable({
      handle: ".handle",
      items: ".column-element",
      update: function (evt) {
        let order = $("#column_element_list").sortable("serialize")
        RenderColumnSettings.instance.cs.column_order= order
        console.log("Order updated", RenderColumnSettings.instance.cs.column_order)
      },
    })
    //show me
    $('[data-attr="movable"]').removeClass("hidden")
    $(".handle").removeClass("hidden")
    $(".handle").addClass("cursor-grab")
    $("#save_columns_button").removeClass("hidden")
    $("#cancel_columns_button").removeClass("hidden")
    $('[data-selector="btn-column-delete"]').removeClass("hidden")
    $(".detail-inputs").attr("disabled", false)
    //hide me
    $("#edit_columns_button").addClass("hidden")
    $("#calendar_view_selection").attr("disabled", true)
  }

  viewMode() {
    $(".sortable").each(function () {
      $(this).sortable("destroy")
    })
    //hide me
    $('[data-attr="movable"]').addClass("hidden")
    $(".handle").addClass("hidden")
    $(".handle").removeClass("cursor-grab")
    $("#save_columns_button").addClass("hidden")
    $("#cancel_columns_button").addClass("hidden")
    $('[data-selector="btn-column-delete"]').addClass("hidden")
    $(".detail-inputs").attr("disabled", true)
    document.getElementById("submit-error-msg").innerHTML = "" 
    //show me
    $("#edit_columns_button").removeClass("hidden")
    $("#calendar_view_selection").attr("disabled", false)
   
  }

  initListeners() {
    //select view
    $("#calendar_view_selection").on("change", function () {
      RenderColumnSettings.instance.getActiveView()
      RenderColumnSettings.instance.cs.addTempIDs()
      RenderColumnSettings.instance.renderColumnsList()
    })

    //add new column item
    $("#new_column_button").on("click", function () {
      RenderColumnSettings.instance.editMode() //interface to edit mode
      let newEntryTmpId = makeId(8)
      let newListEntry = $("#column_item")
        .html()
        .replaceAll("[{temp_id}]", newEntryTmpId)
        .replace("[{column-id-value}]", "")
        .replace("[{column-label-value}]", "") //clone snippit

      $("#column_element_list").append(newListEntry) //append to list
    })

    //set item as other
    $("#column_element_list").on(
      "change",
      '[data-selector="column-other"]',
      function () {
        let id = $(this).attr("data-checkbox-id")

        if ($(this).is(":checked")) {
          $("#column_id_" + id).val("others")
          $("#column_id_" + id).prop("readonly", true)
          $("#column_id_" + id).removeAttr("style")
        } else {
          $("#column_id_" + id).val("")
          $("#column_id_" + id).prop("readonly", false)
        }
      }
    )

    //delete column item
    $("#column_element_list").on(
      "click",
      '[data-selector="btn-column-delete"]',
      function () {
        let id_to_delete = $(this).attr("data-column-resource-id")
        let cancelTitle = new pbI18n().translate("Spalte löschen.")
        let cancelQuestion = new pbI18n().translate(
          "Möchten Sie wirklich die Spalte löschen?"
        )
        Fnon.Ask.Warning(
          cancelQuestion,
          cancelTitle,
          "Ja, löschen",
          "Abbrechen",
          (result) => {
            if (result == true) {
              RenderColumnSettings.instance.removeColumn(id_to_delete)
              Fnon.Hint.Warning('Klick "Übernehmen", um die Änderung in den Kalender zu übertragen.')
            }
          }
        )
      }
    )

    //edit switch
    $("#edit_columns_button").on("click", function () {
      RenderColumnSettings.instance.editMode() //interface to edit mode
    })

    //save switch
    $("#save_columns_button").on("click", function () {
      //validate for empty fields
      let empty_vals = 0
      for (var i = 0; i < $("#column_element_list .form-control").length; i++) {
        var e = $("#column_element_list .form-control")[i]
        if (!e.value) {
          document.getElementById("submit-error-msg").innerHTML =
            '<div class="alert alert-danger error_message" data-i18n="error message empty field"> * Jede Spalte muss eine Beschriftung und eine Identifizierung haben. *</div>'
          e.style.border = "solid 1px red"
          empty_vals++
        }
        if (e.value) {
          e.style.border = ""
        }
      }

      let invalid_vals = 0
      if (empty_vals == 0) {
        for (
          var i = 1;
          i < $('#column_element_list, [data-selector="column_id"]').length - 1;
          i++
        ) {
          var e = $('#column_element_list, [data-selector="column_id"]')[i]
          let re = /^\w+$/
          if (!re.test(e.value)) {
            document.getElementById("submit-error-msg").innerHTML =
              '<div class="alert alert-danger error_message" data-i18n="error message valid ID">* Die Spalten-ID darf nur Buchstaben, Zahlen und Unterstriche enthalten. *</div>'
            e.style.border = "solid 1px red"
            invalid_vals++
          }
        }
      }

      if (empty_vals == 0 && invalid_vals == 0) {
        // validate success
        RenderColumnSettings.instance.viewMode() //interface to view mode
        document.getElementById("submit-error-msg").innerHTML = "" //remove error message
        let order = $("#column_element_list").sortable("serialize")
        RenderColumnSettings.instance.cs.column_order = order

        //validate success while tour
        const cleanedUpOrder = order.filter(item => {
            return item.id != undefined && item.id !=""
        })

        //save data
        if (cleanedUpOrder.length > 0) {
          console.log(e)
          RenderColumnSettings.instance.saveChanges()
          return true
        }
        else {
          
        }
        
        
      }
    })

    //cancel switch
    $("#cancel_columns_button").on("click", function () {
      let cancelTitle = new pbI18n().translate("Sie haben nicht gespeichert.")
      let cancelQuestion = new pbI18n().translate(
        "Möchten Sie wirklich die Detailansicht verlassen? Alle Änderungen gehen verloren."
      )
      Fnon.Ask.Warning(
        cancelQuestion,
        cancelTitle,
        "Ja, verlassen",
        "Abbrechen",
        (result) => {
          if (result == true) {
            RenderColumnSettings.instance.getActiveView()
            RenderColumnSettings.instance.renderColumnsList()
            RenderColumnSettings.instance.viewMode()
          }
        }
      )

      //reload active list
    })
  }
}
