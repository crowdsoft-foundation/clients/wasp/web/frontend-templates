import {getQueryParam} from "/csf-lib/pb-functions.js?v=[cs_version]"

export class Controller {
    constructor(socket) {
        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.socket = socket
        }

        return Controller.instance
    }

    init(ModuleInstance) {
        return new Promise(function(resolve, reject) {
            switch (window.location.pathname) {

                case "/easy2schedule/status-settings.html":
                    import("./render_status_list.js?v=[cs_version]").then((RendererImport) => {
                        new RendererImport.Render(Controller.instance.socket).init(ModuleInstance).then(resolve)
                    })
                    break;
                case "/easy2schedule/statistics.html":
                    import("./render-statistic.js?v=[cs_version]").then((Module) => {
                        new Module.RenderStatistic(Controller.instance.socket, ModuleInstance).init()
                    })
                    break;
                default:
                    import("./render.js?v=[cs_version]").then((RendererImport) => {
                        new RendererImport.Render(Controller.instance.socket).init(ModuleInstance).then(resolve)
                    })
                    break;
            }
        });
    }
}

