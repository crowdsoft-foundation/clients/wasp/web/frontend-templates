import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {stringToHslColor, md5} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbEasy2schedule} from './pb-easy2schedule.js?v=[cs_version]';
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]";


export class RenderStatistic {
    constructor() {
        if (!RenderStatistic.instance) {
            this.tpl = new PbTpl()
            this.easy2schedule = new PbEasy2schedule()
            RenderStatistic.instance = this
        }

        return RenderStatistic.instance
    }

    init() {
        $(".datepicker").datetimepicker({
            format: CONFIG.DATEFORMAT,
            sideBySide: true,
            showClose: true,
            ignoreReadonly: true,
            debug: false,
            widgetPositioning: {horizontal: 'left', vertical: 'bottom'}
        });

        if ($("#loader-overlay")) {
            $("#loader-overlay").hide()
            $("#contentwrapper").show()
        }

        $(document).on('click', '#show_statistic', function (e) {
            let date = new Date();
            let start_date = null;
            let end_date = null;

            if ($("#start_date").val() === "") {
                start_date = new Date(date.getFullYear(), date.getMonth(), 1);
                start_date = moment(start_date).format('YYYY-MM-DD');
            } else {
                start_date = moment($("#start_date").val(), 'DD.MM.YYYY');
                start_date = start_date.format('YYYY-MM-DD');
            }

            if ($("#end_date").val() === "") {
                end_date = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                end_date = moment(end_date).format('YYYY-MM-DD');
            } else {
                end_date = moment($("#end_date").val(), 'DD.MM.YYYY');
                end_date = end_date.format('YYYY-MM-DD');
            }

            let promises = [new PbEasy2schedule().getEventmanagerStatistic(start_date, end_date)];

            Promise.all(promises).then(function (return_values) {
                new PbNotifications().blockForLoading('#statistic_container')

                let data = return_values[0];

                // Remove 'event_id', 'attached_ressources', and 'contacts' from the headers
                const headers = [
                    "resourceId",
                    "status",
                    "tags",
                    "groups",
                    "type"
                ];

                // Remove the columns corresponding to 'event_id', 'attached_ressources', and 'contacts'
                data = data.map(row => [
                    row[1], // resourceId
                    row[4], // status
                    row[5], // tags
                    row[6], // groups
                    row[7]  // type
                ]);

                PbTpl.instance.renderIntoAsync('easy2schedule/templates/statistic/events.html', {
                    "statistic_data": data,
                    "start_date": start_date,
                    "end_date": end_date,
                    "chart_type": "bar"
                }, '#easy2schedule_statistic').then(() => {
                    const chartData = headers.reduce((acc, header) => {
                        acc[header] = {};
                        return acc;
                    }, {});

                    data.slice(1).forEach(row => {
                        headers.forEach((header, index) => {
                            const value = row[index];

                            if (Array.isArray(value)) {
                                value.forEach(item => {
                                    if (header === "tags" && typeof item === 'object') {
                                        const text = item.text;
                                        if (!chartData[header][text]) {
                                            chartData[header][text] = 0;
                                        }
                                        chartData[header][text]++;
                                    } else {
                                        if (!chartData[header][item]) {
                                            chartData[header][item] = 0;
                                        }
                                        chartData[header][item]++;
                                    }
                                });
                            } else {
                                if (!chartData[header][value]) {
                                    chartData[header][value] = 0;
                                }
                                chartData[header][value]++;
                            }
                        });
                    });

                    function getRandomColor(string) {
                        return stringToHslColor(md5(string))
                    }

                    function hslToRgba(hsl, alpha = 0.2) {
                        const [h, s, l] = hsl.match(/\d+/g).map(Number);

                        return `hsla(${h}, ${s}%, ${l}%, ${alpha})`;
                    }

                    function createChart(ctx, labels, datasets) {
                        datasets.forEach(dataset => {
                            const backgroundColors = [];
                            const borderColors = [];

                            labels.forEach((item) => {
                                const color = getRandomColor(item);
                                backgroundColors.push(hslToRgba(color));
                                borderColors.push(color);
                            });

                            dataset.backgroundColor = backgroundColors;
                            dataset.borderColor = borderColors;
                        });

                        new Chart(ctx, {
                            type: "bar",
                            data: {
                                labels: labels,
                                datasets: datasets
                            },
                            options: {
                                indexAxis: 'y',
                                responsive: true,
                                maintainAspectRatio: false,
                                scales: {
                                    y: {
                                        ticks: {
                                            maxRotation: 0, // Adjust the rotation as needed
                                            minRotation: 0  // Adjust the rotation as needed
                                        }
                                    },
                                    x: {
                                        beginAtZero: true,
                                        title: {
                                            display: true,
                                            text: 'Count'
                                        },
                                        ticks: {
                                            stepSize: 1,
                                            callback: function (value) {
                                                if (Number.isInteger(value)) {
                                                    return value;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }

                    const row = document.querySelector('#charts-container .row');

                    headers.forEach(header => {
                        const bucket = chartData[header];
                        const labels = Object.keys(bucket);
                        const data = Object.values(bucket);

                        const col = document.createElement('div');
                        col.className = 'canvas-design chart-box';

                        const canvas = document.createElement('canvas');
                        col.appendChild(canvas);
                        row.appendChild(col);

                        const ctx = canvas.getContext('2d');

                        const datasets = [{
                            label: header,
                            data: data,
                            borderWidth: 1
                        }];

                        createChart(ctx, labels, datasets);
                    });

                    new PbNotifications().unblock('#statistic_container')

                });
            }.bind(this))
                .catch(function (error) {
                    PbTpl.instance.renderIntoAsync('easy2schedule/templates/statistic/events.html', {
                        "no_data": true,
                    }, '#easy2schedule_statistic')
                });
        }.bind(this))
    }


}