import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {
    fireEvent,
    getCookie,
    setCookie,
    getQueryParam,
    get_news_for,
    renderNews,
    showNotification,
    makeId,
    removeParamFromAddressBar, array_merge_distinct
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {ContactManager} from "../../contact-manager/js/contact-manager.js?v=[cs_version]"
import {PbEventStatus} from "../../csf-lib/pbapi/calendar/pb-event-status.js?v=[cs_version]"
import {PbTag} from "../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"
import {PbEventTypes} from "../../csf-lib/pbapi/calendar/pb-event-types.js?v=[cs_version]"
import {PbEventRenderer} from "./eventRenderer.js?v=[cs_version]"
import {PbIcs} from "../../csf-lib/pb-ics.js?v=[cs_version]"

var get = function (url, callback, ignoreErrors) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.ignoreErrors = ignoreErrors

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                if (this.status == 200 || this.status == 204 || ignoreErrors) {
                    callback(this.responseText);
                    resolve(this.status)
                } else {
                    //alert("Beim senden der Anfrage ist etwas schief gelaufen. Bitte prüfen Sie ob der Vorgang erfolgreich war und versuchen Sie es ggf. erneut.");

                    reject()
                }
            }
        })


        xhr.open("GET", url);
        xhr.setRequestHeader("apikey", getCookie("apikey"));
        xhr.send();
    })
}


export class PbEasy2schedule {
    contacts_list_sorted = []
    contactTitles = []
    groupsandmembers = []
    event_status_options = []
    interval = {}
    allEventData = []
    tags_sorted = []

    constructor() {
        if (!PbEasy2schedule.instance) {
            this.initListeners()
            this.initActions()
            $.unblockUI()
            $("body").removeClass("loading")
            $("body").show()
            $("#spinner").css("display", "none")
            $("#content").css("display", "block")

            PbEasy2schedule.instance = this
            PbEasy2schedule.instance.PbEventStatus = new PbEventStatus()


            PbEasy2schedule.instance.notifications = new PbNotifications()
            PbEasy2schedule.instance.tagListModel = new PbTagsList()
            PbEasy2schedule.instance.handledTaskIds = []
            PbEasy2schedule.instance.appointmentTypes = new PbEventTypes()

            document.addEventListener("pbTagsListChanged", function _(event) {
                if (!PbEasy2schedule.instance.handledTaskIds.includes(event.data.args.task_id)) {
                    PbEasy2schedule.instance.handledTaskIds.push(event.data.args.task_id)
                    PbEasy2schedule.instance.renderTagListNew(event.data.args.tags)
                }
            })
        }

        return PbEasy2schedule.instance
    }

    init() {
        return new Promise(function (resolve, reject) {
            let cal = PbEasy2schedule.instance
            cal.setEventsource()

            moment.locale(navigator.language)
            cal.datetimeFormat = "DD.MM.YYYY HH:mm";
            cal.dateFormat = "DD.MM.YYYY";
            cal.mysqldatetimeFormat = "YYYY-MM-DD HH:mm:ss";
            cal.calendarStarttime = "08:00:00"
            cal.calendarCustomStarttime = cal.calendarStarttime
            cal.useCustomStarttime = false
            cal.alternativResources = {}
            cal.resources = []
            cal.headers_left = null
            cal.readOnlyUser = false
            cal.userConfig = null
            cal.columns_count = 0
            cal.renderRange = null
            cal.view_start = null
            cal.view_end = null
            cal.touchduration = 0
            cal.timer = 0
            cal.colors = []
            cal.selectedEvent = null
            cal.isClicked = false
            cal.isDblClicked = false
            cal.jsevent = undefined
            cal.anonymize = false
            if (getQueryParam("anonymize")) {
                cal.anonymize = true
            }

            let promises = []
            promises.push(get(CONFIG.API_BASE_URL + "/cb/ressources", cal.fetchRessources))
            promises.push(get(CONFIG.API_BASE_URL + "/cb/columns", cal.fetchColumns))
            promises.push(get(CONFIG.API_BASE_URL + "/cb/account_headers?apikey=" + getCookie("apikey"), cal.fetchHeaders))
            promises.push(get(CONFIG.API_BASE_URL + "/cb/extended_properties", cal.fetchExtendedProperties))
            promises.push(get(CONFIG.API_BASE_URL + "/cb/account_config", cal.fetchAccountConfiguration))
            promises.push(PbEasy2schedule.instance.PbEventStatus.load())
            promises.push(PbEasy2schedule.instance.appointmentTypes.loadFromApi())
            Promise.all(promises).then((values) => {
                PbEasy2schedule.instance.event_status_options = PbEasy2schedule.instance.PbEventStatus.getStatusList()
                resolve()
            });
            return cal
        })
    }

    fetchPersonalEntriesByTimes(from_date, until_date) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/cb/personal_events?apikey=" + getCookie("apikey") + "&start=" + from_date + "&end=" + until_date + "&timeZone=Europe/Berlin",
                "method": "GET",
                "timeout": 0,
                "success": (result) => {
                    resolve(result)
                },
                "error": reject
            }
            $.ajax(settings)
        })
    }

    fetchEntriesByTimes(from_date, until_date) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/cb/events?apikey=" + getCookie("apikey") + "&start=" + from_date + "&end=" + until_date + "&timeZone=Europe/Berlin",
                "method": "GET",
                "timeout": 0,
                "success": (result) => {
                    resolve(result)
                },
                "error": reject
            }
            $.ajax(settings)
        })
    }


    renderContactsList() {
        //gather list of contacts by H1 configuration from ContactManager and return them as titles 
        let tmp_titles = {}

        for (let configEntry of ContactManager.instance.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
            }
        }
        // reset contactTitles in case of rerendering if contacts are refreshed
        PbEasy2schedule.instance.contactTitles = []
        let contactObject = {}
        for (let entry of PbEasy2schedule.instance.contacts_list_sorted) {
            let contactId = entry.contact_id
            let contactTitleString = ""
            for (let titlefield_key of Object.keys(tmp_titles)) {
                if (entry.data[tmp_titles[titlefield_key]]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                }
                else if (entry.data[tmp_titles[titlefield_key].toLowerCase()]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key].toLowerCase()] + " "
                }
            }
            contactObject = {
                contact_id: contactId,
                title: contactTitleString
            }
            PbEasy2schedule.instance.contactTitles.push(contactObject)
        }

        let tmp = PbEasy2schedule.instance.contactTitles;

        tmp.sort(function (a, b) {
                a = a.title.toLowerCase()
                b = b.title.toLowerCase()
                return a < b ? -1 : 1
            }
        );

        PbEasy2schedule.instance.contacts_list_sorted = tmp;
        PbEasy2schedule.instance.addContactsToContactList()
    }

    fnAppointmentsubmit(evt) {
        var fields = PbEasy2schedule.instance.getEventFormElements()
        evt.preventDefault()

        if (evt.target.name == "copy")
            fields["event_id"].value = ""
        let url = ""
        if (fields["event_id"].value == "") {
            url = CONFIG.API_BASE_URL + "/es/cmd/createNewAppointment";
        } else {
            url = CONFIG.API_BASE_URL + "/es/cmd/changeAppointment";
        }

        let attachedRessources = []
        if (fields["colors"])
            for (let attachedRessource of fields["colors"].selectedOptions) {
                attachedRessources.push(attachedRessource.getAttribute("data-ressource"))
            }

        let attachedContacts = []
        if (document.getElementById("contacts"))
            for (let attachedContact of document.getElementById("contacts").selectedOptions) {
                attachedContacts.push(attachedContact.getAttribute("data-contact-id"))
            }

        let attachedAccessrights = []
        if (document.getElementById("permissions"))
            for (let attachedAccessright of document.getElementById("permissions").selectedOptions) {
                attachedAccessrights.push(attachedAccessright.getAttribute("data-accessrightid"))
            }

        let allDay = false
        if (fields["allday"] && fields["allday"].value == "true") {
            allDay = true

        }

        let attachedTags = []
        if (document.getElementById("tags")) {
            let selected_tags = $('#tags').select2('data')
            for (const [key, value] of Object.entries(selected_tags)) {
                attachedTags.push({
                    "id": value.text,
                    "text": value.text
                })
            }
        }
        let eventStatus = "pending"
        if (document.getElementById("event_status")) {
            eventStatus = $('#event_status').val()
        }
        let appointment_type = []
        if (document.getElementById("appointment_type")) {
            appointment_type = $('#appointment_type').val()
        }


        let reminders = []
        let reminder_data

        $('[data-reminder-id]').each(function () {
            var element = $(this);
            if (element.get(0).dataset.reminderId != "placeholder") {
                let mode = $("#reminder_mode_select_" + element.get(0).dataset.reminderId).val()
                let time_unit = $("#reminder_unit_select_" + element.get(0).dataset.reminderId).val()
                let time_amount = $("#reminder_time_value_" + element.get(0).dataset.reminderId).val()

                let recipient = []
                if (mode == "gui") {
                    recipient = [getCookie("username")]
                } else if (mode == "email") {
                    recipient = PbEasy2schedule.instance.userConfig.reminder_email_configuration.recipients
                }

                switch (time_unit) {
                    case "day":
                        time_amount = time_amount * 60 * 60 * 24
                        break;
                    case "hour":
                        time_amount = time_amount * 60 * 60
                        break;
                    case "min":
                        time_amount = time_amount * 60
                        break;
                }
                reminder_data = {
                    "type": mode,
                    "seconds_in_advance": time_amount,
                    "recipients": recipient
                }

                if (element.get(0).dataset.reminderId && element.get(0).dataset.reminderId.length > 0 && !element.get(0).dataset.reminderId.startsWith("new_") && evt.target.name != "copy") {
                    reminder_data["reminder_id"] = element.get(0).dataset.reminderId
                }

                reminders.push(reminder_data)
            }
        })

        if (!fields["resource"].options[fields["resource"].selectedIndex]?.value) {
            fields["resource"].parentNode.style.borderColorOriginal = fields["resource"].parentNode.style.borderColor
            fields["resource"].parentNode.style.borderColor = "red"
            return false
        }
        if (fields["resource"].parentNode.style.borderColor == "red") {
            fields["resource"].parentNode.style.borderColor = "rgba(34,36,38,.15)"
        }
        let starttime = moment.utc(fields["starttime"].value, PbEasy2schedule.instance.datetimeFormat)
        let endtime = moment.utc(fields["endtime"].value, PbEasy2schedule.instance.datetimeFormat)

        if (allDay == true) {
            let diffDays = endtime.diff(starttime, 'days')
            if (diffDays > 0) {
                endtime = moment.utc(endtime).add(1, 'days')
            }
        }
        let data = {
            event_id: fields["event_id"].value,
            resourceId: fields["resource"].options[fields["resource"].selectedIndex].value,
            textColor: "", //NYI
            color: "", //NYI
            attachedRessources: attachedRessources,
            starttime: starttime.format(PbEasy2schedule.instance.mysqldatetimeFormat),
            endtime: endtime.format(PbEasy2schedule.instance.mysqldatetimeFormat),
            title: fields["title"].value,
            savekind: fields["save"].name,
            extended_props: {"creator_login": getCookie("username"), "comment": $("#event_comment").val()},
            allDay: allDay,
            attachedContacts: attachedContacts,
            data_owners: attachedAccessrights,
            reminders: reminders,
            tags: attachedTags,
            data: {
                event_status: eventStatus,
                appointment_type: appointment_type || []
            }
        }

        let extended_props = document.querySelectorAll("[data-event-property='extended_prop']")
        for (let prop of extended_props) {
            let prop_name = prop.id
            let prop_value = prop.value
            data["extended_props"][prop_name] = prop_value
        }
        PbEasy2schedule.instance.post(url, data, function (response) {
            PbEasy2schedule.instance.toggleOverlay()
        });

        PbEasy2schedule.instance.renderRange = PbEasy2schedule.instance.calendar.state.dateProfile.renderRange
        PbEasy2schedule.instance.view_start = moment(PbEasy2schedule.instance.renderRange.start)
        PbEasy2schedule.instance.view_end = moment(PbEasy2schedule.instance.renderRange.end)

        let new_event_date = moment.utc(fields["starttime"].value, PbEasy2schedule.instance.datetimeFormat)

        if (!new_event_date.isBetween(PbEasy2schedule.instance.view_start.add(-2, 'hours'), PbEasy2schedule.instance.view_end) && confirm("Möchten Sie die Ansicht auf den neu angelegten Termin umstellen?")) {
            PbEasy2schedule.instance.setViewDate(fields["starttime"].value)
        }

    }

    fetchAccountConfiguration(response) {
        console.log("Got configuration")
        response = JSON.parse(response)
        PbEasy2schedule.instance.userConfig = response["user_config"]
        console.log("USERCONFIG", PbEasy2schedule.instance.userConfig)
        if (PbEasy2schedule.instance.userConfig["readonly_user"]) {
            $("#extended_props_wrapper").remove()
            let extended_properties_toggle_element = document.getElementById("extended_props_toggle")
            if (extended_properties_toggle_element) {
                extended_properties_toggle_element.parentNode.removeChild(extended_properties_toggle_element)
            }
        }
    }

    fetchExtendedProperties(response) {
        console.log("Got data for columns", response)
        if (document.getElementById("extended_props_wrapper")) {
            document.getElementById("extended_props_wrapper").innerHTML = response
        }

        if ($("#extended_props_wrapper").html().length > 0) {
            $("#extended_props_element").removeClass("hidden")
        }
    }

    removeNotGrantedViewsFromMenu() {
        for (let button of $(".calendar_view_button")) {
            if (!PbEasy2schedule.instance.headers_left.includes(button.dataset.calendarview)) {
                button.style.display = "None";
                button.parentNode.removeChild(button)
            }
        }
    }


    fetchHeaders(response) {
        console.log("Got headers")
        response = JSON.parse(response)
        PbEasy2schedule.instance.headers_left = response["headers_left"]
        console.log("HEADERS", PbEasy2schedule.instance.headers_left)
        PbEasy2schedule.instance.removeNotGrantedViewsFromMenu()
    }

    fetchRessources(response) {
        response = JSON.parse(response)
        //console.log("RESPONSE", response)
        for (let entry of response) {
            let dataset = {
                id: entry.id,
                name: entry.display_name,
                fontColor: entry.props.eventFontColor,
                backgroundColor: entry.props.eventBackgroundColor,
                loginId: entry.props.login_id,
                ressource_id: entry.ressource_id
            }
            PbEasy2schedule.instance.colors.push(dataset)
        }
        console.log("COLORS", PbEasy2schedule.instance.colors)
        var legend = document.getElementById("colorLegend");
        if (legend) {
            for (let item of PbEasy2schedule.instance.colors) {
                legend.innerHTML += '<div style="background-color: ' + item.backgroundColor + ';color:' + item.fontColor + '">' + item.name + '</div>'
            }
        }

        PbEasy2schedule.instance.addResourcesToDropdown()
    }

    //CROW-540 added filter for multiple ids within muliple views
    addResourcesToDropdown = function () {
        if (document.getElementById("resource")) {
            var dropdown = document.getElementById("resource");
            dropdown.innerHTML = "";
            var uniqueResource = []

            for (let item of PbEasy2schedule.instance.resources) {
                var newObj = item.id;

                if (item.id != "others") {
                    if (!uniqueResource.some(newObj => newObj === item.id)) {
                        let option = document.createElement("option");
                        option.text = item.title;
                        option.value = item.id;
                        dropdown.add(option);

                        uniqueResource.push(newObj);
                    }
                }
            }
        }
        if (document.getElementById("colors")) {
            var dropdown = document.getElementById("colors");
            dropdown.innerHTML = "";

            for (let item of PbEasy2schedule.instance.colors) {
                let option = document.createElement("option");
                option.text = item.name;
                option.value = item.id//JSON.stringify(item)
                option.setAttribute("data-ressource", JSON.stringify(item))
                dropdown.add(option);
            }
        }
    }

    addContactsToContactList = function () {
        if (document.getElementById("contacts")) {
            var dropdown = $('#contacts')

            PbEasy2schedule.instance.resetContactsList()

            for (let item of PbEasy2schedule.instance.contacts_list_sorted) {
                let option = document.createElement("option");
                option.text = item.title
                option.value = item.contact_id//JSON.stringify(item)
                option.setAttribute("data-contact-id", item.contact_id)
                dropdown.append(option);
            }
        }
    }

    resetContactsList() {
        var dropdown = $('#contacts');
        dropdown.find('option').remove()
    }

    addEntriesToAccessrightsDropdown = function () {
        if (document.getElementById("permissions")) {
            var dropdown = document.getElementById("permissions");
            dropdown.innerHTML = "";

            for (let item of PbEasy2schedule.instance.groupsandmembers) {
                let option = document.createElement("option");
                option.text = item.replace("group.", new pbI18n().translate("Group") + ": ").replace("user.", new pbI18n().translate("User") + ": ")
                option.value = item
                option.setAttribute("data-accessrightid", item)
                dropdown.add(option);
            }
        }
    }

    addEntriesToStatusDropdown() {
        if (document.getElementById("event_status")) {
            var dropdown = document.getElementById("event_status");
            dropdown.innerHTML = "";
            for (let item of PbEasy2schedule.instance.event_status_options) {
                let option = document.createElement("option");
                option.value = item.status_id
                option.text = new pbI18n().translate(item.status_name)
                option.setAttribute("data-event_statusid", item.status_id)
                dropdown.add(option);
            }
        }
    }

    addEntriesToTypeDropdown() {
        if (document.getElementById("appointment_type")) {
            var dropdown = document.getElementById("appointment_type");
            let appointmentTypes = PbEasy2schedule.instance.appointmentTypes.getData()
            dropdown.innerHTML = "";
            for (let type of appointmentTypes) {
                let option = document.createElement("option");
                option.value = type.data.type_id
                option.text = new pbI18n().translate(type.data.type_name)
                option.setAttribute("data-event_typeid", type.data.type_id)
                if (type.data.default) {
                    option.setAttribute("selected", "selected")
                }
                dropdown.add(option);
            }
        }
    }

    setEventsource() {
        let cal = PbEasy2schedule.instance
        cal.eventSource = [
            {
                url: CONFIG.API_BASE_URL + '/cb/events?apikey=' + getCookie("apikey") + (PbEasy2schedule.instance.anonymize ? "&anonymize=true" : ""),
                method: 'GET',
                extraParams: {
                    p1: '1',
                    p2: '2'
                },
                failure: function () {
                    window.setTimeout(function () {
                        PbEasy2schedule.instance.calendar.refetchEvents();
                    }, 5000)
                },
                color: 'yellow',   // a non-ajax option
                textColor: 'black' // a non-ajax option
            }
        ]
    }

    fetchColumns(response) {
        console.log("Got data for columns", response)
        response = JSON.parse(response)
        PbEasy2schedule.instance.alternativResources = response
        //systemReadyForCalendarLoad("persons")
    }

    createAppointmentSeries(appointmentDates) {
        var fields = PbEasy2schedule.instance.getEventFormElements()
        let dates = appointmentDates
        let url = CONFIG.API_BASE_URL + "/es/cmd/createNewAppointment"
        let attachedRessources = []
        if (fields["colors"])
            for (let attachedRessource of fields["colors"].selectedOptions) {
                attachedRessources.push(attachedRessource.getAttribute("data-ressource"))
            }

        let attachedContacts = []
        if (document.getElementById("contacts"))
            for (let attachedContact of document.getElementById("contacts").selectedOptions) {
                attachedContacts.push(attachedContact.getAttribute("data-contact-id"))
            }

        let attachedAccessrights = []
        if (document.getElementById("permissions"))
            for (let attachedAccessright of document.getElementById("permissions").selectedOptions) {
                attachedAccessrights.push(attachedAccessright.getAttribute("data-accessrightid"))
            }

        let allDay = false
        if (fields["allday"] && fields["allday"].value == "true") {
            allDay = true
        }

        let attachedTags = []
        if (document.getElementById("tags")) {
            let selected_tags = $('#tags').select2('data')
            for (const [key, value] of Object.entries(selected_tags)) {
                attachedTags.push({
                    "id": value.text,
                    "text": value.text
                })
            }
        }

        let eventStatus = "pending"
        if (document.getElementById("event_status")) {
            eventStatus = $('#event_status').val()
        }
        let appointment_type = []
        if (document.getElementById("appointment_type")) {
            appointment_type = $('#appointment_type').val()
        }

        let reminders = []
        let reminder_data

        $('[data-reminder-id]').each(function () {
            var element = $(this);
            if (element.get(0).dataset.reminderId != "placeholder") {
                let mode = $("#reminder_mode_select_" + element.get(0).dataset.reminderId).val()
                let time_unit = $("#reminder_unit_select_" + element.get(0).dataset.reminderId).val()
                let time_amount = $("#reminder_time_value_" + element.get(0).dataset.reminderId).val()

                let recipient = []
                if (mode == "gui") {
                    recipient = [getCookie("username")]
                } else if (mode == "email") {
                    recipient = PbEasy2schedule.instance.userConfig.reminder_email_configuration.recipients
                }

                switch (time_unit) {
                    case "day":
                        time_amount = time_amount * 60 * 60 * 24
                        break;
                    case "hour":
                        time_amount = time_amount * 60 * 60
                        break;
                    case "min":
                        time_amount = time_amount * 60
                        break;
                }
                reminder_data = {
                    "type": mode,
                    "seconds_in_advance": time_amount,
                    "recipients": recipient
                }

                if (element.get(0).dataset.reminderId && element.get(0).dataset.reminderId.length > 0 && !element.get(0).dataset.reminderId.startsWith("new_") && evt.target.name != "copy") {
                    reminder_data["reminder_id"] = element.get(0).dataset.reminderId
                }

                reminders.push(reminder_data)
            }
        })
        //#TODO: event_status for appointment series might have some unintended consequences
        let data = {
            event_id: "",
            resourceId: fields["resource"].options[fields["resource"].selectedIndex].value,
            textColor: "", //NYI
            color: "", //NYI
            attachedRessources: attachedRessources,
            starttime: moment.utc(fields["starttime"].value, PbEasy2schedule.instance.datetimeFormat).format(PbEasy2schedule.instance.mysqldatetimeFormat),
            endtime: moment.utc(fields["endtime"].value, PbEasy2schedule.instance.datetimeFormat).format(PbEasy2schedule.instance.mysqldatetimeFormat),
            title: fields["title"].value,
            savekind: fields["save"].name,
            extended_props: {"creator_login": getCookie("username"), "comment": $("#event_comment").val()},
            allDay: allDay,
            attachedContacts: attachedContacts,
            data_owners: attachedAccessrights,
            reminders: reminders,
            tags: attachedTags,
            data: {
                "event_status": eventStatus,
                "appointment_type": appointment_type || []
            },
            series: {
                id: "foo",
                name: fields["title"].value

            }
        }

        let extended_props = document.querySelectorAll("[data-event-property='extended_prop']")
        for (let prop of extended_props) {
            let prop_name = prop.id
            let prop_value = prop.value
            data["extended_props"][prop_name] = prop_value
        }

        dates.forEach(date => {
            data['starttime'] = moment.utc(date['starttime'], PbEasy2schedule.instance.datetimeFormat).format(PbEasy2schedule.instance.mysqldatetimeFormat)
            data['endtime'] = moment.utc(date['endtime'], PbEasy2schedule.instance.datetimeFormat).format(PbEasy2schedule.instance.mysqldatetimeFormat)
            PbEasy2schedule.instance.post(url, data, function (response) {
                console.log(response)
            });
        })
    }

    getInterval() {
        // get values from modal and set them to state
        let interval = {}
        if ($('#series-container').is(":visible")) {
            let interval_value = $('#series_interval_value').val()
            let interval_unit = $('#series_interval_unit option').filter(':selected').val()
            let series_length = $('#end_number_value').val()
            let end_date = "" ? $('#series_end_number').prop('checked', true) : $('#series_end_date').val()

            interval["value"] = interval_value
            interval["unit"] = interval_unit
            interval["end_date"] = end_date
            interval["series_length"] = series_length

            console.log("Interval values in if loop:", interval)

        }
        console.log("Interval values:", interval)
        return interval

    }

    calculateDates(interval = "") {

        let dates = []
        let initial_start_time = moment.utc($('#starttime').val(), PbEasy2schedule.instance.dateFormat)
        let starttime = moment.utc($('#starttime').val(), 'DD.MM.YYYY HH:mm').clone()
        let endtime = moment.utc($('#endtime').val(), 'DD.MM.YYYY HH:mm').clone()
        let length = interval["series_length"]
        console.log("Times in calculate", starttime, endtime)
        for (let i = 0; i < length; i++) {
            dates.push({
                starttime: moment(starttime).clone().add(Number(interval["value"]), interval["unit"])['_d'],
                endtime: moment(endtime).clone().add(Number(interval["value"]), interval["unit"])['_d']
            })
            starttime = moment(starttime).clone().add(Number(interval["value"]), interval["unit"])
            endtime = moment(endtime).clone().add(Number(interval["value"]), interval["unit"])
            console.log("Pushing in dates", starttime, endtime)
        }
        console.log("Dates array", dates)
        return dates
    }


    checkSeriesDropdown() {
        var fields = PbEasy2schedule.instance.getEventFormElements()
        if (fields["event_id"].value != "") {
            $('#series-wrapper').remove()
        }
    }


    run(calendar_options = {}) {
        console.log("ReadyForCalendarLoad", calendar_options)

        var calendarEl = document.getElementById('calendar');
        //Fullcalendar Version 4
        PbEasy2schedule.instance.calendar = new FullCalendar.Calendar(calendarEl, {
            schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
            droppable: false,
            editable: false,
            plugins: ['interaction', 'resourceTimeGrid', 'resourceDayGrid', 'dayGridPlugin', 'resourceTimeline', 'list'],
            timeZone: 'Europe/Berlin',
            locale: PbEasy2schedule.instance.getLanguage(),
            defaultView: getQueryParam("v") ? getQueryParam("v") : $(".calendar_view_button:visible").data("calendarview") || "resourceTimeGridMultiDay",
            defaultDate: getQueryParam("viewdate") ? new Date(getQueryParam("viewdate")) : new Date(),
            datesAboveResources: true,
            weekends: true,
            selectable: true,
            minTime: "08:00:00",
            maxTime: '23:59:59',
            nextDayThreshold: "00:00:00",
            height: "auto",
            slotDuration: '00:30:00', //For every of this timespan an own row is rendered
            snapDuration: '00:15:00',
            selectOverlap: true,
            //timezone: 'local',
            allDaySlot: true,
            navLinks: true, // can click day/week names to navigate views
            eventLimit: true, // allow "more" link when too many events
            selectMinDistance: 5,
            buttonIcons: {
                prev: 'fc-icon-chevron-left',
                next: 'fc-icon-chevron-right',
            },
            // navLinkDayClick: function (date, jsEvent) {
            //     console.log('day', date.toISOString());
            //     console.log('coords', jsEvent.pageX, jsEvent.pageY);
            //     calendar.changeView("")
            // },
            loading: function (bool) {
                if (bool) {
                    console.log("loading events")
                    fireEvent("stillLoadingEvents")
                } else {
                    fireEvent("eventsLoaded")


                    PbEasy2schedule.instance.checkForEventTimesNotInView()
                    PbEasy2schedule.instance.make_event_unique()
                    PbEasy2schedule.instance.sortToOthersResource()
                    PbEasy2schedule.instance.fixMonthViewHeight();

                    if (getQueryParam("s")) {
                        PbEasy2schedule.instance.filter_events(undefined, getQueryParam("s"))
                        removeParamFromAddressBar("s")
                    } else {
                        PbEasy2schedule.instance.filter_events()
                    }

                    if (getQueryParam("v")) {
                        changeView(getQueryParam("v"))
                        //removeParamFromAddressBar("v")
                        PbEasy2schedule.instance.viewPreset = true
                    } else {
                        if (!PbEasy2schedule.instance.viewPreset) {
                            PbEasy2schedule.instance.viewPreset = true
                            $("[data-calendarview]:visible").first().click()
                        }
                    }

                }
                //Possibly call you feed loader to add the next feed in line
            },
            dayRender: function (info) {
                if (info.view.type == "resourceTimeGridDay" || info.view.type == "resourceTimeGridDay2") {
                    let dayname = new Intl.DateTimeFormat(navigator.language, {weekday: 'long'}).format(info.date)
                    let html = '<tr id="weekdayname"><th class="fc-day-header fc-widget-header fc-mon fc-past"><span>' + dayname + '</span></th></tr>'
                    $(".fc-day-header").remove();
                    $(".fc-head").prepend(html);
                }
            },
            viewSkeletonRender: function (info) {
                let search = ""
                if (getQueryParam("s")) {
                    search = "&s=" + getQueryParam("s")
                }

                window.history.replaceState(null, null, "?v=" + info.view.type + "&viewdate=" + PbEasy2schedule.instance.calendar.view.currentStart.toISOString() + search)
                /*if (info.view.type.startsWith("resourceTimeGridDay")) {
                    $("#calendar").css('width', '98%');
                } else {
                    $("#calendar").removeAttr('style');
                }*/

                //set header navigation for views
                if (info.view.type.startsWith("list")) {
                    //list view
                    $('.fc-previousday-button, .fc-nextday-button').hide()
                    $('.fc-listWeek-button, .fc-listMonth-button, .fc-listYear-button').show()
                    $(".fc-prev-button").children("span").removeClass("fc-icon fc-icon-chevrons-left").addClass("fc-icon fc-icon-chevron-left")
                    $(".fc-next-button").children("span").removeClass("fc-icon fc-icon-chevrons-right").addClass("fc-icon fc-icon-chevron-right")
                } else if (info.view.type.startsWith("resourceTimeGridMultiDay")) {
                    //week view
                    $('.fc-previousday-button, .fc-nextday-button').show()
                    $(".fc-prev-button").children("span").removeClass("fc-icon fc-icon-chevron-left").addClass("fc-icon fc-icon-chevrons-left")
                    $(".fc-next-button").children("span").removeClass("fc-icon fc-icon-chevron-right").addClass("fc-icon fc-icon-chevrons-right")
                    $('.fc-listWeek-button, .fc-listMonth-button, .fc-listYear-button').hide()
                } else {
                    //all other views
                    $('.fc-previousday-button, .fc-nextday-button, .fc-listWeek-button, .fc-listMonth-button, .fc-listYear-button').hide();
                    $(".fc-prev-button").children("span").removeClass("fc-icon fc-icon-chevrons-left").addClass("fc-icon fc-icon-chevron-left")
                    $(".fc-next-button").children("span").removeClass("fc-icon fc-icon-chevrons-right").addClass("fc-icon fc-icon-chevron-right")
                }

                PbEasy2schedule.instance.calendar.refetchResources()
                PbEasy2schedule.instance.sortToOthersResource()
                PbEasy2schedule.instance.addResourcesToDropdown()
                PbEasy2schedule.instance.addEntriesToStatusDropdown()
                PbEasy2schedule.instance.addEntriesToTypeDropdown()

                new pbI18n().replaceI18n()

                $(".fc-center").click(PbEasy2schedule.instance.selectViewDate)
                if ($.cookie("e2s_menue_mode")) {
                    toggleMenu($.cookie("e2s_menue_mode"))
                }
                let evt = new Event('viewSkeletonRender_finished');
                window.dispatchEvent(evt);
                PbEasy2schedule.instance.fixMonthViewHeight();
            },
            customButtons: {
                nextday: {
                    icon: "fc-icon-chevron-right",
                    click: function () {
                        let cal = $('#calendar');
                        PbEasy2schedule.instance.calendar.incrementDate({day: -100});
                        PbEasy2schedule.instance.calendar.incrementDate({day: +101});
                    }
                },
                previousday: {
                    icon: "fc-icon-chevron-left",
                    click: function () {
                        PbEasy2schedule.instance.calendar.incrementDate({day: -1});
                    }
                },
                selectdate: {
                    icon: " calendar alternate outline icon left",
                    click: function () {
                        PbEasy2schedule.instance.selectViewDate()
                    }
                }
            },
            header: {
                left: " prev,previousday,today,nextday,next, selectdate, listWeek,listMonth,listYear",
                center: 'title',
                right: ''

            },
            buttonText: {
                resourceTimeGridDay: "Tagesansicht Räume",
                resourceTimeGridMultiDay: "Wochenansicht Räume",
                resourceTimeGridDay2: "Tagesansicht Termine",
                resourceTimeGridMultiDay3: "Wochenansicht Termine",
                dayGridMonth: "Monatsansicht",
                resourceTimelineFourDays: "Belegung"
            },
            businessHours: {
                // days of week. an array of zero-based day of week integers (0=Sunday)
                daysOfWeek: [1, 2, 3, 4, 5, 6, 0], // Monday - Sunday
                startTime: '08:00',
                endTime: '23:59',
            },
            displayEventEnd: calendar_options?.displayEventEnd == undefined ? true : calendar_options.displayEventEnd,
            views: {
                resourceTimeGridMultiDay: {
                    type: 'resourceTimeGrid',
                    duration: {days: 7},
                    titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'}
                },
                resourceTimeGridDay: {
                    type: 'resourceTimeGrid',
                    duration: {days: 1},
                    titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'}
                },
                resourceTimeGridDay2: {
                    type: 'resourceTimeGrid',
                    duration: {days: 1},
                    titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'}
                },
                resourceTimeGridMultiDay3: {
                    type: 'resourceTimeGridDay',
                    duration: {days: 7},
                    titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'}
                },
                dayGridMonth: {
                    type: 'dayGridMonth',
                    titleFormat: {year: 'numeric', month: '2-digit', day: '2-digit'},
                    eventLimit: 999
                },
                resourceTimelineFourDays: {
                    type: 'resourceTimeline',
                    duration: {days: 7},
                    resourceAreaWidth: "200px",
                },
                listWeek: {
                    type: 'listWeek'
                },
                listMonth: {
                    type: 'listMonth'
                },
                listYear: {
                    type: 'listYear'
                }
            },
            refetchResourcesOnNavigate: true,
            resources: function (fetchInfo, successCallback, failureCallback) {
                if (!PbEasy2schedule.instance.calendar) {
                    PbEasy2schedule.instance.resources = PbEasy2schedule.instance.getRessourceColumns("resourceTimeGridMultiDay")
                    successCallback(PbEasy2schedule.instance.getRessourceColumns("resourceTimeGridMultiDay"))
                } else {
                    let view_id = PbEasy2schedule.instance.calendar.view.viewSpec.type
                    console.log("Current View Id", PbEasy2schedule.instance.calendar.view.viewSpec.type)
                    if (PbEasy2schedule.instance.getRessourceColumns(view_id)) {
                        PbEasy2schedule.instance.resources = PbEasy2schedule.instance.getRessourceColumns(view_id)
                        successCallback(PbEasy2schedule.instance.getRessourceColumns(view_id))
                    } else {
                        failureCallback()
                    }
                }

            },
            eventClick: function (event) {
                if (event.event._def.resourceEditable != false) {
                    PbEasy2schedule.instance.click_doublick_switch(event, PbEasy2schedule.instance.handleEventClick.bind(event), PbEasy2schedule.instance.handleEventDoubleClick.bind(event));
                }
            },
            dateClick: function (info) {
                if (info.resource && info.resource._resource.id == "others") {
                    alert("Für diese Spalte können keine Termine direkt angelegt werden.")
                    return false;
                }
                //CROW-540 stops adding of event in month view using click
                if (PbEasy2schedule.instance.calendar.view.viewSpec.type == "dayGridMonth") {
                    return false;
                }

                PbEasy2schedule.instance.click_doublick_switch(info, PbEasy2schedule.instance.handleCalendarClick.bind(info), PbEasy2schedule.instance.handleCalendarDoubleClick.bind(info));
            },
            select: function (info) {
                if (info.resource && info.resource._resource.id == "others") {
                    alert("Für diese Spalte können keine Termine direkt angelegt werden.")
                    return false;
                }
                //CROW-540 stops adding of event in month view using range selection
                if (PbEasy2schedule.instance.calendar.view.viewSpec.type == "dayGridMonth") {
                    alert(new pbI18n().translate("In dieser Ansicht können Termine nicht direkt angelegt werden."))
                    return false;
                }

                PbEasy2schedule.instance.handleSelectRange(info);
            },
            eventSources: PbEasy2schedule.instance.eventSource,
            eventRender: function (event, element) {

                if (event.event._def.rendering == 'background' && event.event._def.allDay) {
                    event.event._def.rendering = ""
                }
                let own_events_allowed = false
                if (PbAccount.instance.hasPermission("easy2schedule.events.read.own") && event.event._def.extendedProps.creator_login == getCookie("username")) {
                    own_events_allowed = true
                }


                if (PbAccount.instance.hasPermission("easy2schedule.events.read.all") || own_events_allowed) {
                    PbEasy2schedule.instance.filter_event(event.event)
                    PbEasy2schedule.instance.fixMonthViewHeight();
                    event.event = PbEasy2schedule.instance.customize_event(event.event)
                    //disable possibility to modify event:
                    //event.event._def.resourceEditable=false

                    let participants = []
                    let attachedRessources = event.event._def.extendedProps.attachedRessources
                    if (attachedRessources != null) {
                        for (let res of attachedRessources) {
                            res = JSON.parse(res)
                            participants.push(res.name)
                        }
                    }

                    if (event.event._def.rendering == 'background') {
                        event.el.append(event.event._def.title);
                    } else {
                        if (calendar_options.eventRenderFunction) {
                            if (typeof calendar_options.eventRenderFunction == "string") {
                                calendar_options.eventRenderFunction = eval(calendar_options.eventRenderFunction)
                            }
                            calendar_options.eventRenderFunction(event)
                        } else {
                            console.warn("No suitable eventrenderer found, using default")
                            PbEventRenderer.getRenderer("participants_title")(event)
                        }
                    }

                    if (event.event._def.originalTitle) {
                        event.event._def.title = event.event._def.originalTitle
                    }
                    PbEasy2schedule.instance.addTooltip(event, participants)

                } else {
                    event.event._def.ui.classNames = ["hiddenEvent"]
                    return false
                }
            },
            eventDrop: function (info) {
                console.log(info)
                alert(info.event.title + " was dropped on " + info.event.start.toISOString());

                if (!confirm("Are you sure about this change?")) {
                    info.revert();
                }
            }
        })

        PbEasy2schedule.instance.calendar.render()
        PbEasy2schedule.instance.setTags()
    }

    addTooltip(event, participants) {
        let tooltip_content = ""
        tooltip_content += new pbI18n().translate("Titel:") + " " + event.event._def.title + "<br/>"

        let show_tooltip = false
        if (participants.length > 1) {
            show_tooltip = true
            tooltip_content += new pbI18n().translate("Teilnehmer:") + " " + participants.join(", ") + "<br/>"
        }
        if (event.event._def.resourceIds) {
            let ressourceTitles = []
            if (PbEasy2schedule.instance.calendar.view.type.startsWith("dayGridMonth")) {
                PbEasy2schedule.instance.resources = []
                PbEasy2schedule.instance.resources.push(...PbEasy2schedule.instance.getRessourceColumns("resourceTimeGridDay"))
                PbEasy2schedule.instance.resources.push(...PbEasy2schedule.instance.getRessourceColumns("resourceTimeGridDay2"))
            }

            for (let oneRessource of PbEasy2schedule.instance.resources) {
                if (event.event._def.resourceIds.includes(oneRessource.id) && !ressourceTitles.includes(oneRessource.title)) {
                    if (oneRessource.id != "others") {
                        ressourceTitles.push(oneRessource.title)
                    }
                }
            }
            event.event._def.title = event.event._def.title.replaceAll(" (" + ressourceTitles.join(", ") + ")", "")

            if (PbEasy2schedule.instance.calendar.view.type.startsWith("dayGridMonth")) {
                if (event.event._def.allDay) {
                    event.el.style.border = "3px outset " + PbEasy2schedule.instance.adjustColor(event.event._def.ui.backgroundColor, -40)
                    event.el.style.opacity = 1
                    event.el.style.marginBottom = "3px";
                }
                if (ressourceTitles.length > 0) {
                    event.event._def.originalTitle = event.event._def.title
                    event.event._def.title = ressourceTitles.join(", ")
                }
            }
        }


        for (let [key, value] of Object.entries(event.event._def.extendedProps)) {
            if (key == "attachedRessources") {
                continue
            }

            if (key == "tags" && Array.isArray(value) && value.length > 0) {
                let tagString = "Tags: "
                for (let tagData of value) {
                    tagString += new PbTag({name: tagData?.id, value: tagData?.text}).short() + ", "
                }
                value = tagString.substring(0, tagString.length - 2);
            }

            if (value && value.length > 0) {
                let selector = "#" + key;
                if ($(selector).length != 0) {
                    if ($(selector).prop("tagName") == "SELECT") {
                        value = $(selector + " option[value='" + value + "']").text()
                    }
                }

                tooltip_content += value + "<br>"
                show_tooltip = true
            }
        }

        if (show_tooltip) {
            // http://qtip2.com
            $(event.el).qtip({
                hide: {
                    fixed: true,
                    delay: 100
                },
                content: {
                    text: tooltip_content.replace("%25", "@").replace("%40", "@")
                },
                position: {
                    at: 'top center',
                    my: "bottom center",
                    target: false
                },
                style: {
                    classes: 'qtip-dark qtip-shadow qtip-rounded'
                },
                events: {
                    show: function (event, api) {
                        api.reposition(event)
                    }
                }

            });
        }
    }


    adjustColor(color, amount) {
        return '#' + color.replace(/^#/, '').replace(/../g, color => ('0' + Math.min(255, Math.max(0, parseInt(color, 16) + amount)).toString(16)).substr(-2));
    }

    customize_event(event) {
        //MIM
        if ($.cookie("consumer_id") == "bf6b7cdf-af3a-4d4e-9450-eb71d2791da4" && event._def.extendedProps && event._def.extendedProps.ep_terminok && event._def.extendedProps.ep_terminok == 0) {
            event._def.rendering = ""
            event._def.ui.backgroundColor = "#555555"
            event._def.ui.textColor = "#990000"
        }
        return event

    }

    getLanguage() {
        let available_languages = ["de", "en"]
        let lang = navigator.languages ? navigator.languages[0] : navigator.language;
        let ISOA2 = lang.substr(0, 2);
        if (available_languages.includes(ISOA2)) {
            return ISOA2
        } else {
            return "de"
        }
    }

    getRessourceColumns(view_id) {
        if (document.getElementById("column_count_specific_css")) {
            document.getElementById("column_count_specific_css").parentNode.removeChild(document.getElementById("column_count_specific_css"))
        }
        var style = document.createElement('style');
        style.id = "column_count_specific_css"
        style.type = 'text/css';
        if (view_id == "timeGridDay") {
            //CROW-540 allow all day view events to be editable with complete resource list
            // view_id = "resourceTimeGridDay"

            //OFF
            //$(".fc-view-container").css("width", "36%")
            $("html").scrollLeft(0)
            //$('*[data-calendarview]').removeClass("active")
            //$("*[data-calendarview='timeGridDay']").addClass("active")
        } else {
            $(".fc-view-container").css("width", "100%")
        }
        if (PbEasy2schedule.instance.alternativResources[view_id]) {
            //sets a dark line between days in week view
            PbEasy2schedule.instance.columns_count = PbEasy2schedule.instance.alternativResources[view_id].length
            style.innerHTML = 'th:nth-child(' + PbEasy2schedule.instance.columns_count + 'n+1), td:nth-child(' + PbEasy2schedule.instance.columns_count + 'n+1) {border-right: 1px solid #7987a1;}';
            document.getElementsByTagName('head')[0].appendChild(style);

            return PbEasy2schedule.instance.alternativResources[view_id]
        } else {
            // Fetch all ressources since we need them for the dropdown anyway
            let allressources = []
            for (let a_ressource in PbEasy2schedule.instance.alternativResources) {
                if (
                    (a_ressource == "resourceTimeGridMultiDay" && new PbAccount().hasPermission("easy2schedule.views.rooms_week")) ||
                    (a_ressource == "resourceTimeGridMultiDay3" && new PbAccount().hasPermission("easy2schedule.views.categories_week")) ||
                    (a_ressource == "resourceTimeGridDay" && new PbAccount().hasPermission("easy2schedule.views.rooms_day")) ||
                    (a_ressource == "resourceTimeGridDay2" && new PbAccount().hasPermission("easy2schedule.views.categories_day"))
                ) {
                    allressources.push(...PbEasy2schedule.instance.alternativResources[a_ressource])
                }
            }
            return allressources
        }
    }

    sortToOthersResource() {
        if (!PbEasy2schedule.instance.calendar)
            return
        for (let event of PbEasy2schedule.instance.calendar.getEvents()) {
            event._def.resourceIds = PbEasy2schedule.instance.removeElementFromArray(event._def.resourceIds, "others")
            //event._def.resourceIds = removeElementFromArray(event._def.resourceIds, "eb")
            //console.log("AFTER",event._def.title, event._def.resourceIds)
            for (let id of event._def.resourceIds) {
                if (PbEasy2schedule.instance.isResourceOthers(id)) {
                    event._def.resourceIds.push("others")
                }

            }
        }
        PbEasy2schedule.instance.calendar.rerenderEvents()
    }

    removeElementFromArray(arrOriginal, elementToRemove) {
        return arrOriginal.filter(function (el) {
            return el !== elementToRemove
        });
    }

    isResourceOthers(resource_id) {
        if (resource_id == "others") {
            return false
        }
        for (let res of PbEasy2schedule.instance.resources) {
            if (resource_id == res.id) {
                return false
            }
        }
        return true
    }

    selectViewDate() {
        let renderRange = PbEasy2schedule.instance.calendar.state.dateProfile.renderRange
        let view_start = moment(renderRange.start)

        PbEasy2schedule.instance.addDatepickerToInput("#view_date_input", PbEasy2schedule.instance.dateFormat, view_start.startOf("day"))
        $('#view_date_input').on('dp.change', function (e) {
            PbEasy2schedule.instance.setViewDate($('#view_date_input').val());
            $("#view_date_input").datetimepicker("hide")
            $('#view_date_modal').modal('hide');
        })
        $("#view_date_modal").modal('show', function () {
            $("#view_date_input").datetimepicker("show")
        })
    }

    setViewDate(date) {

        PbEasy2schedule.instance.renderRange = PbEasy2schedule.instance.calendar.state.dateProfile.renderRange
        let view_start = moment(PbEasy2schedule.instance.renderRange.start)
        let view_end = moment(PbEasy2schedule.instance.renderRange.end)
        let current_view_date = moment(PbEasy2schedule.instance.calendar.getDate())
        let new_view_date = moment.utc(date, PbEasy2schedule.instance.dateFormat)

        console.log("DATE", new_view_date.startOf('day'), current_view_date.startOf('day'))
        let diff = new_view_date.startOf('day').diff(current_view_date.startOf('day'), 'days')
        if (diff < 0)
            diff--;

        let back = 1000
        let forward = diff + back
        console.log("Diff between dates: ", diff)

        PbEasy2schedule.instance.calendar.incrementDate({day: -back})
        //calendar.incrementDate({day: forward})
        PbEasy2schedule.instance.calendar.gotoDate(new_view_date.toDate())
    }

    setActiveClassForMenu(clickedElement) {
        $('*[data-calendarview]').removeClass("active")
        let view = $(clickedElement).attr("data-calendarview")
        $("*[data-calendarview='" + view + "']").addClass("active")
    }

    fixMonthViewHeight() {
        setTimeout(function () {
            let viewmode = $.cookie("e2s_menue_mode")
            if (PbEasy2schedule.instance.calendar.view.type.startsWith("dayGridMonth")) {
                if (viewmode == "full")
                    $("#calendar").css('width', 'calc(100% - 219px)');
                else
                    $("#calendar").css('width', 'calc(100% - 75px)');
                PbEasy2schedule.instance.calendar.setOption('height', PbEasy2schedule.instance.calendar.getOption('height'));
            } else if (
                PbEasy2schedule.instance.calendar.view.type.startsWith("resourceTimeGridDay") ||
                PbEasy2schedule.instance.calendar.view.type.startsWith("timeGridDay") ||
                PbEasy2schedule.instance.calendar.view.type.startsWith("list") ||
                PbEasy2schedule.instance.calendar.view.type.startsWith("resourceTimeline")
            ) {
                if (viewmode == "full")
                    $("#calendar").css('width', 'calc(100% - 219px)');
                else
                    $("#calendar").css('width', 'calc(100% - 75px)');

            } else {
                $("#calendar").removeAttr('style');
            }
        }, 200)
    }

    filter_events(e, search = "") {
        if (search && search != "") {
            $("#filter_text").val(getQueryParam("s"))
        }
        //console.log("Filter", PbEasy2schedule.instance.calendar.getEvents())
        for (let event of PbEasy2schedule.instance.calendar.getEvents()) {
            PbEasy2schedule.instance.filter_event(event)
        }
        PbEasy2schedule.instance.calendar.rerenderEvents()
    }

    filter_event(event, search = null) {

        if (search == null && document.getElementById("filter_text") != null) {
            search = document.getElementById("filter_text").value
        }

        let hidden = false
        // if still null, we have no search going on
        if (search == null) {
            return hidden;
        }

        if (search == "") {
            event._def.ui.classNames = ["defaultEvent"]

        } else {
            let splitted = search.split(" ")
            let cnt = 0
            for (search of splitted) {
                search = search.trim()
                if (cnt > 0 && search.length < 2) {
                    continue
                }
                cnt++
                let contacts_string = ""
                if (event._def.extendedProps.attachedContacts) {
                    for (let contact of event._def.extendedProps.attachedContacts) {
                        ContactManager.instance.setActiveContactById(contact)
                        if (ContactManager.instance.active_contact) {
                            contacts_string += JSON.stringify(ContactManager.instance.active_contact.data)
                        }
                    }
                }

                if (hidden || (event._def.title.toUpperCase().indexOf(search.toUpperCase()) == -1
                    && event._def.extendedProps.comment?.toUpperCase().indexOf(search.toUpperCase()) == -1
                    && JSON.stringify(event._def.extendedProps.attachedRessources).toUpperCase().indexOf(search.toUpperCase()) == -1
                    && JSON.stringify(event._def.extendedProps.attachedContacts).toUpperCase().indexOf(search.toUpperCase()) == -1
                    && contacts_string.toUpperCase().indexOf(search.toUpperCase()) == -1
                    && (!event._def.extendedProps.tags || JSON.stringify(event._def.extendedProps.tags).toUpperCase().indexOf(search.toUpperCase()) == -1)
                    && (!event._def.extendedProps.data_owners || JSON.stringify(event._def.extendedProps.data_owners).toUpperCase().indexOf(search.toUpperCase()) == -1)
                )) {
                    event._def.ui.classNames = ["hiddenEvent"]
                    hidden = true
                } else {
                    event._def.ui.classNames = ["defaultEvent"]
                }

                if (search.toUpperCase().includes("RAUM:")) {
                    let search_term = search.toUpperCase().replace("RAUM:", "")

                    for (let ressourceId of event._def.resourceIds) {
                        console.log("RessourceID", ressourceId.toUpperCase(), "SearchTERM", search_term)
                        if (ressourceId.toUpperCase().indexOf(search_term) != -1) {
                            console.log("ROOMSEARCH FOUND")
                            event._def.ui.classNames = ["defaultEvent"]
                            hidden = false || hidden
                        } else {
                            console.log("ROOMSEARCH NOT FOUND")
                            event._def.ui.classNames = ["hiddenEvent"]
                            hidden = true
                        }

                    }
                }
            }
        }

        return hidden
    }

    checkForEventTimesNotInView() {
        PbEasy2schedule.instance.calendarCustomStarttime = PbEasy2schedule.instance.calendarStarttime
        for (event of PbEasy2schedule.instance.calendar.getEvents()) {
            if (event._def.allDay != true) {
                let eventStart = event.start.toLocaleTimeString()
                let eventStartTime = new Date()

                eventStartTime = eventStartTime.setHours(eventStart.split(":")[0], eventStart.split(":")[1], eventStart.split(":")[2]);
                let minTime = new Date()
                minTime.setHours(PbEasy2schedule.instance.calendar.getOption("minTime").split(":")[0], PbEasy2schedule.instance.calendar.getOption("minTime").split(":")[1], PbEasy2schedule.instance.calendar.getOption("minTime").split(":")[2])

                if (eventStartTime < minTime) {
                    if (eventStart.split(":")[0] < PbEasy2schedule.instance.calendarCustomStarttime.split(":")[0]) {
                        PbEasy2schedule.instance.calendarCustomStarttime = (eventStart.split(":")[0] - 2) + ":00:00"
                        PbEasy2schedule.instance.useCustomStarttime = true

                        PbEasy2schedule.instance.calendar.setOption("minTime", PbEasy2schedule.instance.calendarCustomStarttime)
                        PbEasy2schedule.instance.calendar.refetchEvents()
                    }
                }
            }
        }
    }

    handleKeyPress(e) {
        var evtobj = e
        var keyCode = e.key;
        //console.log(evtobj.keyCode)
        if (keyCode == "Delete" && PbEasy2schedule.instance.selectedEvent != null) {
            PbEasy2schedule.instance.deleteEvent(PbEasy2schedule.instance.selectedEvent);
        }
        if (keyCode == "Escape" && $("#overlay").is(":visible")) {
            $("#overlay").toggle()
        }
        if (evtobj.keyCode == 78 && evtobj.altKey && !$("#overlay").is(":visible")) {
            PbEasy2schedule.instance.setFormFields()
        }
        if (keyCode == "F1") {
            if ($("#overlay").is(":visible")) {
                PbEasy2schedule.instance.showHelp()
                $('#legend_wrapper').hide("slide")
            } else {
                $('#legend_wrapper').toggle("slide");
            }
        }
    }

    touchstart(e) {
        timer = setTimeout(onlongtouch.bind(null, e), new PbEasy2schedule().touchduration);
    }

    touchend() {
        //stops short touches from firing the event
        if (new PbEasy2schedule().timer) {
            clearTimeout(new PbEasy2schedule().timer); // clearTimeout, not cleartimeout..
        }
    }

    click_doublick_switch(info, click_function = null, doubleclick_function = null) {
        if (PbEasy2schedule.instance.isClicked) {
            PbEasy2schedule.instance.isDblClicked = true;
            PbEasy2schedule.instance.isClicked = false;
        } else {
            PbEasy2schedule.instance.isClicked = true;
            PbEasy2schedule.instance.isDblClicked = false
            setTimeout(function () {
                if (PbEasy2schedule.instance.isClicked) {
                    click_function(info)
                }
                if (PbEasy2schedule.instance.isDblClicked) {
                    doubleclick_function(info)
                }
                PbEasy2schedule.instance.isClicked = false;
                PbEasy2schedule.instance.isDblClicked = false;
            }, 250);
        }

    }

    handleCalendarClick(info) {
        PbEasy2schedule.instance.unselectEvents();
    }

    unselectEvents() {
        let events = document.getElementsByClassName("fc-event")
        for (let singleEvent of events) {
            singleEvent.style.border = "none";
        }
        PbEasy2schedule.instance.selectedEvent = null;
    }

    handleEventClick(event) {
        console.log("EventClick");
        if (event.event._def.resourceIds.includes("others")) {
            //alert("Dieser Eintrag kann in dieser Ansicht nicht geändert werden. Bitte verwenden Sie eine andere Ansicht.")
            return;
        }
        PbEasy2schedule.instance.unselectEvents();
        PbEasy2schedule.instance.selectEvent(event);
        PbEasy2schedule.instance.jsevent = event.jsEvent
        //if(true) //This works only for chrome on android, but not safari || typeof(jsevent.sourceCapabilities) != "undefined" && typeof(jsevent.sourceCapabilities.firesTouchEvents) != "undefined" && jsevent.sourceCapabilities.firesTouchEvents == true)
        //{
        //    setFormFields("appointment", event);
        //}
        //Do not handle single clicks on events;
        //setFormFields("appointment", event);
    }

    handleEventDoubleClick(event) {
        PbEasy2schedule.instance.selectEvent(event)
        //CROW-540 exclude other column check for list view so that all events can be opened
        if (event.event._def.resourceIds.includes("others") && !PbEasy2schedule.instance.calendar.view.type.startsWith("list")) {
            alert("Dieser Eintrag kann in dieser Ansicht nicht geändert werden. Bitte verwenden Sie eine andere Ansicht.")
            return;
        }
        if (event.event._def.extendedProps.readonly && event.event._def.extendedProps.creator_login != getCookie("username")) {
            alert("Dieser Eintrag kann nicht geändert werden.")
            return;
        }

        if (!PbAccount.instance.hasPermission("easy2schedule.events.read")) {
            showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum öffnen dieses Eintrags."))
            return;
        }

        PbEasy2schedule.instance.setFormFields("appointment", event)
    }

    selectEvent(event) {
        event.el.style.border = "1px solid black"
        PbEasy2schedule.instance.selectedEvent = event
    }

    handleCalendarDoubleClick(info) {
        if (PbAccount.instance.hasPermission("easy2schedule.events.add")) {
            PbEasy2schedule.instance.setFormFields("appointment", info)
        } else {
            showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum hinzufügen eines Eintrags."))
            return;
        }

    }

    handleSelectRange(event) {
        if (PbAccount.instance.hasPermission("easy2schedule.events.add")) {
            PbEasy2schedule.instance.setFormFields("appointment", event);
        } else {
            showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum hinzufügen eines Eintrags."))
            return;
        }
    }

    edit_event(event) {
        if (!PbAccount.instance.hasPermission("easy2schedule.events.read")) {
            showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum öffnen dieses Eintrags."))
            return;
        }
        if (!PbAccount.instance.hasPermission("easy2schedule.events.update")) {
            showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum ändern dieses Eintrags."))
            return;
        }

        if (!event) {
            event = PbEasy2schedule.instance.selectedEvent
        }
        if (event == null) {
            alert("Kein Termin ausgewählt")
            return
        }

        PbEasy2schedule.instance.setFormFields('appointment', event)
    }

    getMyRemindersOfEvent(event_id) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/cb/my_event_reminders/" + event_id,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (result) => resolve(result),
                "error": (result) => console.log("RESULT", result)
            };

            $.ajax(settings)
        })
    }

    addNewReminderForm() {
        let reminder_set = $("#reminder_data_set_template").children().first().clone()
        let reminder_id = "new_" + makeId(20)
        reminder_set.get(0).dataset.reminderId = reminder_id
        reminder_set.find('[name="reminder-mode"] option').removeAttr('selected')

        // This is a piece of shit select-box replacement since it's to stupid to have a watch
        // on the original select whether something is changed programatically there no matter what
        //let mode_select = reminder_set.find('[name="reminder-mode"]')
        let mode_select = $('<select>')
        mode_select.addClass("form-control")
        mode_select.attr("id", "reminder_mode_select_" + reminder_id)

        mode_select.append($("<option>").attr('value', "gui").text("GUI"))
        mode_select.append($("<option>").attr('value', "email").text("Email"))
        reminder_set.find('*[data-reminder-container="mode_select"]').html(mode_select)
        mode_select.find('[value="gui"]').attr('selected', true)

        let time_amount_input = $('<input>')
        time_amount_input.addClass("form-control")
        time_amount_input.attr("id", "reminder_time_value_" + reminder_id)
        time_amount_input.attr("type", "number")
        time_amount_input.attr("min", "0")
        time_amount_input.attr("style", "width: 70px;")
        time_amount_input.val(10)
        reminder_set.find('*[data-reminder-container="time_amount"]').html(time_amount_input)

        let unit_select = $('<select>')
        unit_select.attr("id", "reminder_unit_select_" + reminder_id)
        unit_select.addClass("form-control")
        unit_select.append($("<option>").attr('value', "min").text("Minuten"))
        unit_select.append($("<option>").attr('value', "hour").text("Stunden"))
        unit_select.append($("<option>").attr('value', "day").text("Tage"))
        reminder_set.find('*[data-reminder-container="time_unit_select"]').html(unit_select)

        let delete_reminder_button = $('<button>')

        delete_reminder_button.attr("name", "reminder_delete_button")
        delete_reminder_button.addClass("btn btn-outline-secondary btn-icon")
        delete_reminder_button.attr("id", "reminder_delete_button_" + reminder_id)
        delete_reminder_button.append('<i class="typcn typcn-trash"></i>')
        delete_reminder_button.click(function () {
            $(`[data-reminder-id="${reminder_id}"]`).remove()
            return false;
        })
        reminder_set.find('*[data-reminder-container="delete_reminder_button"]').html(delete_reminder_button)


        $("#reminder_data_set").append(reminder_set)
    }

    deleteReminderById(reminder_id) {
        return new Promise(function (resolve, reject) {
            if (reminder_id.length == 0) {
                resolve()
            }
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/easy2scheduleDeleteReminder",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "reminder_id": reminder_id
                }),
                "success": resolve,
                "error": reject
            };

            $.ajax(settings)
        })
    }

    setRemindersForm(reminders) {
        $("#reminder_data_set").html("")
        for (let reminder of reminders) {
            let reminder_set = $("#reminder_data_set_template").children().first().clone()
            reminder_set.get(0).dataset.reminderId = reminder["reminder_id"]
            reminder_set.find('[name="reminder-mode"] option').removeAttr('selected')

            let mode_select = $('<select>')
            mode_select.addClass("form-control")
            mode_select.attr("id", "reminder_mode_select_" + reminder["reminder_id"])

            mode_select.append($("<option>").attr('value', "gui").text("GUI"))
            mode_select.append($("<option>").attr('value', "email").text("Email"))
            reminder_set.find('*[data-reminder-container="mode_select"]').html(mode_select)
            mode_select.find('[value="' + reminder["type"] + '"]').attr('selected', true)

            let time_amount_input = $('<input>')
            time_amount_input.addClass("form-control")
            time_amount_input.attr("id", "reminder_time_value_" + reminder["reminder_id"])
            time_amount_input.attr("type", "number")
            time_amount_input.attr("min", "0")
            time_amount_input.attr("style", "width: 70px;")

            reminder_set.find('*[data-reminder-container="time_amount"]').html(time_amount_input)

            let unit_select = $('<select>')
            unit_select.attr("id", "reminder_unit_select_" + reminder["reminder_id"])
            unit_select.addClass("form-control")
            unit_select.append($("<option>").attr('value', "min").text("Minuten"))
            unit_select.append($("<option>").attr('value', "hour").text("Stunden"))
            unit_select.append($("<option>").attr('value', "day").text("Tage"))
            reminder_set.find('*[data-reminder-container="time_unit_select"]').html(unit_select)

            let delete_reminder_button = $('<button>')
            delete_reminder_button.attr("name", "reminder_delete_button")
            delete_reminder_button.addClass("btn btn-outline-secondary btn-icon")
            delete_reminder_button.attr("id", "reminder_delete_button_" + reminder["reminder_id"])
            delete_reminder_button.append('<i class="typcn typcn-trash"></i>')
            delete_reminder_button.click(function () {
                let reminder_form = document.getElementById('reminder_form')
                Fnon.Box.Circle(reminder_form, '', {
                    svgSize: {w: '50px', h: '50px'},
                    svgColor: '#3a22fa',
                    background: 'rgba(255,255,255,0.8)',
                })

                PbEasy2schedule.instance.deleteReminderById(reminder["reminder_id"]).then(function () {
                    $('[data-reminder-id="' + reminder["reminder_id"] + '"]').remove()
                    Fnon.Box.Remove(reminder_form, 500)
                })
                return false;
            })
            reminder_set.find('*[data-reminder-container="delete_reminder_button"]').html(delete_reminder_button)

            let minutes = reminder["seconds_in_advance"] / 60
            let hours = minutes / 60
            let days = hours / 24

            reminder_set.find('[name="reminder-time-diff-units"] option').removeAttr('selected')
            if (days >= 1 && Number.isInteger(days)) {
                time_amount_input.val(days)
                unit_select.find('[value="day"]').attr('selected', true)
            } else if (hours >= 1 && Number.isInteger(hours)) {
                time_amount_input.val(hours)
                unit_select.find('[value="hour"]').attr('selected', true)
            } else {
                time_amount_input.val(minutes)
                unit_select.find('[value="min"]').attr('selected', true)
            }

            $("#reminder_data_set").append(reminder_set)
        }


    }

    createEmailMailToLink(formal = true, info) {
        let subject = ""
        let mail_body = ""
        let videosessionUrl = PbEasy2schedule.instance.getVideoSessionUrl(info.event._def.publicId)

        if (!formal) {
            subject = PbTpl.instance.translate('VideosessionInviteSubjectPersonal');
            mail_body = PbTpl.instance.translate("VideosessionInvitePersonal")
        } else {
            subject = PbTpl.instance.translate('VideosessionInviteSubjectFormal');
            mail_body = PbTpl.instance.translate("VideosessionInviteFormal");
        }

        mail_body = mail_body
            .replace("[Datum]", moment(info.event._instance.range.start).utc().format("DD.MM.YYYY"))
            .replace("[Uhrzeit]", moment(info.event._instance.range.start).utc().format("HH:mm"))
            .replaceAll("[Link]", videosessionUrl);

        mail_body = encodeURIComponent(mail_body)
        subject = encodeURIComponent(subject)

        let mailtoLink = `mailto:?subject=${subject}&body=${mail_body}`;
        $("#easy2schedule_invitation_mail_button").attr("href", mailtoLink)
        window.location.href = mailtoLink;
    }

    getVideoSessionUrl(id) {
        return PbTpl.instance.translate("VideosessionBaseUrl").replace("[session_id]", id)
    }


    setFormFields(form_id, info = undefined, startdate = undefined, enddate = undefined, title = "") {


        //CROW-540 commented out block to allow all events to be viewed in calendar view

        // if(PbEasy2schedule.instance.calendar.view.viewSpec.type == "dayGridMonth") {
        //     alert(new pbI18n().translate("In dieser Ansicht können Termine nicht editiert werden. Bitte wechseln sie zu einer anderen Ansicht und versuchen sie es erneut."))

        //     return
        // }

        var fields = PbEasy2schedule.instance.getEventFormElements()
        //$(fields["starttime"]).off()
        //$(fields["endtime"]).off()
        //$(fields["starttime"]).off()
        //$(fields["endtime"]).off()
        //$(fields["resource"]).off()
        //$(fields["colors"]).off()
        PbEasy2schedule.instance.toggleOverlay()
        PbEasy2schedule.instance.resetFormFields()

        if (document.getElementById('reminder_data_set'))
            document.getElementById('reminder_data_set').innerHTML = ""

        var starttime = "";
        var endtime = "";

        var title_input_value = title
        var timezoneoffset = 0;
        if (fields["save"]) {
            fields["save"].name = "create"
            fields["save"].value = "Anlegen"
        }
        $("#easy2schedule_goto_videomeeting_button").addClass("hidden")
        $("#easy2schedule_invitation_mail_button").addClass("hidden")
        $("#easy2schedule_invitation_mail_button").attr("href", "")
        // Check whether we shall open an existing even
        if (info && typeof (info.event) != "undefined") {
            $("#easy2schedule_goto_videomeeting_button").attr("href", PbEasy2schedule.instance.getVideoSessionUrl(info.event._def.publicId))
            $("#easy2schedule_invitation_mail_button").click((e) => {
                e.preventDefault()
                console.log("INFO", info.event._def)
                let name = info.event._def.title
                let summary = info.event._def.title
                let description = info.event._def.extendedProps.comment
                let location = PbEasy2schedule.instance.getVideoSessionUrl(info.event._def.publicId)
                let utcStartTimeMilliseconds = info.event._instance.range.start.getTime() + (info.event._instance.range.start.getTimezoneOffset() * 60000);
                let utcStartDate = new Date(utcStartTimeMilliseconds);
                let start = utcStartDate
                let utcEndTimeMilliseconds = info.event._instance.range.end.getTime() + (info.event._instance.range.end.getTimezoneOffset() * 60000);
                let utcEndDate = new Date(utcEndTimeMilliseconds);
                let end = utcEndDate

                new PbNotifications().ask(
                    'Möchtest du eine förmliche (Sie) oder eine lockere (Du) Vorlage verwenden?',
                    "Vorlage wählen",
                    () => {
                        PbEasy2schedule.instance.createEmailMailToLink(true, info)
                        new PbIcs().downloadIcs(name, summary, description, location, new Date(start), new Date(end))
                    },
                    () => {
                        PbEasy2schedule.instance.createEmailMailToLink(false, info)
                        new PbIcs().downloadIcs(name, summary, description, location, new Date(start), new Date(end))
                    },
                    "warning",
                    "Förmlich",
                    "Locker")
            })
            if (PbAccount.instance?.accountProfileData?.jitsi?.hostname) {
                $("#easy2schedule_invitation_mail_button").removeClass("hidden")
                $("#easy2schedule_goto_videomeeting_button").removeClass("hidden")
            }

            let documentation_url = `${CONFIG.APP_BASE_URL}/documentation/index.html?sid=easy2schedule|event|${info.event._def.publicId}`
            $("#documentation_link").attr("href", documentation_url)
            $("#documentation_link").show()
            let reminder_form = document.getElementById('reminder_form')
            Fnon.Box.Circle(reminder_form, '', {
                svgSize: {w: '50px', h: '50px'},
                svgColor: '#3a22fa',
                background: 'rgba(255,255,255,0.8)',
            })
            PbEasy2schedule.instance.getMyRemindersOfEvent(info.event._def.publicId).then((result) => {
                PbEasy2schedule.instance.setRemindersForm(result);
                Fnon.Box.Remove(reminder_form)
            })

            let own_events_allowed = false
            if (PbAccount.instance.hasPermission("easy2schedule.events.read.own") && info.event._def.extendedProps.creator_login == getCookie("username")) {
                own_events_allowed = true
            }

            if (!own_events_allowed && !PbAccount.instance.hasPermission("easy2schedule.events.read") && !PbAccount.instance.hasPermission("easy2schedule.events.update")) {
                showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum anzeigen dieses Eintrags."))
                return;
            }

            if (info && info.event && info.event._def.extendedProps.readonly && info.event._def.extendedProps.creator_login != getCookie("username")) {
                alert("Dieser Eintrag kann nicht geändert werden.")
                return;
            }

            $("#copy").show()
            //Existing event
            if (info.event._def.extendedProps.comment) {
                $("#event_comment").val(info.event._def.extendedProps.comment)
            }
            fields["allday"].value = info.event._def.allDay
            timezoneoffset = info.event._instance.range.start.getTimezoneOffset();
            starttime = info.event._instance.range.start;
            endtime = info.event._instance.range.end;
            title_input_value = (!info.event._def.originalTitle) ? info.event._def.title : info.event._def.originalTitle
            fields["save"].name = "update";
            fields["save"].value = "Update";
            fields["event_id"].value = info.event._def.publicId;

            for (let option of fields["resource"]) {
                if (info.event._def.resourceIds.includes(option.value)) {
                    option.selected = true
                }
                if (info.event._def.resourceIds.includes("others")) {
                    document.getElementById("save").disabled = true
                } else {
                    document.getElementById("save").disabled = false
                }
            }
            if (!own_events_allowed && !PbAccount.instance.hasPermission("easy2schedule.events.update")) {
                document.getElementById("save").disabled = true
                document.getElementById("copy").disabled = true
            }
            let participants = []
            let participants_names = []
            let ressource_ids = []
            let attachedRessources = info.event._def.extendedProps.attachedRessources
            if (attachedRessources != null) {
                for (let res of attachedRessources) {
                    res = JSON.parse(res)
                    participants.push(res.id)
                    participants_names.push(res.name)
                    ressource_ids.push(res.ressource_id)
                }
            }
            for (let option of fields["colors"]) {
                option.selected = false
                if (participants.includes(JSON.parse(option.getAttribute("data-ressource")).id) ||
                    (JSON.parse(option.getAttribute("data-ressource")).name && participants_names.includes(JSON.parse(option.getAttribute("data-ressource")).name)) ||
                    (JSON.parse(option.getAttribute("data-ressource")).ressource_id && ressource_ids.includes(JSON.parse(option.getAttribute("data-ressource")).ressource_id))) {
                    option.selected = true
                }
            }
            for (let [key, value] of Object.entries(info.event._def.extendedProps)) {
                if (document.getElementById(key)) {
                    document.getElementById(key).value = value
                }
            }

            for (let option of document.getElementById("contacts")) {
                option.selected = false
                if (info.event._def.extendedProps.attachedContacts && info.event._def.extendedProps.attachedContacts.includes(option.dataset.contactId)) {
                    option.selected = true
                }
            }

            for (let option of document.getElementById("event_status")) {
                option.selected = false
                if (info.event._def.extendedProps.data.event_status && info.event._def.extendedProps.data.event_status.includes(option.dataset.event_statusid)) {
                    option.selected = true
                }
            }

            for (let option of document.getElementById("appointment_type")) {
                option.selected = false
                if (info.event._def.extendedProps.data.appointment_type && info.event._def.extendedProps.data.appointment_type.includes(option.dataset.event_typeid)) {
                    option.selected = true
                }
            }

            for (let option of document.getElementById("permissions")) {
                option.selected = false
                if (info.event._def.extendedProps.data_owners && info.event._def.extendedProps.data_owners.includes(option.dataset.accessrightid)) {
                    option.selected = true
                }
            }
            PbEasy2schedule.instance.renderTagListNew(info.event._def.extendedProps.tags)
        } else {
            $("#documentation_link").hide()
            $("#copy").hide()
            if (!info) {
                info = {
                    allDay: 0,
                    startStr: moment.utc().add(120, 'minutes'),
                    endStr: moment.utc().add(180, 'minutes')
                }
            }
            //New event
            fields["event_id"].value = "";
            fields["allday"].value = info.allDay

            if (typeof (info.startStr) != "undefined") {
                starttime = info.startStr;
            } else {
                starttime = info.dateStr;
            }
            if (typeof (info.endStr) != "undefined") {
                endtime = info.endStr;
            } else {
                endtime = moment.utc(info.dateStr).add(60, 'minutes');
            }
            let at_least_one_selected = false
            if (fields["resource"]) {
                for (let option of fields["resource"]) {
                    if (info.resource && info.resource._resource.id == option.value) {
                        at_least_one_selected = true
                        option.selected = true
                    }
                }
            }

            if (!at_least_one_selected) {
                console.log("Default-Ressource", fields["resource"])
                if (fields && fields["resource"] && fields["resource"][0]) {
                    fields["resource"][0].selected = true
                }
            }

            if (getCookie("data_owners")) {
                let owners = JSON.parse(decodeURIComponent(getCookie("data_owners")))
                for (let owner of owners) {
                    for (let option of document.getElementById("permissions")) {
                        if (option.value == owner) {
                            option.selected = true
                        }
                    }
                }
            } else {
                for (let option of document.getElementById("permissions")) {
                    if (option.value.startsWith("group.")) {
                        option.selected = true
                        break;
                    }
                }
            }

            if (getCookie("last_event_types_selection")) {
                let types = JSON.parse(decodeURIComponent(getCookie("last_event_types_selection")))
                for (let type of types) {
                    for (let option of document.getElementById("appointment_type")) {
                        if (option.value == type) {
                            option.selected = true
                        }
                    }
                }
            }

            if (getCookie("last_event_status_selection")) {
                let status = decodeURIComponent(getCookie("last_event_status_selection"))
                for (let option of document.getElementById("event_status")) {
                    if (option.value == status) {
                        option.selected = true
                    }
                }
            }
            if (getCookie("last_event_members_selection")) {
                let values = JSON.parse(decodeURIComponent(getCookie("last_event_members_selection")))
                for (let value of values) {
                    value = JSON.parse(value)
                    for (let option of document.getElementById("colors")) {
                        if (option.value == value.id) {
                            option.selected = true
                        }
                    }
                }
            }

            if (getCookie("last_event_tag_selection")) {
                let values = JSON.parse(decodeURIComponent(getCookie("last_event_tag_selection")));
                var all_tags = []
                for (let value of values) {
                    let tag ={"name": value.id || value.name, "value": value.text || value.value}
                    all_tags.push(tag)
                }
            }
            PbEasy2schedule.instance.renderTagListNew(all_tags)

            for (let option of document.getElementById("appointment_type")) {
                if ($(option).attr("selected") == "selected") {
                    option.selected = true
                }
            }
        }

        fields["starttime"].value = moment.utc(starttime).format(PbEasy2schedule.instance.datetimeFormat);
        if (startdate) {
            fields["starttime"].value = startdate
        }
        PbEasy2schedule.instance.addDatepickerToInput(fields["starttime"])
        fields["starttime"].readOnly = true
        let alldayEndtimeFixed = moment.utc(endtime)

        if (info?.event?._def?.allDay) {
            alldayEndtimeFixed = moment.utc(endtime).add(-1, 'days')
        }
        fields["endtime"].value = moment.utc(alldayEndtimeFixed).format(PbEasy2schedule.instance.datetimeFormat);
        if (enddate) {
            fields["endtime"].value = enddate
        }

        PbEasy2schedule.instance.addDatepickerToInput(fields["endtime"])
        fields["endtime"].readOnly = true
        if (fields["title"])
            fields["title"].value = title_input_value


        $(fields["starttime"]).on('dp.change', PbEasy2schedule.instance.show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
        $(fields["endtime"]).on('dp.change', PbEasy2schedule.instance.show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
        $(fields["starttime"]).on('change', PbEasy2schedule.instance.show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
        $(fields["endtime"]).on('change', PbEasy2schedule.instance.show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
        $(fields["resource"]).on('change', PbEasy2schedule.instance.show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))
        $(fields["colors"]).on('change', PbEasy2schedule.instance.show_collisions.bind(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"]))


        PbEasy2schedule.instance.show_collisions.call(info, fields["starttime"], fields["endtime"], fields["resource"], fields["colors"])

        $("#colors").dropdown({
            clearable: true,
        })


        $("select").dropdown({
            clearable: false
        })

        for (let select of $("select")) {
            for (let option of select.options) {
                if (option.selected) {
                    $("#" + select.id).dropdown('set selected', option.value);
                } else {
                    $("#" + select.id).dropdown('remove selected', option.value);
                }
            }
        }

        // Add a default-reminder if new event
        if (!info || typeof (info.event) == "undefined") {
            $("#new_reminder_button").click()
        }
    }

    show_collisions(starttime_node, endtime_node, ressource_node, participants_node, delayed_recheck = true) {
        $(".ERROR").removeClass("ERROR")
        let event = this
        let starttime = $(starttime_node).val()
        starttime = moment.utc(starttime, PbEasy2schedule.instance.datetimeFormat)
        let endtime = $(endtime_node).val()
        endtime = moment.utc(endtime, PbEasy2schedule.instance.datetimeFormat)
        let ressources = $(ressource_node).val()
        let participants = $(participants_node).val()
        starttime_node.removeAttribute('style')
        endtime_node.removeAttribute('style')
        if (ressource_node)
            ressource_node.removeAttribute('style')
        if (participants_node)
            participants_node.removeAttribute('style')

        if (!PbEasy2schedule.instance.calendar)
            return
        let allEvents = PbEasy2schedule.instance.calendar.getEvents()
        let starttimeconflicts = []
        let endtimeconflicts = []

        //console.log("THIS EVENT", this.event)

        for (let ev of allEvents) {
            if (typeof (event) != "undefined" && typeof (event.event) != "undefined") {
                // console.log("Comparing event_ids wheter same:", event.event._def.defId, ev._def.defId)
                if ((event.event && (event.event._def.defId == ev._def.defId)) || (event.event._def.title == ev._def.title)) {
                    //console.log("!!!SKIPPED!!!", event.event._def)
                    continue
                }
            }

            let evStarttime = moment.utc(ev._instance.range.start)
            let evEndtime = moment.utc(ev._instance.range.end)

            if (starttime.isBetween(evStarttime, evEndtime, null, '[)')) {
                starttimeconflicts.push(ev)
                //console.log("STARTTIME intersects with event", ev._def.title, evStarttime)
            }
            if (endtime.isBetween(evStarttime, evEndtime, null, '[)')) {
                endtimeconflicts.push(ev)
                //console.log("ENDTIME intersects with event", ev._def.title, evEndtime)
            }
            if (evStarttime.isBetween(starttime, endtime, null, '[)')) {
                starttimeconflicts.push(ev)
                //console.log("evSTARTTIME intersects with event", ev._def.title, evStarttime)
            }
            if (evStarttime.isBetween(starttime, endtime, null, '[)')) {
                endtimeconflicts.push(ev)
                //console.log("evENDTIME intersects with event", ev._def.title, evEndtime)
            }
        }
        let conflicts = {}
        if (ressource_node)
            $("#" + ressource_node.id + "_label").removeClass("ERROR")
        if (participants_node)
            $("#" + participants_node.id + "_label").removeClass("ERROR")

        if (starttimeconflicts.length > 0 || endtimeconflicts.length > 0) {
            for (let starttimeconflict of starttimeconflicts) {
                //console.log("starttimeconflict", starttimeconflict)
                let result = PbEasy2schedule.instance.getRealCollisionData(starttimeconflict, ressources, participants)

                if (Object.entries(result).length > 0) {
                    conflicts["starttime"] = result
                    starttime_node.style.border = "1px solid red"

                    if (result["withResources"]) {
                        $('*[data-value="' + result["withResources"] + '"]').addClass("ERROR")
                        if ($(ressource_node.parentNode).hasClass("selection ui dropdown")) {
                            $(ressource_node.parentNode).addClass("ERROR")
                            $(ressource_node.parentNode).click(function () {
                                $(ressource_node.parentNode).removeClass("ERROR")
                            })
                        }
                    }

                    if (result["withPersons"]) {
                        for (let error_value of result["withPersons"]) {
                            $('*[data-value="' + error_value + '"]').addClass("ERROR")
                            $(ressource_node.parentNode).click(function () {
                                $(ressource_node.parentNode).removeClass("ERROR")
                            })
                        }

                        for (let colidingParticipantOption of result["withPersons"]) {
                            for (let option of participants_node.options) {
                                if (colidingParticipantOption == option.value) {
                                    //$(option).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500);
                                }
                            }
                        }
                    }
                }
            }

            for (let endtimeconflict of endtimeconflicts) {
                //console.log("endtimeconflict", endtimeconflict)
                let result = PbEasy2schedule.instance.getRealCollisionData(endtimeconflict, ressources, participants)
                if (Object.entries(result).length > 0) {
                    conflicts["endtime"] = result
                    endtime_node.style.border = "1px solid red"

                    if (result["withResources"]) {
                        $('*[data-value="' + result["withResources"] + '"]').addClass("ERROR")
                        if ($(ressource_node.parentNode).hasClass("selection ui dropdown")) {
                            $(ressource_node.parentNode).addClass("ERROR")
                        }
                    }

                    if (result["withPersons"]) {
                        for (let error_value of result["withPersons"]) {
                            $('*[data-value="' + error_value + '"]').addClass("ERROR")
                        }


                        for (let colidingParticipantOption of result["withPersons"]) {
                            for (let option of participants_node.options) {
                                //console.log("COLIDING OPTIONS", colidingParticipantOption, option.value)
                                if (colidingParticipantOption == option.value) {
                                    //$(option).fadeOut(500).fadeIn(500).fadeOut(500).fadeIn(500);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (delayed_recheck) {
            setTimeout(function () {
                PbEasy2schedule.instance.show_collisions.call(event, starttime_node, endtime_node, ressource_node, participants_node, false)
            }, 200)
        }

        //console.log("CONFLICTS", conflicts)
        return conflicts
    }

    getEventFormElements() {
        var elements = {
            starttime: document.getElementById("starttime"),
            endtime: document.getElementById("endtime"),
            title: document.getElementById("title"),
            resource: document.getElementById("resource"),
            save: document.getElementById("save"),
            event_id: document.getElementById("event_id"),
            colors: document.getElementById("colors"),
            allday: document.getElementById("allday"),
            event_status: document.getElementById("event_status"),
            appointment_type: document.getElementById("appointment_type")
        }
        return elements;
    }

    toggleOverlay() {
        if ($.cookie("e2s_show_extended_props") == "true") {
            $('#extended_props_wrapper').show();
        }
        if (document.getElementById("overlay").style.display == "block") {
            document.getElementById("overlay").style.display = "none";
        } else {
            document.getElementById("overlay").style.display = "block";
            $('.modal-body').scrollTop(0);
            $('.tab-label').removeClass("tx-indigo")
            $('.tab-label').addClass("text-muted")
            $('.tabs').removeClass("active")
            $('#tab01 .tab-label').removeClass('text-muted');
            $('#tab01 .tab-label').addClass('tx-indigo');
            $('#tab01').addClass("active")
            $("fieldset").removeClass("show")
            $('#tab01body').addClass("show");
        }
    }


    resetFormFields() {
        console.log("RESET FORM FIELDS")

        var fields = PbEasy2schedule.instance.getEventFormElements();
        for (let field in fields) {
            if (fields[field])
                fields[field].value = ""
        }

        let extended_props = document.querySelectorAll("[data-event-property='extended_prop']")
        for (let prop of extended_props) {
            if (prop.tagName == "SELECT" && prop.hasAttribute("data-default-value")) {
                let options = prop.childNodes
                for (let option of options) {
                    if (option.value == prop.getAttribute("data-default-value")) {
                        option.selected = true
                    }
                }
            } else {
                prop.value = ""
            }
        }
        if (document.getElementById("contacts")) {
            for (let option of document.getElementById("contacts")) {
                option.selected = false
            }
        }
        if (document.getElementById("permissions")) {
            for (let option of document.getElementById("permissions")) {
                option.selected = false
            }
        }
        if (document.getElementById("event_status")) {
            $('#event_status').val('pending')
        }

        if (document.getElementById("appointment_type")) {
            $('#appointment_type').val([])
        }

        $("#event_comment").val("")

        if (document.getElementById("tags")) {
            for (let option of document.getElementById("tags")) {
                option.selected = false
            }
        }

        $("#series-container").hide()

    }

    /* getEventFormElements() {
        var elements = {
            starttime: document.getElementById("starttime"),
            endtime: document.getElementById("endtime"),
            title: document.getElementById("title"),
            resource: document.getElementById("resource"),
            save: document.getElementById("save"),
            event_id: document.getElementById("event_id"),
            colors: document.getElementById("colors"),
            allday: document.getElementById("allday"),
            event_status: document.getElementById("event_status")
        }
        return elements;
    } */

    addDatepickerToInput(element, format = PbEasy2schedule.instance.datetimeFormat, initialtime = null) {
        var datePickerObjectStarttime = $(element).data("DateTimePicker");
        if (typeof datePickerObjectStarttime !== "undefined") {
            // it's already been Initialize . Just update the date.
            if (initialtime) {
                datePickerObjectStarttime.date(initialtime);
            } else {
                datePickerObjectStarttime.date(element.value);
            }
        } else {
            // it hasn't been initialized yet. Initialize it with the date.
            $(element).datetimepicker({
                format: format,
                sideBySide: true,
                showClose: true,
                ignoreReadonly: true,
                debug: false,
                widgetPositioning: {horizontal: 'left', vertical: 'bottom'}
            });

            if (initialtime) {
                console.log("INITIALTIME", initialtime)
                $(element).data("DateTimePicker").date(initialtime)
            } else {
                $(element).data("DateTimePicker").date(element.value)
            }
        }
    }

    post(url, data, callback, retry_counter = 0) {
        if (data.event_id == "") {
            setCookie("data_owners", JSON.stringify(data.data_owners), {expires: 3650, path: '/'})
            setCookie("last_event_types_selection", JSON.stringify(data.data.appointment_type), {expires: 3650, path: '/'})
            setCookie("last_event_status_selection", data.data.event_status, {expires: 3650, path: '/'})
            setCookie("last_event_members_selection", JSON.stringify(data.attachedRessources), {expires: 3650, path: '/'})
            setCookie("last_event_tag_selection", JSON.stringify(data.tags), {expires: 3650, path: '/'})
        }
        var data = JSON.stringify(data);

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                if (this.status == 200 || this.status == 204) {
                    callback(this.responseText);
                } else {
                    //PbEasy2schedule.instance.calendar.refetchEvents();
                    if (retry_counter < 50) {
                        window.setTimeout(() => {
                            PbEasy2schedule.instance.post(url, JSON.parse(data), callback, retry_counter + 1)
                        }, 200)
                    }
                }
            }
        });

        xhr.open("POST", url);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("apikey", getCookie("apikey"));
        xhr.send(data);
    }

    deleteEvent(event = null) {
        if (event == null) {
            event = PbEasy2schedule.instance.selectedEvent
        }
        if (!event) {
            alert("Bitte wählen Sie den Termin erst aus (Antippen)")
        } else {
            let own_events_allowed = false
            if (PbAccount.instance.hasPermission("easy2schedule.events.delete.own") && event.event._def.extendedProps.creator_login == getCookie("username")) {
                own_events_allowed = true
            }

            if (!own_events_allowed && !PbAccount.instance.hasPermission("easy2schedule.events.delete")) {
                showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum löschen dieses Eintrags."))
                return;
            }


            if (!confirm("Wollen Sie den ausgewählten Termin löschen?")) {
                return
            }

            if (event.event._def.extendedProps.readonly && selectedEvent.event._def.extendedProps.creator_login != getCookie("username")) {
                alert("Sie können diesen Eintrag nicht löschen.")
            } else {
                $(event.el).tooltip("hide")
                let selectedEventId = event.event._def.publicId;
                let url = CONFIG.API_BASE_URL + "/es/cmd/deleteAppointment";
                let data = {
                    "event_id": selectedEventId
                }
                PbEasy2schedule.instance.post(url, data, function (response) {
                    event.event.remove()
                    //calendar.refetchEvents();
                    PbEasy2schedule.instance.selectedEvent = null;
                });
            }
        }
    }

    deleteEventById(event_id) {
        let url = CONFIG.API_BASE_URL + "/es/cmd/deleteAppointment";
        let data = {
            "event_id": event_id
        }
        PbEasy2schedule.instance.post(url, data, function (response) {
            event.event.remove()
            //calendar.refetchEvents();
            PbEasy2schedule.instance.selectedEvent = null;
        });

    }

    updateEvent() {
        let own_events_allowed = false
        if (PbAccount.instance.hasPermission("easy2schedule.events.update.own") && event.event._def.extendedProps.creator_login == getCookie("username")) {
            own_events_allowed = true
        }

        if (!own_events_allowed && !PbAccount.instance.hasPermission("easy2schedule.events.update.all")) {
            showNotification(new pbI18n().translate("Sie haben nicht die notwendige Berechtigung zum ändern dieses Eintrags."))
            return;
        }
        var fields = PbEasy2schedule.instance.getEventFormElements()

        let url = CONFIG.API_BASE_URL + "/es/cmd/changeAppointment";
        let data = {
            id: fields["id"].value,
            resourceId: fields["resource"].options[fields["resource"].selectedIndex].value,
            textColor: fields["colors"].options[fields["colors"].selectedIndex].value.split("||")[0],
            color: fields["colors"].options[fields["colors"].selectedIndex].value.split("||")[1],
            starttime: moment.utc(fields["starttime"].value, datetimeFormat).format(mysqldatetimeFormat),
            endtime: moment.utc(fields["endtime"].value, datetimeFormat).format(mysqldatetimeFormat),
            title: fields["title"].value,
            savekind: fields["save"].name
        }
        PbEasy2schedule.instance.post(url, data, function (response) {
            PbEasy2schedule.instance.toggleOverlay()
        });
        evt.preventDefault();
    }

    showHelp() {
        $('#legend_wrapper').hide("slide")
        if (document.getElementById("extended_form_help")) {
            $("#custom_help_wrapper").html(document.getElementById("extended_form_help").innerHTML)
        }
        $("#helpFormWrapper").toggle("slide")
    }

    getRealCollisionData(event, ressource, participants) {
        let conflicts = {}
        if (participants == null)
            participants = []
        if (event._def.resourceIds.includes(ressource)) {
            conflicts["withResources"] = ressource
        }

        let intersection_result = participants.filter(value => event._def.extendedProps.attachedRessources.join("-").includes(value))

        if (intersection_result.length > 0) {
            conflicts["withPersons"] = intersection_result
        }
        return conflicts
    }

    make_event_unique() {
        let hits = []
        if (!PbEasy2schedule.instance.calendar)
            return
        let allEvents = PbEasy2schedule.instance.calendar.getEvents()
        for (let event of allEvents) {
            for (let ev of allEvents) {

                if (ev._def.publicId == event._def.publicId) {
                    if (hits.includes(event._def.publicId)) {
                        ev.remove()
                        PbEasy2schedule.instance.calendar.refetchEvents()
                    }
                    hits.push(ev._def.publicId)
                }
            }
        }

    }

    toggleExtendedProps(event) {
        console.log("EXTENDED PROPS TOGGLED", PbEasy2schedule.instance.userConfig)
        if (!PbEasy2schedule.instance.userConfig["readonly_user"]) {
            $('#extended_props_wrapper').toggle("slide", function () {
                $.cookie("e2s_show_extended_props", $('#extended_props_wrapper').is(':visible'), {expires: 365});
            });
        }
    }

    changeView(view_name) {
        PbEasy2schedule.instance.viewPreset = true
        PbEasy2schedule.instance.calendar.changeView(view_name)

        $("[data-calendarview]").removeClass("active")
        $(`[data-calendarview=${view_name}]`).addClass("active")
        let search = ""
        if (getQueryParam("s")) {
            search = "&s=" + getQueryParam("s")
        }
        toggleMenu($.cookie("e2s_menue_mode") || "full")
        window.history.replaceState(null, null, "?v=" + view_name + "&viewdate=" + PbEasy2schedule.instance.calendar.view.currentStart.toISOString() + search)
    }

    toggleMenu(mode, callback = function () {
    }) {
        $.cookie("e2s_menue_mode", mode);
        if (mode == "icons") {
            $("#calendar").animate({marginLeft: 74, complete: callback});
            $("#calendar").addClass("small_menu")
            $("#calendar").removeClass("wide_menu")
            $("#icon_menue").show("slideDown")
            $(".menue_icon_only").show("slideDown")
            $("#opened_menue").hide("slideUp")
            $("#menue_header_icon").hide("slideUp")
            $('#side_menu_wrapper').animate({width: 75, complete: callback});
            $(".fc-axis").addClass("small_menu")
            $(".fc-left").addClass("small_menu")
            $(".fc-axis").removeClass("wide_menu")
        }
        if (mode == "full") {
            $("#calendar").animate({marginLeft: 219, complete: callback});
            $("#calendar").removeClass("small_menu")
            $("#calendar").addClass("wide_menu")
            $(".fc-axis").addClass("wide_menu")
            $("#icon_menue").hide("slideUp")
            $(".menue_icon_only").hide("slideUp")
            $("#opened_menue").show("slideDown")
            $("#menue_header_icon").show("slideDown")
            $('#side_menu_wrapper').animate({width: 220}, {complete: callback});
            $(".fc-axis").removeClass("small_menu")
            $(".fc-left").removeClass("small_menu")
        }
        PbEasy2schedule.instance.fixMonthViewHeight()
    }

    renderTagList() {
        for (let [key, value] of Object.entries(PbEasy2schedule.instance.allEventData)) { //create array of tags
            if (value.tags != null) {
                for (let [new_key, new_value] of Object.entries(value.tags)) { //get values from tags
                    PbEasy2schedule.instance.tags_sorted.push({
                        "id": new_value.id,
                        "text": new_value.text
                    })

                    let temp_tags_sorted = PbEasy2schedule.instance.tags_sorted.filter( //find and filter duplicate ids
                        (tag, index) => index === PbEasy2schedule.instance.tags_sorted.findIndex(
                            other => tag.id === other.id
                        ));

                    PbEasy2schedule.instance.tags_sorted = temp_tags_sorted

                }
            }
        }
    }

    addTagsToTagList() {
        $(".select-tags").select2({
            data: PbEasy2schedule.instance.tags_sorted,
            tags: true,
            tokenSeparators: [',', ' '],
            selectOnClose: true
        })
    }

    setTags() {
        PbEasy2schedule.instance.renderTagListNew()
        // PbEasy2schedule.instance.fetchEntriesByTimes("2021-09-01T00:00:00", "2025-09-30T00:00:00").then(value => {
        //     PbEasy2schedule.instance.allEventData = value
        //     PbEasy2schedule.instance.renderTagList()
        //     PbEasy2schedule.instance.addTagsToTagList()
        // }).catch(err => {
        //     console.log(err);
        // });
    }

    initActions() {
    }

    renderTagListNew(selectedTags = []) {
        PbEasy2schedule.instance.notifications.blockForLoading("#tag_container")
        let self = this
        let tags = array_merge_distinct(PbEasy2schedule.instance.tags_sorted || [], $("#tags").val() || [])
        return new Promise((resolve, reject) => {
            PbEasy2schedule.instance.tagListModel.load(false, "easy2schedule").then(() => {
                for (let tag of tags) {
                    if (!tag) {
                        continue
                    }
                    if (typeof tag == "string") {
                        tag = new PbTag().fromShort(tag).getData()
                    }

                    tag = new PbTag({"name": tag.id || tag.name, "value": tag.text || tag.value})

                    PbEasy2schedule.instance.tagListModel.addTagToList(tag)
                }

                let selectedTagsReduced = []

                if (Array.isArray(selectedTags) && selectedTags.length > 0) {
                    selectedTags.map((val) => {
                        let tag = new PbTag({"name": val.id || val.name, "value": val.text || val.value})
                        PbEasy2schedule.instance.tagListModel.addTagToList(tag)
                        selectedTagsReduced.push(val.text || val.value)
                    })
                }

                let tagOptions = PbEasy2schedule.instance.tagListModel.getSelectOptions(selectedTagsReduced)

                PbTpl.instance.renderIntoAsync('csf-lib/pbapi/tagmanager/templates/tag-select2.tpl', {
                    "multiple": true,
                    "addButton": true,
                    "propertyPath": "tags",
                    "options": tagOptions
                }, "#tag_container").then(() => {
                    $('[data-propertyname="tags"]').select2({
                        tags: new PbAccount().hasPermission("easy2schedule.localtags.add"),
                        tokenSeparators: [',', ' '],
                        selectOnClose: false
                    })
                    $('[data-propertyname="tags"]').attr("disabled", false)

                    $("#addGlobalTagButton").click(() => {
                        PbNotifications.instance.ask(
                            '<div><input type="text" id="newGlobalTag" placeholder="" class="form-control"/></div>',
                            'New global tag',
                            () => {
                                PbEasy2schedule.instance.tagListModel.addTagToList(new PbTag().fromShort($("#newGlobalTag").val()).setUsableFor(["easy2schedule"]), true)
                            })
                        $("#newGlobalTag").focus()
                    })
                    PbEasy2schedule.instance.notifications.unblock("#tag_container")
                    resolve()
                })
            })
        })
    }

    getEventmanagerStatistic(start_date = null, end_date = null) {
        try {
            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": CONFIG.API_BASE_URL + `/cb/statistic/entries?start=${start_date}&end=${end_date}`,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": getCookie("apikey")
                    },
                    "success": function (response) {
                        resolve(response)
                    }.bind(this),
                    "error": function (response) {
                        reject(response)
                    }
                };
                $.ajax(settings)
            }.bind(this))
        } catch (e) {
            this.onCommandError(e)
        }
    };

    initListeners() {
        document.addEventListener("DOMContentLoaded", function (event) {
            //get_translations()
            //document.addEventListener('readyforcalendarload', runCalendar)
            if (document.getElementById('appointment')) {
                document.getElementById('appointment').addEventListener("submit", PbEasy2schedule.instance.fnAppointmentsubmit, false);
                document.getElementById('appointment').addEventListener('submit', event => {
                    //$("#tag_container").html('<select name="tags" id="tags" class="select-tags form-control" multiple="multiple"></select>') //recreate select2 dropdown because of display errors
                    PbEasy2schedule.instance.setTags()
                });
            }
            document.addEventListener("keydown", PbEasy2schedule.instance.handleKeyPress, false);
            window.addEventListener("touchstart", PbEasy2schedule.instance.touchstart, false);
            window.addEventListener("touchend", PbEasy2schedule.instance.touchend, false);
            if (document.getElementById('filter_text'))
                document.getElementById('filter_text').addEventListener("keyup", PbEasy2schedule.instance.filter_events, false);

            document.addEventListener("news_available", function (news_data) {
                renderNews(news_data)
            }, false);
            get_news_for("easy2schedule")
            get_news_for("easy2track")

        });

        $('a.fc-time-grid-event').on('click', function () {
            PbEasy2schedule.instance.checkSeriesDropdown()
        })

        $('#save').on('click', function (event) {
            if ($('#series-container').is(":visible")) {
                let interval = PbEasy2schedule.instance.getInterval()
                console.log('Interval on clock', interval)
                PbEasy2schedule.instance.createAppointmentSeries(PbEasy2schedule.instance.calculateDates(interval))
            }

        })
    }

}