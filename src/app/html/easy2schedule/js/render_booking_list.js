import {isEmail, handleGUIbyidError} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {getCookie} from '../../csf-lib/pb-functions.js?v=[cs_version]';
import { NotificationsFilter } from "./notifications-filter.js?v=[cs_version]"
import {ColumnSettings} from './column-settings.js?v=[cs_version]'
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbEasy2schedule} from './pb-easy2schedule.js?v=[cs_version]';
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"

export class RenderBookingList {
    constructor(usersetting) {
        if (!RenderBookingList.instance) {
            RenderBookingList.instance = this
            RenderBookingList.instance.init()
        }
        RenderBookingList.instance.usersetting = usersetting
        RenderBookingList.instance.nf = new NotificationsFilter()
        RenderBookingList.instance.current_view_data = moment().startOf('day')
        return RenderBookingList.instance
    }

    init() {
        $("#searchButton").on('click', function(e) {
            renderSearchResults()
            searchMode()
            $('#searchModal').modal('toggle');
            
        })
        
        $("#previousDay").on('click', function(e) {
            RenderBookingList.instance.changeViewDate(-1)

        })
        
        $("#nextDay").on('click', function(e) {
            RenderBookingList.instance.changeViewDate(1)
        })
        
        $("#currentDay").on('click', function(e) {
            RenderBookingList.instance.changeViewDate(moment().startOf('day'))
        })

        $('[data-selector="list_view"]').on('click', function(e) {
            listMode()
        })
        $(document).on('click', '[data-edit-item-id]', function (e) {
            alert("Diese Funktion ist noch nicht implementiert")
        })
        

        $(document).on('click', '[data-delete-item-id]', function (e) {
            let target = e.currentTarget
            let id = target.dataset.deleteItemId

            let cancelTitle = new pbI18n().translate("Buchung löschen.")
            let cancelQuestion = new pbI18n().translate("Möchten Sie wirklich die Buchung löschen?")
                Fnon.Ask.Warning(
                  cancelQuestion,
                  cancelTitle,
                  "Ja, löschen",
                  "Abbrechen",
                  (result) => {
                    if (result == true) {

                        RenderBookingList.instance.deleteEventById(id)
                       
     
                    }
                  }
                )


        })
    }

    changeViewDate(days) {
        
        if(Number.isInteger(days)) {
            RenderBookingList.instance.current_view_data = RenderBookingList.instance.current_view_data.add(days, 'days')
        } else {
            RenderBookingList.instance.current_view_data = days
        }
        
        let startMoment = RenderBookingList.instance.current_view_data.startOf('day')
        let start = startMoment.format(PbEasy2schedule.instance.mysqldatetimeFormat)
        let endMoment = RenderBookingList.instance.current_view_data.endOf('day')
        let end = endMoment.format(PbEasy2schedule.instance.mysqldatetimeFormat)
        
        PbEasy2schedule.instance.fetchPersonalEntriesByTimes(start, end).then((entries) => {
            RenderBookingList.instance.renderBookingEntries(entries, startMoment);
        })
    }

    deleteEventById(id) {
        
        
        let url = CONFIG.API_BASE_URL + "/es/cmd/deleteAppointment";
        let data = {
            "event_id": id
        }
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": url,
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "Content-Type": "application/json",
                  "apikey":  getCookie("apikey")
                },
                "data": JSON.stringify(data),
                "success": (response) => {resolve(response)},
                "error": () => reject(),
              };
              

            $.ajax(settings)
        })    
    }   

    listMode() {
        $("#search-view").hide()
        $("#list-view").show()
    }
    
    searchMode() {
        $("#search-view").show()
        $("#list-view").hide()
    }
    
    timelineMode() {
        
    }
    
    extractTimeFromDate(date) {
            const d = new Date(date);
            let time =  (("0" + d.getHours().toString()).slice(-2)) + ":" + (("0" + d.getMinutes().toString()).slice(-2))
            return time
    }
    
    renderSearchResults() {
        search_data
        $("#search-list-container").html("")
    
        //render search results item within day
        for (let entry of search_data) {
            let newListItem = $("#search_item").html()
            let listItemContent = newListItem.replaceAll("[{resouce-id}]", entry.title )
            $("#search-list-container").append(listItemContent)
        }
    }

    getIdToTitleMap() {
        const column_config = new ColumnSettings().columns
        let column_set = []
        for (let view in column_config) {
            for (let ressource of column_config[view]) {
                column_set[ressource.id] = ressource
            }
        }
        return column_set
    }
    
    renderBookingEntries(entries, startdate) {
        new ColumnSettings().init().then(() => {
          let column_set = RenderBookingList.instance.getIdToTitleMap()

          moment.locale("de")
          var curDateSelected = startdate.format("ll")
          var curDate = startdate.format("Do")
          var curDay = startdate.format("dd")

          $("#list_display_date_range").html(curDateSelected)
          $("#list-table").html("")

          //first render container for day
          let newDayList = $("#list_day_container").html()
          let dayListContent = newDayList
            .replaceAll("[{week-day}]", curDay)
            .replace("[{day}]", curDate)
          $("#list-table").append(dayListContent)

          //then render booking items within day
          if (entries.length == 0) {
            let newListItem = $("#list_item_empty").html()
            $('[data-selector="list-item-container-' + curDay + '"]').append(
              newListItem
            )
          } else {
            for (let entry of entries) {
              let newListItem = $("#list_item").html()
              let listItemContent = newListItem
                .replace(
                  "[{start-time}]",
                  RenderBookingList.instance.extractTimeFromDate(
                    entry.starttime
                  )
                )
                .replace(
                  "[{end-time}]",
                  RenderBookingList.instance.extractTimeFromDate(entry.endtime)
                )
                .replace(
                  "[{resouce-id}]",
                  column_set[entry.resourceId]
                    ? column_set[entry.resourceId].title
                    : entry.resourceId
                )
                .replaceAll("[{event-id}]", entry.correlation_id)
              $('[data-selector="list-item-container-' + curDay + '"]').append(
                listItemContent
              )
            }
          }
        })
    }
}
