import {PbTpl} from "/csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "/csf-lib/pb-notifications.js?v=[cs_version]"
import {pbI18n} from "/csf-lib/pb-i18n.js?v=[cs_version]"
import {PbEventStatus} from "/csf-lib/pbapi/calendar/pb-event-status.js?v=[cs_version]"


export class Render {
    constructor() {
        if (!Render.instance) {
            Render.instance = this
            Render.datatable = undefined
            Render.instance.notifications = new PbNotifications()
            Render.instance.handledTaskIds = []
            Render.instance.i18n = new pbI18n()
            Render.instance.tpl = new PbTpl()
            Render.instance.handledTaskIds = []
        }

        return Render.instance
    }

    init(ModuleInstance) {
        document.addEventListener("notification", function (event) {
            if (this.handledTaskIds.includes(event.data.data.correlation_id)) {
                return
            }

            if (event.data.data.event == "appointmentStatusRemoved") {
                this.handledTaskIds.push(event.data.data.correlation_id)
                this.renderStart()
            }
            if (event.data.data.event == "appointmentStatusSet") {
                this.handledTaskIds.push(event.data.data.correlation_id)
                this.renderStart()
            }
        }.bind(this))

        $("#add_column_button").click(function () {
            this.openFieldTypeEditor(undefined, {})
        }.bind(this))

        this.moduleInstance = ModuleInstance
        return this.renderStart().then(() => {
            $("#loader-overlay").hide()
        })
    }

    renderStart() {
        return new Promise(async function (resolve, reject) {
            let et = new PbEventStatus()
            await et.load()

            let status = et.getStatusList()

            let datatableOptions = {
                pageLength: 1000,
                stateSave: true,
                paging: false,
                info: false,
                retrieve: true,
                rowId: function (a) {
                    return "status_id_" + a.status_id;
                },
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: this.i18n.translate("Add"),
                        className: "btn btn-indigo",
                        action: function (e, dt, node, config) {
                            this.openFieldTypeEditor(undefined, {})
                        }.bind(this)
                    }
                ],
                columns: [
                    {title: this.i18n.translate('Name'), data: "status_name"},
                    {title: this.i18n.translate('ID'), data: "status_id"},
                    {
                        title: this.i18n.translate('Color'),
                        data: "color",
                        render: function(value) {
                            return `<input class="datatable-input-color" type="color" readonly="readonly" disabled="disabled" value="${value}">`
                        }
                    },
                    {
                        width: "40px",
                        data: null,
                        render: function (status_type, type, row, meta) {
                            return `<i data-selector="dt_edit" data-id="${status_type.status_id}" class="fa fa-pen"></i>&nbsp;&nbsp;&nbsp;<i data-selector="dt_delete" data-id="${status_type.status_id}" class="fa fa-trash"></i>`
                        },
                        orderable: false
                    }
                ],
                responsive: true,
                pagingType: 'first_last_numbers',
                lengthChange: false,
                language: {
                    url: '/style-global/lib/datatables.net-dt/i18n/de-DE.json'
                },
            }

            if (!this.datatable) {
                this.datatable = new DataTable('#datatable', datatableOptions);
            }

            this.datatable.clear()
            this.datatable.rows.add(status).draw()
            this.notifications.unblockAll()

            this.datatable.off("click")
            this.datatable.on('click', '[data-selector="dt_edit"]', async function (e) {
                e.preventDefault();

                var row = this.datatable.row("#status_id_" + String(e.target.dataset.id))

                var data = row.data();

                this.openFieldTypeEditor("#status_id_" + e.target.dataset.id, data)

            }.bind(this))

            this.datatable.on('click', '[data-selector="dt_delete"]', function (e) {
                e.preventDefault();
                new PbEventStatus().deleteByStatusId(e.currentTarget.dataset?.id)
            }.bind(this));

            resolve()
        }.bind(this))
    }

    async openFieldTypeEditor(row_id, data) {
        await this.tpl.renderIntoAsync("easy2schedule/templates/datatable_statustypes.tpl", {"row_data": data}, "body", "append")

        $("#datatableModal").modal('show')
        $('#datatableModal').on('hidden.bs.modal', function () {
            $('#saveRow').off("click")
            $('#datatableModal').remove()
        })

        $('#saveRow').click(function () {
            var formData = $('#editRowForm').serializeArray()

            if ($(row_id)) {
                this.notifications.blockForLoading(row_id)
            }

            let payload = {"data": {"default": false}, "data_owners": []}
            for (let field of formData) {
                if (field.value == "true") field.value = true
                if (field.value == "false") field.value = false
                payload["data"][field.name] = field.value
            }

            new PbEventStatus().set(payload)

            // Close the modal
            $("#datatableModal").modal('hide')

        }.bind(this))
    }
}
