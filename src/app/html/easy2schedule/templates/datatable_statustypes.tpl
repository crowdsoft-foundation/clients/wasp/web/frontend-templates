<div id="datatableModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Bearbeiten</h4></div>
            <div class="modal-body">
                <form id="editRowForm">
                    <input type="hidden" id="status_id" name="uuid" value="{{ row_data.status_id }}">
                    <div class="form-group"><label for="status_id" data-i18n="ID:">ID:</label><input type="text" class="form-control" id="status_id" name="status_id" value="{{ row_data.status_id }}"
                    {% if row_data.status_id %} readonly {% endif %}></div>
                    <div class="form-group"><label for="status_name" data-i18n="Name:">Name:</label><input type="text" class="form-control" id="status_name" name="status_name" value="{{ row_data.status_name }}"></div>
                    <div class="form-group"><label for="status_id" data-i18n="Color:">Color:</label><input type="color" class="form-control" id="color" name="color" value="{{ row_data.color }}"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveRow">Save changes</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#default").change((e) => {
            if (e.target.value == "false") {
                e.target.value = "true"
            } else {
                e.target.value = "false"
            }
        })
    </script>
</div>
