<div id="datatableModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Row</h4></div>
            <div class="modal-body">
                <form id="editRowForm">
                    <input type="hidden" id="uuid" name="uuid" value="{{ row_data.uuid }}">
                    <div class="form-group"><label for="type_id" data-i18n="ID:">ID:</label><input type="text" class="form-control" id="type_id" name="type_id" value="{{ row_data.data.type_id }}"></div>
                    <div class="form-group"><label for="type_name" data-i18n="Name:">Name:</label><input type="text" class="form-control" id="type_name" name="type_name" value="{{ row_data.data.type_name }}"></div>
                    <div class="form-group"><label for="type_id" data-i18n="Color:">Color:</label><input type="color" class="form-control" id="color" name="color" value="{{ row_data.data.color }}"></div>
                    <div class="form-group"><label for="default" data-i18n="Default:">Default:</label>&nbsp;<input type="checkbox" class="" id="default" name="default" value="{% if row_data.data.default %}true{% else %}false{% endif %}" {% if row_data.data.default %} checked="checked" {% endif %}></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="saveRow">Save changes</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#default").change((e) => {
            if (e.target.value == "false") {
                e.target.value = "true"
            } else {
                e.target.value = "false"
            }
        })
    </script>
</div>

