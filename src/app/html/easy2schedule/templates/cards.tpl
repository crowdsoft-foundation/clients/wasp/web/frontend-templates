{% for date, entries in events %}
<div class="list-group-heading">{{ date | weekday }} {{ date | datenotime }}</div>

<div class="timeline">
    <div class="hour hour-before"><!-- before hours --></div>
    <div class="hour hour-08">08</div>
    <div class="hour hour-09">09</div>
    <div class="hour hour-10">10</div>
    <div class="hour hour-11">11</div>
    <div class="hour hour-12">12</div>
    <div class="hour hour-13">13</div>
    <div class="hour hour-14">14</div>
    <div class="hour hour-15">15</div>
    <div class="hour hour-16">16</div>
    <div class="hour hour-17">17</div>
    <div class="hour hour-18">18</div>
    <div class="hour hour-19">19</div>
    <div class="hour hour-20">20</div>
    <div class="hour hour-after"><!-- after hours --></div>

    <div class="pd-t-20" style="grid-row: 1;"></div>
    <!-- padding for top -->

    {% for item in entries %}
    {% if item.allDay %}
    {% if item.attachedRessources.length < 1 %}
    <div class="event" style="grid-column: 1 / span 60;"></div>
    {% endif %}
    {%if item.attachedRessources.length > 1 %}
    <div class="event" style="grid-column: 1 / span 60; background-color: #7987a1;"></div>
    {% else %}
    {% for resource in item.attachedRessources %}
    <div class="event" style="grid-column: 1 / span 60; background-color: {{ resource.backgroundColor }};"></div>
    {% endfor %}
    {% endif %}

    {% else %}

    {% if item.attachedRessources.length < 1 %}
    <div class="event" style="grid-column:{{ item.eventColStart}} / span {{ item.eventSpan}};"></div>
    {% endif %}

    {%if item.attachedRessources.length > 1 %}
    <div class="event" style="grid-column:{{ item.eventColStart}} / span {{ item.eventSpan}}; background-color: #7987a1;"></div>
    {% else %}
    {% for resource in item.attachedRessources %}
    <div class="event" style="grid-column: {{ item.eventColStart}} / span {{ item.eventSpan}}; background-color: {{ resource.backgroundColor }};"></div>
    {% endfor %}
    {% endif %}

    {% endif %}
    {% endfor %}
</div>
{% for item in entries %}
<div data-entry-selector="{{ item.id }}" class="card card-event" data-id="{{ item.id }}">
    <div class="list-group">
        <div class="list-group-item">
            <div class="list-item-color align-items-center justify-content-between" style="border-left-color: {{ item.columnColor }}">
                <div {% if item.title !="Anonymized" %}data-entry-title-selector="{{ item.id }}" {% endif %} class="list-item-title d-flex flex-wrap align-items-center mr-auto">{{ item.title }}
                    <div class="attached-resources image-grouped">
                        {%if item.attachedRessources.length > 0 %}
                        {% for resource in item.attachedRessources %}
                        <div class="participant-avatar" style="background-color: {{ resource.backgroundColor }}; color: {{ resource.fontColor}}">{{ resource.initials }}</div>
                        {% endfor %}
                        {% endif %}
                    </div>
                    <div class="d-flex align-items-center mg-l-10">
                <span class="badge badge-{{item.data.event_status}}" style="background-color: {{ statusColors[item.data.event_status] }}" data-i18n={{item.data.event_status}}>
                {{item.data.event_status}}
                </span>
                    </div>
                </div>

                <div class="d-flex align-items-center pr-3">

                    {% if item.allDay %}
                    <div class="list-item-time" data-i18n="All day">All-day</div>
                    {% else %}
                    <div class="list-item-time pr-1">{{ item.start | time}}</div>
                    <div> &mdash;</div>
                    <div class="list-item-time pl-1">{{ item.end | time}}</div>
                    {% endif %}
                </div>
                <div class="action-dropdown dropdown">
                    <button type="button" class="dropdown-toggle" id="portlet-action-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon ion-ios-more"></i></button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="portlet-action-dropdown">
                        {% if item.title != "Anonymized" %}
                        <a data-entry-quickaction-edit="{{ item.id }}" data-entry-quickaction-title="{{ item.title }}" class="dropdown-item" href="#" data-i18n="Edit"></a>
                        <a data-entry-quickaction-delete="{{ item.id }}" data-entry-quickaction-title="{{ item.title }}" class="dropdown-item" href="#" data-i18n="delete"></a>
                        {% endif %}
                    </div>

                </div>
            </div>
        </div><!-- list-group-item -->
    </div><!-- list-group -->
</div>
{% endfor %}

{% endfor %}

