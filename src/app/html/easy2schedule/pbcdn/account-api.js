import {CONFIG} from '../../pbcdn/config.js?v=[cs_version]';

export class Account {
    constructor() {
        if (!Account.instance) {
            Account.instance = this;
        }

        return Account.instance;
    }

    checkLogin(isLoginPage = false, checkagainTime = 30000, health_endpoint = "/has_valid_login") {
        console.log("Checking login...")
        let url_for_redirect_after_login = '/login/?r=' + location.pathname
        let apikey = $.cookie("apikey");
        if (typeof (apikey) == "undefined") {
            let redirect_file = "index.html"
            let current_url = window.location.pathname;
            let current_filename = current_url.substring(current_url.lastIndexOf('/') + 1);

            if (redirect_file != current_filename) {
                $.blockUI({
                    message: '<div class="responseMessage">Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...</div>',
                    timeout: 3000,
                    baseZ: 9999999999999999,
                    onUnblock: function () {
                        $(location).attr('href', url_for_redirect_after_login)
                    }
                });
            }
        } else {
            var settings = {
                "async": false,
                "crossDomain": true,
                "url": CONFIG.API_BASE_URL + health_endpoint,
                "method": "GET",
                "headers": {
                    "apikey": $.cookie("apikey"),
                    "Cache-Control": "no-cache"
                }
            }

            $.ajax(settings).success(function (response) {
                //setTimeout(Account.instance.checkLogin, checkagainTime, isLoginPage, checkagainTime);
                if (isLoginPage) {
                    Account.instance.redirectDialog()
                }
            });
            $.ajax(settings).error(function (response) {
                if (!isLoginPage) {
                    $.blockUI({
                        message: '<div class="responseMessage">Ihre Anmeldung ist abgelaufen. Sie werden jetzt zur Anmelde-Seite umgeleitet...</div>',
                        timeout: 3000,
                        baseZ: 9999999999999999,
                        onUnblock: function () {
                            $(location).attr('href', url_for_redirect_after_login)
                        }
                    });
                }
            });
        }
    }

    logout(redirect=null, message=null) {
        let apikey = $.cookie("apikey");
        let result = false

        let postUrl = CONFIG.API_BASE_URL + "/logout";
        $.ajax({
            type: "GET",
            url: postUrl,
            contentType: 'application/json;charset=UTF-8',
            data: "",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('apikey', apikey);
            },
            success: function (response) {
                let realRedirect = '/login/'
                let timeout = 3000
                if(redirect == false) {
                    redirect = "#"
                    timeout = 60000000
                }
                if(redirect.length > 0) {
                    realRedirect = redirect
                }
                let realMessage = "Sie wurden abgemeldet."
                if(message != null) {
                    realMessage = message
                }

                if (typeof (response) != "undefined") {
                    $.removeCookie("apikey", {path: '/'});

                    $.blockUI({
                        message: '<div class="responseMessage">' + realMessage + '</div>',
                        timeout: timeout,
                        onUnblock: function () {
                            $(location).attr('href', realRedirect)
                        }
                    });
                } else {
                    $.removeCookie("apikey", {path: '/'});
                    $.blockUI({message: '<div class="responseMessage">' + realMessage + '</div>'});

                }
            },
            error: function () {
                $.removeCookie("apikey", {path: '/'});
                $.blockUI({message: '<div class="responseMessage">' + realMessage + '</div>'});
            }
        }).done(function (o) {

        });


    }

    getapikey() {
        let apikey = $.cookie("apikey");
        console.debug(apikey)
        let username = document.getElementById("login").value;
        let password = document.getElementById("password").value;

        $.blockUI({message: '<div class="responseMessage">Ihre Zugangsdaten werden überprüft ...</div>'});
        let postUrl = API_BASE_URL + "/login";

        $.ajax({
            type: "GET",
            url: postUrl,
            contentType: 'application/json;charset=UTF-8',
            data: "",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password));
            },
            success: function (response) {
                if (typeof (response) != "undefined") {
                    //console.debug(response)
                    $.blockUI({message: '<div class="responseMessage">Login successfull</div>', timeout: 1000});
                    $.cookie("apikey", response.apikey, {path: '/'});
                    $.cookie("consumer_name", response.consumer_name, {path: '/'});
                    $.cookie("consumer_id", response.consumer.id, {path: '/'})
                    $.cookie("username", response.username, {path: '/'});
                    //$(location).attr('href', 'beta.html')
                    redirectDialog(DEFAULT_LOGIN_REDIRECT)
                } else {
                    $.removeCookie("apikey", {path: '/'});
                    $.unblockUI()
                    $.blockUI({
                        message: '<div class="responseMessage">Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut</div>',
                        timeout: 3000
                    });
                }
            },
            error: function () {
                $.removeCookie("apikey", {path: '/'});
                $.unblockUI()
                $.blockUI({
                    message: '<div class="responseMessage">Die angegebenen Login-Daten sind nicht gültig. Bitte versuchen Sie es erneut</div>',
                    timeout: 3000
                });

            }
        }).done(function (o) {

        });
    }

    getapikeyfor(username, password, callback) {
        let postUrl = CONFIG.API_BASE_URL + "/login";
        $.ajax({
            type: "GET",
            url: postUrl,
            contentType: 'application/json;charset=UTF-8',
            data: "",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Basic ' + btoa(username + ":" + password));
            },
            success: function (response) {
                //console.log("RESPONSE", response)
                if (typeof (response) != "undefined") {
                    callback(response.apikey)
                } else {
                   callback(response.apikey)
                }
            },
            error: function () {
               callback(false)
            }
        }).done(function (o) {

        });
    }

    redirectDialog(url = null) {
        if (url == null) {
            location.href = "/login/"
        } else {
            location.href = url
        }
    }

    getCookie(name) {
        var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
        if (match) return match[2];
    }

}