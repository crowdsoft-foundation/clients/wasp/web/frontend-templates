(window["webpackJsonpDistPbs"] = window["webpackJsonpDistPbs"] || []).push([[3],{

/***/ "./app/src/event-manager.js?v=[cs_version]":
/*!*************************************************!*\
  !*** ./app/src/event-manager.js?v=[cs_version] ***!
  \*************************************************/
/*! exports provided: EventManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventManager", function() { return EventManager; });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var datatables_net__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! datatables.net */ "./node_modules/datatables.net/js/jquery.dataTables.mjs");
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(r, l) { var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (null != t) { var e, n, i, u, a = [], f = !0, o = !1; try { if (i = (t = t.call(r)).next, 0 === l) { if (Object(t) !== t) return; f = !1; } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0); } catch (r) { o = !0, n = r; } finally { try { if (!f && null != t["return"] && (u = t["return"](), Object(u) !== u)) return; } finally { if (o) throw n; } } return a; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw new Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw new Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }
function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
/* eslint-disable prettier/prettier */


var EventManager = /*#__PURE__*/function (_renderCurrentlySelectedEvent, _loadFromApi, _isSlotAvailable, _createBooking, _setWorkFlowState, _renderTable, _renderBookingForm) {
  function EventManager(apiurl, apikey, tpl, target_selector) {
    var _this = this;
    var event_id = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : undefined;
    var redirect_url_success = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : undefined;
    var redirect_url_failure = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : undefined;
    var filters = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : [];
    var no_list = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : false;
    var data_privacy_url = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : "";
    var compatibility_mode = arguments.length > 10 && arguments[10] !== undefined ? arguments[10] : true;
    _classCallCheck(this, EventManager);
    this.apikey = apikey;
    this.apiurl = apiurl;
    this.redirect_url_success = redirect_url_success;
    this.redirect_url_failure = redirect_url_failure;
    this.compatibility_mode = compatibility_mode;
    this.data_privacy_url = data_privacy_url;
    this.tpl = tpl;
    this.filters = filters;
    this.target_selector = target_selector;
    this.currently_selected_event_id = event_id;
    this.no_list = no_list;
    var date_format_options = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      timezone: "UTC"
    };
    this.intl_dtf = new Intl.DateTimeFormat(undefined, date_format_options);
    this.events = [];
    this.event_template = {};
    this.table_id = "pbs_booking_table";
    this.lastDate = undefined;
    EventManager.setDatePickerLanguage();
    ["pushState", "replaceState"].forEach(function (changeState) {
      // store original values under underscored keys (`window.history._pushState()` and `window.history._replaceState()`):
      window.history["_" + changeState] = window.history[changeState];
      window.history[changeState] = function (state, _, url) {
        window.history["_" + changeState](state, _, url);
        _this.onChangeState(state, url);
      };
    });
    if (!this.no_list) {
      this.renderTable(undefined, true);
    }
    this.renderCurrentlySelectedEvent();
  }
  _createClass(EventManager, [{
    key: "onChangeState",
    value: function onChangeState(state, url) {
      if (state !== null && state !== void 0 && state.current_event_id) {
        this.currently_selected_event_id = state.current_event_id;
        this.renderCurrentlySelectedEvent();
      }
    }
  }, {
    key: "getEventById",
    value: function getEventById(event_id) {
      return this.events.filter(function (data) {
        return data.event_id === event_id;
      })[0];
    }
  }, {
    key: "renderCurrentlySelectedEvent",
    value: function renderCurrentlySelectedEvent() {
      return (_renderCurrentlySelectedEvent = _renderCurrentlySelectedEvent || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
        var event;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              if (!(this.events.length == 0)) {
                _context.next = 3;
                break;
              }
              _context.next = 3;
              return this.loadFromApi();
            case 3:
              event = this.getEventById(this.currently_selected_event_id);
              if (event) {
                _context.next = 6;
                break;
              }
              return _context.abrupt("return");
            case 6:
              event.start_string = this.formatDateForDisplay(new Date(event.start));
              event.end_string = this.formatDateForDisplay(new Date(event.end));
              _context.next = 10;
              return this.tpl.renderIntoAsync("/event_details.html", {
                "event": event
              }, this.target_selector);
            case 10:
              jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_form_cancel_button").click(function (e) {
                this.renderTable();
              }.bind(this));
              jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_book_button").click(function (e) {
                this.renderBookingForm(e);
              }.bind(this));
              jquery__WEBPACK_IMPORTED_MODULE_0___default()("#" + this.table_id).remove();
              if (this.no_list) {
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_form_cancel_button").remove();
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_book_button").remove();
              }

              //window.setTimeout(this.renderTable.bind(this), 3000);
            case 14:
            case "end":
              return _context.stop();
          }
        }, _callee, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "loadFromApi",
    value: function loadFromApi() {
      return (_loadFromApi = _loadFromApi || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
        var _this$filters;
        var date,
          date_part,
          filter,
          settings,
          _args2 = arguments;
        return _regeneratorRuntime().wrap(function _callee2$(_context2) {
          while (1) switch (_context2.prev = _context2.next) {
            case 0:
              date = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : undefined;
              date_part = "";
              filter = "";
              if (date) {
                date_part = "&date=" + date.toISOString().split("T")[0];
              }
              if (((_this$filters = this.filters) === null || _this$filters === void 0 || (_this$filters = _this$filters.tags) === null || _this$filters === void 0 ? void 0 : _this$filters.length) > 0) {
                filter = "&tags=" + this.filters.tags.join(",");
              }
              settings = {
                url: this.apiurl + "/get_events?_" + date_part + filter,
                method: "GET",
                timeout: 0,
                headers: {
                  apikey: this.apikey,
                  "Content-Type": "application/json"
                },
                success: function (response) {
                  this.events = this.reformatApiData(response.events);
                }.bind(this)
              };
              _context2.next = 8;
              return jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
            case 8:
            case "end":
              return _context2.stop();
          }
        }, _callee2, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "reformatApiData",
    value: function reformatApiData(events) {
      var rows = [];
      for (var _i = 0, _Object$entries = Object.entries(events); _i < _Object$entries.length; _i++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
          id = _Object$entries$_i[0],
          _event = _Object$entries$_i[1];
        if (new Date(_event.data.enddatetime) < new Date()) {
          continue;
        }
        rows.push({
          event_id: id,
          start: _event.data.startdatetime,
          end: _event.data.enddatetime,
          name: _event.data.name,
          desc: _event.data.desc,
          location: _event.data.location
        });
      }
      return rows;
    }
  }, {
    key: "formatDateForApi",
    value: function formatDateForApi(date) {
      var year = date.getFullYear();
      var month = String(date.getMonth() + 1).padStart(2, "0");
      var day = String(date.getDate()).padStart(2, "0");
      var hours = String(date.getHours()).padStart(2, "0");
      var minutes = String(date.getMinutes()).padStart(2, "0");
      var seconds = String(date.getSeconds()).padStart(2, "0");
      return "".concat(year, "-").concat(month, "-").concat(day, " ").concat(hours, ":").concat(minutes, ":").concat(seconds);
    }
  }, {
    key: "formatDateForDisplay",
    value: function formatDateForDisplay(date) {
      var year = date.getFullYear();
      var month = String(date.getMonth() + 1).padStart(2, "0");
      var day = String(date.getDate()).padStart(2, "0");
      var hours = String(date.getHours()).padStart(2, "0");
      var minutes = String(date.getMinutes()).padStart(2, "0");
      var seconds = String(date.getSeconds()).padStart(2, "0");
      return "<span class=\"pbs_date\">".concat(day, ".").concat(month, ".").concat(year, "</span> <span class=\"pbs_time\">").concat(hours, ":").concat(minutes, "</span>");
    }
  }, {
    key: "isSlotAvailable",
    value: function isSlotAvailable(_x) {
      return (_isSlotAvailable = _isSlotAvailable || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(event_id) {
        return _regeneratorRuntime().wrap(function _callee3$(_context3) {
          while (1) switch (_context3.prev = _context3.next) {
            case 0:
              return _context3.abrupt("return", new Promise(function (resolve, reject) {
                var settings = {
                  url: "".concat(this.apiurl, "/get_events/").concat(event_id),
                  method: "GET",
                  timeout: 0,
                  headers: {
                    apikey: this.apikey,
                    "Content-Type": "application/json"
                  },
                  success: function success(response) {
                    var max_participants = response.events[event_id].data.max_participants;
                    var current_participants = Object.entries(response.events[event_id].data.participants).length;
                    if (max_participants == 0 || current_participants < max_participants) {
                      resolve(true);
                    } else {
                      resolve(false);
                    }
                  }
                };
                jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
              }.bind(this)));
            case 1:
            case "end":
              return _context3.stop();
          }
        }, _callee3, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "createBooking",
    value: function createBooking(_x2, _x3) {
      return (_createBooking = _createBooking || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(event_id, booking_data) {
        var event;
        return _regeneratorRuntime().wrap(function _callee5$(_context5) {
          while (1) switch (_context5.prev = _context5.next) {
            case 0:
              event = this.getEventById(event_id);
              return _context5.abrupt("return", new Promise(function (resolve, reject) {
                this.isSlotAvailable(event_id).then(function (result) {
                  if (!result) {
                    resolve(false);
                  } else {
                    var _ref;
                    var payload = {
                      "email": booking_data.email,
                      "firstname": booking_data.firstname,
                      "lastname": booking_data.lastname,
                      "phone": booking_data.phone,
                      "address": booking_data.address,
                      "redirect_url_success": encodeURIComponent(this.redirect_url_success),
                      "redirect_url_failure": encodeURIComponent(this.redirect_url_failure),
                      "eventdata": {
                        "event_id": event.event_id,
                        "desc": event.desc,
                        "enddatetime": event.end,
                        "name": event.name,
                        "startdatetime": event.start
                      }
                    };
                    this.tpl.renderIntoAsync("/loading.html", {}, this.target_selector);
                    // var settings = {
                    //     url: `${this.apiurl}/es/cmd/signUpForEvent`,
                    //     method: "POST",
                    //     timeout: 0,
                    //     headers: {
                    //         apikey: this.apikey,
                    //         "Content-Type": "application/json"
                    //     },
                    //     data: JSON.stringify(payload),
                    //     error: function (response) {
                    //         resolve();
                    //     },
                    //     success: function (response) {
                    //         resolve(response);
                    //     }.bind(this)
                    // };
                    // $.ajax(settings);
                    var settings = {
                      "url": "".concat(this.apiurl, "/statemachine/create_state_machine_from_template/default_registration_config"),
                      "method": "POST",
                      "timeout": 0,
                      "headers": {
                        "apikey": this.apikey,
                        "Content-Type": "application/json"
                      },
                      "data": JSON.stringify(payload),
                      "success": function (_x4) {
                        return (_ref = _ref || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4(response) {
                          var state_machine_id;
                          return _regeneratorRuntime().wrap(function _callee4$(_context4) {
                            while (1) switch (_context4.prev = _context4.next) {
                              case 0:
                                state_machine_id = response.meta.persistent_id;
                                _context4.next = 3;
                                return this.setWorkFlowState(state_machine_id, "new_registration");
                              case 3:
                                resolve(response);
                              case 4:
                              case "end":
                                return _context4.stop();
                            }
                          }, _callee4, this);
                        }))).apply(this, arguments);
                      }.bind(this),
                      "error": function error(response) {
                        reject();
                      }
                    };
                    jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
                  }
                }.bind(this));
              }.bind(this)));
            case 2:
            case "end":
              return _context5.stop();
          }
        }, _callee5, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "setWorkFlowState",
    value: function setWorkFlowState(_x5, _x6) {
      return (_setWorkFlowState = _setWorkFlowState || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee6(state_machine_id, state_id) {
        var payload,
          _args6 = arguments;
        return _regeneratorRuntime().wrap(function _callee6$(_context6) {
          while (1) switch (_context6.prev = _context6.next) {
            case 0:
              payload = _args6.length > 2 && _args6[2] !== undefined ? _args6[2] : {};
              return _context6.abrupt("return", new Promise(function (resolve, reject) {
                var settings = {
                  "url": "".concat(this.apiurl, "/statemachine/state_machine_instance/").concat(state_machine_id, "/fast_forward/state_id/").concat(state_id),
                  "method": "POST",
                  "timeout": 0,
                  "headers": {
                    "apikey": this.apikey,
                    "Content-Type": "application/json"
                  },
                  "data": JSON.stringify(payload),
                  "success": function success(response) {
                    resolve(response);
                  },
                  "error": function error(response) {
                    resolve(response);
                  }
                };
                jquery__WEBPACK_IMPORTED_MODULE_0___default.a.ajax(settings);
              }.bind(this)));
            case 2:
            case "end":
              return _context6.stop();
          }
        }, _callee6, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "renderTable",
    value: function renderTable() {
      return (_renderTable = _renderTable || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee9() {
        var _ref2;
        var date,
          skip_event_id_delete,
          urlParams,
          newUrl,
          _args9 = arguments;
        return _regeneratorRuntime().wrap(function _callee9$(_context9) {
          while (1) switch (_context9.prev = _context9.next) {
            case 0:
              date = _args9.length > 0 && _args9[0] !== undefined ? _args9[0] : this.lastDate || undefined;
              skip_event_id_delete = _args9.length > 1 && _args9[1] !== undefined ? _args9[1] : false;
              urlParams = new URLSearchParams(window.location.search);
              if (!date && urlParams.get("date")) date = new Date(urlParams.get("date"));
              urlParams["delete"]("state");
              if (!skip_event_id_delete) {
                urlParams["delete"]("event_id");
              }
              newUrl = "".concat(window.location.pathname, "?").concat(urlParams.toString());
              history.pushState({
                current_event_id: "",
                state: ""
              }, null, newUrl);
              this.lastDate = date;
              this.tpl.renderIntoAsync("/datepicker.html", {
                data: {}
              }, this.target_selector).then(function () {
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker({
                  minDate: new Date(),
                  dateFormat: "dd.mm.yy",
                  changeMonth: true,
                  changeYear: true
                });
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker("setDate", date);
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").on("change", function () {
                  var newDate = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_datepicker").datepicker("getDate");
                  var userTimezoneOffset = newDate.getTimezoneOffset() * 60000;
                  newDate = new Date(newDate.getTime() - userTimezoneOffset);
                  this.renderTable(newDate);
                }.bind(this));
              }.bind(this));
              _context9.next = 12;
              return this.loadFromApi(date);
            case 12:
              return _context9.abrupt("return", new Promise(function (_x7, _x8) {
                return (_ref2 = _ref2 || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee8(resolve, reject) {
                  var data, _iterator, _step, _ref3, datatableOptions, table_id, table;
                  return _regeneratorRuntime().wrap(function _callee8$(_context8) {
                    while (1) switch (_context8.prev = _context8.next) {
                      case 0:
                        if (this.compatibility_mode != true) {
                          data = [];
                          _iterator = _createForOfIteratorHelper(this.events);
                          try {
                            for (_iterator.s(); !(_step = _iterator.n()).done;) {
                              event = _step.value;
                              event.start = this.formatDateForDisplay(new Date(event.start));
                              event.end = this.formatDateForDisplay(new Date(event.end));
                              data.push(event);
                            }
                          } catch (err) {
                            _iterator.e(err);
                          } finally {
                            _iterator.f();
                          }
                          this.tpl.renderIntoAsync("/event_list.html", {
                            data: data
                          }, this.target_selector, "append").then(function () {
                            jquery__WEBPACK_IMPORTED_MODULE_0___default()('[data-selector="pbs_bookbutton"]').click(this.renderBookingForm.bind(this));
                            jquery__WEBPACK_IMPORTED_MODULE_0___default()('[data-goto_event_id]').click(function (e) {
                              e.preventDefault();
                              var urlParams = new URLSearchParams(window.location.search);
                              urlParams.set("event_id", e.target.dataset.goto_event_id);
                              var newUrl = "".concat(window.location.pathname, "?").concat(urlParams.toString());
                              history.pushState({
                                current_event_id: e.target.dataset.goto_event_id
                              }, null, newUrl);
                            });
                          }.bind(this));
                        } else {
                          datatableOptions = {
                            language: {
                              emptyTable: this.tpl.i18n.translate("No events available")
                            },
                            searching: false,
                            ordering: false,
                            pageLength: 1000,
                            stateSave: true,
                            paging: false,
                            info: false,
                            retrieve: true,
                            // rowId: function (a) {
                            //     return "uuid_" + a.uuid;
                            // },
                            // dom: 'Bfrtip',
                            // buttons: [
                            //     {
                            //         text: this.tpl.i18n.translate("Add"),
                            //         className: "btn btn-indigo",
                            //         action: function (e, dt, node, config) {
                            //             this.openFieldTypeEditor(undefined, {})
                            //         }.bind(this)
                            //     }
                            // ],
                            columns: [
                            // {title: this.tpl.i18n.translate('Start'), data: "start"},
                            // {title: this.tpl.i18n.translate('End'), data: "end"},
                            {
                              title: this.tpl.i18n.translate("Von"),
                              className: "dt-body-center dt-from-column",
                              data: "start",
                              render: function (value) {
                                return this.formatDateForDisplay(new Date(value)); //this.intl_dtf.format(new Date(value))
                              }.bind(this)
                            }, {
                              title: this.tpl.i18n.translate("bis"),
                              className: "dt-body-center dt-until-column",
                              data: "end",
                              render: function (value) {
                                return this.formatDateForDisplay(new Date(value)); //this.intl_dtf.format(new Date(value))
                              }.bind(this)
                            }, {
                              title: this.tpl.i18n.translate("Veranstaltungsname"),
                              className: "dt-body-center dt-eventname-column",
                              data: "name",
                              render: function (value, display, row, row_meta) {
                                return "<a href=\"#\" data-goto_event_id=\"".concat(row.event_id, "\">").concat(value, "</a>");
                              }.bind(this)
                            },
                            // {
                            //     title: this.tpl.i18n.translate("Beschreibung"),
                            //     data: "desc",
                            //     render: function(value) {
                            //         return value;
                            //     }.bind(this)
                            // },
                            {
                              width: "40px",
                              data: null,
                              render: function render(data, slot, row, meta) {
                                return "<button class=\"btn btn-info btn-pills login-btn-primary\" data-selector=\"pbs_bookbutton\" data-event_name=\"".concat(row.name, "\" data-event_id=\"").concat(row.event_id, "\" data-start=\"").concat(row.start, "\" data-end=\"").concat(row.end, "\">Buchen</button>");
                              },
                              orderable: false
                            }],
                            responsive: true,
                            pagingType: "first_last_numbers",
                            lengthChange: false
                            // language: {
                            //     url: '/style-global/lib/datatables.net-dt/i18n/de-DE.json'
                            // },
                          };
                          table_id = this.table_id;
                          jquery__WEBPACK_IMPORTED_MODULE_0___default()("#" + table_id).remove();
                          this.datatable = undefined;
                          table = jquery__WEBPACK_IMPORTED_MODULE_0___default()("<table>");
                          table.attr("id", table_id);
                          table.attr("class", "cell-border compact stripe hover");
                          jquery__WEBPACK_IMPORTED_MODULE_0___default()(this.target_selector).after(table);
                          if (!this.datatable) {
                            EventManager.setDatePickerLanguage();
                            this.datatable = new datatables_net__WEBPACK_IMPORTED_MODULE_1__["default"]("#" + table_id, datatableOptions);
                            EventManager.setDatePickerLanguage();
                          }
                          this.datatable.clear();
                          this.datatable.rows.add(this.events).draw();
                          //$(this.target_selector).hide()
                          this.datatable.off("click");
                          this.datatable.on("click", "[data-goto_event_id]", function (_x9) {
                            return (_ref3 = _ref3 || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee7(e) {
                              var urlParams, newUrl;
                              return _regeneratorRuntime().wrap(function _callee7$(_context7) {
                                while (1) switch (_context7.prev = _context7.next) {
                                  case 0:
                                    e.preventDefault();
                                    urlParams = new URLSearchParams(window.location.search);
                                    urlParams.set("event_id", e.target.dataset.goto_event_id);
                                    newUrl = "".concat(window.location.pathname, "?").concat(urlParams.toString());
                                    history.pushState({
                                      current_event_id: e.target.dataset.goto_event_id
                                    }, null, newUrl);
                                  case 5:
                                  case "end":
                                    return _context7.stop();
                                }
                              }, _callee7);
                            }))).apply(this, arguments);
                          });
                          this.datatable.on("click", '[data-selector="pbs_bookbutton"]', this.renderBookingForm.bind(this));

                          // this.datatable.on('click', '[data-selector="dt_delete"]', function (e) {
                          //     e.preventDefault();
                          //     new PbEventTypes().deleteByUUId(e.currentTarget.dataset?.id)
                          // }.bind(this));

                          resolve();
                        }
                      case 1:
                      case "end":
                        return _context8.stop();
                    }
                  }, _callee8, this);
                }))).apply(this, arguments);
              }.bind(this)));
            case 13:
            case "end":
              return _context9.stop();
          }
        }, _callee9, this);
      }))).apply(this, arguments);
    }
  }, {
    key: "renderBookingForm",
    value: function renderBookingForm(_x10) {
      return (_renderBookingForm = _renderBookingForm || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee11(e) {
        var urlParams, newUrl, event_id, event_name, data;
        return _regeneratorRuntime().wrap(function _callee11$(_context11) {
          while (1) switch (_context11.prev = _context11.next) {
            case 0:
              e.preventDefault();
              urlParams = new URLSearchParams(window.location.search);
              urlParams.set("state", "bookingform");
              urlParams.set("event_id", e.target.dataset.event_id);
              newUrl = "".concat(window.location.pathname, "?").concat(urlParams.toString());
              history.pushState({
                current_event_id: e.target.dataset.event_id,
                state: "bookingform"
              }, null, newUrl);
              jquery__WEBPACK_IMPORTED_MODULE_0___default()("#" + this.table_id).hide();
              event_id = e.target.dataset.event_id;
              event_name = e.target.dataset.event_name;
              data = {
                start_string: this.intl_dtf.format(new Date(e.target.dataset.start)),
                end_string: this.intl_dtf.format(new Date(e.target.dataset.end)),
                start_date: e.target.dataset.start,
                end_date: e.target.dataset.end,
                event_name: event_name,
                data_privacy_url: this.data_privacy_url
              };
              this.tpl.renderIntoAsync("event_booking_form.html", {
                data: data
              }, this.target_selector).then(function () {
                var _ref4;
                jquery__WEBPACK_IMPORTED_MODULE_0___default()(this.target_selector).html(jquery__WEBPACK_IMPORTED_MODULE_0___default()(this.target_selector).html().replaceAll("{{data_privacy_url}}", this.data_privacy_url));
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_form_submit_button").click(function (_x11) {
                  return (_ref4 = _ref4 || _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee10(e) {
                    var result, payload, tpl_data;
                    return _regeneratorRuntime().wrap(function _callee10$(_context10) {
                      while (1) switch (_context10.prev = _context10.next) {
                        case 0:
                          result = document.querySelector("#pbs_event_booking_form").reportValidity();
                          if (result) {
                            _context10.next = 3;
                            break;
                          }
                          return _context10.abrupt("return");
                        case 3:
                          payload = {};
                          payload["firstname"] = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_form_firstname_input").val();
                          payload["lastname"] = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_form_lastname_input").val();
                          payload["email"] = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_form_email_input").val();
                          payload["phone"] = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_form_phone_input").val();
                          payload["address"] = jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_form_address_input").val();
                          tpl_data = {
                            start_string: this.intl_dtf.format(new Date(e.target.dataset.start)),
                            end_string: this.intl_dtf.format(new Date(e.target.dataset.end)),
                            event_name: event_name
                          };
                          this.createBooking(event_id, payload).then(function (result) {
                            if (result) {
                              this.tpl.renderIntoAsync("/event_booking_success.html", {
                                data: tpl_data
                              }, this.target_selector).then(function () {}.bind(this));
                            } else {
                              this.tpl.renderIntoAsync("/not_available.html", {}, this.target_selector).then(function () {
                                resolve(false);
                              }.bind(this));
                            }
                          }.bind(this));
                        case 11:
                        case "end":
                          return _context10.stop();
                      }
                    }, _callee10, this);
                  }))).apply(this, arguments);
                }.bind(this));
                jquery__WEBPACK_IMPORTED_MODULE_0___default()("#pbs_event_booking_form_cancel_button").click(function (e) {
                  this.renderTable();
                }.bind(this));
              }.bind(this));
              jquery__WEBPACK_IMPORTED_MODULE_0___default()(this.target_selector).show();
            case 12:
            case "end":
              return _context11.stop();
          }
        }, _callee11, this);
      }))).apply(this, arguments);
    }
  }], [{
    key: "setDatePickerLanguage",
    value: function setDatePickerLanguage() {
      jquery__WEBPACK_IMPORTED_MODULE_0___default.a.datepicker.regional.de = {
        closeText: "Schließen",
        prevText: "Zurück",
        nextText: "Vor",
        currentText: "Heute",
        monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
        monthNamesShort: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
        dayNames: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
        dayNamesShort: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
        dayNamesMin: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
        weekHeader: "KW",
        dateFormat: "dd.mm.yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""
      };
      jquery__WEBPACK_IMPORTED_MODULE_0___default.a.datepicker.setDefaults(jquery__WEBPACK_IMPORTED_MODULE_0___default.a.datepicker.regional.de);
    }
  }]);
  return EventManager;
}();

/***/ })

}]);
//# sourceMappingURL=3.main.js.map