import {PbAccount} from "/csf-lib/pb-account.js?v=[cs_version]"
import {socketio} from "/csf-lib/pb-socketio.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {Controller} from "./controller.js?v=[cs_version]"
import {RenderIternaryList} from "./render-iternary-list.js?v=[cs_version]";


export class CruisesModule {
    actions = {}
    config = []

    constructor() {
        if (!CruisesModule.instance) {
            CruisesModule.instance = this
            CruisesModule.instance.socket = new socketio()
            CruisesModule.instance.initListeners()
            CruisesModule.instance.initActions()
            CruisesModule.instance.acc = new PbAccount()
        }

        return CruisesModule.instance
    }

    initActions() {
        let self = this
        this.actions["cruisesModuleInitialised"] = function (myevent, callback = () => "") {
            self.acc.init().then(() => {
                if (!self.acc.hasPermission("beta_access")) {
                    self.acc.logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new Controller(CruisesModule.instance.socket).init(CruisesModule.instance)
                    callback()
                }
            })
        }

        this.actions["CruisesListContractView"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("beta_access")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    import('./render-iternary-list.js?v=[cs_version]').then(() => {
                    })

                    callback(myevent)
                }
            })
            callback(myevent)
        }
    }

    initListeners() {

    }
}
