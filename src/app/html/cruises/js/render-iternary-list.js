import {PbTpl} from "/csf-lib/pb-tpl.js?v=[cs_version]"
import {PbIternaries} from "/csf-lib/pbapi/ocean-mate/pb-iternaries.js?v=[cs_version]"
import {PbNotifications} from "/csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTagsList} from "/csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"
import {PbTag} from "/csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbAccount} from "/csf-lib/pb-account.js?v=[cs_version]";
import {setQueryParam, getQueryParam} from "/csf-lib/pb-functions.js?v=[cs_version]";

export class RenderIternaryList {
    current_state = "initial"

    constructor(contract_manager_instance) {
        if (!RenderIternaryList.instance) {
            RenderIternaryList.instance = this;

            RenderIternaryList.instance.tpl = new PbTpl();
            RenderIternaryList.instance.it = new PbIternaries();
            RenderIternaryList.instance.cmi = contract_manager_instance;
            RenderIternaryList.instance.tagListModel = new PbTagsList();

            this.initListeners();
        }

        return RenderIternaryList.instance
    }

    init() {
        return RenderIternaryList.instance.it.loadFromApi().then(async function (response) {
            await RenderIternaryList.instance.tagListModel.load(false, "ocean-mate")
            this.renderStart().then(() => {
                let iternary_id = RenderIternaryList.instance.it.getActiveIternary()?.id

                if (iternary_id != undefined) {
                    $("#" + iternary_id).click()
                }

                $("#loader-overlay").hide()
            })
        }.bind(this));
    }

    renderStart() {
        return new Promise((resolve, reject) => {
            let search = $("#search_input").val();
            RenderIternaryList.instance.renderContractList(search).then(resolve)

            if (getQueryParam("iternary")) {
                RenderIternaryList.instance.renderContractDetails(getQueryParam("iternary"))
            }
        })
    }

    renderContractList(searchFilter = '') {
        $("#azContractList").html("");
        let iternaries = [];

        const entries = Object.keys(RenderIternaryList.instance.it.data);

        for (let key of entries) {
            const value = RenderIternaryList.instance.it.data[key];
            iternaries.push(value);
        }

        // Filter the itineraries based on the search filter if it is not empty
        if (searchFilter) {
            iternaries = iternaries.filter(iternary => {
                const searchString = iternary.id + '|' + iternary.title + '|' + iternary.description + '|' + iternary.from_port + '|' + iternary.to_port;
                return searchString.toLowerCase().indexOf(searchFilter.toLowerCase()) !== -1
            });
        }

        return RenderIternaryList.instance.tpl.renderIntoAsync('cruises/templates/cruises_iternaries_list.html', {"iternaries": iternaries}, "#azContractList").then(function () {
            $(".az-contact-item").click(function (event) {
                $('body').addClass('az-content-body-show');

                new RenderIternaryList().renderContractDetails(event.currentTarget.id);
            });

            // Functionality to scroll the list UP and DOWN
            $('#upScroller').on('click', function () {
                $('#azContractList').get(0).scrollBy({
                    top: -500,
                    behavior: 'smooth'
                });
            });

            $('#downScroller').on('click', function () {
                $('#azContractList').get(0).scrollBy({
                    top: 500,
                    behavior: 'smooth'
                });
            });
        }.bind(this));
    }

    renderContractDetails(id) {
        // Case to select active iternary
        if (id === getQueryParam("iternary")) {
            $('#' + id).addClass('active-itinerary');
        } else {
            $('#' + id).addClass('active-itinerary');
            $('#' + getQueryParam("iternary")).removeClass('active-itinerary');
        }

        setQueryParam("iternary", id)
        RenderIternaryList.instance.it.setActiveIternary(id)


        RenderIternaryList.instance.tpl.renderIntoAsync('cruises/templates/cruises_contracts_details.html', {"iternary": RenderIternaryList.instance.it.getActiveIternary()}, "#azContactBody").then(function () {

            if (RenderIternaryList.instance.current_state == "edit") {
                RenderIternaryList.instance.editMode()
            } else {
                RenderIternaryList.instance.viewMode()
            }

            $("#edit_iternary_button").click(function (e) {
                e.preventDefault()
                RenderIternaryList.instance.editMode()
            })

            $("#cancel_edit_iternary_button").click(function (e) {
                e.preventDefault()
                RenderIternaryList.instance.viewMode()
            })

            $("#save_iternary_button").click(function (e) {
                e.preventDefault()
                PbNotifications.instance.blockForLoading('#save_iternary_button')
                RenderIternaryList.instance.mergedFormDataIntoActiveIternary();
                RenderIternaryList.instance.viewMode()
                RenderIternaryList.instance.it.updateActiveIternary();

            })
            let selectedTags = RenderIternaryList.instance.it.getActiveIternary().tags
            let tagOptions = RenderIternaryList.instance.tagListModel.getSelectOptions(selectedTags)
            PbTpl.instance.renderIntoAsync('csf-lib/pbapi/tagmanager/templates/tag-select2.tpl', {
                "multiple": true,
                "addButton": true,
                "propertyPath": "tags",
                "options": tagOptions
            }, "#tag_container").then(() => {
                $('[data-propertyname="tags"]').select2({
                    tags: new PbAccount().hasPermission("contacts.localtags.add"),
                    tokenSeparators: [',', ' '],
                    selectOnClose: false
                })

                $("#addGlobalTagButton").click(() => {
                    PbNotifications.instance.ask(
                        '<div><input type="text" id="newGlobalTag" placeholder="" class="form-control"/></div>',
                        'New global tag',
                        () => {
                            RenderIternaryList.instance.tagListModel.addTagToList(new PbTag().fromShort($("#newGlobalTag").val()).setUsableFor(["ocean-mate"]), true)
                        })
                    $("#newGlobalTag").focus()
                })
            })
        }.bind(this))
    }

    mergedFormDataIntoActiveIternary() {
        var form = document.getElementById('iternaryForm');
        if (form.reportValidity()) {
            let formData = new FormData(form);
            let iternary = RenderIternaryList.instance.it.getActiveIternary();

            // Manually check the state of the 'update_behaviour' checkbox - because when it is not checked, it will not appear in the FormData.
            let updateBehaviourCheckbox = document.querySelector('input[name="update_behaviour"]');
            if (updateBehaviourCheckbox) {
                if (updateBehaviourCheckbox.checked) {
                    formData.set('update_behaviour', 'force');
                } else {
                    formData.set('update_behaviour', 'ignore');
                }
            }

            // Manually check the state of the 'hide' checkbox - because when it is not checked, it will not appear in the FormData.
            let hideCheckbox = document.querySelector('input[name="hide"]');
            if (hideCheckbox) {
                if (hideCheckbox.checked) {
                    formData.set('hide', 'true');
                } else {
                    formData.set('hide', 'false');
                }
            }

            // Let check all changed fields.
            for (var formEntry of formData.entries()) {

                let dotSplittedInputName = formEntry[0].split(".");
                let inputValue = formEntry[1];
                let obj = iternary;

                // special case for update_behaviour
                if (formEntry[0] == 'update_behaviour') {
                    let checkbox = document.querySelector(`input[name="${formEntry[0]}"]`);
                    if (checkbox && checkbox.type === 'checkbox') {
                        if (checkbox.checked) {

                            inputValue = 'force';
                        } else {

                            inputValue = 'ignore';
                        }
                    }
                }

                // special case for hide
                if (formEntry[0] == 'hide') {
                    let checkbox = document.querySelector(`input[name="${formEntry[0]}"]`);
                    if (checkbox && checkbox.type === 'checkbox') {
                        if (checkbox.checked) {
                            inputValue = true;
                        } else {
                            inputValue = false;
                        }
                    }
                }

                for (let i = 0; i < dotSplittedInputName.length; i++) {
                    if (i === dotSplittedInputName.length - 1) {
                        obj[dotSplittedInputName[i]] = inputValue;
                    } else {
                        obj = obj[dotSplittedInputName[i]];
                    }
                }
            }
            iternary.tags = $("#tags").val()
        } else {
            alert('Form is invalid!');
        }
    }

    initListeners() {
        document.addEventListener("command", function _(event) {
            if (event.data.data.event == "iternaryUpdated") {
                this.init().then(() => {
                    RenderIternaryList.instance.viewMode()
                    PbNotifications.instance.unblock('#save_iternary_button')
                    PbNotifications.instance.showHint('Saved')
                })
            }
        }.bind(this))

        $("#search_input").keyup(function (e) {
            e.preventDefault();
            this.searchList($("#search_input").val());
        }.bind(this));
    }

    searchList(search = "") {
        RenderIternaryList.instance.renderContractList(search);
    }

    editMode() {
        RenderIternaryList.instance.current_state = "edit";
        $("#edit_iternary_button").hide();
        $("#cancel_edit_iternary_button").show();
        $("#save_iternary_button").show()

        $("#iternary_title").attr("disabled", false)
        $("#iternary_update_behaviour").attr("disabled", false)
        $("#iternary_hide").attr("disabled", false)
        $("#iternary_intro").attr("disabled", false)
        $("#iternary_description").attr("disabled", false)
        $(".singleOccupancyRate").attr("disabled", false)
        $(".doubleOccupancyRate").attr("disabled", false)
        $("#tags").attr("disabled", false)
    };

    viewMode() {
        RenderIternaryList.instance.current_state = "viewdetails";
        $("#edit_iternary_button").show();
        $("#cancel_edit_iternary_button").hide();
        $("#save_iternary_button").hide()

        $("#iternary_title").attr("disabled", true)
        $("#iternary_intro").attr("disabled", true)
        $("#iternary_update_behaviour").attr("disabled", true)
        $("#iternary_hide").attr("disabled", true)
        $("#iternary_description").attr("disabled", true)
        $(".singleOccupancyRate").attr("disabled", true)
        $(".doubleOccupancyRate").attr("disabled", true)
        $("#tags").attr("disabled", true)
    };
}