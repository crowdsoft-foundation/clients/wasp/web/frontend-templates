import {PbTpl} from "/csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "/csf-lib/pb-notifications.js?v=[cs_version]"
import {getCookie} from "/csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "/pb-config.js?v=[cs_version]"
import {pbI18n} from "/csf-lib/pb-i18n.js?v=[cs_version]"

export class Render {
    constructor() {
        if (!Render.instance) {
            Render.instance = this
            Render.instance.notifications = new PbNotifications()
            Render.instance.handledTaskIds = []
            Render.instance.i18n = new pbI18n()
            Render.instance.tpl = new PbTpl()
            Render.instance.timers = {}

            //document.addEventListener("event", function _(e) {
            //    if (e.data && e.data.data && e.data.data.event == "timetrackerUpdated") {
            //        this.renderAllTimers()
            //    }
            //}.bind(this))
        }

        return Render.instance
    }

    init(ModuleInstance) {
        this.moduleInstance = ModuleInstance
        this.renderAll().then(() => {
            $("#loader-overlay").hide()
        })
    }

    renderAll() {
        let data = {'foo': 'bar', 'foo2': 'bar', 'foo3': 'bar', 'foo4': 'bar'}

        return Render.instance.tpl.renderIntoAsync('pharmacy/templates/blub.html', {"data": data}, "#main_body_content").then(function () {
            //this.notifications.unblock("#main_body_content")
        }.bind(this))
    }

}
