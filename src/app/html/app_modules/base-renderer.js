import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]";
import {PbEditor} from "../../csf-lib/pb-editor.js?v=[cs_version]";
import {CONFIG} from "../../pb-config.js?v=[cs_version]"

export class BaseRenderer {
    fire_event(name, data) {
        let custom_event = new CustomEvent(name, {
            detail: data
        });
        document.dispatchEvent(custom_event);
    }
}
