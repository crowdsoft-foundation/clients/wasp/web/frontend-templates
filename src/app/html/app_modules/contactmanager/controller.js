import {Renderer} from "./renderer.js?v=[cs_version]"
import {Model} from "./model.js?v=[cs_version]"
import {PbNotifications} from "/csf-lib/pb-notifications.js?v=[cs_version]"

export class Controller {
    targetselectors = {
        "list": "",
        "detail": "",
        "form": ""
    }

    constructor(AppInstance, targetselectors = Controller.targetselectors) {
        this.targetselectors = targetselectors

        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.currentTaskId = undefined
            Controller.instance.appInstance = AppInstance
            Controller.instance.renderer = new Renderer(AppInstance, targetselectors)
            Controller.instance.model = new Model()
            Controller.instance.addEventListeners()
        }

        return Controller.instance
    }

    lazyLoadTranslations(module_name, language = null) {
        let i18n = new pbI18n()
        if (language == null || language == undefined) {
            language = i18n.instance.getAvailableBrowserLanguage()
        }

        let url = module_name + "/i18n/" + language + ".json?v=[cs_version]"
        return new Promise((resolve, reject) => {
            i18n.loadLanguageFile(url, i18n.setI18n, true).then(() => {
                resolve()
            })
        })
    }

    init() {
        return new Promise((resolve, reject) => {
            Controller.instance.model.loadData().then(() => {
                resolve()
            })
        });
    }

    renderListView(current_contact_id = undefined, force_reload = false) {
        Renderer.instance.notifications.blockForLoading("#res_list_wrapper")
        return new Promise((resolve, reject) => {
            console.log("RENDER LIST VIEW", Controller.instance.model)
            Controller.instance.renderer.renderListView(Controller.instance.model.contacts, current_contact_id).then(() => {
                Controller.instance.addListViewListeners()
                Renderer.instance.notifications.unblock("#res_list_wrapper")
                resolve()
            })
        });
    }

    renderDetailView(task_id = undefined, force_reload = false) {
        Renderer.instance.notifications.blockForLoading("#container_tasklist")
        return new Promise((resolve, reject) => {
            Controller.instance.addDetailViewListeners()
            Renderer.instance.notifications.unblock("#container_tasklist")
            resolve()
        });
    }

    renderFormView(task_id = undefined, force_reload = false) {
        Renderer.instance.notifications.blockForLoading("#container_tasklist")
        return new Promise((resolve, reject) => {
            Controller.instance.addFormViewListeners()
            Renderer.instance.notifications.unblock("#container_tasklist")
            resolve()
        });
    }

    addEventListeners() {
        // document.addEventListener("taskmanager_refresh_list", function (args) {
        //     Controller.instance.renderListView()
        // })

        // $(document).off('command')
        // $(document).on('command', function (event) {
        //     event = event.originalEvent
        //     if (event.data.data.command == "refreshQueueEntries") {
        //         Controller.instance.model.fetchDataFromApi(true).then(() => {
        //             Renderer.instance.fire_event("taskmanager_refresh_list_view", event.data.data)
        //             Renderer.instance.fire_event("taskmanager_refresh_detail_view", event.data.data)
        //             Renderer.instance.fire_event("taskmanager_refresh_form_view", event.data.data)
        //         })
        //     }
        // })
    }

    addListViewListeners() {
        $('[data-selector="list-entry"]').on('click', function (event) {
            event.preventDefault()
            event.stopPropagation()
            let contactId = $(this).attr("id")
            Controller.instance.currentContactId = contactId
            Controller.instance.renderDetailView(Controller.instance.currentContactId)
        })

        $("#contact-list-button-cancel").on("click", function () {
            if (Controller.instance.renderer.targetselectors.list == Controller.instance.renderer.targetselectors.detail) {
                Controller.instance.renderListView()
            }
        })
    }

    addDetailViewListeners() {
    }

    addFormViewListeners() {
    }

}
