import {PbContacts} from "/csf-lib/pbapi/contacts/pb-contacts.js?v=[cs_version]"
import {PbTagsList} from "/csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"

export class Model {
    targetselectors = {
        "list": "",
        "detail": "",
        "form": ""
    }

    constructor() {
        this.tags = []
        this.previous_data = []
        this.contacts = []
        if (!Model.instance) {
            Model.instance = this
        }
        return Model.instance
    }

    loadData() {
        let allPromises = []
        allPromises.push(this.fetchContacts())
        allPromises.push(this.fetchTags())
        return Promise.all(allPromises)
    }

    fetchTags() {
        return new Promise((resolve, reject) => {
            new PbTagsList().load().then(function (response) {
                Model.instance.tags = response.getSelectOptions()
                resolve(response.getSelectOptions())
            })
        });
    }

    fetchContacts() {
        return new Promise((resolve, reject) => {
            new PbContacts().loadFromApi().then(function (response) {
                Model.instance.contacts = response.getSelectOptions()
                resolve(response)
            })
        });
    }
}
