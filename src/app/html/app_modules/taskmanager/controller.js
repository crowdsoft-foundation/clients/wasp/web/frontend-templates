import {Renderer} from "./renderer.js?v=[cs_version]"
import {Model} from "./model.js?v=[cs_version]"
import {Render} from "../../task-manager/js/render.js?v=[cs_version]"
import {TaskManager} from "../../task-manager/js/task-manager.js?v=[cs_version]"
import {PbNotifications} from "/csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTag} from "../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"

export class Controller {
    targetselectors = {
        "list": "",
        "detail": "",
        "form": ""
    }

    constructor(AppInstance, targetselectors = Controller.targetselectors) {
        this.targetselectors = targetselectors

        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.currentTaskId = undefined
            Controller.instance.appInstance = AppInstance
            Controller.instance.renderer = new Renderer(AppInstance, targetselectors)
            Controller.instance.model = new Model()
            Controller.instance.addEventListeners()
            Controller.instance.self_induced_task_ids = []
            Controller.instance.tagListModel = new PbTagsList()
        }

        return Controller.instance
    }

    init() {
        return new Promise((resolve, reject) => {
            Controller.instance.model.loadData().then(() => {
                resolve()
            })
        });
    }

    renderListView(current_task_id = undefined, force_reload = false) {
        Controller.instance.renderer.notifications.blockForLoading("#res_list_wrapper")
        return new Promise((resolve, reject) => {
            Controller.instance.model.fetchDataFromApi(force_reload).then(() => {
                let filter = Model.instance.getFilter("status_filter")
                Model.instance.setFilter(filter)

                Controller.instance.renderer.renderListView(Controller.instance.model.data, current_task_id).then(() => {
                    Controller.instance.addListViewListeners()
                    Controller.instance.renderer.notifications.unblock("#res_list_wrapper")
                    resolve()
                })

                // setTimeout(function () {
                //     filter.value = "title"
                //     Model.instance.setFilter(filter)
                //     Controller.instance.renderer.renderListView(Controller.instance.model.data, current_task_id).then(() => {
                //         new PbNotifications().showHint("Filtered by field 'title' and value 'title'")
                //         Controller.instance.addListViewListeners()
                //         Renderer.instance.notifications.unblock("#res_list_wrapper")
                //         resolve()
                //     })
                // }, 5000)
                //
                //
                // setTimeout(function () {
                //     Model.instance.resetFilters()
                //     filter = Model.instance.getFilter("status_filter")
                //     filter.sort_order = "desc"
                //     filter.value = ["pending", "onhold"]
                //     Model.instance.setFilter(filter)
                //     Controller.instance.renderer.renderListView(Controller.instance.model.data, current_task_id).then(() => {
                //         new PbNotifications().showHint("Filtered by status in 'pending' and 'onhold'")
                //         Controller.instance.addListViewListeners()
                //         Renderer.instance.notifications.unblock("#res_list_wrapper")
                //         resolve()
                //     })
                // }, 10000)
                //
                // setTimeout(function () {
                //     let filter = Model.instance.getFilter("title_filter")
                //     filter.value = "test"
                //     Model.instance.setFilter(filter)
                //
                //     Controller.instance.renderer.renderListView(Controller.instance.model.data, current_task_id).then(() => {
                //         new PbNotifications().showHint("Added filter for title")
                //         Controller.instance.addListViewListeners()
                //         Renderer.instance.notifications.unblock("#res_list_wrapper")
                //         resolve()
                //     })
                // }, 15000)
                //
                //
                // setTimeout(function () {
                //     Model.instance.resetFilters()
                //     Controller.instance.renderer.renderListView(Controller.instance.model.data, current_task_id).then(() => {
                //         new PbNotifications().showHint("Removed all filters")
                //         Controller.instance.addListViewListeners()
                //         Renderer.instance.notifications.unblock("#res_list_wrapper")
                //         resolve()
                //     })
                // }, 20000)
            })
        });
    }

    renderDetailView(task_id = Controller.instance.currentTaskId, force_reload = false) {
        Renderer.instance.notifications.blockForLoading("#container_tasklist")
        return new Promise((resolve, reject) => {
            Controller.instance.model.fetchDataFromApi(force_reload).then(() => {
                let data = Controller.instance.model.getTaskById(task_id)
                if (data) {
                    data["groups"] = Controller.instance.model.groups;
                    data["resources"] = Controller.instance.model.ressources;
                    data["contacts"] = Controller.instance.model.contacts;
                    data["calendarentries"] = Controller.instance.model.calendarentries;
                    data["tags"] = Controller.instance.model.tags;
                    data["queue_lists"] = Controller.instance.model.queue_lists;
                }
                Controller.instance.renderer.renderDetailView(data).then(() => {
                    Controller.instance.addDetailViewListeners()
                    Renderer.instance.notifications.unblock("#container_tasklist")
                    resolve()
                })
            })
        });
    }

    renderDetailViewContacts(task_id = Controller.instance.currentTaskId, force_reload = false) {
        return new Promise((resolve, reject) => {
            Controller.instance.model.fetchDataFromApi(force_reload).then(() => {
                let data = Controller.instance.model.getTaskById(task_id)
                if (data) {
                    data["contacts"] = Controller.instance.model.contacts;
                }
                Controller.instance.renderer.renderDetailViewContacts(data).then(() => {
                    //Controller.instance.addDetailViewListeners()
                    Renderer.instance.notifications.unblock("#container_tasklist")
                    resolve()
                })
            })
        });
    }

    renderFormView(task_id = undefined, force_reload = false) {

        Renderer.instance.notifications.blockForLoading("#container_tasklist")
        return new Promise((resolve, reject) => {
            Controller.instance.model.fetchDataFromApi(force_reload).then(() => {
                let data = Controller.instance.model.getTaskById(task_id)
                if (data) {
                    data["groups"] = Controller.instance.model.groups;
                    data["resources"] = Controller.instance.model.ressources;
                    data["contacts"] = Controller.instance.model.contacts;
                    data["calendarentries"] = Controller.instance.model.calendarentries;
                    data["tags"] = Controller.instance.model.tags;
                    data["queue_lists"] = Controller.instance.model.queue_lists;
                }
                Controller.instance.renderer.renderFormView(data).then(() => {
                    Controller.instance.addFormViewListeners()
                    Renderer.instance.notifications.unblock("#container_tasklist")
                    resolve()
                })
            })
        });
    }

    addEventListeners() {
        document.addEventListener("taskmanager_refresh_list", function (args) {
            Controller.instance.renderListView()
        })

        document.addEventListener("contact_select_btn_clicked", function (args) {
            Controller.instance.model.setContact(Controller.instance.currentTaskId, args.detail.contact_id)
            Controller.instance.renderDetailViewContacts()
        })


        $(document).off('command')
        $(document).on('command', function (event) {
            event = event.originalEvent
            if (event?.data?.data?.command == "refreshQueueEntries") {
                Controller.instance.model.fetchDataFromApi(true).then(() => {
                    Renderer.instance.fire_event("taskmanager_refresh_list_view", event.data.data)
                    Renderer.instance.fire_event("taskmanager_refresh_detail_view", event.data.data)
                    Renderer.instance.fire_event("taskmanager_refresh_form_view", event.data.data)
                })
            }
        })

        $(document).on('taskmanager_form_input_change', function () {
            $(document).off('taskmanager_refresh_detail_view')
            $(document).on('taskmanager_refresh_detail_view', function (event) {
                if (!Controller.instance.self_induced_task_ids.includes(event.originalEvent.detail.args.task_id) && event.originalEvent.detail.args.entry_id == Controller.instance.currentTaskId) {
                    PbNotifications.instance.ask(
                        'The task you are currently editing was modified by another user. Do you want to reload the task now?',
                        'Task was edited by another user',
                        () => {
                            Controller.instance.renderFormView(Controller.instance.currentTaskId, true)
                            $(document).on('taskmanager_refresh_detail_view', function (event) {
                                Controller.instance.renderDetailView(Controller.instance.currentTaskId)
                            })
                        },
                        () => {

                        },
                        "warning",
                        "Reload",
                        "Abort")
                } else {
                    Controller.instance.renderListView(Controller.instance.currentTaskId)
                    Controller.instance.renderDetailView(Controller.instance.currentTaskId)
                }
            })
        })
    }

    addListViewListeners() {
        $('[data-selector="list-entry"]').on('click', function (event) {
            event.preventDefault()
            event.stopPropagation()
            let task_id = $(this).attr("id")
            Controller.instance.currentTaskId = task_id
            $('[data-selector="list-entry"]').removeClass("active")
            $("#" + task_id).addClass("active")
            Controller.instance.renderDetailView(Controller.instance.currentTaskId)
        })


        $("#new-entry").on("click", function (event) {
            event.preventDefault();
            event.stopPropagation();

            Controller.instance.currentTaskId = "new";
            Controller.instance.renderFormView(Controller.instance.currentTaskId)
        })

        $("#button-cancel").on("click", function () {
            Controller.instance.renderListView()
        })
    }

    addDetailViewListeners() {
        $("#button-cancel").on("click", function () {
            Controller.instance.renderListView()
        })

        $("#button-edit-task").on("click", function () {
            Controller.instance.renderer.notifications.blockForLoading(Controller.instance.renderer.targetselectors.list)
            Controller.instance.renderFormView(Controller.instance.currentTaskId)
            $(Controller.instance.renderer.targetselectors.list).attr("disabled", true)
        })

        $(document).off('taskmanager_refresh_detail_view')
        $(document).on('taskmanager_refresh_detail_view', function (event) {
            Controller.instance.renderDetailView(Controller.instance.currentTaskId)
            Controller.instance.renderListView(Controller.instance.currentTaskId)
        })
    }

    addFormViewListeners() {
        $('[data-selector="list-item-delete"]').on("click", function (e) {
            let todoId = $(e.currentTarget).attr("data-todoid");
            Controller.instance.renderer.removeTodoFromTask(todoId);
            Controller.instance.model.removeTodoFromTask(Controller.instance.currentTaskId, todoId);
        })

        $("#newTodoButton").on("click", function () {
            let todoValue = $("#todo_input").val();

            Controller.instance.renderer.addTodoToTask(todoValue);
            Controller.instance.model.addTodoToTask(Controller.instance.currentTaskId, todoValue);
        })

        $("#button-cancel").on("click", function () {
            Controller.instance.renderer.notifications.unblock(Controller.instance.renderer.targetselectors.list)
            Controller.instance.renderDetailView(Controller.instance.currentTaskId, true)
        })

        $("#remove_contact").on("click", function () {
            Controller.instance.model.removeContacts(Controller.instance.currentTaskId);
            Controller.instance.renderDetailViewContacts()
        })

        $("#addGlobalTagButton").click(() => {
            PbNotifications.instance.ask(
                '<div><input type="text" id="newGlobalTag" placeholder="" class="form-control"/></div>',
                'New global tag',
                () => {
                    Controller.instance.tagListModel.addTagToList(new PbTag().fromShort($("#newGlobalTag").val()).setUsableFor(["eventmanager"]), true)
                })
            $("#newGlobalTag").focus()
        })

        $("#button-save-task").on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();

            let formcheckResult = $("#task_form")[0].checkValidity()
            if(!formcheckResult) {
                let invalidElements = $("#task_form").find(':invalid');
                invalidElements.each(function() {
                  $(this).addClass("formcheckfailed")
                });
                return
            }


            Controller.instance.renderer.notifications.unblock(Controller.instance.renderer.targetselectors.list)
            let formElements = $("#container_tasklist").find("input, select, textarea");
            formElements.each(function (index, element) {
                let value = $(element).val();
                let targetdotpath = $(element).attr("data-targetdotpath");

                if (targetdotpath != undefined) {
                    if ($(element).is(':checkbox') || $(element).is(':radio')) {
                        value = $(element).is(':checked');
                    }

                    switch (targetdotpath) {
                        case "entry_data.todos.status":
                            Controller.instance.model.setTodoStatus(Controller.instance.currentTaskId, $(element).attr("data-todoid"), value);
                            break;
                        default:
                            Controller.instance.model.setTaskDataByDotPath(Controller.instance.currentTaskId, targetdotpath, value);
                            break;
                    }


                }
            });

            let editorContent = Controller.instance.renderer.editor.getContent();
            Controller.instance.model.setDescription(Controller.instance.currentTaskId, editorContent);

            Controller.instance.model.saveTask(Controller.instance.currentTaskId).then((response) => {
                Controller.instance.self_induced_task_ids.push(response.task_id)
            })
        })
    }

}
