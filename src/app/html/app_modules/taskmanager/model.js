import {PbApiRequest} from "/csf-lib/pbapi/pb-api-request.js?v=[cs_version]"
import {pbPermissions} from "/csf-lib/pb-permission.js?v=[cs_version]"
import {PbContacts} from "/csf-lib/pbapi/contacts/pb-contacts.js?v=[cs_version]"
import {PbEvents} from "/csf-lib/pbapi/calendar/pb-events.js?v=[cs_version]"
import {PbTagsList} from "/csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"
import {PbQueues} from "/csf-lib/pbapi/queue-manager/pb-queues.js?v=[cs_version]"
import {PbFilter} from "/csf-lib/pb-filter.js?v=[cs_version]"
import {makeId} from "/csf-lib/pb-functions.js?v=[cs_version]"

export class Model extends PbFilter {
    targetselectors = {
        "list": "",
        "detail": "",
        "form": ""
    }

    constructor() {
        super();
        this.data = []
        this.previous_data = []
        this.queues = []
        if (!Model.instance) {
            Model.instance = this
        }

        Model.instance.addFilter("status_filter", {
            "property_name": "data",
            "field": "entry_data.status",
            "operator": "in",
            "value": ["open", "planned", "pending",  "onhold", "closed"],
            "sort_field": "status",
            "sort_order": "asc"
        })

        Model.instance.addFilter("title_filter", {
            "property_name": "data",
            "field": "entry_data.title",
            "ignore_case": true,
            "operator": "like",
            "value": "",
            "sort_field": "entry_data.title",
            "sort_order": "asc"
        })


        return Model.instance
    }

    setDescription(task_id, description) {
        let task = this.getTaskById(task_id)
        task.entry_data.description = description
    }

    setTodoStatus(task_id, todo_id, status) {
        let task = this.getTaskById(task_id)

        let todo = task.entry_data.todos.find(todo => todo.id === todo_id)
        todo["done"] = status
        return this
    }

    removeTodoFromTask(task_id, todo_id) {
        let task = this.getTaskById(task_id)
        task.entry_data.todos = task.entry_data.todos.filter(todo => todo.id !== todo_id)
        return this
    }

    addTodoToTask(task_id, value) {
        // Get the task object by ID
        let task = this.getTaskById(task_id);

        if (task) {
            let newTodo = {
                "desc": value,
                "done": false,
                "id": makeId(10)
            };

            task.entry_data.todos.push(newTodo);
            return task;
        } else {
            console.error(`Task with ID ${task_id} not found.`);
        }
    }

    setContact(task_id, contactId) {
        this.removeContacts(task_id)
        let new_contact_link = {
            "resolver": "contact-manager",
            "type": "contact",
            "id": contactId
        }
        this.setTaskDataByDotPath(task_id, "links", [new_contact_link])
        return this
    }

    removeContacts(task_id, contactId = undefined) {
        let task = this.getTaskById(task_id);
        if (contactId) {
            task.links = task.links.filter(link => link.id !== contactId);
        } else {
            task.links = task.links.filter(link => link.type !== 'contact');
        }
        return this
    };

    loadData() {
        let allPromises = []
        allPromises.push(this.fetchDataFromApi())
        allPromises.push(this.fetchQueueLists())
        allPromises.push(this.fetchRessources())
        allPromises.push(this.fetchUsergroups())
        allPromises.push(this.fetchContacts())
        allPromises.push(this.fetchCalendarEntries())
        allPromises.push(this.fetchTags())
        return Promise.all(allPromises)
    }

    fetchTags() {
        return new Promise((resolve, reject) => {
            new PbTagsList().load().then(function (response) {
                Model.instance.tags = response.getSelectOptions([], "array")
                console.log("TAGS", Model.instance.tags)
                resolve(Model.instance.tags)
            })
        });
    }

    fetchCalendarEntries() {
        return new Promise((resolve, reject) => {
            new PbEvents().loadFromApi().then(function (response) {
                Model.instance.calendarentries = response.getEventList()
                resolve(response)
            })
        });
    }

    fetchContacts() {
        return new Promise((resolve, reject) => {
            new PbContacts().loadFromApi().then(function (response) {
                Model.instance.contacts = response.getSelectOptions()
                resolve(response)
            })
        });
    }

    fetchUsergroups() {
        return new Promise((resolve, reject) => {
            new pbPermissions().fetchGroupsFromApi(true).then(function (response) {
                Model.instance.groups = response
                resolve(response)
            })
        });
    }

    fetchRessources() {
        return new Promise((resolve, reject) => {
            new PbQueues().fetchRessources().then(function (response) {
                Model.instance.ressources = response
                resolve(response)
            }.bind(this))
        })

        // return new Promise((resolve, reject) => {
        //     new PbApiRequest("/get_taskmanager_resources").execute().then(function (response) {
        //         Model.instance.ressources = response
        //         resolve(response)
        //     })
        // });
    }

    fetchQueueLists() {
        return new Promise((resolve, reject) => {
            new PbQueues().fetchQueueLists().then(function (response) {
                Model.instance.queue_lists = response
                resolve(response)
            }.bind(this))
        })
    }

    getTaskById(taskId) {
        let task = Model.instance.data.find(task => task.entry_id === taskId)
        if(!task) {
            task = {
                        "entry_id": "new",
                        "entry_data": {
                            "queue_id": "",
                            "title": "",
                            "description": "",
                            "status": "open",
                            "follow_up": "",
                            "assigned_to": [],
                            "tags": [],
                            "todos": [],
                            "due_date": "",
                            "create_easy2schedule_event": false,
                            "order": 1,
                            "resource": ""
                        },
                        "data_owners": [],
                        "links": []
                    }
            Model.instance.data.push(task)
        }

        return task
    }

    setTaskDataByDotPath(taskId, dotpath, value) {
        let task = Model.instance.getTaskById(taskId)
        // resolve dotpath to object reference and set field to value
        let parts = dotpath.split('.')
        let i
        for (i = 0; i < parts.length - 1; i++) {
            task = task[parts[i]]
        }
        task[parts[i]] = value
    }

    saveTask(id) {
        if (id === "new" || id === undefined) {
            let newTask = Model.instance.getTaskById("new")
            delete newTask.entry_id
            return new Promise((resolve, reject) => {
                new PbQueues().newQueueEntry(newTask).then((response) => resolve(response))
            })
        } else {
            let currentTask = Model.instance.data.find(task => task.entry_id === id)
            return new Promise((resolve, reject) => {
                new PbQueues().updateQueueEntry(currentTask).then((response) => resolve(response))
            })
        }
    }

    fetchDataFromApi(force_reload = false) {
        let filter = {
            "filters": []
        }
        return new Promise((resolve, reject) => {
            new PbQueues().search(filter, force_reload).then(function (response) {
                Model.instance.previous_data = Model.instance.data
                Model.instance.data = response
                resolve(response.tasks)
            })
        })
    }
}
