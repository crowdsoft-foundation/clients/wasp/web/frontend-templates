import {Renderer} from "./renderer.js";
import {Model} from "./model.js";
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]";

export class Controller {
    targetselectors = {
        "list": "",
        "detail": "",
        "form": ""
    }

    constructor(AppInstance, targetselectors = Controller.targetselectors) {
        this.targetselectors = targetselectors

        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.appInstance = AppInstance
            Controller.instance.renderer = new Renderer(AppInstance, targetselectors)
            Controller.instance.model = new Model()
            Controller.instance.addEventListeners()
        }

        return Controller.instance
    }

    renderListView() {
        Renderer.instance.notifications.blockForLoading("#res_list_wrapper")
        return new Promise((resolve, reject) => {
            Controller.instance.model.fetchDataFromApi().then((data) => {
                Controller.instance.renderer.renderListView(data).then(() => {
                    Renderer.instance.notifications.unblock("#res_list_wrapper")
                    resolve()
                })
            })
        });
    }

    renderDetailView() {
        return new Promise((resolve, reject) => {
            Controller.instance.renderer.renderDetailView().then(() => {
                resolve()
            })
        });
    }

    renderFormView() {
        return new Promise((resolve, reject) => {
            Controller.instance.renderer.renderFormView().then(() => {
                resolve()
            })
        });
    }

    addEventListeners() {
        document.addEventListener("dummy_refresh_list", function (args) {
            Controller.instance.renderListView()
        })
    }

}
