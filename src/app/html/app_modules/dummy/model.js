import {PbApiRequest} from "/csf-lib/pbapi/pb-api-request.js?v=[cs_version]"
export class Model {
    targetselectors = {
        "list": "",
        "detail": "",
        "form": ""
    }

    constructor() {
        this.data = []
        if (!Model.instance) {
            Model.instance = this
        }

        return Model.instance
    }

    fetchDataFromApi() {
        return new Promise((resolve, reject) => {
            if (this.data.length > 0) {
                resolve(this.data)
            }
            new PbApiRequest("/cb/ressources").execute().then(function(response) {
                this.data = response
                resolve(response)
            }.bind(this))
        });
    }
}
