import {BASECONFIG} from "./pbjs/pb-config.js?v=[cs_version]";
export const CONFIG = BASECONFIG

CONFIG.NEEDED_JS_FILES = [
    {name: "jquery.cookie", path: "./js/pbjs/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
    {name: "jquery.blockUI", path: "./js/pbjs/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
    {name: "socket.io", path: "./js/pbjs/vendor/socket.io.js?v=[cs_version]", loaded: null},
    {name: "jquery.datatables", path: "/style-global/lib/datatables.net/js/jquery.dataTables.min.js?v=[cs_version]", loaded: null},
    {name: "jitsi_external", path: "https://meet.jit.si/external_api.js?v=[cs_version]", loaded:null},
]
CONFIG.PUBLIC_KEY = "6d2fceb4ab834b9e501554ead19778a3a5859c85"
CONFIG.PAGEOWNER = "planblick"
CONFIG.ENABLESTATISTICS = true

if (document.location.href.indexOf("http://localhost") == 0) {
    CONFIG.CIRCUIT_BASE_URL = "http://localhost:7070/circuits/"
    CONFIG.APP_BASE_URL = "http://localhost:7070"
    //CONFIG.API_BASE_URL = "https://api.planblick.com"
    //CONFIG.SOCKET_SERVER = "https://api.planblick.com"
    CONFIG.API_BASE_URL = "http://localhost:8000"
    CONFIG.SOCKET_SERVER = "http://localhost:8000"
    CONFIG.LOGIN_URL = "http://localhost:7070/login/"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
    CONFIG.URL_VERSION_APPENDIX = Math.floor(Date.now() / 1000)
    CONFIG.DEFAULT_LOGIN_REDIRECT = "../merchant.html"
    CONFIG.ENABLESTATISTICS = false

} else if (document.location.href.indexOf("testblick.de") != -1) {
    CONFIG.CIRCUIT_BASE_URL = "https://testblick.de/circuits/"
    CONFIG.APP_BASE_URL = "https://testblick.de/simplycollect/"
    CONFIG.API_BASE_URL = "https://odin.planblick.com"
    CONFIG.SOCKET_SERVER = "https://odin.planblick.com"
    CONFIG.LOGIN_URL = "https://testblick.de/login/"
    CONFIG.AVAILABLE_LANGUAGES = ["de", "en"]
    CONFIG.URL_VERSION_APPENDIX = Math.floor(Date.now() / 1000)
    CONFIG.DEFAULT_LOGIN_REDIRECT = "../merchant.html"
    CONFIG.ENABLESTATISTICS = false
} else {
    CONFIG.LOGIN_URL = "https://www.simplycollect.de/login/"
    CONFIG.APP_BASE_URL = "https://www.simplycollect.de"
    CONFIG.CIRCUIT_BASE_URL = "https://www.simplycollect.de/circuits/"
    CONFIG.URL_VERSION_APPENDIX = Math.floor(Date.now() / 1000)
    CONFIG.DEFAULT_LOGIN_REDIRECT = "../merchant.html"
}