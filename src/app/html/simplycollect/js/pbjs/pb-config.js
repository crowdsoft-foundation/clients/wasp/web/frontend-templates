
export var BASECONFIG = {}
BASECONFIG.DEFAULT_LOGIN_REDIRECT = "/"
BASECONFIG.JS_CACHE_ACTIVE = true
BASECONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT = 5000
BASECONFIG.FILE_LOAD_FAILED_MAX_RETRIES = 10
BASECONFIG.DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
BASECONFIG.DB_DATEFORMAT = "YYYY-MM-DD HH:mm:ss"
BASECONFIG.NEEDED_JS_FILES = [
    {name: "jquery.cookie", path: "./js/pb/vendor/jquery/jquery.cookie.js?v=[cs_version]", loaded: null},
    {name: "jquery.blockUI", path: "./js/pb/vendor/jquery/jquery.blockUI.js?v=[cs_version]", loaded: null},
]
BASECONFIG.SOCKETIO_SUBPATH = "/socket"
BASECONFIG.JITSI_DOMAIN = "meet.jit.si"
BASECONFIG.AVAILABLE_LANGUAGES = []
BASECONFIG.URL_VERSION_APPENDIX = "0"

if (document.location.href.indexOf("http://localhost") == 0) {
    BASECONFIG.CIRCUIT_BASE_URL = "http://localhost:8080/circuits/"
    BASECONFIG.API_BASE_URL = "http://localhost:8000"
    BASECONFIG.APP_BASE_URL = "http://localhost:7000"
    BASECONFIG.SOCKET_SERVER = "http://localhost:8000"
    BASECONFIG.SOCKETIO_SUBPATH = "/socket"
    BASECONFIG.COOKIE_DOMAIN = "localhost"
    BASECONFIG.LOGIN_URL = "http://localhost:7000/login/"
    BASECONFIG.REGISTER_URL = "http://localhost:8080/register.html"
} else if (document.location.href.indexOf("http://fk.planblick.com") == 0) {
    BASECONFIG.CIRCUIT_BASE_URL = "http://fk.planblick.com:8080/static/"
    BASECONFIG.API_BASE_URL = "http://fk.planblick.com:8000"
    BASECONFIG.APP_BASE_URL = "http://fk.planblick.com:7000"
    BASECONFIG.SOCKET_SERVER = "http://fk.planblick.com:8000"
    BASECONFIG.COOKIE_DOMAIN = "crowdsoft.net"
    BASECONFIG.LOGIN_URL = "http://fk.planblick.com:7000/login"
    BASECONFIG.REGISTER_URL = "http://fk.planblick.com:7000/registration/"
} else {
    BASECONFIG.CIRCUIT_BASE_URL = "./circuits/"
    BASECONFIG.API_BASE_URL = "https://api.planblick.com"
    BASECONFIG.APP_BASE_URL = "https://crowdsoft.net"
    BASECONFIG.APP_STARTPAGE = "/"
    BASECONFIG.SOCKET_SERVER = "https://api.planblick.com"
    BASECONFIG.COOKIE_DOMAIN = "crowdsoft.net"
    BASECONFIG.LOGIN_URL = "https://crowdsoft.net/login/"
    BASECONFIG.REGISTER_URL = "https://crowdsoft.net/registration/"
}
