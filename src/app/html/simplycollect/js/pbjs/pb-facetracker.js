import {fireEvent, waitFor, speech, getCookie} from "./pb-functions.js?v=[cs_version]";
import {CONFIG} from "../pb-config.js?v=[cs_version]";
import {pbMedia} from "./pb-media.js?v=[cs_version]";

export class pbFacetracker {
    constructor(facerecognition_active = true) {
        this.facerecognition_active = facerecognition_active
        this.newFace = true
        this.debugging = false
        this.showVideo = true
        this.system_ready = false
        this.tracking_active = false
        this.video = document.getElementById('pbMediaVideo') || document.createElement("video")
        this.video.id = "pbMediaVideo"
        this.video.width = "360"
        this.video.height = "280"
        this.no_detection_count = 0
        if (this.debugging || this.showVideo) {
            $("body").append(this.video)
        }

        if (!pbFacetracker.instance) {
            pbFacetracker.instance = this;
            pbFacetracker.instance.init()
        }
        return pbFacetracker.instance;
    }

    init() {
        pbFacetracker.instance.video.addEventListener('playing', () => {
            console.log("Initializing Facetracker")
            $("#overlay").hide()
            $(".loading").hide()
            $("#content").show()
            var canvas = document.getElementById("face_detection") || faceapi.createCanvasFromMedia(pbFacetracker.instance.video)
            canvas.id = "face_detection"
            if (!pbFacetracker.instance.debugging) {
                canvas.style.display = "none"
            }
            $("body").append(canvas)
            var displaySize = {width: pbFacetracker.instance.video.width, height: pbFacetracker.instance.video.height}
            faceapi.matchDimensions(canvas, displaySize)
            pbFacetracker.instance.canvas = canvas
            pbFacetracker.instance.displaySize = displaySize

        })
        overlay
        $("#overlay").show()
        $(".loading").show()
        $("#content").hide()
        Promise.all([
            faceapi.nets.tinyFaceDetector.loadFromUri(CONFIG.ML_MODEL_URL),
            faceapi.nets.faceLandmark68Net.loadFromUri(CONFIG.ML_MODEL_URL),
            faceapi.nets.faceRecognitionNet.loadFromUri(CONFIG.ML_MODEL_URL),
            //faceapi.nets.faceExpressionNet.loadFromUri(CONFIG.ML_MODEL_URL),
        ]).then(pbFacetracker.instance.modelsLoaded)
    }

    modelsLoaded() {
        console.log("Models loaded")
        pbFacetracker.instance.system_ready = true
    }

    startTracking() {
        waitFor(function () {
            return pbFacetracker.instance.system_ready
        }, pbFacetracker.instance._activateScanning)

    }

    _activateScanning() {
        console.log("Initializing facescanner")
        document.addEventListener("capturingFacetrackingVideo", function circuitLoader(event) {
            console.log("Start facescanning")
            pbFacetracker.instance.startFaceScanner()
        })

        new pbMedia($("#mediaOptions").get(0), "videoWithAudioInput").init().start(function () {
            fireEvent("capturingFacetrackingVideo")
        })
    }

    startFaceScanner() {
        console.log("Start scanning for faces")
        pbFacetracker.instance.interval = setInterval(() => {
            pbFacetracker.instance.scanForImage()
        }, 200)
    }

    stopFaceScanner() {
        console.log("Stop scanning for faces")
        clearInterval(pbFacetracker.instance.interval)
    }

    async scanForImage() {
        if (!this.newFace) {
            return
        }
        let canvas = pbFacetracker.instance.canvas
        let displaySize = pbFacetracker.instance.displaySize
        let video = pbFacetracker.instance.video
        if (!canvas)
            return
        var ctx = canvas.getContext('2d');
        // https://github.com/justadudewhohacks/face-api.js
        const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceDescriptors()
        const resizedDetections = faceapi.resizeResults(detections, displaySize)

        if (!detections[0]) {
            //console.log("No faces found")
            //pbFacetracker.instance.previous_detections = undefined
            pbFacetracker.instance.no_detection_count += 1
            if (pbFacetracker.instance.no_detection_count > 10) {
                pbFacetracker.instance.previous_detections = undefined
            }
            return
        }
        pbFacetracker.instance.no_detection_count = 0

        if (detections[0] && pbFacetracker.instance.previous_detections && pbFacetracker.instance.previous_detections[0]) {
            let desc_1 = pbFacetracker.instance.previous_detections[0].descriptor
            let desc_2 = detections[0].descriptor

            let dist = faceapi.euclideanDistance(desc_1, desc_2)
            //console.log("DISTANCE", dist)
            if (dist <= 0.7) {
                //console.log("Skipping same person", dist)
                return
            }
        }

        pbFacetracker.instance.previous_detections = detections
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
        if (pbFacetracker.instance.debugging) {
            faceapi.draw.drawDetections(canvas, resizedDetections)
        }

        if (resizedDetections[0]) {
            let point_a = {
                x: parseInt(resizedDetections[0].detection._box.x) - 30,
                y: parseInt(resizedDetections[0].detection._box.y) - 30
            }
            let point_b = {
                x: parseInt(resizedDetections[0].detection._box.x + resizedDetections[0].detection._box._width) + 30,
                y: parseInt(resizedDetections[0].detection._box.y + resizedDetections[0].detection._box._height) + 30
            }

            let out_canvas = document.getElementById("detectedImage")
            if (!out_canvas) {
                out_canvas = document.createElement("canvas")
                out_canvas.id = "detectedImage"
                if (!pbFacetracker.instance.debugging) {
                    out_canvas.style.display = "none"
                }

                $("body").append(out_canvas)

            }


            pbFacetracker.instance.crop(canvas, point_a, point_b, out_canvas)

            if (pbFacetracker.instance.newFace == true) {
                pbFacetracker.instance.newFace = false
                pbFacetracker.instance.freezeFaceFrame()
                //setTimeout((function() { this.newFace=true }).bind(this), 3000)
            }
        }
    }


    freezeFaceFrame() {
        document.getElementById("detectedImage").toBlob((blob) => {
            //console.log(blob)
            //fireEvent("newVisitorRecognized", {"image_blob": blob})
        })
        let image_data_url = document.getElementById("detectedImage").toDataURL("image/png")
        let freezed_image = document.getElementById("freezedImage")
        if (!freezed_image) {
            freezed_image = document.createElement("img")
            freezed_image.id = "freezedImage"

            if (!pbFacetracker.instance.debugging) {
                freezed_image.style.display = "none"
            }

            $("body").append(freezed_image)
        }
        freezed_image.src = image_data_url


        if (pbFacetracker.instance.facerecognition_active == true) {

            var settings = {
                "url": CONFIG.FACE_RECOGNITION_URL,
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({"image_data_url": image_data_url}),
                "success": function (response) {
                    console.log("RESULT", response.message)
                    if (response.message != false) {
                        fireEvent("faceRecognized", response)
                    } else {
                        fireEvent("faceNotRecognized")
                    }
                },
                "error": function () {
                    fireEvent("faceNotRecognized")
                }
            };

            $.ajax(settings).done()
        } else {
            fireEvent("faceNotRecognized")
        }
        //fireLocalEvent("newVisitorDiscovered", {"image_data_url": image_data_url})

    }

    crop(can, a, b, out_canvas = null) {
        // get your canvas and a context for it

        var ctx = can.getContext('2d');

        // get the image data you want to keep.
        var imageData = ctx.getImageData(a.x, a.y, b.x, b.y);

        // create a new cavnas same as clipped size and a context
        if (out_canvas == null) {
            out_canvas = document.createElement('canvas');
        }
        out_canvas.width = b.x - a.x;
        out_canvas.height = b.y - a.y;

        var outCtx = out_canvas.getContext('2d');

        // put the clipped image on the new canvas.
        outCtx.putImageData(imageData, 0, 0);

        return imageData;
    }

}