import {getCookie, fireEvent, makeId} from "./pbjs/pb-functions.js?v=[cs_version]";
import {pbShoppingSession} from "./pbjs/pb-shoppingsession.js?v=[cs_version]"
import {PbAccount} from "./pbjs/pb-account.js?v=[cs_version]"
import {pbI18n} from "./pbjs/pb-i18n.js?v=[cs_version]";

export class pbApp {
    constructor() {
        if (!pbApp.instance) {
            pbApp.instance = this;
            pbApp.instance.init()
        }
        return pbApp.instance;
    }

    init() {
        let table = $('#basket').DataTable({
            "columnDefs": [
                {
                    "targets": [4, 5],
                    "className": 'dt-right'
                },
                {
                    "targets": [3],
                    "className": 'dt-center'
                }
            ],
            "language": {
                "search": '<i class="typcn typcn-zoom" style="font-size:1.5rem;"></i>',
                "lengthMenu": "_MENU_ pro Seite",
                "zeroRecords": "Keine Einträge",
                "info": "Seite _PAGE_ von _PAGES_",
                "infoEmpty": "Keine Einträge vorhanden",
                "infoFiltered": "(gefiltert von _MAX_ Einträgen insgesamt)",
                "paginate": {
                    "first": "<<",
                    "last": ">>",
                    "next": ">",
                    "previous": "<"
                },
            },
            "paging": false,
            "info": false,
            "iDisplayLength": 1000,
            "ordering": false,
            "bFilter": false
        })
        var column = table.column(6);

        let urlParams = new URLSearchParams(window.location.search)
        if (urlParams.has("mid")) {
            // Toggle the visibility
            column.visible(false);
        }

        $("#addToBasket").click(new myButtonFunctions().addToBasket)
        $("#chat_send_button").click(function (event) {
            event.preventDefault();
            new myButtonFunctions().sendChatMessage();
            $('#chat_message_input').val("")
        })
        $("#clearChatMessagesButton").click(new myButtonFunctions().clearChatMessages)
        $("#printOrderButton").click(new myButtonFunctions().printOrder)
        $('#order_notices').bind('keypress input propertychange', function () {
            new myButtonFunctions().updateOrderNotices($('#order_notices').val())
        });

        $('#submitBasketToCustomer').click(new myButtonFunctions().submitBasketToCustomer)
        $('#product_add_button').click(new myButtonFunctions().addProductTobasket)

        $('*[data-replace="username"]').html(getCookie("username"))
        //$('*[data-replace="merchantName"]').html(new pbI18n().translate(getCookie("consumer_name")))

        $('#loader-overlay').hide();
        $('#contentwrapper').show();
        $('#logout').click(() => new PbAccount().logout(true))

        $('#chat_message_input').keyup(function (e) {
            if (e.keyCode == 13) {
                $('#chat_send_button').click()
            }
        });

        $('#updateMerchantOnlineState').click(function () {
            fireEvent("isMerchantOnline", {}, getCookie("consumer_name"))
        })

        $('#sendPaypalMeLink').click(function () {
            let table_selector = '#basket'
            let table = $(table_selector).DataTable()
            let payment_amount = parseFloat(table.row(':last').data()[5].replace(",", "."))
            let link = $('#paypal-me-template').clone()
            link.removeAttr("style")
            link.removeAttr("id")
            link.attr("href", new pbI18n().translate('paypal-me-address') + "/" + payment_amount + "EUR")
            link = String(link.prop('outerHTML'))
            $('#order_notices').val($('#order_notices').val() + "\n" + link)
            new myButtonFunctions().updateOrderNotices($('#order_notices').val())
        })

    }
}

var newBasketPos = 0

class myButtonFunctions {
    submitBasketToCustomer() {
        fireEvent("orderSubmitted", {
            "sent_time": new Date(),
            "message_id": makeId(10)
        }, new pbShoppingSession().roomName)
    }

    updateOrderNotices(newcontent) {
        fireEvent("orderNoticeUpdate", {
            "newcontent": newcontent,
            "sent_time": new Date(),
            "message_id": makeId(10)
        }, new pbShoppingSession().roomName)
    }

    printOrder() {
        var prtContent = document.getElementById("printContent").cloneNode(true)
        prtContent.querySelectorAll('a').forEach((a) => {
                console.log("MODIFYING LINK" + a);
                a.innerHTML = a.innerHTML + ": " + a.href
            });
        var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0')
        WinPrint.document.write(document.getElementById("headContent").innerHTML)
        WinPrint.document.write('<link rel="stylesheet" href="./css/print.css">')
        WinPrint.document.write(prtContent.outerHTML)
        WinPrint.focus();
        setTimeout(function () {
            WinPrint.print();
        }, 1000)

        //WinPrint.close();
    }


    clearChatMessages() {
        $("#chat_messages_container").html("")
    }

    sendChatMessage() {
        fireEvent("newChatMessage", {
            "from_username": getCookie("username"),
            "message": $("#chat_message_input").val(),
            "sent_time": new Date(),
            "shopping_session": new pbShoppingSession().roomName,
            "message_id": makeId(10)
        }, new pbShoppingSession().roomName)
    }

    addProductTobasket(withOptionsColumn = true) {
        let table_selector = '#basket'
        let table = $(table_selector).DataTable()
        newBasketPos = table.rows().count() + 1
        let newId = $('#product_id').val()
        let newName = $('#product_name').val()
        let newCnt = $('#product_amount').val()
        let newPrice = $('#product_price').val().replace(",", ".")
        let newArticleSum = newCnt * newPrice
        let editoptions = '<!--a href="" class="editor_edit">Edit</a> /--><i class="editor_remove far fa-trash-alt"></i>'
        let newRow = [
            newBasketPos,
            newId,
            newName,
            newCnt,
            new Intl.NumberFormat('de-DE', {style: 'currency', currency: 'EUR'}).format(newPrice),
            new Intl.NumberFormat('de-DE', {style: 'currency', currency: 'EUR'}).format(newArticleSum)
        ]
        if (withOptionsColumn) {
            newRow.push(editoptions)
        }
        let newRowId = "artnr_" + newId

        fireEvent("addToBasket", {
            "table_selector": table_selector,
            "new_row_id": newRowId,
            "new_row_data": newRow
        }, new pbShoppingSession().roomName)

        $('product_id').val("")
        $('product_name').val("")
        $('product_amount').val("")
        $('product_price').val("")

        // Delete a record
        $(table_selector).off('click')
        $(table_selector).on('click', '.editor_remove', function (e) {
            e.preventDefault();

            fireEvent("removeFromBasket", {
                "table_selector": table_selector,
                "rowid": $(this).closest('tr').attr("id")
            }, new pbShoppingSession().roomName)
        });
    }

    addToBasket(withOptionsColumn = true) {
        $("#addArticleModal").modal()
    }
}
