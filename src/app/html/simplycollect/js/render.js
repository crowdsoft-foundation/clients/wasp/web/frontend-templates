import {PbAccount} from "./pbjs/pb-account.js?v=[cs_version]";

export class Render {
    static logins(containerElement, forceReload = false, logins = undefined) {
        // prepare data for display
        containerElement.innerText = ""
        if (forceReload) {
            new PbAccount().init().then((value) => {
                Render.logins(containerElement, false)
            })
            return
        }

        if (!logins) {
            logins = new PbAccount().logins
        }
        logins.sort(function (a, b) {
            let textA = a.username.toUpperCase();
            let textB = b.username.toUpperCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        })

        // Rendering-variant 1: Manually created nodes for dom
        for (let login of logins) {
            let loginContainer = document.createElement("div")
            loginContainer.id = "login_" + login.userid
            loginContainer.setAttribute('data-loginid', login.userid);
            loginContainer.className = "login-container"

            let loginNameContainer = document.createElement("span")
            loginNameContainer.innerText = login.username
            loginNameContainer.className = "login-name"

            let deleteLoginButton = document.createElement("button")
            deleteLoginButton.id = "deleteLoginButton_" + login.userid
            deleteLoginButton.name = "deleteLoginButton"
            deleteLoginButton.innerHTML = "L&ouml;schen"
            deleteLoginButton.onclick = () => {
                if (confirm("Wollen Sie den Login " + login.username + " wirklich löschen?")) {
                    new PbAccount().deleteLogin(
                        login.userid,
                        function () {
                            alert("Login " + login.username + " wurde gelöscht.");
                            Render.logins(document.getElementById("bootstraptable"), true)
                        })
                }
            }
            deleteLoginButton.setAttribute('data-loginid', login.userid);
            deleteLoginButton.setAttribute('data-loginname', login.username);


            loginContainer.append(loginNameContainer)
            loginContainer.append(deleteLoginButton)
            containerElement.append(loginContainer)

        }

        // Renderin-variant 2: Bootstrap table

        // We still have to create the buttons because bootstrap cannot handle this properly
        // Although we could use html-strings here as well.
        let deleteLoginButtons = []

        for (let idx in logins) {
            let login = logins[idx]
            let deleteLoginButton = document.createElement("button")
            deleteLoginButton.id = "deleteLoginButtonBootstrap_" + login.userid
            deleteLoginButton.name = "deleteLoginButtonBootstrap"
            deleteLoginButton.innerHTML = "L&ouml;schen"

            deleteLoginButton.setAttribute('data-loginid', login.userid);
            deleteLoginButton.setAttribute('data-loginname', login.username);
            logins[idx].options = deleteLoginButton.outerHTML
        }

        $(function () {
            $('#bootstraptable').bootstrapTable({
                data: logins
            });
            $('#bootstraptable').bootstrapTable('load', logins)
        });

        // We have to remove the click handlers in case we already went through here already
        $(document).off("click", 'button[name ="deleteLoginButtonBootstrap"]')
        // Then (re-)add the click handler performing the delete action
        $(document).on("click", 'button[name ="deleteLoginButtonBootstrap"]', function clickhandler(event) {
                if (confirm("Wollen Sie den Login " + event.target.dataset.loginname + " wirklich löschen?")) {
                    new PbAccount().deleteLogin(
                        event.target.dataset.loginid,
                        function () {
                            alert("Login " + event.target.dataset.loginname + " wurde gelöscht.");
                            Render.logins(document.getElementById("bootstraptable"), true)
                        })
                }
            }
        )
    }
}