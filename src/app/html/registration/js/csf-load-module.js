import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]";
import {getCookie, fireEvent, togglePassword} from "../../csf-lib/pb-functions.js?v=[cs_version]";
import {clearForm, stepValidatePassword} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {validateRegistrationForm, validateInviteForm, registerInvite} from "./register.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"


export class RegistrationModule {
    actions = {}
    task_id_handlers = {}
    constructor() {
        if (!RegistrationModule.instance) {
            this.initListeners()
            this.initActions()
            RegistrationModule.instance = this;
        }

        return RegistrationModule.instance;
    }

    initActions() {

        //init action in pb-event-actions
        this.actions["registrationModuleInitialised"] = function (myevent, callback = () => "") {

            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }
            stepValidatePassword("password")
            callback(myevent)
        }


        this.actions["registerButtonClicked"] = function (myevent, callback = () => "") {

            let onsuccess = function (response) {
                response = JSON.parse(response)

                console.log("response from onsuccess", response, typeof response)

                // For now we have to act on successfull request, since currently we cannot connect to socketio without an account
                window.setTimeout(function() {
                    Fnon.Hint.Success(new pbI18n().translate('Du bekommst gleich eine Email von uns. Bitte bestätige deine Registierung in dem du auf den Link in der Email klickst.'), { displayDuration: 6000 })
                clearForm('login', 'email', 'password', 'password_confirmation')
                Fnon.Box.Remove('form')
                }, 3000)


                // RegistrationModule.instance.task_id_handlers[response.taskid] = function (args) {
                //     delete RegistrationModule.instance.task_id_handlers[response.taskid]
                //     Fnon.Box.Remove('form')
                //     if (args.data.data.args["error_message"]) {
                //         Fnon.Hint.Danger(new pbI18n().translate("Login wasn't created") + " <br/>" + new pbI18n().translate(args.data.data.args["error_message"]), { displayDuration: 6000 }, function () {
                //
                //         })
                //     } else {
                //         Fnon.Hint.Success(new pbI18n().translate('Du bekommst gleich eine Email von uns. Bitte bestätige deine Registierung in dem du auf den Link in der Email klickst.'), { displayDuration: 6000 })
                //         clearForm('login', 'email', 'password', 'password_confirmation')
                //     }
                // }

            }

            let onerror = function (response) {

                Fnon.Box.Remove('form')
                Fnon.Hint.Danger(new pbI18n().translate("Login wasn't created") + " <br/>" + new pbI18n().translate(args.data.data.args["error_message"]), { displayDuration: 6000 }, function () {
                })

            }

            console.log("MYEVENT", myevent)
            new PbAccount().startRegistrationProcess(myevent.data.login, myevent.data.email, myevent.data.password, onsuccess, onerror)
            document.getElementById('login-error-msg').innerHTML = ""

            callback(myevent)
        }

        this.actions["registerInviteButtonClicked"] = function (myevent, callback = () => "") {
            console.log("DATA", myevent.data.login)
            registerInvite(getCookie("apikey"), myevent.data.login, myevent.data.password, myevent.data.email)
            callback(myevent)
        }
    }

    initListeners() {
        $("#register_button").click(function(event) {
            event.preventDefault()
            validateRegistrationForm()
        })


        $(document).on('click', '#invite_confirm_button', function (e) {
            e.preventDefault()
            validateInviteForm()
    
        })

        $("#pw-toggle-new").click(function () {
            togglePassword("password", "pw-toggle-new")
        })
        $("#pw-toggle-new-confirm").click(function () {
            togglePassword("password_confirmation", "pw-toggle-new-confirm")
        })

        document.addEventListener("notification", function(args) {
            console.log("GOT NOTIFICATION WITH ARGS", args.data.data.args.correlation_id, RegistrationModule.instance.task_id_handlers)
            if(Object.keys(RegistrationModule.instance.task_id_handlers).includes(args.data.data.args.correlation_id)) {
                RegistrationModule.instance.task_id_handlers[args.data.data.args.correlation_id](args)
            }
        })



    }
}