<table class="table az-table-reference mg-0" id="bootstraptable">
    <thead>
    <tr>
        <th data-field="service" class="" data-i18n="service"></th>
        <th data-field="options" class="" data-i18n="Downloads"></th>
    </tr>
    </thead>
    <tbody>
        {% for key, value in data %}
        <tr>
            <td class="" data-i18n="{{key}}"></td>
            <td class="">
                <a class="btn btn-indigo" target="_blank" data-i18n="csv" href="{{value.csv}}"></a>
                <a class="btn btn-indigo" target="_blank" data-i18n="json" href="{{value.json}}"></a>
            </td>
        </tr>
        {% endfor %}
    </tbody>
</table>