import {PbTimer} from "../js/timer.js"

let utcReturnValue = Math.floor(Math.random() * 1000);
global.moment = () => {
    return {utc: () => utcReturnValue}
}

let mock_internal_fireEvent = undefined

jest.mock("../../csf-lib/pb-functions.js", () => {
    return {
        fireEvent: function (name, data) {
            mock_internal_fireEvent = name
        }
    }
});

test('Can create new instance of PbTimer', () => {
    let timer = new PbTimer()
    timer.setData({
        "id": 1,
        "times": [],
        "time": 0,
        "title": ""
    })
    expect(timer.getData()).toStrictEqual({
        "id": 1,
        "times": [],
        "time": 0,
        "title": ""
    })
});

test('Cannot create new instance of PbTimer if id is undefined', () => {
    let timer = new PbTimer()
    let func = () => timer.setData({
        "id": undefined,
        "times": [],
        "time": 0,
        "title": ""
    })
    expect(func).toThrow(Error);
});

test('Test whether tick function updates time', () => {
    let timer = new PbTimer()
    timer.setData({
        "id": 1,
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })
    timer.tick()
    expect(timer.getData().times[0].end).toBe(utcReturnValue)
});

test('Test whether starting and stopping of ticking works', () => {
    let timer = new PbTimer()
    timer.setData({
        "id": 1,
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })

    expect(timer.interval).toBeDefined()
    timer.stopTicking()
    expect(timer.interval).not.toBeDefined()
    timer.startTicking()
    expect(timer.interval).toBeDefined()
});

test('Test whether timetrackerCreated gets fired on backend event properly', async () => {
    let timer_real = new PbTimer()
    timer_real.setData({
        "id": "tmp_1",
        "times": [{"start": 0, "end": 0}],
        "time": 0,
        "title": ""
    })

    document.addEventListener("timetrackerCreated", function _(e) {
        mock_internal_fireEvent = "timetrackerCreated"

    })
    const event = new Event('event')
    event.data = {"data": {"event": "timetrackerCreated", "args": {"data": {"uuid": "asdas12345"}}}}
    document.dispatchEvent(event)
    expect(mock_internal_fireEvent == "timetrackerCreated").toBe(true)
    mock_internal_fireEvent = undefined
});
//# sourceMappingURL=timer.test.js.map