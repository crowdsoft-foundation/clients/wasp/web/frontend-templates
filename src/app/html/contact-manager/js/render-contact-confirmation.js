import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]";
import {getQueryParam} from "../../csf-lib/pb-functions.js?v=[cs_version]"


export class RenderGuestConfirmation {
    constructor(){
        if(!RenderGuestConfirmation.instance){
            RenderGuestConfirmation.instance = this
            RenderGuestConfirmation.instance.socket = new socketio()
            
        }

        return RenderGuestConfirmation.instance
    }

    initListeners(){
        
        RenderGuestConfirmation.instance.socket.commandhandlers["contactVerifiedSuccessfully"] = function (args) {
            console.log("args", args)
            if(args.task_id==getQueryParam("corid")){
                $('#success-display-message').prop('hidden', false)
                alert("Contact was successfully verified")
            }
            
        }

        RenderGuestConfirmation.instance.socket.commandhandlers["contactVerifiedFailed"] = function (args) {
            // Backend says verification failed for whatever reasons
            console.log("args", args)
            if(args.task_id==getQueryParam("corid")){
                $('#error-display-message').prop('hidden', false)
                alert("Contact verification failed")
            }
            
        } 
    }
}