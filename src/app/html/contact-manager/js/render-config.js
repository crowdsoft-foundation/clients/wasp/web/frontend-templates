import {ContactManager} from "./contact-manager.js?v=[cs_version]"
import {getCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]";
import {validateForm, handleGUIbyidError} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"


export class RenderConfig {
    fieldconfig = [];
    active_field = {}
    new_mode = false
    edit_mode = false
    position_list = []
    display_positions = {}
    templates = {
        name: {
            input_type: "text",
            css_classes: ["fas fa-address-card"],
            locked_id: false
        },
        vorname: {
            input_type: "text",
            css_classes: ["fas fa-address-card"],
            locked_id: false
        },
        street: {
            input_type: "text",
            css_classes: ["fas fa-road"],
            locked_id: true
        },
        housenumber: {
            input_type: "text",
            css_classes: ["fas fa-house-user"],
            locked_id: true
        },
        zipcode: {
            input_type: "text",
            css_classes: ["fas fa-map-marker"],
            locked_id: true
        },
        city: {
            input_type: "text",
            css_classes: ["fas fa-city"],
            locked_id: true
        },
        firma: {
            input_type: "text",
            css_classes: ["fas fa-briefcase"],
            locked_id: false
        },
        email: {
            input_type: "email",
            css_classes: ["fas fa-at"],
            locked_id: true
        },
        accepts_mail_notifications: {
            input_type: "checkbox",
            css_classes: ["fas fa-envelope"],
            locked_id: true
        },
        telefon: {
            input_type: "tel",
            css_classes: ["fas fa-phone-square"],
            locked_id: false
        },
        passwort: {
            input_type: "password",
            css_classes: ["fas fa-asterisk"],
            locked_id: false
        },
        datum: {
            input_type: "date",
            css_classes: ["fas fa-calendar-check"]
        },
        kommentar: {
            input_type: "textarea",
            css_classes: ["fas fa-comment-dots"],
            cols: 3,
            locked_id: false
        },
        text: {
            input_type: "text",
            css_classes: ["fas fa-address-card"],
            locked_id: false
        },
        textarea: {
            input_type: "textarea",
            css_classes: ["fas fa-address-card"],
            cols: 3,
            locked_id: false
        },
        switch: {
            input_type: "switch",
            css_classes: ["fas fa-address-card"],
            locked_id: false
        },
        checkbox: {
            input_type: "checkbox",
            css_classes: ["fas fa-address-card"],
            locked_id: false
        },
        select: {
            input_type: "select",
            css_classes: ["fas fa-address-card"],
            locked_id: false
        }
    }

    constructor() {
        if (!RenderConfig.instance) {
            RenderConfig.instance = this;
            RenderConfig.instance.cm = new ContactManager();
            RenderConfig.instance.initListeners()
        }

        return RenderConfig.instance;
    }

    renderDropdownOptions() {
        if (document.getElementById('field_template_selection')) {
            $('#field_template_selection').html('')
            const dropdownOptions = []
            for (const [key, value] of Object.entries(RenderConfig.instance.templates)) {
                dropdownOptions.push({name: key})
            }
            dropdownOptions.forEach(option => {
                let dropdownOption = document.createElement('option')
                dropdownOption.text = pbI18n.instance.translate(option.name)
                dropdownOption.value = option.name
                document.getElementById('field_template_selection').appendChild(dropdownOption)
            })
        }

        /* PbTpl.instance.renderInto('../templates/option.tpl', dropdownOptions, '#field_template_selection', "append") */
    }

    renderFieldList() {
        RenderConfig.instance.renderDropdownOptions()
        $('#field_element_list').html("")
        if (RenderConfig.instance.cm.fieldconfig) {
            RenderConfig.instance.cm.fieldconfig.forEach(element => {
                RenderConfig.instance.renderConfigField(element)
            });
            RenderConfig.instance - this.createFieldPositionList()
            RenderConfig.instance.startDraggable()

        } else {
            //loading Spinner
        }
    }

    renderConfigField(fieldObject) {
        let list = $('#field_element_list')
        const {
            css_classes,
            display_in_list_view,
            display_pos_in_list_h1,
            display_pos_in_list_h2,
            field_id,
            input_type,
            name,
            optional,
            shortname,
        } = fieldObject;
        //fill HTML with values
        const fieldItem = $("#field_item").html()
        const new_entry = fieldItem.replaceAll("[{field_id}]", field_id).replace("[{field-label-value}]", name).replace("[{field-type-value}]", input_type)
        list.append(new_entry)
    }

    renderConfigDetailView() {
        RenderConfig.instance.stopDraggable()
        RenderConfig.instance.renderSpecializedInputs(RenderConfig.instance.active_field.input_type)
        if (RenderConfig.instance.new_mode) {
            let chosen_template = $('#field_template_selection').val()
            let template_values = RenderConfig.instance.templates[chosen_template]
            $("#field_type_value").val(template_values.input_type)
            $("#field_id_value").val(chosen_template)
            $("#field_label_value").val(chosen_template.charAt(0).toUpperCase() + chosen_template.slice(1))
            $("#field_icon_value").val(template_values.css_classes[0] ? template_values.css_classes[0] : "fas fa-address-card")
            $("#field_icon").removeClass()
            $("#field_icon").addClass(template_values.css_classes[0] ? template_values.css_classes[0] : "fas fa-address-card")
            console.log("new mode", template_values.css_classes[0])
            if (chosen_template == "kommentar" || chosen_template == "textarea") {
                RenderConfig.instance.renderSpecializedInputs("textarea")
                $("#field_textarea_cols_value").val("3")
            }
            if (chosen_template == "select") {
                RenderConfig.instance.renderSpecializedInputs("select")
            }
            RenderConfig.instance.lockSensitiveIds("new", template_values.locked_id)
        }
        if (RenderConfig.instance.edit_mode) {
            $("#field_type_value").val(RenderConfig.instance.active_field.input_type)
            $("#field_id_value").val(RenderConfig.instance.active_field.field_id)
            $("#field_label_value").val(RenderConfig.instance.active_field.name)
            $("#field_icon_value").val(RenderConfig.instance.active_field.css_classes[0] ? RenderConfig.instance.active_field.css_classes[0] : "fas fa-address-card")
            $("#field_icon").removeClass()
            $("#field_icon").addClass(RenderConfig.instance.active_field.css_classes[0] ? RenderConfig.instance.active_field.css_classes[0] : "fas fa-address-card")
            console.log("edit_mode", RenderConfig.instance.active_field.css_classes[0])
            if (RenderConfig.instance.active_field.optional == false) {
                $("#field_required_value").prop("checked", true)
            } else {

                $("#field_required_value").prop("checked", false)
            }
            $("#field_textarea_cols_value").val(RenderConfig.instance.active_field.cols ? RenderConfig.instance.active_field.cols : 0)

            RenderConfig.instance.lockSensitiveIds("edit")
        }


    }

    saveValuesFromDetailView() {
        if (RenderConfig.instance.active_field != undefined) {
            RenderConfig.instance.active_field.input_type = $("#field_type_value").val()
            RenderConfig.instance.active_field.field_id = $("#field_id_value").val()
            RenderConfig.instance.active_field.name = $("#field_label_value").val()
            RenderConfig.instance.active_field.css_classes[0] = $("#field_icon_value").val()
            RenderConfig.instance.active_field.optional = $("#field_required_value").is(":checked") ? false : true
            RenderConfig.instance.active_field.cols = $("#field_textarea_cols_value").val()
            RenderConfig.instance.active_field.selectOptions = []
            // Loop over all entries with the data-attribute data-entryidnumber
            // get the value of the data-attribute
            // get a second input by data-entrylabelnumber="{{value}}"
            // get the values of both inputs and write them into a dictionary {"value": value, "label": label}
            // push the dictionary into the selectOptions array
            $('[data-entryidnumber]').each(function (index) {
                let entryId = $(this).data("entryidnumber")
                let entryLabel = $(`[data-entrylabelnumber="${entryId}"]`).val()
                let entryValue = $(this).val()
                if (entryValue != "") {
                    RenderConfig.instance.active_field.selectOptions.push({value: entryValue, label: entryLabel})
                }
            })
        }
    }

    setActiveField(id) {
        return new Promise(function (resolve, reject) {
            let tmp = RenderConfig.instance.cm.fieldconfig
            tmp.forEach(element => {
                if (element.field_id == id) {
                    RenderConfig.instance.active_field = element
                }
            })
            resolve()
        })
    }

    setActiveFieldEmpty() {
        RenderConfig.instance.active_field = {
            css_classes: []
        }
    }


    validateFieldID(id) {
        let validate_active_field_id = function () {
            let existingIDs = RenderConfig.instance.cm.fieldconfig.map(element => element.field_id)
            if ((RenderConfig.instance.active_field.field_id == id)) {
                let matches = 0
                existingIDs.forEach((field_id) => {
                    if (field_id == id) {
                        matches += 1;
                    }
                })
                if (matches > 1) {
                    return false
                }
                return true;
            } else {
                for (let field_id of existingIDs) {
                    if (field_id == id) {
                        return false;
                    }
                }

                return true
            }
        }

        if (RenderConfig.instance.new_mode) {
            let formcheck_results = validateForm({"field_id_value": "input"}, {"input": validate_active_field_id}, false)
            console.log("FORMCHECK", formcheck_results)
            if (!formcheck_results["field_id_value"]) {
                $("#field_id_value").val("")
                Fnon.Alert.Warning('Das ausgewählte Feld existiert bereits, sie können ein weiteres Feld dieser Art anlegen indem sie eine andere ID eingeben.', 'Feld ID existiert bereits', 'Okay', () => {
                    validateForm({
                        field_id_value: "input",
                        field_label_value: "input",
                    })

                })

                return false
            }
        }

        return true
    }


    lockSensitiveIds(mode = "", override = false) {
        let reservedIds = ["email", "switch", "checkbox"]
        let existingIDs = RenderConfig.instance.cm.fieldconfig.map(element => element.field_id)

        $("#field_id_value").prop("disabled", false)
        if (reservedIds.some((id) => id == ($("#field_id_value").val()))) {
            $("#field_id_value").prop("disabled", true)
            if (existingIDs.some((id) => id == ($("#field_id_value").val())) && mode == "new") {
                $("#field_id_value").prop("disabled", false)
            }
        }

        if (override) {
            $("#field_id_value").prop("disabled", true)
        }

    }


    // List Position methods

    createFieldPositionList() {
        RenderConfig.instance.position_list = RenderConfig.instance.cm.fieldconfig.map(config => {
            return {
                id: config.field_id,
                h1: config.display_pos_in_list_h1,
                h2: config.display_pos_in_list_h2,
                display: config.display_in_list_view
            }
        })
    }

    applyFieldPositionList() {
        RenderConfig.instance.cm.fieldconfig.forEach(config => {
            RenderConfig.instance.position_list.forEach(element => {
                if (element.id == config.field_id) {
                    config.display_pos_in_list_h1 = element.h1
                    config.display_pos_in_list_h2 = element.h2
                    config.display_in_list_view = element.display

                    console.log("List position updated", config)
                }
            })
        })
    }

    fillSelectPosition() {
        let position_dropdowns = document.querySelectorAll('[data-group ="info"]')
        position_dropdowns.forEach(element => {
            RenderConfig.instance.cm.fieldconfig.forEach(config => {
                element.options.add(new Option(config.name, config.field_id))
            })
        })
    }

    saveValuesFromPositionView() {
        document.getElementById("submit-error-msg").innerHTML = ""

        let title_1 = $('#title-one').val()
        let title_2 = $('#title-two').val()
        let title_3 = $('#title-three').val()
        let title_4 = $('#title-four').val()
        let subtitle_1 = $('#subtitle-one').val()
        let subtitle_2 = $('#subtitle-two').val()
        let subtitle_3 = $('#subtitle-three').val()
        let subtitle_4 = $('#subtitle-four').val()

        let array = [title_1, title_2, title_3, title_4, subtitle_1, subtitle_2, subtitle_3, subtitle_4]
        array = array.filter(element => element != "")

        if (array.length >= 1) {
            if (!RenderConfig.instance.checkForDuplicates(array)) {
                RenderConfig.instance.display_positions = {
                    maintitle_1: title_1,
                    maintitle_2: title_2,
                    maintitle_3: title_3,
                    maintitle_4: title_4,
                    subtitle_1: subtitle_1,
                    subtitle_2: subtitle_2,
                    subtitle_3: subtitle_3,
                    subtitle_4: subtitle_4
                }
                RenderConfig.instance.transformToFieldPositionList()
                RenderConfig.instance.applyFieldPositionList()
                Fnon.Hint.Success(new pbI18n().translate("Data has been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
                RenderConfig.instance.toggleEditmode(false)
                fireEvent("ConfigUpdated")
            } else {
                document.getElementById("submit-error-msg").innerHTML =
                    '<div class="alert alert-danger error_message" data-i18n="error message duplicate field"> * Sie haben einem Wert mehrere Positionen zugewiesen. Das ist nicht zulässig *</div>'


            }
        } else {
            document.getElementById("submit-error-msg").innerHTML =
                '<div class="alert alert-danger error_message" data-i18n="error message empty field"> * Bitte weisen Sie mindestens einen Wert zu. *</div>'
        }


    }

    /* const index = Data.findIndex(item => item.name === 'John') */
    checkForDuplicates(array) {
        let resultToReturn = false;

        resultToReturn = array.some((element, index) => {
            return array.indexOf(element) !== index
        })
        return resultToReturn
    }


    preselectValuesForPositionView() {
        let title_1 = $('#title-one')
        let title_2 = $('#title-two')
        let title_3 = $('#title-three')
        let title_4 = $('#title-four')
        let subtitle_1 = $('#subtitle-one')
        let subtitle_2 = $('#subtitle-two')
        let subtitle_3 = $('#subtitle-three')
        let subtitle_4 = $('#subtitle-four')

        let title_array = [title_1, title_2, title_3, title_4]
        let subtitle_array = [subtitle_1, subtitle_2, subtitle_3, subtitle_4]

        RenderConfig.instance.position_list.forEach(element => {
            if (element.h1 != 0 && element.h1 != undefined) {
                title_array[element.h1 - 1].val(element.id)
            } else if (element.h2 != 0 && element.h2 != undefined) {
                subtitle_array[element.h2 - 1].val(element.id)
            }
        })
    }

    transformToFieldPositionList() {
        console.log("Before", RenderConfig.instance.position_list)
        RenderConfig.instance.position_list.forEach(element => {
            element.h1 = 0
            element.h2 = 0
            element.display = false
        })
        if (RenderConfig.instance.display_positions != {}) {
            for (const [key, value] of Object.entries(RenderConfig.instance.display_positions)) {
                RenderConfig.instance.position_list.forEach(element => {
                    if (value == element.id) {
                        if (key.includes('main')) {
                            element.h1 = parseInt(key.slice(-1), 10)
                            element.display = true
                        } else if (key.includes('sub')) {
                            element.h2 = parseInt(key.slice(-1), 10)
                            element.display = true

                        }
                    }

                })
            }
        }
        console.log("After", RenderConfig.instance.position_list)

    }


    toggleEditmode(enabled) {
        if (enabled == true) {
            $('#edit_list_view_button').addClass("hidden")
            $('#cancel_list_view_button').removeClass("hidden")
            $('#save_list_view_button').removeClass("hidden")
            $('[data-group="info"]').prop('disabled', false)

        } else {
            $('#edit_list_view_button').removeClass("hidden")
            $('#cancel_list_view_button').addClass("hidden")
            $('#save_list_view_button').addClass("hidden")
            $('[data-group="info"]').prop('disabled', true)
        }
    }


    sortConfigByOrder(order) {
        const list_of_ids = order.map(entry => entry.id)
        RenderConfig.instance.cm.fieldconfig.sort(function (a, b) {
            return list_of_ids.indexOf(a.field_id) - list_of_ids.indexOf(b.field_id);
        })
    }

    initial_load_config_view() {
        RenderConfig.instance.renderFieldList();
    }


    startDraggable() {

        $("#field_element_list").sortable({
            handle: ".handle",
            items: ".field-element",
            update: function (evt) {
                let new_order = $("#field_element_list").sortable("serialize")
                // let new_order = Render.instance.get_id_order()
                // TaskManager.instance.setOrderValueByIds(new_order)
                RenderConfig.instance.sortConfigByOrder(new_order)
                fireEvent("ConfigUpdated")
            },
        })
        $('[data-attr="movable"]').removeClass("hidden")
        $(".handle").removeClass("hidden")
        $(".handle").addClass("cursor-grab")
    }

    stopDraggable() {

        $(".sortable").each(function () {
            $(this).sortable("destroy")
        })
        //hide me
        $('[data-attr="movable"]').addClass("hidden")
        $(".handle").addClass("hidden")
        $(".handle").removeClass("cursor-grab")

    }


    renderSpecializedInputs(inputType) {
        $("#specialized_field_settings").html("")

        //to expand for other fields create special snippit in template and add to if statement here
        if (inputType == "textarea") {
            let special_fields = `#${inputType}`
            $("#specialized_field_settings").append($(special_fields).html())
        } else if (inputType == "select") {
            this.addSelectFieldEntry()
        } else {
            $("#specialized_field_settings").html("")
        }
    }

    addSelectFieldEntry(number = undefined) {
        console.log("RENDERING")
        let selectedOptions = []
        if (RenderConfig.instance.active_field.selectOptions && number == undefined) {
            number = RenderConfig.instance.active_field.selectOptions?.length + 1
            selectedOptions = RenderConfig.instance.active_field.selectOptions
        } else {
            const inputElements = $('input[data-entryidnumber]');
            let maxEntryIdNumber = Math.max(...inputElements.map(function () {
                return Number($(this).attr('data-entryidnumber'));
            }).get());
            if (maxEntryIdNumber === -Infinity) {
                maxEntryIdNumber = 0;
            }
            number = maxEntryIdNumber + 1
        }
        new PbTpl().renderIntoAsync("contact-manager/templates/select-entry.html", {
            number: number,
            selectOptions: selectedOptions
        }, "#specialized_field_settings", "append").then(function () {
            // $(`#addSelectEntry${number}`).click(function () {
            //     this.addSelectFieldEntry(number + 1)
            // }.bind(this))

            $('[data-entryaddButton]').each(function (index) {
                //remove click listeners
                $(this).off("click")
                $(this).click(function () {
                    const inputElements = $('input[data-entryidnumber]');
                    const maxEntryIdNumber = Math.max(...inputElements.map(function () {
                        return Number($(this).attr('data-entryidnumber'));
                    }).get());
                    number = maxEntryIdNumber + 1
                    RenderConfig.instance.addSelectFieldEntry(number)
                })
            })
            $('[data-entrynumber]').each(function (element) {
                //remove click listeners
                $(this).off("click")
                $(this).click(function () {
                    let number = $(this).data("entrynumber")
                    $(`#selectEntry${number}`).remove()
                })
            })
        }.bind(this))
    }

    initListeners() {

        // delete list entry
        $("#field_element_list").on(
            "click",
            'button[data-selector="btn-field-delete"]',
            function (e) {
                e.preventDefault()
                let deleteTitle = new pbI18n().translate("Delete contact form field")
                let deleteQuestion = new pbI18n().translate("Do you really want to delete the contact form field?")
                Fnon.Ask.Warning(deleteQuestion, deleteTitle, new pbI18n().translate("Ok"), new pbI18n().translate("Cancel"), (result) => {
                    if (result == true) {
                        const id_to_delete = e.target.parentElement.dataset["fieldDeleteId"]
                        if (id_to_delete != undefined) {
                            RenderConfig.instance.cm.removeFieldFromConfig(id_to_delete)
                            // delete list entry: data-field-delete-id
                            fireEvent("ConfigUpdated")
                        }
                    }
                })

            })

        $('#field_type_value').on('change', (e) => {
            if (e.target.value == "textarea") {
                RenderConfig.instance.renderSpecializedInputs("textarea")
            } else if (e.target.value == "select") {
                RenderConfig.instance.renderSpecializedInputs("select")
            } else {
                RenderConfig.instance.renderSpecializedInputs()
            }

        })

        // edit list entry
        $("#field_element_list").on(
            "click",
            'button[data-selector="btn-field-edit"]',
            function (e) {
                let id = $(this).attr('data-field-edit-id')
                RenderConfig.instance.setActiveField(id).then(RenderConfig.instance.renderConfigDetailView)
                RenderConfig.instance.new_mode = false
                RenderConfig.instance.edit_mode = true
                $("#list-view").hide()
                $("#description_text").hide()
                $("#detail-view").show()
                $(".az-content-title").html(`<span data-i18n="new contact fields title">${RenderConfig.instance.active_field.name}</span>`)
            })

        $(document).on("click", "#save_field_button", function (e) {
            e.preventDefault()

            if (
                validateForm({
                    field_id_value: "input",
                    field_label_value: "input",
                }) == true
            ) {
                if (RenderConfig.instance.validateFieldID($("#field_id_value").val())) {
                    if (RenderConfig.instance.edit_mode) {
                        RenderConfig.instance.saveValuesFromDetailView()
                        RenderConfig.instance.cm.updateField(
                            RenderConfig.instance.active_field
                        )
                        fireEvent("ConfigUpdated")
                    } else if (RenderConfig.instance.new_mode) {
                        RenderConfig.instance.saveValuesFromDetailView()
                        RenderConfig.instance.cm.addFieldToConfig(
                            RenderConfig.instance.active_field
                        )
                        fireEvent("ConfigUpdated")
                    }
                    Fnon.Hint.Success(new pbI18n().translate("Data has been updated"), {displayDuration: 2000})
                    Fnon.Wait.Remove(500)

                    $("#list-view").show()
                    $(".az-content-title").html(
                        `<span data-i18n="contact fields title">Configure the fields for Contact Manager</span>`
                    )
                    $("#detail-view").hide()
                } else {

                    console.log("False ID")
                }
            }
        })


        //selection of template for a new field activates button
        $(document).on('change', '#field_template_selection', function (e) {
            if ($("#field_template_selection").val() != "") {
                $("#new_field_button").prop('disabled', false)
            } else {
                $("#new_field_button").prop('disabled', true)
            }
        })

        //show edit view for new field from template
        $(document).on('click', '#new_field_button', function (e) {
            $("#list-view").hide()
            $("#detail-view").show()
            $("#description_text").hide()
            RenderConfig.instance.new_mode = true
            RenderConfig.instance.edit_mode = false
            RenderConfig.instance.setActiveFieldEmpty()
            RenderConfig.instance.renderConfigDetailView()
            $(".az-content-title").html(`<span data-i18n="new contact fields title">New Contact Field</span>`)
            // $("#field_id_value").val("company")
            // $("#field_label_value").val("company")
            // $("#save_field_button").click()
        })

        // cancel detailview:
        $(document).on('click', '#cancel_field_button', () => {
            RenderConfig.instance.new_mode = false
            RenderConfig.instance.edit_mode = false
            RenderConfig.instance.startDraggable()
            $("#list-view").show()
            $("#description_text").show()
            $("#detail-view").hide()
        })

        // update icon and icon modal listeners
        $('#edit_field_icon_button').on('click', function () {
            $("#modal-icon").modal('show')
        })

        $(".btn-icon-selection").on('click', function (e) {
            let new_class = $(this).children().attr('class')
            $("#temp_field_icon_value").val(new_class)
            $("#field_icon_value").val(new_class)
        })

        $('#close_modal_button,#close_modal').on('click', function () {
            $("#modal-icon").modal('hide')
            $("#temp_field_icon_value").val("")
        })

        $('#update_icon_button').on('click', function (e) {
            let new_icon = $("#temp_field_icon_value").val()
            $("#field_icon_value").val(new_icon)
            $("#temp_field_icon").html(`<i class='${new_icon}' id='field_icon'></i>`)
            $("#modal-icon").modal('hide')
            $("#temp_field_icon_value").val("")
        })

        $(document).on("ConfigUpdated", function (e) {
            RenderConfig.instance.cm.updateFieldConfig()
        })

        //List view listeners
        $(document).on('click', '#edit_list_view_button', function (e) {
            RenderConfig.instance.toggleEditmode(true)
        })

        $(document).on('click', '#save_list_view_button', function (e) {
            //validate for at least one value selected
            RenderConfig.instance.saveValuesFromPositionView()
        })

        $(document).on('click', '#cancel_list_view_button', function (e) {
            //to do: reset values from dropdowns
            let deleteTitle = new pbI18n().translate("Sie haben nicht gespeichert.")
            let deleteQuestion = new pbI18n().translate("Möchten Sie wirklich abbrechen? Alle Änderungen gehen verloren.")
            Fnon.Ask.Warning(deleteQuestion, deleteTitle, 'Ja, verlassen', 'Abbrechen', (result) => {
                if (result == true) {
                    RenderConfig.instance.preselectValuesForPositionView()
                    document.getElementById("submit-error-msg").innerHTML = ""
                    RenderConfig.instance.toggleEditmode(false)
                }
            })
        })

    }
}