import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTag} from "../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"

export class RenderArchived {
    contacts_list_sorted = []
    groupsandmembers = []
    attachedContacts = []

    constructor(contact_manager_instance, tags_manager_instance) {
        if (!RenderArchived.instance) {
            RenderArchived.instance = this
            RenderArchived.instance.contacts_list_sorted = contact_manager_instance.contacts_list
            RenderArchived.instance.cm = contact_manager_instance
            RenderArchived.instance.tags = tags_manager_instance
            RenderArchived.instance.tagListModel = new PbTagsList()
            RenderArchived.instance.sort_contact_list("asc")
            RenderArchived.instance.handledTaskIds = []
        }

        return RenderArchived.instance
    }

    refreshContacts() {
        RenderArchived.instance.contacts_list_sorted = RenderArchived.instance.cm.contacts_list
        RenderArchived.instance.sort_contact_list("asc", $("#search_input_archive").val())
    }

    sort_contact_list(direction, search = "") {
        let tmp = JSON.parse(JSON.stringify(RenderArchived.instance.cm.contacts_list))

        let sort_by_pos_1 = undefined
        let sort_by_pos_2 = undefined

        if (!RenderArchived.instance.cm.fieldconfig.filter) {
            return
        }

        if (RenderArchived.instance.cm.fieldconfig.filter(element => element["display_pos_in_list_h1"] == 1).length > 0) {
            sort_by_pos_1 = RenderArchived.instance.cm.fieldconfig.filter(element => element["display_pos_in_list_h1"] == 1)[0]["field_id"]
        }

        if (RenderArchived.instance.cm.fieldconfig.filter(element => element["display_pos_in_list_h1"] == 2).length > 0) {
            sort_by_pos_2 = RenderArchived.instance.cm.fieldconfig.filter(element => element["display_pos_in_list_h1"] == 2)[0]["field_id"]
        }

        const transformer = (element) => {
            let compare_string_1 = element["data"][sort_by_pos_1] ? element["data"][sort_by_pos_1].trim() : ""
            let compare_string_2 = element["data"][sort_by_pos_2] ? element["data"][sort_by_pos_2].trim() : ""
            return {
                "compare_string": compare_string_1 + compare_string_2,
                "id": element["contact_id"]
            }
        }
        let tmp_tmp = tmp.map(element => transformer(element))

        let order = tmp_tmp.sort((a, b) => a["compare_string"].toLowerCase().localeCompare(b["compare_string"].toLowerCase())).map(element => element.id)

        tmp.sort((a, b) => {
            if (order.indexOf(a["contact_id"]) > order.indexOf(b["contact_id"])) {
                return 1
            } else {
                return -1
            }
        })

        // Filter by search value now
        let search_values = search.split(" ")
        let to_be_removed_indexes = []
        for (let i = 0; i < tmp.length; i++) {
            for (let search of search_values) {
                if (search.length > 0) {
                    let found = false
                    for (let value of Object.values(tmp[i].data)) {
                        if (typeof value === "string" && value.toLowerCase().indexOf(search.toLowerCase()) != -1) {
                            found = true
                        }
                        if (Array.isArray(value) && value.join().toLowerCase().includes(search.toLowerCase())) {
                            found = true
                        }
                    }
                    if (!found) {
                        to_be_removed_indexes.push(i)
                    }
                }
            }
        }
        to_be_removed_indexes = [...new Set(to_be_removed_indexes)]

        let remove_counter = 0
        for (let idx of to_be_removed_indexes) {
            tmp.splice(idx - remove_counter, 1)
            remove_counter++
        }
        // Filter end

        RenderArchived.instance.contacts_list_sorted = tmp
        RenderArchived.instance.renderContactListArchived()
    }

    initial_load_list_archived() {
        RenderArchived.instance.refreshContacts()
        RenderArchived.instance.renderContactListArchived()
        new PbNotifications().unblock('#list_container')
    }

    renderContactListArchived() {
        $("#azContactListArchived").html("");

        for (let entry of RenderArchived.instance.contacts_list_sorted) {
            let newListItem = $("#contactItem").clone();
            let contactTagArray = [];
            let contactTagSnippet = "";

            if (newListItem.length > 0) {
                newListItem.attr("id", entry.contact_id);

                for (let titlefield_key of Object.keys(entry.data)) {
                    if (entry.data.tags) {
                        contactTagArray = entry.data.tags;
                    }
                }

                contactTagArray.sort();
                RenderArchived.instance.localTags = contactTagArray;
                for (var index = 0; index < contactTagArray.length; index++) {
                    contactTagSnippet += `<span class="badge badge-data-tags mr-2 mt-1">${contactTagArray[index]}</span>`
                }

                let listItemContent = newListItem.html().replace("[{contact-title}]", entry?.data?.displayTitle).replace("[{contact-subtitle}]", entry?.data?.displaySubtitle).replace("[{contact-id}]", entry.contact_id).replace("[{contact-tags}]", contactTagSnippet)

                newListItem.html(listItemContent)
                $("#azContactListArchived").append(newListItem)
            }
        }

        $('[data-contact-recycle]').click((event) => {
            let contactId = $(event.currentTarget).data('contact-recycle');
            new PbNotifications().blockForLoading('#list_container')
            new PbNotifications().ask(new pbI18n()
                    .translate("Möchten Sie den Kontakt wirklich wiederherstellen?"),
                new pbI18n().translate("Achtung..."),
                () => {
                    RenderArchived.instance.cm.restoreContact(contactId).catch(() => {
                        new PbNotifications().unblock('#list_container')
                    })
                },
                () => {
                    new PbNotifications().unblock('#list_container')
                })
        })
    }
}