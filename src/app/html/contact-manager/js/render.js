import {ContactManager} from "./contact-manager.js?v=[cs_version]"
import {
    array_merge_distinct, fireEvent,
    getCookie,
    setCookie,
    multiStringReplace,
    objectDiff,
    getQueryParam
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbTag} from "../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"


$.fn.destroyDropdown = function () {
    return $(this).each(function () {
        $(this).parent().dropdown('destroy').replaceWith($(this));
    });
};

$.fn.showDropdown = function () {
    return $(this).each(function () {

    });
};

export class Render {
    contacts_list_sorted = []
    contacts_titles_sorted = []
    contact_labels = []
    contactTitles = []
    current_state = "initial"
    pending_tasks = []
    pending_data_owner_delete_tasks = []
    groupsandmembers = []
    attachedContacts = []
    attachedPermissions = []

    constructor(contact_manager_instance, tags_manager_instance) {
        if (!Render.instance) {
            Render.instance = this
            Render.instance.contacts_list_sorted = contact_manager_instance?.contacts_list
            Render.instance.groupsandmembers = contact_manager_instance?.getSortedGroupsandMembers()
            Render.instance.cm = contact_manager_instance
            Render.instance.tags = tags_manager_instance
            Render.instance.tagListModel = new PbTagsList()
            Render.instance.sort_contact_list("asc")
            Render.instance.handledTaskIds = []

            document.addEventListener("pbTagsListChanged", function _(event) {
                if (!Render.instance.handledTaskIds.includes(event.data.args.task_id)) {
                    Render.instance.handledTaskIds.push(event.data.args.task_id)
                    Render.instance.renderTagList(event.data.args.tags)
                }
            })
        }

        return Render.instance
    }

    refreshContacts() {
        Render.instance.contacts_list_sorted = ContactManager.instance.contacts_list
        Render.instance.sort_contact_list("asc")
    }

    sort_contact_list(direction, search = "") {
        let tmp = JSON.parse(JSON.stringify(ContactManager.instance.contacts_list)) //ContactManager.instance.contacts_list
        // Perform sort here and write sort-result to tmp
        let sort_by_pos_1 = undefined
        let sort_by_pos_2 = undefined
        if (!Render.instance?.cm?.fieldconfig?.filter) {
            return
        }
        if (Render.instance.cm.fieldconfig.filter(element => element["display_pos_in_list_h1"] == 1).length > 0) {
            sort_by_pos_1 = Render.instance.cm.fieldconfig.filter(element => element["display_pos_in_list_h1"] == 1)[0]["field_id"]
        }
        if (Render.instance.cm.fieldconfig.filter(element => element["display_pos_in_list_h1"] == 2).length > 0) {
            sort_by_pos_2 = Render.instance.cm.fieldconfig.filter(element => element["display_pos_in_list_h1"] == 2)[0]["field_id"]
        }

        const transformer = (element) => {
            let compare_string_1 = element["data"][sort_by_pos_1] ? element["data"][sort_by_pos_1].trim() : ""
            let compare_string_2 = element["data"][sort_by_pos_2] ? element["data"][sort_by_pos_2].trim() : ""
            return {
                "compare_string": compare_string_1 + compare_string_2,
                "id": element["contact_id"]
            }
        }
        let tmp_tmp = tmp.map(element => transformer(element))

        let order = tmp_tmp.sort((a, b) => a["compare_string"].toLowerCase().localeCompare(b["compare_string"].toLowerCase())).map(element => element.id)

        tmp.sort((a, b) => {
            if (order.indexOf(a["contact_id"]) > order.indexOf(b["contact_id"])) {
                return 1
            } else {
                return -1
            }
        })


        // Filter by search value now
        let search_values = search.split(" ")
        let to_be_removed_indexes = []
        for (let i = 0; i < tmp.length; i++) {
            for (let search of search_values) {
                if (search.length > 0) {
                    let found = false
                    for (let value of Object.values(tmp[i].data)) {
                        if (typeof value === "string" && value.toLowerCase().indexOf(search.toLowerCase()) != -1) {
                            found = true
                        }
                        if (Array.isArray(value) && value.join().toLowerCase().includes(search.toLowerCase())) {
                            found = true
                        }
                    }
                    if (!found) {
                        to_be_removed_indexes.push(i)
                    }
                }
            }
        }
        to_be_removed_indexes = [...new Set(to_be_removed_indexes)]
        let remove_counter = 0
        for (let idx of to_be_removed_indexes) {
            tmp.splice(idx - remove_counter, 1)
            remove_counter++
        }
        // Filter end

        Render.instance.contacts_list_sorted = tmp
        Render.instance.renderContactList()
    }

    initial_load_list_view() {
        new Render().renderContactList()
    }

    initial_load_list_archived() {
        new Render().renderContactListArchived()
    }

    includesQueryParam() {
        if (getQueryParam("ci")) {
            $("#" + getQueryParam("ci")).click()
        }
    }


    resetDetailViewContainer() {
        //$("#azContactBody").find(':first-child').remove()
        $("#contactInfoHeader").hide()
        $("#advancedOptions").hide()
        $("#interactions").hide()
        $("#contactInfoBodyContainer").html($("#azDefaultBody").html())

    }

    renderContactList() {
        $("#azContactList").html("")
        //find out from the config settings, which details are to be shown in the list view
        let tmp_titles = {}
        let tmp_subtitles = {}
        for (let configEntry of ContactManager.instance.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
            }
            if (configEntry.display_pos_in_list_h2 > 0) {
                tmp_subtitles[configEntry.display_pos_in_list_h2] = configEntry.field_id
            }
        }

        for (let entry of Render.instance.contacts_list_sorted) {

            let contactTitleString = ""
            let contactSubtitleString = ""
            let contactTagArray = []
            let contactTagSnippet = ""

            for (let titlefield_key of Object.keys(tmp_titles)) {
                if (entry.data[tmp_titles[titlefield_key]]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                }
            }
            for (let titlefield_key of Object.keys(tmp_subtitles)) {
                if (entry.data[tmp_subtitles[titlefield_key]]) {
                    contactSubtitleString += entry.data[tmp_subtitles[titlefield_key]] + " "
                }
            }
            for (let titlefield_key of Object.keys(entry.data)) {
                if (entry.data.tags) {
                    contactTagArray = entry.data.tags
                }
            }
            contactTagArray.sort()
            Render.instance.localTags = contactTagArray
            for (var index = 0; index < contactTagArray.length; index++) {
                contactTagSnippet += `<span class="badge badge-data-tags mr-2 mt-1">${contactTagArray[index]}</span>`
            }

            let newListItem = $("#contactItem").clone()

            if (newListItem.length > 0) {
                newListItem.attr("id", entry.contact_id)
                let listItemContent = newListItem.html().replace("[{contact-title}]", contactTitleString).replace("[{contact-subtitle}]", contactSubtitleString).replace("[{contact-tags}]", contactTagSnippet)
                newListItem.html(listItemContent)
                $("#azContactList").append(newListItem)
            }
        }
    }

    renderContactDetailEditor(id) {
        let field_value
        $('select.ui').destroyDropdown();

        ContactManager.instance.setActiveContactById(id)

        //build Body of contact details
        $("#contactInfoBodyContainer").html("")
        let contactDetailsBody = $("#contactInfoBody")

        for (let entry of ContactManager.instance.fieldconfig) {
            Render.instance.addRowToForm(entry, id)
        }

        // creating dropdown of contacts with Header 1
        Render.instance.contacts_titles_sorted = new ContactManager().contacts_list;
        Render.instance.renderContactTitles();
        Render.instance.addContactsToContactDropdown('child_contacts', 'no contact linked');
        Render.instance.addContactsToContactDropdown('parent_contacts', 'no contact linked');

        if (id == undefined) {
            field_value = ""
        } else {

            Render.instance.setPreselectOptions("child_contacts", ContactManager.instance.active_contact.child_ids)
            Render.instance.setPreselectOptions("parent_contacts", ContactManager.instance.active_contact.parent_ids)
            Render.instance.renderContactLabels("parent_contacts_labels", ContactManager.instance.active_contact.parent_ids)
            Render.instance.renderContactLabels("child_contacts_labels", ContactManager.instance.active_contact.child_ids)

        }

        // no longer neccessary because of addRowToForm
        // $("#contactInfoBodyContainer").appendTo(contactDetailsBody)

        // temp call for testing
        //Render.instance.addEntriesToAccessrightsDropdown('visibility_permissions','Sichbarkeit nicht eingeschrankt');

        //build Header of contact details
        let contactDetailsHeader = $("#contactInfoHeader")

        //build complete contact details
        $("#azContactBody").html("")
        $("#azContactBody").append(contactDetailsHeader)
        $("#azContactBody").append(contactDetailsBody)

        // fill native select with accessrights
        Render.instance.addEntriesToAccessrightsDropdown(Render.instance.groupsandmembers)
        Render.instance.renderTagList()
        Render.instance.renderAttachmentList()
        // fille native select with
        //Render.instance.renderEntriesForAccessrightsDropdown()
        //Supress console errors if element is not visible
        $.fn.transition.settings.silent = true
        $('#visibility_permissions').dropdown()
        $('#visibility_permissions').addClass("ui search dropdown permissions wd-sm-300 wd-md-300")
        $('#visibility_permissions').dropdown()
        $('#parent_contacts').dropdown()
        $('#parent_contacts').addClass("ui search dropdown contacts wd-sm-300 wd-md-300")
        $('#parent_contacts').dropdown()
        $('#child_contacts').dropdown()
        $('#child_contacts').addClass("ui search dropdown contacts wd-sm-300 wd-md-300")
        $('#child_contacts').dropdown()
        $('select').showDropdown();


        if (id == undefined) {
            // We add a new contact
            Render.instance.editMode()
            $("#interactions").html("")
        } else {
            Render.instance.viewMode()
            Render.instance.renderContactDetailPills(id)
        }

        //display layer if hidden
        $('body').addClass('az-content-body-show')

    }

    renderContactDetailPills(id) {
        $("#history-list-container").html("")
        $("#interactions").show()

        function reset() {
            $("#history_container").hide()
        }

        reset()
        let params = {
            "headline": pbI18n.instance.translate("Activity"),
            "pills": [
                {
                    id: "easy2scheduleEvents",
                    name: pbI18n.instance.translate("CS:Kalender"),
                    icon: "far fa-calendar-alt",
                    permission: "easy2schedule",
                    onclick: function () {
                        return true;
                    },
                    link: `${CONFIG.APP_BASE_URL}/easy2schedule/?s=${id}&v=listYear`
                },
                {
                    id: "documents",
                    name: pbI18n.instance.translate("Documentation"),
                    icon: "far fa-file-alt",
                    permission: "frontend.documentation.views.contact",
                    onclick: function (e) {
                        return true;
                    },
                    link: `${CONFIG.APP_BASE_URL}/documentation/index.html?sid=contact-manager|contact|${id}`
                },
                {
                    id: "history",
                    name: pbI18n.instance.translate("History"),
                    icon: "fa fa-history",
                    permission: "frontend.contactmanager.views.history",
                    toggle: "true",
                    onclick: function (e) {
                        e.preventDefault()
                        if ($("#history_container").is(":visible")) {
                            reset()
                        } else {
                            Fnon.Wait.Circle('', {
                                svgSize: {w: '50px', h: '50px'},
                                svgColor: '#3a22fa',
                            })
                            $(document).on("showHistoryData", (data) => {
                                new Render().showEntryHistory(data)
                            })
                            ContactManager.instance.fetchContactsHistoryFromApi().then((response) => {
                                fireEvent("showHistoryData", response)
                            })

                        }

                    }
                },
                {
                    id: "maps",
                    name: pbI18n.instance.translate("Maps"),
                    icon: "far fa-map",
                    permission: "frontend.contactmanager.views",
                    toggle: "true",
                    onclick: function (e) {
                        let mapservices = {
                            gmaps: "https://www.google.de/maps/place/",
                            openstreetmaps: "https://www.openstreetmap.org/search?query="
                        }

                        let queryString = Render.instance.getFormattedAddressForMaps()
                        if (queryString.length > 10) {
                            let favoriteMapService = PbAccount.instance?.accountProfileData?.favorite_map_service || "gmaps"
                            let mapServiceUrl = mapservices[favoriteMapService] || mapservices["gmaps"]
                            let url = encodeURI(mapServiceUrl + queryString)
                            $(e.currentTarget).attr('href', url)
                        } else {
                            PbNotifications.instance.showAlert(new pbI18n().translate("Sorry, we don't have enough data to show this contact on a map."))
                            return false
                        }
                    },
                    link: '#1',
                    target: "_blank"
                },
                {
                    id: "appointment_invitation",
                    name: pbI18n.instance.translate("appointment_invitation"),
                    icon: "far fa-clock",
                    permission: "beta_access",
                    onclick: function (e) {
                        e.preventDefault()
                        let url = new PbAccount().accountProfileData?.appointment_booking_website_url
                        if (!url) {
                            PbNotifications.instance.showAlert(new pbI18n().translate("Sorry, you have not set up an appointment booking website yet."), new pbI18n().translate("Beta-Function, not available for everyone at the moment."), "warning")
                            return false
                        }

                        $("#appointmentlink_generator_modal").modal('show')
                        $("#dismiss_button_appointmentlink_generator_modal").click(function () {
                            $("#appointmentlink_generator_modal").modal('hide')
                        })
                        $("#generate_button_appointmentlink_generator_modal").click(function () {
                            let url = new PbAccount().accountProfileData?.appointment_booking_website_url

                            let urlObject = new URL(url);
                            let searchParams = urlObject.searchParams;
                            searchParams.set('cid', ContactManager.instance.active_contact.contact_id);
                            searchParams.set('d', $("#appointment_duration_in_minutes").val());
                            setCookie("appointment_duration_in_minutes", $("#appointment_duration_in_minutes").val(), 365)
                            if ($("#appointment_click_selector").val().length > 0) {
                                searchParams.set("c", $("#appointment_click_selector").val())
                                setCookie("appointment_click_selector", $("#appointment_click_selector").val(), 365)
                            }
                            urlObject.search = searchParams.toString();

                            if ($("#appointment_anchor").val().length > 0) {
                                urlObject.hash = $("#appointment_anchor").val();
                                setCookie("appointment_anchor", $("#appointment_anchor").val(), 365)
                            } else {
                                urlObject.hash = "#mw"
                            }
                            let updatedUrl = urlObject.toString();
                            let formal = $('input[name="radioSelection"]:checked').val() == "formal"
                            let personal = $('input[name="radioSelection"]:checked').val() == "personal"
                            let demo = $('input[name="radioSelection"]:checked').val() == "demo"
                            setCookie("appointment_formal", $('input[name="radioSelection"]:checked').val(), 365)
                            let subject = ""
                            let mail_body = ""

                            if (personal) {
                                subject = PbTpl.instance.translate('AppointmentInviteSubjectPersonal');
                                mail_body = PbTpl.instance.translate("AppointmentInviteBodyPersonal")
                            }
                            if (formal) {
                                subject = PbTpl.instance.translate('AppointmentInviteSubjectFormal');
                                mail_body = PbTpl.instance.translate("AppointmentInviteBodyFormal");
                            }

                            if (demo) {
                                subject = PbTpl.instance.translate('AppointmentInviteOnlineDemonstrationSubjectFormal');
                                mail_body = PbTpl.instance.translate("AppointmentInviteOnlineDemonstration");
                            }

                            let guesstimated_firstname = ContactManager.instance.active_contact.data?.firstname || ContactManager.instance.active_contact.data?.vorname || ContactManager.instance.active_contact.data?.Vorname || ""
                            let guesstimated_lastname = ContactManager.instance.active_contact.data?.name || ContactManager.instance.active_contact.data?.lastname || ContactManager.instance.active_contact.data?.Nachname || ContactManager.instance.active_contact.data?.name || ContactManager.instance.active_contact.data?.Name || ""
                            let guesstimated_email = ContactManager.instance.active_contact.data?.email || ContactManager.instance.active_contact.data?.mail || ""
                            mail_body = mail_body
                                .replaceAll("[Firstname]", guesstimated_firstname)
                                .replaceAll("[Lastname]", guesstimated_lastname)
                                .replaceAll("[Link]", updatedUrl);
                            mail_body = encodeURIComponent(mail_body)
                            subject = encodeURIComponent(subject)
                            let mailtoLink = `mailto:${guesstimated_email}?subject=${subject}&body=${mail_body}`;
                            window.location.href = mailtoLink;
                        })

                    },
                    link: '#1',
                    target: "_blank"
                },
                {
                    id: "questionaire_pill",
                    name: pbI18n.instance.translate("Questionaire"),
                    icon: "far fa-question-circle",
                    permission: "frontend.documentation.views.contact.questionaire",
                    onclick: function (e) {
                        return true;
                    },
                    link: `${CONFIG.APP_BASE_URL}/questionaire/questionaire_form.html?questionaire_config_id=contact_questionaire&ref_code=contact_${id}`
                },
                {
                    id: "tasks_pill",
                    name: pbI18n.instance.translate("Tasks"),
                    icon: "fas fa-tasks",
                    permission: "frontend.taskmanager.views.search",
                    onclick: function (e) {
                        return true;
                    },
                    link: `${CONFIG.APP_BASE_URL}/task-manager/tasksearch.html?search=links:${id}`
                }
            ]
        }

        PbTpl.instance.renderInto('snippets/pills.tpl', params, "#interactions")
        getCookie("appointment_click_selector") ? $("#appointment_click_selector").val(decodeURIComponent(getCookie("appointment_click_selector"))) : $("#appointment_click_selector").val("")
        getCookie("appointment_duration_in_minutes") ? $("#appointment_duration_in_minutes").val(decodeURIComponent(getCookie("appointment_duration_in_minutes"))) : $("#appointment_duration_in_minutes").val("30")
        getCookie("appointment_anchor") ? $("#appointment_anchor").val(decodeURIComponent(getCookie("appointment_anchor"))) : $("#appointment_anchor").val("")
        
        switch(getCookie("appointment_formal")) {
            case "formal":
                $('input[name="radioSelection"][value="formal"]').prop('checked', true)
                break;
            case "personal":
                $('input[name="radioSelection"][value="personal"]').prop('checked', true)
                break;
            case "demo":
                $('input[name="radioSelection"][value="demo"]').prop('checked', true)
                break;
            default:
                $('input[name="radioSelection"][value="personal"]').prop('checked', true)
        }
        for (let param of params.pills) {
            if (param.onclick) {
                $(`#pilllink_${param.id}`).click(param.onclick)
            }
        }
    }

    renderContactListArchived() {
        $("#azContactListArchived").html("")
        //find out from the config settings, which details are to be shown in the list view
        let tmp_titles = {}
        let tmp_subtitles = {}
        for (let configEntry of ContactManager.instance.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
            }
            if (configEntry.display_pos_in_list_h2 > 0) {
                tmp_subtitles[configEntry.display_pos_in_list_h2] = configEntry.field_id
            }
        }

      // const filteredContacts = Render.instance.contacts_list_sorted.filter(contact => !contact.archived);


        for (let entry of Render.instance.contacts_list_sorted) {
            let contactTitleString = ""
            let contactSubtitleString = ""
            let contactTagArray = []
            let contactTagSnippet = ""

            for (let titlefield_key of Object.keys(tmp_titles)) {
                if (entry.data[tmp_titles[titlefield_key]]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                }
            }
            for (let titlefield_key of Object.keys(tmp_subtitles)) {
                if (entry.data[tmp_subtitles[titlefield_key]]) {
                    contactSubtitleString += entry.data[tmp_subtitles[titlefield_key]] + " "
                }
            }
            for (let titlefield_key of Object.keys(entry.data)) {
                if (entry.data.tags) {
                    contactTagArray = entry.data.tags
                }
            }
            contactTagArray.sort()
            Render.instance.localTags = contactTagArray
            for (var index = 0; index < contactTagArray.length; index++) {
                contactTagSnippet += `<span class="badge badge-data-tags mr-2 mt-1">${contactTagArray[index]}</span>`
            }

            let newListItem = $("#contactItem").clone()

            if (newListItem.length > 0) {
                newListItem.attr("id", entry.contact_id)
                let listItemContent = newListItem.html().replace("[{contact-title}]", contactTitleString).replace("[{contact-subtitle}]", contactSubtitleString).replace("[{contact-tags}]", contactTagSnippet)
                newListItem.html(listItemContent)
                $("#azContactListArchived").append(newListItem)
            }
        }
    }

    getFormattedAddressForMaps() {
        let data = ContactManager.instance.active_contact.data
        return `${data.street || ""} ${data.housenumber || ""}, ${data.zipcode || ""} ${data.city || ""}`
    }

    showEntryHistory(data) {
        if (data.originalEvent.data.history && Array.isArray(data.originalEvent.data.history)) {
            $("#history_container").show()
            $("#history-list-container").html("")
            let previous_data = {}
            let newHistoryItem = $("#history-item").html()

            for (let {payload, type} of data.originalEvent.data.history) {
                Render.instance.currentlyShownHistoryId = payload.contact_id
                let {creator, create_time} = payload

                let localized_create_time = moment.utc(create_time).local().format();
                let local_create_time = new Date(localized_create_time).toLocaleString()

                let historyDetail = new pbI18n().translate("No details available")
                if (payload != null && previous_data != null) {
                    historyDetail = ""
                    let old_values = objectDiff(previous_data, payload, [previous_data.correlation_id, previous_data.create_time])
                    let new_values = objectDiff(payload, previous_data, [payload.correlation_id, payload.create_time])
                    if (old_values.data == undefined) {
                        old_values.data = {}
                    }
                    if (new_values.data == undefined) {
                        new_values.data = {}
                    }
                    let intersect_keys = array_merge_distinct(Object.keys(old_values.data), Object.keys(new_values.data))
                    for (let key of intersect_keys) {
                        if (key == "order") {
                            continue
                        }
                        let key_translated = new pbI18n().translate(key)
                        let old_value = old_values.data[key] ? old_values.data[key] : ""
                        let new_value = new_values.data[key] ? new_values.data[key] : false
                        if (new_value !== false && old_value != new_value) {
                            historyDetail += `<code><xmp> ${key_translated} : --> ${JSON.stringify(new_value)} </xmp></code>`
                        }
                    }
                }


                let details = {
                    'history-creator': creator,
                    'history-time': local_create_time,
                    'history-type': new pbI18n().translate(type),
                    'history-detail': historyDetail
                }

                previous_data = payload


                let historyItemContent = multiStringReplace(details, newHistoryItem)
                $("#history-list-container").prepend(historyItemContent)
            }
            Fnon.Wait.Remove(500)
        }
    }

    editMode() {
        Render.instance.current_state = "edit"
        //set content sections visible that may be hidden
        $("#contactInfoHeader").show()
        $("#advancedOptions").show()
        //set correct button state
        $("#delete_contact_button").hide()
        $("#edit_contact_button").hide()
        $("#cancel_edit_contact_button").show()
        $("#save_contact_button").show()
        //set correct form field state 
        $(".ui.dropdown.permissions").removeClass("disabled")
        $(".ui.dropdown.contacts").removeClass("hidden")
        $(".select-tags.form-control").attr("disabled", false)
        $("#child_contacts_labels").hide()
        $(".ui.dropdown.parentcontacts").removeClass("hidden")
        $("#parent_contacts_labels").hide()
        $(".form-control.detail-inputs").attr("disabled", false)
        $('.contact-detail span[data-group=alternate]').addClass("hidden")
        $('.contact-detail span[data-group=field_alternate]').removeClass("hidden")
        $("#addGlobalTagButton").attr("disabled", false)

    }

    viewMode() {
        Render.instance.current_state = "viewdetails"
        //set content sections visible that may be hidden
        $("#contactInfoHeader").show()
        $("#advancedOptions").show()
        //set correct button state
        $("#delete_contact_button").show()
        $("#edit_contact_button").show()
        $("#cancel_edit_contact_button").hide()
        $("#save_contact_button").hide()
        //set correct form field state
        $(".ui.dropdown.permissions").addClass("disabled")
        $(".ui.dropdown.contacts").addClass("hidden")
        $(".select-tags.form-control").attr("disabled", true)
        $("#child_contacts_labels").show()
        $(".ui.dropdown.parentcontacts").addClass("hidden")
        $("#parent_contacts_labels").show()
        $(".form-control.detail-inputs").attr("disabled", true)
        $('.contact-detail span[data-group=alternate]').removeClass("hidden")
        $('.contact-detail span[data-group=field_alternate]').addClass("hidden")
        $("#addGlobalTagButton").attr("disabled", true)

    }

    saveContactDetails() {
        let active_contact_details
        let type, id

        let attachedPermissions = []
        for (let attachedPermission of document.getElementById('visibility_permissions').selectedOptions) {
            attachedPermissions.push(attachedPermission.getAttribute('data-accessrightid'))
        }
        Render.instance.cm.active_contact_data_owners = attachedPermissions
        let attachedChildContacts = []
        for (let attachedChildContact of document.getElementById("child_contacts").selectedOptions) {
            attachedChildContacts.push(attachedChildContact.getAttribute("data-contact-id"))
        }
        let attachedParentContacts = []
        for (let attachedParentContact of document.getElementById("parent_contacts").selectedOptions) {
            attachedParentContacts.push(attachedParentContact.getAttribute("data-contact-id"))
        }
        let attachedTags = []
        if (document.getElementById("tags")) {
            let selected_tags = $('#tags').select2('data')
            for (const [key, value] of Object.entries(selected_tags)) {
                const tagKey = value?.id
                let tagValue = value?.text
                attachedTags.push(tagKey)
            }
        }

        if (ContactManager.instance.active_contact) {
            active_contact_details = {
                contact_id: ContactManager.instance.active_contact.contact_id,
                data: {tags: attachedTags},
                data_owners: attachedPermissions,
                parent_ids: attachedParentContacts,
                child_ids: attachedChildContacts
            }
        } else {
            setCookie("data_owners",  JSON.stringify(attachedPermissions), {expires: 3650, path: '/'})
            active_contact_details = {
                data: {tags: attachedTags},
                data_owners: attachedPermissions,
                parent_ids: attachedParentContacts,
                child_ids: attachedChildContacts
            }
        }
        let missingFields = false
        for (let entry of ContactManager.instance.fieldconfig) {
            $("#" + entry.field_id).css("border-color", "initial")
            if ($("#" + entry.field_id).attr("required") == "required" && $("#" + entry.field_id).val().length < 1) {
                $("#" + entry.field_id).css("border-color", "red")
                missingFields = true
            }
        }
        if (missingFields) {
            Fnon.Hint.Danger(new pbI18n().translate('Required fields are missing.'))
            return
        }

        for (let entry of ContactManager.instance.fieldconfig) {

            //active_contact_details.data[entry.field_id] = $("#" + entry.field_id).val() != "on" ? $("#" + entry.field_id).val() : true

            if ($("#" + entry.field_id).attr('type') == "checkbox" && $("#" + entry.field_id).is(":checked")) {
                active_contact_details.data[entry.field_id] = true
            } else if ($("#" + entry.field_id).attr('type') == "checkbox" && $("#" + entry.field_id).is(":not(:checked)")) {
                active_contact_details.data[entry.field_id] = false
            } else {
                active_contact_details.data[entry.field_id] = $("#" + entry.field_id).val()
            }
        }


        if (active_contact_details.contact_id != undefined) {
            ContactManager.instance.setContact(active_contact_details).then((response) => {
                Render.instance.pending_tasks.push(response.task_id)
                Fnon.Hint.Success('Änderungen gespeichert')
                if (getQueryParam("r") != undefined) {
                    location.href = getQueryParam("r")
                }
            })
        } else {
            ContactManager.instance.setNewContact(active_contact_details).then((response) => {
                Render.instance.pending_tasks.push(response.task_id)
                Fnon.Hint.Success('Neuer Kontakt gespeichert')
                if (getQueryParam("r") != undefined) {
                    location.href = getQueryParam("r")
                }
            })
        }

    }

    addRowToForm(entry, id) {
        let disable_field = true
        let field_class = "form-control detail-inputs wd-sm-400 wd-md-300"
        let newField = $("#contactInfoBodyDetail").clone()
        let field_value = ""
        let inputAlternate = ""
        let dataGroupAlternate = "no_alternate"
        let dataGroupField = "field_no_alternate"
        let customBefore = ""
        let customAfter = ""

        if (id == undefined) {
            // if state new, there are no saved values or contact id
            ContactManager.instance.setActiveContactById()
        } else {
            // if state edit, get field values
            let search = entry => entry.contact_id === id;
            let current_index = ContactManager.instance.contacts_list.findIndex(search)
            if (current_index >= 0) {
                field_value = ContactManager.instance.contacts_list[current_index].data[entry.field_id]
            } else {
                ContactManager.instance.setActiveContactById()
            }
        }
        let newInput = new Render().renderInputField(entry, field_value, disable_field, field_class)
        newField.attr("id", "fieldentry_" + entry.field_id)
        newInput = ($(newInput).prop('outerHTML'))

        //check for fields that have alternative view and set attributes
        if (field_value) {
            inputAlternate = Render.instance.renderAlternateForField(entry.input_type, field_value)
            dataGroupAlternate = "alternate"
            dataGroupField = "field_alternate"
            inputAlternate = ($(inputAlternate).prop('outerHTML'))
            if (!inputAlternate) {
                inputAlternate = ""
                dataGroupAlternate = "no_alternate"
                dataGroupField = "field_no_alternate"
            }
        }

        if (entry.input_type == "switch") {
            customBefore = '<label class="switch">'
            customAfter = '<span class="slider round"></span></label>'
        }

        let label = pbI18n.instance.translate(entry.name)
        if (!entry.optional) {
            label += "*"
        }
        let fieldContent = newField.html().replace("[{contact-detail-label}]", label).replace("[{contact-detail-custom-before}]", customBefore).replace("[{contact-detail-field}]", newInput).replace("[{contact-detail-custom-after}]", customAfter).replace("[{contact-detail-icon}]", Render.instance.renderIcon(entry.css_classes)).replace("[{contact-detail-link}]", inputAlternate).replace("[{data-group-alternate}]", dataGroupAlternate).replace("[{data-group-alternate-field}]", dataGroupField)
        newField.html(fieldContent)

        $("#contactInfoBodyContainer").append(newField)
    }


    renderInputField(entry, value, disable_field, field_class) {
        let field_type = entry.input_type
        let field_id = entry.field_id
        let field_name = entry.name

        let field_required = entry.optional
        if (field_required == false) {
            field_required = true
        } else {
            field_required = false
        }
        let field_value = value

        let common_attr = {
            'id': field_id,
            'name': field_name,
            'value': field_value,
            'class': field_class,
            'required': field_required,
            'disabled': disable_field
        }

        if (field_type == "textarea") {
            let field = $('<textarea>').attr(common_attr).text(field_value).attr("rows", entry.cols)
            return field
        } else if(field_type == "select") {
            let field = $('<select>').attr(common_attr)
            let options = entry.selectOptions
            for (let option of options) {
                let optionElement = $('<option>').attr('value', option.value).text(option.label)
                if (option.value == field_value) {
                    optionElement.attr('selected', true)
                }
                field.append(optionElement)
            }
            return field
        } else {
            let attr = {}

            if (field_type == "date") {
                field_type = "future_date"
                attr = {
                    'type': field_type,
                    'onfocus': 'this.type="date";',
                    'onblur': 'this.type="future_date";this.value = this.value.split("-").reverse().join(".");'
                }

            }
            if (field_type == "checkbox" || field_type == "switch") {
                common_attr = {
                    'id': field_id,
                    'name': field_name,
                    'value': field_value,
                    'class': "form-control detail-inputs wd-20",
                    'required': field_required,
                    'disabled': disable_field
                }
                attr = {
                    'type': 'checkbox',
                }

                let field = $('<input>').attr(common_attr).attr(attr)
                if (field_value || field_value == "on") {
                    field.attr("checked", true)
                }
                return field
            } else {
                attr = {
                    'type': field_type,
                }
            }

            let field = $('<input>').attr(common_attr).attr(attr)

            return field
        }


    }

    renderAlternateForField(input_type, value) {

        if (input_type == "tel" || input_type == "telefon") {
            //create alternate view mode with href with number with only num and +
            let cleanValue = value.replace(/[^0-9+]/g, '')
            let thelink = $('<a>', {
                'html': `${value} <i class="typcn typcn-arrow-forward-outline pd-l-10 tx-20"></i>`,
                'href': `tel:${cleanValue}`
            })

            return thelink
        }
        if (input_type == "email" || input_type == "mail") {
            //create alternate view mode with mailto href
            let thelink = $('<a>', {
                'html': `${value} <i class="typcn typcn-arrow-forward-outline pd-l-10 tx-20"></i>`,
                'href': `mailto:${value}`
            })
            return thelink
        } else {
            return false
        }


    }

    renderIcon(icon_class) {

        icon_class = icon_class[0]
        let icon = ""

        //catch wrong icon format but allow other icon types
        if (icon_class.startsWith("fa-") == true) {
            icon_class = "fas fa-address-card"
        }

        icon = `<i class="${icon_class}"></i>`
        return icon
    }

    highlightSelectedListItem(item_id) {
        let item = $("#" + item_id)

        if (item != null) {
            item.addClass('selected')
            item.children().eq(1).addClass('active')
            item.siblings().removeClass('selected')
            item.siblings().find('.az-contact-star').removeClass('active')
        }
    }

    renderAttachmentList() {
        $("#attachments_container").html("")
        if(ContactManager.instance.active_contact?.data?.attachments) {
            for(let [key, value] of Object.entries(ContactManager.instance.active_contact?.data?.attachments)) {
                let link = $('<a>', {"href": key, "target": "_blank"})
                if(key.startsWith("questionaire")) {
                    let href=`/questionaire/questionaire_form.html?questionaire_config_id=${value.config}&questionaire_id=${value.questionaire_id}&ref_code=contact_${ContactManager.instance.active_contact.contact_id}`
                    window.setTimeout(()=> { $("#pilllink_questionaire_pill").attr("href", href), $("#pilllink_questionaire_pill").css("background-color", "#63d363");}, 200)
                    link.attr("href", href)
                    link.html(value.config)
                } else {
                    link.html(key)
                }
                let li = $('<li>', {"class": ""})
                li.append(link)
                $("#attachments_container").append(li)
            }
        }

    }
    renderTagList(selectedTags = []) {
        let self = this
        return new Promise((resolve, reject) => {
            Render.instance.tagListModel.load(false, "contact-manager").then(() => {
                let tag_strings = ContactManager.instance.active_contact?.data.tags || []
                let tags = []

                for (let contact of ContactManager.instance.contacts_list) {
                    if (contact.data.tags) {
                        for (let localTag of contact.data.tags) {
                            let myTag = new PbTag().fromShort(localTag)
                            Render.instance.tagListModel.addTagToList(myTag)
                        }
                    }
                }

                for (let tag of tag_strings) {
                    tags.push(new PbTag().fromShort(tag).getData())
                }

                if (selectedTags.length > 0) {
                    let alreadySelectedTags = []
                    for (let alreadySelected of $('[data-propertyname="tags"]').val()) {
                        alreadySelectedTags.push(new PbTag().fromShort(alreadySelected).getData())
                    }

                    tags = array_merge_distinct(alreadySelectedTags, selectedTags)
                }

                for (let tag of tags) {
                    tag = new PbTag({"name": tag.id || tag.name, "value": tag.text || tag.value})
                    if (!selectedTags.includes(tag.short())) {
                        selectedTags.push(tag.short())
                    }
                    Render.instance.tagListModel.addTagToList(tag)
                }

                let tagOptions = Render.instance.tagListModel.getSelectOptions(selectedTags)
                PbTpl.instance.renderIntoAsync('csf-lib/pbapi/tagmanager/templates/tag-select2.tpl', {"multiple": true, "addButton": true, "propertyPath": "tags", "options": tagOptions}, "#tag_container").then(() => {
                    $('[data-propertyname="tags"]').select2({
                        tags: new PbAccount().hasPermission("contacts.localtags.add"),
                        tokenSeparators: [',', ' '],
                        selectOnClose: false
                    })

                    $("#addGlobalTagButton").click(() => {
                        PbNotifications.instance.ask(
                            '<div><input type="text" id="newGlobalTag" placeholder="" class="form-control"/></div>',
                            'New global tag',
                            () => {
                                Render.instance.tagListModel.addTagToList(new PbTag().fromShort($("#newGlobalTag").val()).setUsableFor(["contact-manager"]), true)
                            })
                        $("#newGlobalTag").focus()
                    })

                    if (Render.instance.current_state == "edit") {
                        Render.instance.editMode()
                    } else {
                        Render.instance.viewMode()
                    }
                    resolve()
                })
            })
        })
    }

    addEntriesToAccessrightsDropdown(entries) {
        let dropdown = document.getElementById("visibility_permissions");
        dropdown.innerHTML = "";

        let selected_owners = (ContactManager.instance.active_contact && ContactManager.instance.active_contact.data_owners) || []

        for (let item of entries) {
            let option = document.createElement("option");
            option.text = item.replace("group.", new pbI18n().translate("Group") + ": ").replace("user.", new pbI18n().translate("User") + ": ")
            option.value = item
            option.setAttribute("data-accessrightid", item)
            if (selected_owners.includes(option.value)) {
                option.selected = true
            }
            dropdown.add(option);
        }

        if (!ContactManager.instance.active_contact) {
            console.log("ASDAS", ContactManager.instance.active_contact)
            if (getCookie("data_owners")) {
                let owners = JSON.parse(decodeURIComponent(getCookie("data_owners")))
                for (let owner of owners) {
                    for (let option of dropdown.options) {
                        if (option.value == owner) {
                            option.selected = true
                            break;
                        }
                    }
                }
            } else {
                for (let option of dropdown.options) {
                    if (option.value.startsWith("group.")) {
                        option.selected = true
                        break;
                    }
                }
            }
        }
    }

    renderContactTitles() {
        let tmp_titles = {}

        for (let configEntry of ContactManager.instance.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
            }
        }

        let contactObject = {}
        Render.instance.contactTitles = []
        for (let entry of Render.instance.contacts_titles_sorted) {
            let contactId = entry.contact_id
            let contactTitleString = ""
            for (let titlefield_key of Object.keys(tmp_titles)) {
                if (entry.data[tmp_titles[titlefield_key]]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                }
                else if (entry.data[tmp_titles[titlefield_key].toLowerCase()]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key].toLowerCase()] + " "
                }
            }
            contactObject = {
                contact_id: contactId,
                title: contactTitleString
            }
            Render.instance.contactTitles.push(contactObject)
        }
        let tmp = Render.instance.contactTitles;
        tmp.sort(function (a, b) {
                a = a.title.toLowerCase()
                b = b.title.toLowerCase()
                return a < b ? -1 : 1
            }
        );
        Render.instance.contacts_titles_sorted = tmp;
        Render.instance.contact_labels = tmp;
    }

    addContactsToContactDropdown(select_id, placeholder_pbI18n) {
        // $(select_id).dropdown({ placeholder: new pbI18n().translate(placeholder_pbI18n), })
        var dropdown = document.getElementById(select_id)
        $(dropdown).children().remove().end()

        let option = document.createElement('option')
        option.text = new pbI18n().translate("no contact linked")
        option.value = '' //JSON.stringify(item)
        option.setAttribute('data-contact-id', '')
        dropdown.add(option)

        if (ContactManager.instance.active_contact == undefined) {
            for (let item of Render.instance.contacts_titles_sorted) {
                let option = document.createElement('option');
                option.text = item.title;
                option.value = item.contact_id; //JSON.stringify(item)
                option.setAttribute('data-contact-id', item.contact_id);
                dropdown.add(option);
            }
        } else {
            for (let item of Render.instance.contacts_titles_sorted) {
                if (item.contact_id != ContactManager.instance.active_contact.contact_id) {
                    let option = document.createElement('option');
                    option.text = item.title;
                    option.value = item.contact_id; //JSON.stringify(item)
                    option.setAttribute('data-contact-id', item.contact_id);
                    dropdown.add(option);
                }
            }
        }


    }

    setPreselectOptions(selectElementId, dataSource) {
        for (let option of document.getElementById(selectElementId)) {
            let field_value = dataSource
            if (field_value && field_value.includes(option.value)) {
                option.selected = true
            }
        }
    }

    renderContactLabels(targetElementId, selectedContacts) {
        let labels = Render.instance.contact_labels.filter(element => {
            return selectedContacts.indexOf(element.contact_id) != -1;
        })

        $('#' + targetElementId).html('')
        labels.forEach(element => {
            addRow(element)
        })

        function addRow(element) {
            const span = document.createElement('span');
            let targetUrl = '/contact-manager/?ci=' + `${element.contact_id}`
            span.innerHTML = `
            <a href=${targetUrl} target= "_blank" class="ui label visible clickable" data-value=${element.contact_id}><i class="far fa-address-card pr-2"></i>${element.title}</a>
            `
            document.getElementById(targetElementId).appendChild(span);
        }
    }


}