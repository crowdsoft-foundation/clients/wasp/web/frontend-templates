import { Saga, SagaStep } from "../../csf-lib/pb-saga.js?v=[cs_version]";
import {CONFIG} from "../../pb-config.js?v=[cs_version]"

export class ContactManagerGuest {
    config = []
    constructor() {
        ContactManagerGuest.instance = this
    }

    setConfig(response){
        console.log("In set config", response)
        this.config = response.data
    }

    fetchConfigAsGuest = (apikey) => {
        return new Promise ((resolve, reject) => {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/contacts/fieldconfig",
                "method": "GET",
                "timeout": 0,
                "headers": {
                  "apikey": `${apikey}`
                }}
                $.ajax(settings).done(function (response) {
                    ContactManagerGuest.instance.setConfig(response)
                    resolve(response)
                });
            
        })
    }
}






/* let step1 = new SagaStep()
step1.setName("step_create_contact")
step1.setEventName("createNewContact")
step1.setOrder(0)

let future_data = step1.getDefaultData()
future_data.bearbeitende_beraterin= "asd"
future_data.firstname = "Foo"
future_data.lastname = "Bar"
step1.setPayload(future_data)
console.log(step1.getJSON())


let step2 = new SagaStep()
step2.setName("step_start_contact_verification_process")
step2.setEventName("verifyContact")
step2.setOrder(1)
step2.setPayload({})
let mySaga = new Saga()
mySaga.addStep(step1)
mySaga.addStep(step2)
mySaga.create() */
/* let saga = new Saga()
saga.nextStep()
let until_step = "verifyContact"
saga.fastForward() */

// Schritt 0 Invite-Link
// Schritt 1 Kontakt anlegen
// Schritt 2 Email verschicken
// Schritt 3 Bestätigung
// Schritt 4 (optional) Login anlegen

// Schritt 0 Invite-Link
// Schritt 1 Registration Email verschicken
// Schritt 2 Registrierungsformular abschicken & verarbeiten
// Schritt 3 Double OptIn
// Schritt 4 Account anlegen
// Schritt 5 Login anlegen

/* let recent_task_id = ""
    let socket = new socketio()
    socket.commandhandlers["newSagaCreated"] = function (args) {
        console.log("recent_task_id", recent_task_id)
        if (recent_task_id == args.task_id) {
            //nextSagaStepByCorrelationId(args.task_id)
            sagaFastForwardByCorrelationId(args.task_id, 1)
        }
    }

    socket.commandhandlers["showTaskError"] = function (args) {
        alert("Fehler:" + args.text)
    }

    function setSagaTaskId(response) {
        console.log("MY RESPONSE", response);
        recent_task_id = response.task_id
    }

    function nextSagaStepByCorrelationId(correlationId) {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/saga/next/correlation_id/" + correlationId,
            "method": "GET",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey")
            },
            "success": (response) => console.log(response)
        };

        $.ajax(settings)
    }

    function sagaFastForwardByCorrelationId(correlation_id, until_step) {
        
    }

    function createSaga() {
        let lastname = $("#lastname").val()
        let firstname = $("#firstname").val()
        let email = $("#email").val()
        let saga_name = `Guest-Contact_${lastname}_${firstname}_${email}`
        
        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/createNewSaga",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey"),
                "Content-Type": "application/json"
            },
            "success": setSagaTaskId,
            "data": JSON.stringify({
                "saga_name": saga_name,
                "saga_data": [
                    {
                        "step_name": "step_create_contact",
                        "event_name": "createNewContact",
                        "order": 0,
                        "payload": {
                            "external_id": "a145fef9-848f-4ecc-a9ec-0fe7ef10f926",
                            "data": {
                                "firstname": firstname,
                                "lastname": lastname,
                                "alias": "",
                                "telefon": "",
                                "email": email,
                                "datum_des_eingangs": "",
                                "bearbeitende_beraterin": "",
                                "comment": "",
                                "_verified": false
                            },
                            "data_owners": [],
                            "parent_ids": [],
                            "child_ids": []
                        }
                    },
                    {
                        "step_name": "step_start_contact_verification_process",
                        "event_name": "verifyContact",
                        "order": 1,
                        "payload": {
                            "contact_id": "",
                            "external_id": "a145fef9-848f-4ecc-a9ec-0fe7ef10f926",
                            "data": {}
                        }
                    },
                    {
                        "step_name": "step_finish_contact_verification_process",
                        "event_name": "contactVerified",
                        "order": 2,
                        "payload": {
                            "external_id": "a145fef9-848f-4ecc-a9ec-0fe7ef10f926",
                            "data": {}
                        }
                    },
                    {
                        "step_name": "step_create_login",
                        "event_name": "createLogin",
                        "order": 3,
                        "payload": {
                            "username": email,
                            "password": "randomgenerated",
                            "email": email,
                            "roles": [
                                "role.guest_user"
                            ],
                            "links": [
                                {
                                    "resolver": "contact-manager",
                                    "type": "contact",
                                    "external_id": "a145fef9-848f-4ecc-a9ec-0fe7ef10f926"
                                }
                            ]
                        }
                    }
                ],
                "data_owners": []
            }),
        };

        $.ajax(settings)
        
    } */