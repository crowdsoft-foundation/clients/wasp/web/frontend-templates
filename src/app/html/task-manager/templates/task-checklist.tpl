<div class="d-flex align-items-center pd-b-5">
    <h6 class="page-title pd-r-10">Checkliste: </h6>
    <input id="todo_input" name="todo_input" type="text" class="form-control" required="" value="">
    <button class="btn btn-indigo-white" id="newTodoButton"><i class="typcn typcn-plus tx-18 pr-1"></i></button>
</div>
<div class="ml-2 mr-3 mb-5 mb-md-2" id="checklist_entries_container">
    <ul class="list-group">
        {% for entry in entries %}
        <li id="todo_entry_{{entry.id}}" class="d-flex justify-content-between align-items-center list-item">
            <label class="mb-0"><input data-selector="list-item-checkbox" data-todoid="{{entry.id}}" id="checkbox_{{entry.id}}" type="checkbox" class="list-item-checkbox" {% if entry.done  %}checked{% endif %}>
                <span class="ml-2 tx-dark list-item-text {% if entry.done  %}done{% endif %}" data-selector="list-item-text" id="text_{{entry.id}}">{{entry.desc}}</span>
            </label>
            <button data-selector="list-item-delete" data-todoid="{{entry.id}}" class="remove btn btn-with-icon pd-0"><i class="typcn typcn-times"></i></button>
        </li>
        {% endfor %}
    </ul>
</div>