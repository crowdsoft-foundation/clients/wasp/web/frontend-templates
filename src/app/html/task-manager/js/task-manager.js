import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {ContactManager} from '../../contact-manager/js/contact-manager.js?v=[cs_version]'
import {pbPermissions} from "../../csf-lib/pb-permission.js?v=[cs_version]"
import {pbLinkResolver} from "../../csf-lib/pb-linkresolver.js?v=[cs_version]"
import {pbEventEntries} from "../../easy2schedule/js/pb-event-entries.js?v=[cs_version]"

export class TaskManager {
    loaded = 0
    queue_list = {}
    queue_entries = {}
    active_queue_list_id = undefined
    active_queue_list_data = undefined
    active_entry_id = undefined
    active_entry_data = undefined
    active_entry_links = undefined
    contacts_list_sorted = [];
    contactTitles = [];
    eventsToLink = []
    active_entry_contact = undefined;
    active_entry_event_id = undefined;
    relatedEventTypesForHistory = ["newQueueEntryCreated","queueEntryUpdated", "queueEntriesOrderUpdated"]
    task_templates = {}
    resources = []
    active_resource = undefined
    template_groups = []
    active_template_group = undefined
    comments= undefined

    constructor() {
        if (!TaskManager.instance) {
            TaskManager.instance = this
            TaskManager.instance.socket = new socketio()
            TaskManager.instance.socket.commandhandlers["refreshQueues"] = function (args) {

            }
            TaskManager.instance.socket.commandhandlers["refreshQueueEntries"] = function (args) {

            }
            TaskManager.instance.socket.commandhandlers["refreshTemplates"] = function(args) {

            }
        }

        return TaskManager.instance
    }

    init(queue_id = null) {
        return new Promise(function (resolve, reject) {
            new pbPermissions().init().then(function () {
                let ee = new pbEventEntries()
                let arr = [ee.init(), new ContactManager().init(), TaskManager.instance.getEventsToLink(), TaskManager.instance.fetchTaskManagerResources()]
                Promise.all(arr)
                    .then(function (res) {
                        TaskManager.instance.groups = ee.possible_groups
                        for (let member of ee.group_members) {
                            TaskManager.instance.groups.push(member)
                        }
                        TaskManager.instance.groups.sort((a, b) =>
                            a.toLowerCase() > b.toLowerCase() ? 1 : -1
                        );

                        TaskManager.instance.fetchQueueData(queue_id)
                            .then(function (res) {
                                TaskManager.instance.finishLoadingLevel()
                                resolve()
                            })
                            .catch(function (err) {
                                reject()
                            }) // This is executed
                    }).then(function () {
                    TaskManager.instance.contacts_list_sorted = new ContactManager().contacts_list
                    TaskManager.instance.renderContactsList()
                })
            })
        })
    }

    initGuest(template_id = null) {
        return new Promise(function (resolve, reject) {
            let ee = new pbEventEntries()
            let arr = [TaskManager.instance.fetchTaskTemplateById(template_id)]
            Promise.all(arr).then(function (results) {
                resolve(results)
            }).catch(function (err) {
                console.log("Error while initGuest->fetchTaskTemplateById", err)
            })
        })
    }

    setOrderValueByIds(data = [], skip_saveing=false) {
        let entries_order = {}
        let i = 0
        for (let id of data) {
            if (Object.keys(TaskManager.instance.queue_entries[TaskManager.instance.active_queue_list_id]).includes(id)) {
                entries_order[id] = i
                i++
            } else {
                alert("Think about how to solve THIS case")
            }
        }
        TaskManager.instance.entries_order = entries_order
        if(!skip_saveing) {
            TaskManager.instance.saveOrder()
        }

    }

    saveOrder() {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateQueueEntriesOrder",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "entries_order": TaskManager.instance.entries_order
                }),
                "success": resolve,
                "error": reject
            };

            $.ajax(settings)
        })
    }


    raiseLoadingLevel(value) {
        TaskManager.instance.loaded += value
        if (TaskManager.instance.loaded > 100) {
            TaskManager.instance.loaded = 100
        }
        Fnon.Wait.Change(new pbI18n().translate("Loading...") + ' ' + TaskManager.instance.loaded + '%');
    }

    finishLoadingLevel() {
        return new Promise(function (resolve, reject) {
            let interval = window.setInterval(function () {
                TaskManager.instance.raiseLoadingLevel(5)
                if (TaskManager.instance.loaded == 100) {
                    Fnon.Wait.Remove(500)
                    window.clearInterval(interval)
                }
            }, 10)
        })
    }

    fetchQueueData(queue_id, resolve, reject) {
        return new Promise(function (resolve, reject) {
            TaskManager.instance.raiseLoadingLevel(10)
            TaskManager.instance.fetchQueuesList()
                .then(function (res) {
                    let req = []
                    if (queue_id == null) {
                        for (let [key, value] of Object.entries(TaskManager.instance.queue_list)) {
                            req.push(TaskManager.instance.fetchQueueEntries(key))
                        }
                    } else {
                        req.push(TaskManager.instance.fetchQueueEntries(queue_id))
                    }
                    Promise.all(req)
                        .then(function (res) {
                            resolve()
                        })
                        .catch(function (err) {
                            reject()
                        }) // This is executed

                })
                .catch(function (err) {
                    reject()
                }) // This is executed
        })
    }

    fetchQueuesList(resolve, reject) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/queues/list",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (response) => {
                    TaskManager.instance.setQueuesList(response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
    }

    setQueuesList(response) {
        if (response["queues"]) {
            TaskManager.instance.queue_list = response["queues"]
            fireEvent("renderQueueList", {"data": TaskManager.instance.queue_list})
        }
    }

    fetchQueueEntries(queue_id) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_queue_entries/" + queue_id,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (response) => {
                    TaskManager.instance.setQueueEntries(queue_id, response)
                    resolve()
                },
                "error": reject
            };

            $.ajax(settings)
        })
    }

    setQueueEntries(queue_id, response) {
        if (response["queue_entries"]) {
            TaskManager.instance.queue_entries[queue_id] = response["queue_entries"]
        }
    }

    getQueueEntries() {
        return TaskManager.instance.queue_entries[TaskManager.instance.active_queue_list_id]
    }

    setActiveQueueById(queue_id) {
        TaskManager.instance.active_queue_list_id = undefined
        TaskManager.instance.active_queue_list_data = undefined
        TaskManager.instance.active_queue_list_data_owners = undefined

        if (!queue_id) {
            TaskManager.instance.active_queue_list_id = undefined
            TaskManager.instance.active_queue_list_data = undefined
            TaskManager.instance.active_queue_list_data_owners = undefined
        } else {
            for (let [id, value] of Object.entries(TaskManager.instance.queue_list)) {
                if (id == queue_id) {
                    TaskManager.instance.active_queue_list_id = id
                    TaskManager.instance.active_queue_list_data = value["queue_data"]
                    TaskManager.instance.active_queue_list_data_owners = value["owners"]
                    break
                }
            }
        }
    }

    getActiveQueueDataOwners() {
        return TaskManager.instance.active_queue_list_data_owners
    }

    getActiveQueueData() {
        return TaskManager.instance.active_queue_list_data
    }

    getActiveQueueId() {
        return TaskManager.instance.active_queue_list_id
    }

    setActiveQueueEntryById(entry_id) {
        return new Promise(function (resolve, reject) {
            TaskManager.instance.active_entry_id = undefined
            TaskManager.instance.active_entry_data = undefined
            TaskManager.instance.active_entry_links = undefined

            if (!entry_id) {
                TaskManager.instance.active_entry_id = undefined
                TaskManager.instance.active_entry_data = undefined
                TaskManager.instance.active_entry_links = undefined
                resolve()
            } else {
                const resolver = new pbLinkResolver()

                if(TaskManager.instance.active_queue_list_id != undefined) {
                    for (let [id, value] of Object.entries(TaskManager.instance.queue_entries[TaskManager.instance.active_queue_list_id])) {
                        if (id == entry_id) {
                            TaskManager.instance.active_entry_id = id
                            TaskManager.instance.active_entry_data = value["entry_data"]
                            TaskManager.instance.active_entry_links = value["links"]
                            // ToDo: Resolve links
                            if (TaskManager.instance.active_entry_links) {
                                resolver.getData(value["links"]).then(
                                    function (res) {
                                        TaskManager.instance.handleResolverLinks(res)
                                        resolve()
                                    }
                                )
                            }
                            break
                        }
                    }
                }
                
            }
        })
    }

    getActiveQueueEntryId() {
        return TaskManager.instance.active_entry_id
    }

    getActiveQueueEntryData() {
        return TaskManager.instance.active_entry_data
    }

    getActiveQueueEntryLinks() {
        return TaskManager.instance.active_entry_links
    }

    handleResolverLinks(link) {
        const contactlink = link["contact-manager"] || undefined
        const eventlink = link["easy2schedule"] || undefined
        if (contactlink && contactlink.contact && Object.keys(contactlink).length > 0) {
            TaskManager.instance.active_entry_contact = Object.keys(contactlink.contact)[0]
        } else {
            TaskManager.instance.active_entry_contact = undefined;
        }


        TaskManager.instance.active_entry_event_id = undefined
        if (typeof eventlink == "object" && typeof eventlink.event == "object") {
            for (let key of Object.keys(eventlink.event)) {
                if (key != "remove" && key != "event") {
                    TaskManager.instance.active_entry_event_id = key

                }
            }
        }

    }

    renderContactsList() {
        let tmp_titles = {}

        for (let configEntry of ContactManager.instance.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
            }
        }

        let contactObject = {}
        for (let entry of TaskManager.instance.contacts_list_sorted) {
            let contactId = entry.contact_id
            let contactTitleString = ""
            for (let titlefield_key of Object.keys(tmp_titles)) {
                if (entry.data[tmp_titles[titlefield_key]]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                }
                else if (entry.data[tmp_titles[titlefield_key].toLowerCase()]) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key].toLowerCase()] + " "
                }
            }
            contactObject = {
                contact_id: contactId,
                title: contactTitleString
            }
            TaskManager.instance.contactTitles.push(contactObject)
        }

        let tmp = TaskManager.instance.contactTitles;

        tmp.sort(function (a, b) {
                a = a.title.toLowerCase()
                b = b.title.toLowerCase()
                return a < b ? -1 : 1
            }
        );

        TaskManager.instance.contacts_list_sorted = tmp;
    }

    createNewQueue(name, data_owners = []) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewQueue",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "queue_data": {
                        "queue_name": name
                    },
                    "data_owners": data_owners
                }),
                "success": (response) => {
                    resolve(response["task_id"])
                },
                "error": reject
            };

            $.ajax(settings)
        })
    }

    saveChangesOnQueue() {
        return new Promise(function (resolve, reject) {
            let payload = {}
            payload["queue_data"] = TaskManager.instance.active_queue_list_data
            payload["data_owners"] = TaskManager.instance.active_queue_list_data_owners
            payload["queue_id"] = TaskManager.instance.active_queue_list_id

            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateQueue",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    resolve()
                },
                "error": reject
            };

            $.ajax(settings)
        })
    }

    deleteQueueById(queue_id) {
        return new Promise(function (resolve, reject) {
            let payload = {}
            payload["queue_id"] = queue_id

            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteQueue",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    resolve()
                },
                "error": reject
            };

            $.ajax(settings)
        })
    }

    deleteQueuesByName(name) {
        for (let [id, value] of Object.entries(TaskManager.instance.queue_list)) {
            if (value["queue_data"]["queue_name"] == name) {
                TaskManager.instance.deleteQueueById(id)
            }
        }
    }

    setTaskTemplates(response) {
        if (response) {
            TaskManager.instance.task_templates = response.templates
        }
    }

    getTaskTemplates() {
        return TaskManager.instance.task_templates
    }

    setTaskManagerResources(response) {
        if(response) {
            TaskManager.instance.resources = response
        }
    }
    getTaskManagerResources() {
        return TaskManager.instance.resources
    }

    setActiveResource(resource_id) {
        TaskManager.instance.active_resource = TaskManager.instance.resources.find(element => element.resource_id == resource_id)
    }

    getActiveResource() {
        return TaskManager.instance.active_resource 
    }

    setTaskManagerTemplateGroups (response) {
        if(response) {
            TaskManager.instance.template_groups = response
        }
    }

    getTaskManagerTemplateGroups() {
        return TaskManager.instance.template_groups
    }

    setActiveTemplateGroup(group_id) {
        TaskManager.instance.active_template_group = TaskManager.instance.template_groups.filter(element => element.group_id == group_id)[0]
    }

    getActiveTemplateGroup() {
        return TaskManager.instance.active_template_group
    }


    //#####################

    createNewQueueEntry(title, description, status, assigned_to = [], due_date="", due_time, order = 0, resource="default", links, data_owners = [], create_easy2schedule_event=false, tags=[], todos=[]) {
        return new Promise(function (resolve, reject) {
            let payload = {
                "entry_data": {
                    "queue_id": TaskManager.instance.active_queue_list_id,
                    "title": title,
                    "description": description,
                    "status": status,
                    "assigned_to": assigned_to,
                    "due_date": due_date,
                    "due_time": due_time,
                    "create_easy2schedule_event": create_easy2schedule_event,
                    "order": order,
                    "resource": resource,
                    "tags": tags,
                    "todos": todos
                },
                "data_owners": data_owners,
                "links": links
            }

            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewQueueEntry",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    resolve(response["task_id"])
                },
                "error": reject
            };

            $.ajax(settings)
        })
    }

    saveChangesOnQueueEntry() {
        return new Promise(function (resolve, reject) {
            let payload = {}
            payload["entry_data"] = TaskManager.instance.active_entry_data
            payload["entry_id"] = TaskManager.instance.active_entry_id
            payload["links"] = TaskManager.instance.active_entry_links
            if(payload["entry_data"]["create_time"]) {
                delete payload["entry_data"]["create_time"]
            }

            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateQueueEntry",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    resolve()
                },
                "error": reject
            };

            $.ajax(settings)
        })
    }

    deleteQueueEntryById(queue_id) {
        return new Promise(function (resolve, reject) {
            let payload = {}
            payload["entry_id"] = queue_id

            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteQueueEntry",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    resolve()
                },
                "error": reject
            };

            $.ajax(settings)
        })
    }

    getEventsToLink() {
        return new Promise(function (resolve, reject) {
            let apikey = getCookie("apikey")
            let from_date = new Date()
            let current_date = new Date()
            let until_date = new Date(current_date.setMonth(current_date.getMonth() + 12)).toISOString().slice(0, -5)
            from_date = from_date.toISOString().slice(0, -5)

            var settings = {
                "url": CONFIG.API_BASE_URL + "/cb/events?apikey=" + apikey + "&custom_param1=something&custom_param2=somethingelse&start=" + from_date + "&end=" + until_date + "&timeZone=Europe/Berlin",
                "method": "GET",
                "timeout": 0,
                "success": (response) => {
                    TaskManager.instance.eventsToLink = response
                    fireEvent("eventsToLinkUpdated", {"data": TaskManager.instance.eventsToLink})
                    resolve()
                },
                "error": reject
            };

            $.ajax(settings)
        })
    }

    deleteActiveQueue() {
        TaskManager.instance.deleteQueueById(TaskManager.instance.active_queue_list_id)
    }

    deleteActiveQueueEntry() {
        TaskManager.instance.deleteQueueById(TaskManager.instance.active_entry_id)
    }


    fetchActiveQueueEntryHistory() {
        TaskManager.instance.fetchTaskHistoryFromApi(TaskManager.instance.active_entry_id, TaskManager.instance.active_entry_data["create_time"]).then((response) => {
            fireEvent("showHistoryData", response)
        })
    }

    fetchTaskHistoryFromApi(search = "", starttime=false) {
        let event_types = TaskManager.instance.relatedEventTypesForHistory
        starttime = starttime.substring(0,10)
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + `/es/event/history?search=${search}&event_types=${event_types.join(",")}&starttime=${starttime}`,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (response)=> resolve(response)
            }

            $.ajax(settings)
        })
    }
    

    fetchTaskTemplates(queue_id) {
        return new Promise ( function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/task_templates/list_by_queue/" + queue_id,
                "method": "GET",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey")
                },
                "success": (response) => {
                    TaskManager.instance.setTaskTemplates(response)
                    console.log("Task response", response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
    }

    fetchTaskTemplateById(template_id) {
        return new Promise ( function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/task_template/by_id/" + template_id,
                "method": "GET",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey")
                },
                "success": (response) => {
                    resolve(response.data)
                },
                "error": reject
            };
            $.ajax(settings)
        })
    }

    createNewTaskTemplate (queue_id, title, description, status, assigned_to = [], due_date="", resource="default", name="", links, data_owners = [], create_easy2schedule_event=false) {
        let payload = {
            "template_data": {
            "queue_id": queue_id,
            "title": title,
            "name": name,
            "description": description,
            "status": status,
            "assigned_to": assigned_to,
            "due_date": due_date,
            "resource": resource,
            "links": links,
            "create_easy2schedule_event": create_easy2schedule_event
            },
            "data_owners": data_owners
        }

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewTaskTemplate",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    console.log(response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        

        })
    }

    deleteTaskTemplate(template_id) {
        
        return new Promise ( function(resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteTaskTemplate",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "template_id": template_id
                }),
                "success": (response) => {
                    console.log(response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
       
    }

    fetchTaskManagerResources() {
        return new Promise ( function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_taskmanager_resources",
                "method": "GET",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey")
                },
                "success": (response) => {
                    TaskManager.instance.setTaskManagerResources(response)
                    console.log("Task response", response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
    }

    createNewTaskManagerResource (name, tags={}, type="default", data_owners = []) {

        let payload = {
            "resource_data": {
                "name": name,
                "tags": tags
            },
            "type": type,
            "data_owners": data_owners  
        }

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewTaskManagerResource",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    console.log(response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
    }

    updateTaskManagerResource (resource_id, name, tags={}, type="default", data_owners = []) {

        let payload = {
            "resource_id": resource_id,
            "resource_data": {
                "name": name,
                "tags": tags
            },
            "type": type,
            "data_owners": data_owners  
        }

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateTaskManagerResource",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    console.log(response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
    }

    deleteTaskManagerResource(resource_id) {
        
        return new Promise ( function(resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteTaskManagerResource",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "resource_id": resource_id
                }),
                "success": (response) => {
                    console.log(response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
       
    }

    fetchTaskManagerTemplateGroups() {
        return new Promise ( function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_taskmanager_template_groups",
                "method": "GET",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey")
                },
                "success": (response) => {
                    TaskManager.instance.setTaskManagerTemplateGroups(response)
                    console.log("NEW GROUPS FETCHED")
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
    }

    createNewTaskManagerTemplateGroup(name, start_date=null, templates=[], scheduling_interval=null, data_owners = []) {
 
        let payload = {
            "template_group_data": {
                "name": name,
                "templates": templates
            },
            "start_date": start_date,
            "scheduling_interval": scheduling_interval,
            "data_owners": data_owners  
        }

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewTaskManagerTemplateGroup",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    console.log(response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
    }

    updateTaskManagerTemplateGroup (group_id, name, start_date=null, templates=[], scheduling_interval=null, data_owners = []) {

        let payload = {
            "group_id": group_id,
            "template_group_data": {
                "name": name,
                "templates": templates
            },
            "start_date": start_date,
            "scheduling_interval": scheduling_interval,
            "data_owners": data_owners  
        }

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateTaskManagerTemplateGroup",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => {
                    console.log(response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
    }

    deleteTaskManagerTemplateGroup(group_id) {
        
        return new Promise ( function(resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteTaskManagerTemplateGroup",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "group_id": group_id
                }),
                "success": (response) => {
                    console.log(response)
                    resolve()
                },
                "error": reject
            };
            $.ajax(settings)
        })
       
    }

    setComments(data){
        TaskManager.instance.comments = data
    }

    getComments() {
        return TaskManager.instance.comments
    }

    getCommentsLength() {
        let length = 0
        if(TaskManager.instance.comments != undefined){
            length = Object.entries(TaskManager.instance.comments['comments']).length
        }
        return length
    }

    fetchCommentsByTaskId(task_id) {
        return new Promise((resolve, reject) => {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_comments_by_target/taskmanager|task|" + task_id,
                "method": "GET",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey")
                },
                "success": (response) => {
                    TaskManager.instance.setComments(response)
                    resolve()
                },
                "error": reject
              };
              
              $.ajax(settings)
        })
    }

    createNewComment(comment, target_id) {
        return new Promise((resolve,reject) => {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewComment",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "data": {
                    "comment": comment,
                    "target_id": "taskmanager|task|" + target_id
                  },
                  "data_owners": []
                }),
                "success": (response) => {
                    resolve()
                },
                "error": reject
              };
              
              $.ajax(settings)
        })
    }

    deleteCommentById(comment_id) {
        return new Promise ((resolve, reject) => {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/deleteComment",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "comment_id": comment_id
                }),
                "success": (response) => {
                    resolve()
                },
                "error": reject
              };
              
              $.ajax(settings)
        })
    }
}