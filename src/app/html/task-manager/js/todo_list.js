import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"

export class TodoList {
    constructor(onAdd, onChange, onDelete) {
        this.onAdd = onAdd
        this.onChange = onChange
        this.onDelete = onDelete
    }

    initListener() {
        let self = this
        $("#newTodoButton").click(()=>{
            self.onAdd($("#todo_input").val())
        }).bind(this)

        $('[data-selector="list-item-checkbox"]').change((event)=>{
            self.onChange($(event.target).data("todoid"), $(event.target).is(':checked'))
        })

        $('[data-selector="list-item-delete"]').click((event)=>{
            self.onDelete($(event.currentTarget).data("todoid"))
        })
    }

    render(entries, targetSelector) {
        let self = this
        PbTpl.instance.renderIntoAsync('task-manager/templates/task-checklist.tpl', {"entries": entries}, targetSelector).then(() =>{
            self.initListener()
        })
        return this
    }
}