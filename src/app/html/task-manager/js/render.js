import {TaskManager} from "./task-manager.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbLinkResolver} from "../../csf-lib/pb-linkresolver.js?v=[cs_version]"
import {
    getCookie,
    setCookie,
    multiStringReplace,
    array_intersect,
    objectDiff,
    array_merge_distinct,
    dateDiff,
    isValidDate,
    showNotification,
    makeId,
    getQueryParam
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {TaskManagerFilter} from "./task-manager-filter.js?v=[cs_version]"
import {TodoList} from "./todo_list.js?v=[cs_version]"
import {PbTag} from "../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"

export class Render {
    contacts_list_sorted = []
    current_state = "initial"
    pending_tasks = []
    task_description = undefined
    cookie_queue_id = "queue_id_" + getCookie("username")
    cookie_queue_id_value = ""
    cookie_task_id = "task_id_" + getCookie("username")
    cookie_task_id_value = ""
    cookie_url = ""
    search = undefined

    constructor() {
        if (!Render.instance) {
            Render.instance = this
            Render.instance.tm = new TaskManager()
            Render.instance.handledTaskIds = []
            if ($('#task_description').length) {
                Render.instance.task_description = new Quill('#task_description', {
                    modules: {
                        toolbar: ['bold', 'italic']
                    }, placeholder: 'Beschreibung...', theme: 'bubble', readOnly: true
                })
            }
            Render.instance.initListeners()
            Render.instance.tagListModel = new PbTagsList()
            Render.instance.editMode = false
            Render.instance.alarm_interval = undefined

        }

        return Render.instance
    }

    renderTaskListView() {
        //use as global render function to display list view
        $("#list-view").show()
        $("#task-detail-view").hide()

        Render.instance.toggleActivityMenu(false)

        // pulldown menu filled with lists
        Render.instance.tm.setActiveQueueById(Render.instance.tm.active_queue_list_id)
        Render.instance.renderQueueList()
        Render.instance.renderActiveQueueData()
        Render.instance.renderQueueEntries()

        $(`#task-list-section option[value=${Render.instance.tm.active_queue_list_id}]`).prop("selected", true)
    }

    renderTaskListViewWithCookies() {
        Render.instance.renderTaskListView()
        //set cookie value if there is an active queue
        if (Render.instance.tm.active_queue_list_id !== undefined) {
            setCookie(Render.instance.cookie_queue_id, Render.instance.tm.active_queue_list_id, {
                expires: 365,
                path: '/'
            })
            setCookie(Render.instance.cookie_task_id, "", {expires: 0, path: '/'})
        }
        Render.instance.renderURLfromCookie(false)
    }


    renderQueueList() {
        $("#task-list-section").html("")
        $('#task-list-section').append($('<option>', {
            value: "0", text: "Keine Liste ausgewählt"
        }))

        for (let [key, value] of Object.entries(Render.instance.tm.queue_list)) {
            $('#task-list-section').append($('<option>', {
                value: key, text: value.queue_data.queue_name
            }))
        }
    }

    renderActiveQueueData() {
        let queueName = new pbI18n().translate("No list selected")
        let queueFilterOne = ""
        let queueFilterTwo = ""
        let queueFilterThree = ""

        if (Render.instance.tm.active_queue_list_id != undefined) {
            queueName = Render.instance.tm.active_queue_list_data.queue_name
        }

        if ($('#task-list-filter-status option').filter(':selected').val() !== "0") {
            queueFilterOne = `<span class="badge badge-secondary ml-2"><i class="fa fa-filter"></i>${$('#task-list-filter-status option').filter(':selected').text()}</span>`
        }

        if ($('#task-list-filter-responsible option').filter(':selected').val() !== "0") {
            queueFilterTwo = `<span class="badge badge-secondary ml-2"><i class="fa fa-filter"></i>${$('#task-list-filter-responsible option').filter(':selected').text()}</span>`

        }
        if ($('#no_filter').is(':checked') && $('#search_input').val() !== "") {
            queueFilterThree = `<span class="badge badge-warning ml-2"><i class="fa fa-filter"></i>Suche ignoriert Filter</span>`
            queueFilterOne = ""
            queueFilterTwo = ""
        }

        $("#select-list-name").html(queueName + queueFilterOne + queueFilterTwo + queueFilterThree)

    }

    replace_variables_of_list_entry(listItem) {
        let newListItem = $("#task-item").html()
        let [key, value] = listItem
        let [ressource, tags] = Render.instance.displayListViewResource(value.entry_data.resource)
        let details = {
            'task-title': value.entry_data.title,
            'task-details': Render.instance.displayListViewDescription(value.entry_data.description),
            'task-status': Render.instance.displayListViewStatus(value.entry_data.status),
            'task-responsible': Render.instance.displayListViewAssignee(value.entry_data.assigned_to),
            'task-id': key,
            'task-date': Render.instance.displayListViewDate(value.entry_data.due_date),
            'task-resource': ressource,
            'task-resource-tags': tags,
            'task-tags': value.entry_data.tags ? "<span class='badge badge-data-tags'>" + value.entry_data.tags.join("</span> <span class='badge badge-data-tags'> ") + "</span>" : "",
        }
        let listItemContent = multiStringReplace(
            details,
            newListItem
        )
        return listItemContent
    }

    filter_list_data_by_assignee(assignee_dropdown_value, render_queue_entry_list) {
        let search_in_properties = []
        const tmFilter = new TaskManagerFilter(Render.instance.tm)

        if (assignee_dropdown_value != 0) {
            search_in_properties = ["assigned_to"]
            let invert_search = false
            let username = "user." + getCookie("username")
            if (assignee_dropdown_value == "responsible_mine") {
                render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, username, render_queue_entry_list, invert_search)
            } else if (assignee_dropdown_value == "responsible_other") {
                invert_search = true
                render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, username, render_queue_entry_list, invert_search)
                render_queue_entry_list = tmFilter.remove_not_assigned(render_queue_entry_list)
            } else if (assignee_dropdown_value == "responsible_none") {
                render_queue_entry_list = tmFilter.filter_for_not_assigned(render_queue_entry_list)
            }
        }

        return render_queue_entry_list
    }

    filter_list_data_by_status(status_dropdown_value, render_queue_entry_list) {
        let search_in_properties = []
        let invert_search = false
        const tmFilter = new TaskManagerFilter(Render.instance.tm)


        if (status_dropdown_value == 0) {
            invert_search = true
            search_in_properties = ["status"]
            status_dropdown_value = "closed"
            let temp_render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, status_dropdown_value, render_queue_entry_list, invert_search)
            render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, 'canceled', temp_render_queue_entry_list, invert_search)
        } else if (status_dropdown_value == 1) {
            invert_search = true
            search_in_properties = ["status"]
            status_dropdown_value = 'open'
            let temp_render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, status_dropdown_value, render_queue_entry_list, invert_search)
            render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, 'pending', temp_render_queue_entry_list, invert_search)
            render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, 'onhold', temp_render_queue_entry_list, invert_search)
            render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, 'planned', temp_render_queue_entry_list, invert_search)
        } else {
            search_in_properties = ["status"]
            render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, status_dropdown_value, render_queue_entry_list)
        }

        return render_queue_entry_list
    }

    renderQueueEntries() {
        $("#task-list-container").html("")
        let disable_filters = $('#no_filter').is(':checked')
        const tmFilter = new TaskManagerFilter(Render.instance.tm)
        let searchterm = $('#search_input').val()
        let assignee_dropdown_value = $('#task-list-filter-responsible option').filter(':selected').val()
        let status_dropdown_value = $('#task-list-filter-status option').filter(':selected').val()

        Render.instance.stopDraggable()
        if (assignee_dropdown_value == 0 && status_dropdown_value == 0) {
            Render.instance.startDraggable()
        }

        let search_in_properties = ["title", "description", "assigned_to", "tags"]
        let render_queue_entry_list = tmFilter.get_filtered_list(search_in_properties, searchterm)

        if (!disable_filters) {
            render_queue_entry_list = Render.instance.filter_list_data_by_assignee(assignee_dropdown_value, render_queue_entry_list)
            render_queue_entry_list = Render.instance.filter_list_data_by_status(status_dropdown_value, render_queue_entry_list)
        }
        Render.instance.all_tags_in_list = []
        if (Object.entries(render_queue_entry_list).length > 0) {
            for (let item of Object.entries(render_queue_entry_list)) {
                if(item[1]?.entry_data?.tags) {
                    for (let tag of item[1]?.entry_data?.tags) {
                        if (tag.includes("System:")) {
                            item[1].entry_data.tags = item[1].entry_data.tags.filter(e => e !== tag)
                        }
                    }
                    if (item[1].entry_data.tags) {
                        Render.instance.all_tags_in_list = new Set([...Render.instance.all_tags_in_list, ...item[1].entry_data.tags])
                    }
                }
                let listItemContent = Render.instance.replace_variables_of_list_entry(item)
                $('#task-list-container').append(listItemContent);
            }
        } else {
            $("#task-list-container").html(new pbI18n().translate("No entries"))
        }
    }

    get_id_order() {
        var elem = document.querySelectorAll('[data-selector="list-entry"]')

        let id_order = []
        for (let element of elem) {
            if (!element.id.startsWith("[{")) {
                id_order.push(element.id)
            }
        }
        return id_order
    }

    filter_by_status(desired_status, real_status) {
        if (desired_status == 0 && real_status != 'closed') {
            return true
        }
        if (desired_status == real_status) {
            return true
        } else {
            return false
        }
    }

    filter_by_assigned_to(assignee_dropdown_value, assigned_to) {
        let my_entry_condition = []
        for (let [group_name, group_data] of Object.entries(Render.instance.tm.groups)) {
            my_entry_condition.push(group_name)
        }
        my_entry_condition.push("user." + getCookie("username"))

        return (assignee_dropdown_value == "responsible_mine" && array_intersect(my_entry_condition, assigned_to).length > 0) || (assignee_dropdown_value == "responsible_other" && !(array_intersect(my_entry_condition, assigned_to).length > 0)) && (assigned_to.length > 0) || (assignee_dropdown_value == "responsible_none" && !(assigned_to.length > 0)) || assignee_dropdown_value == "0"

    }

    renderTagList(selectedTags = []) {
        let self = this

        return new Promise((resolve, reject) => {
            Render.instance.tagListModel.load(false, "task-manager").then(() => {
                let entries = Object.entries(Render.instance.tm.getQueueEntries())
                for (let entry of entries) {
                    entry = entry[1]
                    if (entry.entry_data?.tags) {
                        for (let localTag of entry.entry_data.tags) {
                            let myTag = new PbTag().fromShort(localTag)
                            Render.instance.tagListModel.addTagToList(myTag)
                        }
                    }
                }

                let tag_strings = Render.instance.tm?.getActiveQueueEntryData()?.tags || []

                for (let tag of tag_strings) {
                    let tago = new PbTag().fromShort(tag)
                    Render.instance.tagListModel.addTagToList(tago)
                }

                let tagOptions = Render.instance.tagListModel.getSelectOptions(selectedTags)

                PbTpl.instance.renderIntoAsync('csf-lib/pbapi/tagmanager/templates/tag-select2.tpl', {
                    "multiple": true,
                    "addButton": true,
                    "propertyPath": "tags",
                    "options": tagOptions
                }, "#tag_container").then(() => {
                    $('[data-propertyname="tags"]').select2({
                        tags: true,
                        tokenSeparators: [',', ' '],
                        selectOnClose: false
                    })

                    $("#addGlobalTagButton").click(() => {
                        PbNotifications.instance.ask(
                            '<div><input type="text" id="newGlobalTag" placeholder="" class="form-control"/></div>',
                            'New global tag',
                            () => {
                                Render.instance.tagListModel.addTagToList(new PbTag().fromShort($("#newGlobalTag").val()).setUsableFor(["taskmanager"]), true)
                            })
                        $("#newGlobalTag").focus()
                    })

                    if (Render.instance.editMode) {
                        $("#tags").attr("disabled", false)
                        $("#addGlobalTagButton").attr("disabled", false)
                    } else {
                        $("#tags").attr("disabled", true)
                        $("#addGlobalTagButton").attr("disabled", true)
                    }
                    resolve()
                })
            })
        })
    }

    renderDetailData(edit_mode = false, template_id = undefined) {
        window.onpopstate = function (e) {
            window.onpopstate = () => ""
            $("#button-cancel").click()
        }
        Render.instance.resetDetailViewContainer()
        Render.instance.addContactsToContactList()
        /* Render.instance.addEventsToEventsDropdown() */
        Render.instance.addAssigneesToDropdown()
        Render.instance.renderDueTimeDrowdown()
        Render.instance.renderResourceDropdown()
        Render.instance.toggleEditmode(true)
        Render.instance.toggleActivityMenu(false)
        /* Render.instance.renderCommentCounter() */
        Render.instance.setFocusToTaskTitle()
        $('#select-task-status').val("open");
        $('#select-task-responsible').val("default");
        $('#select-task-contact').val("default");
        $('#select-task-appointment').val("default");
        Render.instance.renderTaskTemplateDrowdown('detail-view-template-dropdown')

        if (Render.instance.tm.getActiveQueueEntryId()) {
            Render.instance.toggleEditmode(false)
            Render.instance.toggleActivityMenu(true)
            Render.instance.task_description.clipboard.dangerouslyPasteHTML(0, Render.instance.tm.getActiveQueueEntryData().description)
            if (Render.instance.tm.getActiveQueueEntryData().due_date) {
            } else if (!Render.instance.tm.getActiveQueueEntryData().due_date) {
            }

            $("#task-title").val(Render.instance.tm.getActiveQueueEntryData().title || "")

            let tags = ""

            if (Render.instance.tm.getActiveQueueEntryData().tags) {
                for (let tag of Render.instance.tm.getActiveQueueEntryData().tags) {
                    tags += `${tag}`
                }
            }

            Render.instance.renderTagList(Render.instance.tm.getActiveQueueEntryData().tags).then(() => {

            })


            let ticket_status = ['open', 'pending', 'closed', 'onhold', 'planned', 'canceled']
            if (ticket_status.includes(Render.instance.tm.getActiveQueueEntryData().status)) {
                $('#select-task-status').val(Render.instance.tm.getActiveQueueEntryData().status)
            } else {
                $('#select-task-status').val("open")
            }

            /* $("#select-task-responsible").html("") */

            let assignee = ""
            if (Render.instance.tm.getActiveQueueEntryData().assigned_to && Render.instance.tm.getActiveQueueEntryData().assigned_to.length > 0) {
                //assignee = Render.instance.tm.getActiveQueueEntryData().assigned_to.pop()  this was removing the array entry after first load
                if (Array.isArray(Render.instance.tm.getActiveQueueEntryData().assigned_to)) {
                    assignee = Render.instance.tm.getActiveQueueEntryData().assigned_to.join(" ")
                }
            }

            $(`#select-task-responsible option[value="${assignee}"]`).prop("selected", true)

            if (Render.instance.tm.active_entry_contact != undefined) {
                $('#select-task-contact').val(Render.instance.tm.active_entry_contact)
                $('.clickable_div').css('cursor', 'pointer')
                $('.select2-container--default .select2-selection--single .select2-selection__rendered').css({
                    'color': '#5b47fb',
                });
            } else {
                $('#select-task-contact').val("default")
                $('.clickable_div').css('cursor', 'auto')
                $('.select2-container--default .select2-selection--single .select2-selection__rendered').css({
                    'color': '#1c273c'
                });
            }


            if (Render.instance.tm.active_entry_event_id != undefined) {
                $('#select-task-appointment').val(Render.instance.tm.active_entry_event_id);
            } else {
                $('#select-task-appointment').val("default");
            }

            if (Render.instance.tm.getActiveQueueEntryData().due_date != undefined) {
                const date = new Date(Render.instance.tm.getActiveQueueEntryData().due_date)
                $("#select-task-due-time").removeClass("hidden")

                let due_date = null
                if (typeof Render.instance.tm.getActiveQueueEntryData().due_date == 'string') {
                    due_date = Render.instance.tm.getActiveQueueEntryData().due_date.slice(0, 10)
                }

                $('#select-task-due-date').val(due_date);
                const hours = date.getHours()
                const minutes = date.getMinutes()
                if (hours == `${CONFIG.DEFAULT_TASK_DUEDATE_TIME_HOUR}`) {
                    $('#select-task-due-time-hour').val("")
                    $('#select-task-due-time-minute').val("")
                } else {
                    $(`#select-task-due-time-hour option[value="${hours.toString()}"]`).prop("selected", true)
                    $(`#select-task-due-time-minute option[value="${minutes.toString()}"]`).prop("selected", true)

                    $("#create_easy2schedule_entry_for_due_date").prop('checked', Render.instance.tm.getActiveQueueEntryData().create_easy2schedule_event)
                }
            } else {
                $('#select-task-due-date').val("");
                $("#select-task-due-time-hour").val("")
                $("#select-task-due-time-minute").val("")
            }

            if (Render.instance.tm.getActiveQueueEntryData().resource != undefined) {
                $('#select-task-resource').val(Render.instance.tm.getActiveQueueEntryData().resource)
            } else {
                $('#select-task-resource').val("default");
            }

            Render.instance.tm.fetchCommentsByTaskId(Render.instance.tm.getActiveQueueEntryId()).then(() => {
                Render.instance.showQueueEntryComments(Render.instance.tm.getComments())
            })

        } else if (template_id != undefined) {
            let template = Render.instance.tm.task_templates[template_id]
            $("#task-title").val(template.entry_data.title || "")
            Render.instance.task_description.clipboard.dangerouslyPasteHTML(0, template.entry_data.description)
            $('#select-task-status').val(template.entry_data.status || "open");

            let assignee = ""
            if (template.entry_data.assigned_to && template.entry_data.assigned_to.length > 0) {
                assignee = template.entry_data.assigned_to
            }
            $(`#select-task-responsible option[value="${assignee}"]`).prop("selected", true)
            if (template.entry_data.due_date != undefined) {
                const date = new Date(template.entry_data.due_date)
                $("#select-task-due-time").removeClass("hidden")
                if (typeof template.entry_data.due_date === 'string') {
                    $('#select-task-due-date').val(template.entry_data.due_date.slice(0, 10));
                }
                const hours = date.getHours()
                const minutes = date.getMinutes()
                if (hours == `${CONFIG.DEFAULT_TASK_DUEDATE_TIME_HOUR}`) {
                    $('#select-task-due-time-hour').val("");
                    $('#select-task-due-time-minute').val("")
                } else {
                    $('#select-task-due-time-hour').val(hours.toString());
                    $('#select-task-due-time-minute').val(minutes.toString())
                }
            } else {
                $('#select-task-due-date').val("");
                $("#select-task-due-time-hour").val("")
                $("#select-task-due-time-minute").val("")
            }

            if (template.entry_data.resource != undefined) {
                $('#select-task-resource').val(template.entry_data.resource)
            } else {
                $('#select-task-resource').val("default");
            }

            let template_contact = undefined
            let template_event = undefined
            const handleResolverLinks = (link) => {
                const contactlink = link["contact-manager"] || undefined
                const eventlink = link["easy2schedule"] || undefined

                if (contactlink && contactlink.contact && Object.keys(contactlink).length > 0) {
                    template_contact = Object.keys(contactlink.contact)[0]
                    $('#select-task-contact').val(template_contact);
                    $('#select-task-contact').trigger('change')

                } else {
                    template_contact = undefined;
                    $('#select-task-contact').val("default");
                    $('.clickable_div').css('cursor', 'auto');
                    $('.select2-container--default .select2-selection--single .select2-selection__rendered').css({
                        'color': '#1c273c'
                    });
                }


                if (typeof eventlink == "object" && typeof eventlink.event == "object") {
                    for (let key of Object.keys(eventlink.event)) {
                        if (key != "remove" && key != "event") {
                            template_event = key
                            $('#select-task-appointment').val(template_event);
                        } else {
                            $('#select-task-appointment').val("default");
                        }
                    }
                } else {
                    template_event = undefined
                }

            }

            const resolver = new pbLinkResolver()
            if (template.entry_data.links) {
                resolver.getData(template.entry_data.links).then(function (res) {
                    handleResolverLinks(res)
                    /* resolve() */
                })
            }
        } else {

        }

        let queue_select_entries = []
        for (let [id, value] of Object.entries(Render.instance.tm.queue_list)) {
            $("#select-list-name-dropdown").append(new Option(value.queue_data.queue_name, id))
        }

        let selected_id = $("#task-list-section").val()
        if (Render.instance.tm.getActiveQueueEntryData()) {
            selected_id = Render.instance.tm.getActiveQueueEntryData().queue_id
        } else if ($("#task-list-section").val() == "0") {
            selected_id = $("#select-list-name-dropdown").prop('selectedIndex', 0)
            let queue_id = $("#select-list-name-dropdown").val()
            Render.instance.tm.fetchTaskTemplates(queue_id).then(function () {
                Render.instance.renderTaskTemplateDrowdown('detail-view-template-dropdown')
                Render.instance.renderTaskTemplateDrowdown('list-view-template-dropdown')
            })
        }
        let entries = [
            {"id": "id1", "done": "done", "desc": "This is the first todo"},
            {"id": "id2", "done": "", "desc": "This is the second todo"}
        ]

        new TodoList(Render.instance.onToDoAdd, Render.instance.onToDoChange, Render.instance.onToDoDelete).render(Render.instance.tm.active_entry_data?.todos || [], '#checklist_container')

        $(`#select-list-name-dropdown option[value="${selected_id}"]`).prop("selected", true)
        $("#list-view").hide()
        $("#task-detail-view").show()
    }

    onToDoAdd(desc) {
        let newTodo = {"id": makeId(10), "desc": desc, "done": false}
        if (Render.instance.tm.active_entry_data) {
            if (Render.instance.tm.active_entry_data.todos) {
                Render.instance.tm.active_entry_data.todos.push(newTodo)
            } else {
                Render.instance.tm.active_entry_data["todos"] = [newTodo]
            }

            Render.instance.tm.saveChangesOnQueueEntry().then(() => {
                new TodoList(Render.instance.onToDoAdd, Render.instance.onToDoChange, Render.instance.onToDoDelete).render(Render.instance.tm.active_entry_data?.todos || [], '#checklist_container')
            })
        } else {
            if (Render.instance.newToDos) {
                Render.instance.newToDos.push(newTodo)
            } else {
                Render.instance.newToDos = [newTodo]
            }
            new TodoList(Render.instance.onToDoAdd, Render.instance.onToDoChange, Render.instance.onToDoDelete).render(Render.instance.newToDos || [], '#checklist_container')
        }
    }

    onToDoChange(id, status) {
        if (Render.instance.newToDos) {
            if (Render.instance.newToDos) {
                for (let idx in Render.instance.newToDos) {
                    if (Render.instance.newToDos[idx].id == id) {
                        Render.instance.newToDos[idx]["done"] = status
                        break;
                    }
                }
            }
        } else {
            if (Render.instance.tm.active_entry_data.todos) {
                for (let idx in Render.instance.tm.active_entry_data.todos) {
                    if (Render.instance.tm.active_entry_data.todos[idx].id == id) {
                        Render.instance.tm.active_entry_data.todos[idx]["done"] = status
                        break;
                    }
                }
            }
        }
        Render.instance.tm.saveChangesOnQueueEntry().then(() => {
            new TodoList(Render.instance.onToDoAdd, Render.instance.onToDoChange, Render.instance.onToDoDelete).render(Render.instance.tm.active_entry_data?.todos || [], '#checklist_container')
        })
    }

    onToDoDelete(id) {
        if (Render.instance.tm.active_entry_data) {
            if (Render.instance.tm.active_entry_data.todos) {
                for (let idx in Render.instance.tm.active_entry_data.todos) {
                    if (Render.instance.tm.active_entry_data.todos[idx].id == id) {
                        Render.instance.tm.active_entry_data.todos.splice(idx, 1)
                        break;
                    }
                }
            }
            Render.instance.tm.saveChangesOnQueueEntry().then(() => {
                new TodoList(Render.instance.onToDoAdd, Render.instance.onToDoChange, Render.instance.onToDoDelete).render(Render.instance.tm.active_entry_data?.todos || [], '#checklist_container')
            })
        } else {
            if (Render.instance.newToDos) {
                for (let idx in Render.instance.newToDos) {
                    if (Render.instance.newToDos[idx].id == id) {
                        Render.instance.newToDos.splice(idx, 1)
                        break;
                    }
                }
            }
            new TodoList(Render.instance.onToDoAdd, Render.instance.onToDoChange, Render.instance.onToDoDelete).render(Render.instance.newToDos || [], '#checklist_container')
        }
    }

    renderContactsList() {
        let tmp_titles = {}

        for (let configEntry of ContactManager.instance.fieldconfig) {
            if (configEntry.display_pos_in_list_h1 > 0) {
                tmp_titles[configEntry.display_pos_in_list_h1] = configEntry.field_id
            }
        }

        let contactObject = {}

        for (let entry of TaskManager.instance.contacts_list_sorted) {
            let contactId = entry.contact_id
            let contactTitleString = ""
            for (let titlefield_key of Object.keys(tmp_titles)) {
                if (entry.data[tmp_titles[titlefield_key]] != undefined) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key]] + " "
                }
                else if (entry.data[tmp_titles[titlefield_key].toLowerCase()] != undefined) {
                    contactTitleString += entry.data[tmp_titles[titlefield_key].toLowerCase()] + " "
                }
            }
            contactObject = {
                contact_id: contactId, title: contactTitleString
            }
            TaskManager.instance.contactTitles.push(contactObject)
        }

        let tmp = TaskManager.instance.contactTitles;

        tmp.sort(function (a, b) {
            a = a.title.toLowerCase()
            b = b.title.toLowerCase()
            return a < b ? -1 : 1
        });

        TaskManager.instance.contacts_list_sorted = tmp;
        TaskManager.instance.addContactsToContactList();
    }

    addContactsToContactList() {
        var dropdown = document.getElementById('select-task-contact')
        if (dropdown != null) {
            dropdown.innerHTML = ''
            let option = document.createElement('option')
            option.text = new pbI18n().translate("No contact linked")
            option.value = 'default' //JSON.stringify(item)
            option.setAttribute('data-contact-id', '')
            dropdown.add(option)
            for (let item of TaskManager.instance.contacts_list_sorted) {
                let option = document.createElement('option');
                option.text = item.title;
                option.value = item.contact_id; //JSON.stringify(item)
                option.setAttribute('data-contact-id', item.contact_id);
                dropdown.add(option);
            }
        }

    }

    addEventsToEventsDropdown(data = undefined) {
        if (data) {
            var dropdown = document.getElementById('select-task-appointment')
            if (dropdown != null) {
                dropdown.innerHTML = ''
                let option = document.createElement('option')
                option.text = new pbI18n().translate("No appointment linked")
                option.value = 'default' //JSON.stringify(item)
                option.setAttribute('data-contact-id', '')
                dropdown.add(option)
                for (let item of Render.instance.tm.eventsToLink) {
                    if (item.title != "Anonymized") {
                        let option = document.createElement('option');
                        option.text = item.title + ", " + item.start;
                        option.value = item.id; //JSON.stringify(item)
                        option.setAttribute('data-contact-id', item.id);
                        dropdown.add(option);
                    }

                }
            }
        }


    }

    addAssigneesToDropdown() {
        let option = document.createElement('option')
        option.text = new pbI18n().translate("Not assigned")
        option.value = 'default' //JSON.stringify(item)
        option.setAttribute('data-contact-id', '')
        $("#select-task-responsible").append(option)

        for (let group_name of Render.instance.tm.groups) {
            let display_group_name = group_name.replace("group.", new pbI18n().translate("Group") + ": ").replace("user.", new pbI18n().translate("User") + ": ")
            $("#select-task-responsible").append(new Option(display_group_name, group_name))
        }
    }

    displayListViewDescription(value) {
        let shortDescription = value

        // grab just the first paragraph if no anchor links are included
        if (shortDescription.indexOf("<p>") != -1 && shortDescription.indexOf("<a") === -1) {
            let start = shortDescription.indexOf("<p>")
            let end = shortDescription.indexOf("</p>") + 4
            let newShortDescription = shortDescription.slice(start, end)
            shortDescription = newShortDescription
        }
        //don't display description if there are anchor links
        if (shortDescription.indexOf("<a") != -1) {
            shortDescription = ""
        }
        return shortDescription
    }

    displayListViewStatus(value) {
        let statusBadge
        statusBadge = `<span class="badge badge-success badge_${value}">${new pbI18n().translate(value)}</span>`
        return statusBadge
    }

    displayListViewAssignee(value) {
        let assigned
        if (!value || value.length == 0) {
            assigned = ""
        } else {
            assigned = `<i class="far fa-user"></i> ${value} `
        }
        return assigned
    }

    displayListViewDate(value) {
        let date
        if (!value) {
            date = ""
        } else {
            date = new Date(value)
            let local_date
            if (date.getHours() == CONFIG.DEFAULT_TASK_DUEDATE_TIME_HOUR) {
                local_date = moment.utc(value).local().format(CONFIG.DATEFORMAT)
            } else {
                local_date = moment.utc(value).local().format(CONFIG.DATETIMEWIHTOUTSECONDSFORMAT)
            }

            date = `<i class="far fa-clock"></i> ${local_date} `
        }
        return date
    }

    displayListViewTime(value) {
        let time
        if (!value) {
            time = ""
        } else {
            let local_time = new Date(value).toLocaleDateString()
            time = `<i class="fa-clock"></i> ${local_time} `
        }
        return time
    }

    displayListViewResource(id) {
        let resource = ""
        let tags = []
        let result_tags = ""

        if (!id || id.length == 0) {
            resource = ""
            tags = []
        } else {
            let this_id = id
            for (let value of Render.instance.tm.resources) {
                if (this_id.includes(value.resource_id)) {
                    let resource_name = value.data.name
                    resource = ` ${resource_name}`
                    tags = Object.values(value.data.tags) || []
                }
            }

            for (let tag of tags) {
                result_tags += `<span class="badge badge-data-tags">${tag}</span>`
            }
        }
        return [resource, result_tags]
    }

    renderDueTimeDrowdown() {
        let hours = ["08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"]
        let quarters = ["00", "15", "30", "45"]
        var due_time_hour = document.getElementById('select-task-due-time-hour')
        if (due_time_hour != null) {
            due_time_hour.innerHTML = ''
            let option = document.createElement('option')
            option.text = "--"
            option.value = ""
            due_time_hour.appendChild(option)
            for (let i = 0; i < hours.length; i++) {
                let option = document.createElement('option');
                option.text = hours[i]
                option.value = i + 8
                due_time_hour.appendChild(option);
            }
        }
        var due_time_minute = document.getElementById('select-task-due-time-minute')
        if (due_time_minute != null) {
            due_time_minute.innerHTML = ''
            let option = document.createElement('option')
            option.text = "--"
            option.value = ""
            due_time_minute.appendChild(option)
            for (let i = 0; i < quarters.length; i++) {
                let option = document.createElement('option');
                option.text = quarters[i]
                option.value = quarters[i] == "00" ? "0" : quarters[i]
                due_time_minute.appendChild(option);
            }
        }

    }

    renderTaskTemplateDrowdown(targetElementID) {
        let targetElement = document.getElementById(targetElementID)
        targetElement.innerHTML = ''

        if (Object.keys(Render.instance.tm.task_templates).length != 0) {
            $('#button-new-task-template').prop('disabled', false)
            $('#select-task-template').prop('disabled', false)
            for (let [key, value] of Object.entries(Render.instance.tm.task_templates)) {
                let option = document.createElement('div');
                option.innerHTML = ` ${value.entry_data.name}`
                option.dataset.id = key
                option.classList.add("dropdown-item", "cursor-pointer")
                targetElement.appendChild(option)
                $(option).click(() => Render.instance.renderDetailData(true, option.dataset.id))
            }
        } else if (Object.keys(Render.instance.tm.task_templates).length === 0) {
            $('#button-new-task-template').prop('disabled', true)
            $('#select-task-template').prop('disabled', true)

        }


    }

    renderTaskTemplateList() {
        let template_list = Object.entries(Render.instance.tm.task_templates)
        let template_dropdown_values = template_list.map(element => {
            [element[0], element[1].template_name]
        })

        //element[0] ist template_id, element[1].template_name sollte template name sein
    }

    renderResourceDropdown() {
        let resourceDropdown = $('#select-task-resource')
        if (resourceDropdown != null) {
            resourceDropdown.innerHTML = ''
            let option = document.createElement('option')
            option.text = new pbI18n().translate("No resource linked")
            option.value = 'default' //JSON.stringify(item)
            option.setAttribute('data-resource-id', '')
            resourceDropdown.append(option)
            if (Render.instance.tm.resources.length > 0) {
                $('#select-task-resource').prop('disabled', false)
                for (let item of Render.instance.tm.resources) {
                    let option = document.createElement('option');
                    option.text = item.type + ': ' + item.data.name
                    option.value = item.resource_id; //JSON.stringify(item)
                    option.setAttribute('data-resource-id', item.resource_id)
                    resourceDropdown.append(option);
                }
            } else {
                $('#select-task-resource').prop('disabled', true)
            }

        }
    }


    clearQuillClipboard() {
        Render.instance.task_description.root.innerHTML = ""
    }

    clearHistory() {
        $("#history").hide()
        $("#history-list-container").html("")
        $('#button-task-history').attr('data-selector', 'off').removeClass("btn-primary-pill").addClass("btn-light-pill")
    }

    clearComment() {
        $("#comment").hide()
        $("#comment-list-container").html("")
        $('#button-task-comment').attr('data-selector', 'off').removeClass("btn-primary-pill").addClass("btn-light-pill")
    }

    resetDetailViewContainer() {
        $("#select-list-name-dropdown").find('option[value]').remove();
        $("#select-task-responsible").find('option[value]').remove();
        $('#select-task-contact').find('option[value]').remove();
        $('#select-task-resource').find('option[value]').remove()
        $('#select-task-appointment').val("0");
        $("#select-task-due-date").val("")
        $("#select-task-due-time").addClass("hidden")
        $("#select-task-due-time-hour").val("")
        $("#select-task-due-time-minute").val("")
        $("#task-title").val("")
        $("#create_easy2schedule_entry_for_due_date")[0].checked = false

        Render.instance.clearQuillClipboard()
        Render.instance.toggleActivityMenu(false)

    }

    toggleEditmode(enabled) {
        if (enabled == true) {
            Render.instance.editMode = true
            $('#btn-group-task').show()
            $('#button-edit-task').hide()
            Render.instance.setFocusToTaskTitle()

            Render.instance.task_description.enable(true)
            $('[data-group="info"]').prop('disabled', false)
            $("#create_easy2schedule_entry_for_due_date").prop('disabled', false)
            $('.clickable_div').css({'z-index': '-1000', 'cursor': 'default'});
            $('.select2-container--default .select2-selection--single .select2-selection__rendered').css({
                'color': '#596882', 'cursor': 'default'
            });
            $('#select-task-template').show()
            $("#tags").attr("disabled", false)
            $("#addGlobalTagButton").attr("disabled", false)

        } else {
            Render.instance.editMode = false
            $('#button-edit-task').show()
            $('#btn-group-task').hide()
            Render.instance.task_description.enable(false)
            $('[data-group="info"]').prop('disabled', true)
            $('.clickable_div').css('z-index', '1');
            $('#select-task-template').hide()
            $("#create_easy2schedule_entry_for_due_date").prop('disabled', true)
            $("#tags").attr("disabled", true)
            $("#addGlobalTagButton").attr("disabled", true)
        }
    }

    toggleActivityMenu(visable, exception) {

        if (visable == true && !exception) {
            $('#activity-nav').show()
            $('#button-task-history-wrapper').show()
            $('#button-task-comment-wrapper').show()
            let documentation_url = `${CONFIG.APP_BASE_URL}/documentation/index.html?sid=task-manager|task|${Render.instance.tm.getActiveQueueEntryId()}`
            $("#button-task-documentation").attr("href", documentation_url)
            $('#button-task-documentation-wrapper').show()
        } else if (visable == true && exception === "history") {
            $('#button-task-history-wrapper').hide()
            Render.instance.clearHistory()
        } else {
            $('#activity-nav').hide()
            $('#button-task-history-wrapper').hide()
            Render.instance.clearHistory()
            $('#button-task-comment-wrapper').hide()
            $('#button-task-documentation-wrapper').hide()

            Render.instance.clearComment()
        }
    }

    startDraggable() {

        $("#task-list-container").sortable({
            handle: ".handle", items: ".task-element", update: function (evt) {
                let new_order = Render.instance.get_id_order()
                TaskManager.instance.setOrderValueByIds(new_order)
            },
        })
        $(".handle").removeClass("hidden")
        $(".handle").removeClass("cursor-no")
        $(".handle").addClass("cursor-grab")
    }

    stopDraggable() {

        $(".sortable").each(function () {
            $(this).sortable("destroy")
        })
        //hide me
        $('[data-attr="movable"]').addClass("hidden")
        //   $(".handle").addClass("hidden")
        $(".handle").removeClass("cursor-grab")
        $(".handle").addClass("cursor-no")
        $(".cursor-no").on('click', function (event) {
            event.stopPropagation()
            event.stopImmediatePropagation()
            Fnon.Alert.Primary('Bitte entfernen Sie die Listenfilter, um die Drag & Drop-Funktion zu verwenden.', 'Funktion nicht verfügbar', 'OK, verstanden', () => {
                //add call back function here if you want
            });
        })

    }

    setFocusToTaskTitle() {
        setTimeout(() => {
            $('#task-title').focus()
        }, 500)
    }

    showQueueEntryComments(data) {
        const formatTimeStamp = (datestring) => {
            let browserCompatibleTimeStamp = datestring.replaceAll('-', '/')
            let timestamp = new Date(browserCompatibleTimeStamp)
            let offset = timestamp.getTimezoneOffset() / 60
            let hours = timestamp.getHours()
            timestamp.setHours(hours - offset)
            let localeTimeStamp = new Date(timestamp).toLocaleString()
            return localeTimeStamp.slice(0, -3)
        }
        let comments = undefined
        if (data) {
            comments = data['comments']
            if (Object.keys(data).length != 0) {
                $("#comment").show()
                $("#comment-list-container").html("")
                let newCommentItem = $("#comment-item").html()

                for (let [key, value] of Object.entries(comments)) {
                    var details = {
                        'comment-delete-id': key,
                        'comment-creator': value.creator,
                        'comment-time': formatTimeStamp(value.creation_time),
                        'comment-detail': value.data.comment
                    }
                    let commentItemContent = multiStringReplace(details, newCommentItem)
                    $("#comment-list-container").prepend(commentItemContent)
                    if (getCookie('username') != value.creator || !PbAccount.instance.hasPermission("frontend.taskmanager.comments.delete.own")) {
                        $(`#delete_${key}`).remove()
                    }
                }
            }
        }
        $('#button-task-comment').removeClass("btn-light-pill").addClass("btn-primary-pill")
    }


    showTaskEntryHistory(data) {
        if (data.originalEvent.data.history && Array.isArray(data.originalEvent.data.history)) {
            $("#history").show()
            $("#history-list-container").html("")
            let previous_data = undefined
            let newHistoryItem = $("#history-item").html()

            for (let {payload, type} of data.originalEvent.data.history) {
                let {creator, create_time} = payload

                let localized_create_time = moment.utc(create_time).local().format();
                let local_create_time = new Date(localized_create_time).toLocaleString()

                if (type != "queueEntriesOrderUpdated" && previous_data) {
                    let historyDetail = ""
                    let old_values = objectDiff(previous_data, payload, [previous_data.correlation_id, previous_data.create_time, previous_data.entry_id])
                    let new_values = objectDiff(payload, previous_data, [payload.correlation_id, payload.create_time, payload.entry_id])
                    if (old_values.entry_data == undefined) {
                        old_values.entry_data = {}
                    }
                    if (new_values.entry_data == undefined) {
                        new_values.entry_data = {}
                    }
                    let intersect_keys = array_merge_distinct(Object.keys(old_values.entry_data), Object.keys(new_values.entry_data))
                    for (let key of intersect_keys) {
                        if (key == "order") {
                            continue
                        }
                        let key_translated = new pbI18n().translate(key)
                        let old_value = old_values.entry_data[key] ? old_values.entry_data[key] : ""
                        let new_value = new_values.entry_data[key] ? new_values.entry_data[key] : false
                        if (new_value !== false && old_value != new_value) {
                            historyDetail += `<code><xmp> ${key_translated} : --> ${JSON.stringify(new_value)} </xmp></code>`
                        }
                    }
                    var details = {
                        'history-creator': creator,
                        'history-time': local_create_time,
                        'history-type': new pbI18n().translate(type),
                        'history-detail': historyDetail
                    }
                    previous_data = payload
                } else {
                    var details = {
                        'history-creator': creator,
                        'history-time': local_create_time,
                        'history-type': new pbI18n().translate(type),
                        'history-detail': ""
                    }
                    if (!previous_data) {
                        previous_data = payload
                    }
                }

                if (type == "queueEntriesOrderUpdated") {
                    let new_pos = payload.entries_order[Render.instance.tm.getActiveQueueEntryId()]
                    var details = {
                        'history-creator': creator,
                        'history-time': local_create_time,
                        'history-type': new pbI18n().translate(type),
                        'history-detail': new pbI18n().translate("<code>Neue Position in der Liste: " + (new_pos + 1) + "</code>"),
                    }
                }

                let historyItemContent = multiStringReplace(details, newHistoryItem)
                $("#history-list-container").prepend(historyItemContent)

                Fnon.Wait.Remove(500)

            }
        }
    }

    saveQueueEntry(callback=()=>"") {
        if ($('[data-group="info"]').prop('disabled') == false) {
            let mode = undefined
            // Save changed data    
            if (!Render.instance.tm.active_entry_data) {
                mode = "new"
                Render.instance.tm.active_entry_data = {}
            } else {
                mode = "update"
            }
            let queue_id = $('#select-list-name-dropdown option').filter(':selected').val()
            Render.instance.tm.setActiveQueueById(queue_id)
            Render.instance.tm.active_entry_data.queue_id = queue_id
            Render.instance.tm.active_entry_data.title = $('#task-title').val()
            Render.instance.tm.active_entry_data.tags = $('#tags').val()
            if (Render.instance.newToDos) {
                Render.instance.tm.active_entry_data.todos = Render.instance.newToDos
                Render.instance.newToDos = undefined
            }


            //clean up description input
            let descriptionHTML = Render.instance.task_description.container.firstChild.innerHTML
            if (descriptionHTML == "<p><br></p>") {
                Render.instance.clearQuillClipboard()
                Render.instance.tm.active_entry_data.description = ""
            } else {
                Render.instance.tm.active_entry_data.description = descriptionHTML
            }

            Render.instance.tm.active_entry_data.status = $('#select-task-status option').filter(':selected').val()

            if ($('#select-task-responsible option').filter(':selected').val() != 'default') {
                Render.instance.tm.active_entry_data.assigned_to = [$('#select-task-responsible option').filter(':selected').val()]
            } else {
                Render.instance.tm.active_entry_data.assigned_to = []
            }

            if ($('#select-task-resource option').filter(':selected').val() != 'default') {
                Render.instance.tm.active_entry_data.resource = [$('#select-task-resource option').filter(':selected').val()]
            } else {
                Render.instance.tm.active_entry_data.resource = ""
            }

            Render.instance.tm.active_entry_data.create_easy2schedule_event = $("#create_easy2schedule_entry_for_due_date")[0].checked

            if ($("#select-task-due-time-hour").val() != "" && $("#select-task-due-time-minute").val() != "") {
                Render.instance.tm.active_entry_data.due_date = $('#select-task-due-date').val()
                const date = new Date(Render.instance.tm.active_entry_data.due_date)
                const hours = Number($("#select-task-due-time-hour").val())
                const minutes = Number($("#select-task-due-time-minute").val())
                date.setHours(hours, minutes)
                Render.instance.tm.active_entry_data.due_date = date
            } else {
                Render.instance.tm.active_entry_data.due_date = $('#select-task-due-date').val()
                const date = new Date(Render.instance.tm.active_entry_data.due_date)
                // If no time is set, the default time is set to CONFIG.DEFAULT_TASK_DUEDATE_TIME_HOUR
                // Beware!!!
                date.setHours(CONFIG.DEFAULT_TASK_DUEDATE_TIME_HOUR, 0)
                Render.instance.tm.active_entry_data.due_date = date
            }
            /* Render.instance.tm.active_entry_data.due_time = $("#select-task-due-time-hour").val() + ":" + $("#select-task-due-time-minute").val() */

            Render.instance.tm.active_entry_links = []
            if ($('#select-task-contact option').filter(':selected').val() && $('#select-task-contact option').filter(':selected').val() != "default") {
                Render.instance.tm.active_entry_links.push({
                    "resolver": "contact-manager",
                    "type": "contact",
                    "id": $('#select-task-contact option').filter(':selected').val()
                })
            }
            if ($('#select-task-appointment option').filter(':selected').val() && $('#select-task-appointment option').filter(':selected').val() != "default") {
                Render.instance.tm.active_entry_links.push({
                    "resolver": "easy2schedule",
                    "type": "event",
                    "id": $('#select-task-appointment option').filter(':selected').val()
                })
            }


            if (mode == "new") {
                Fnon.Wait.Circle('', {
                    svgSize: {w: '50px', h: '50px'}, svgColor: '#3a22fa',
                })
                let order = Object.entries(Render.instance.tm.getQueueEntries()).length
                if (Render.instance.tm.active_entry_data.due_date == undefined && Render.instance.tm.active_entry_data.due_time != undefined) {
                    Fnon.Alert.Danger({
                        title: 'Setting a Deadline not possible',
                        message: 'Please set a date if you want to set a deadline',
                        callback: () => {
                        }
                    });
                } else {
                    Render.instance.tm.createNewQueueEntry(Render.instance.tm.active_entry_data.title, Render.instance.tm.active_entry_data.description, Render.instance.tm.active_entry_data.status, Render.instance.tm.active_entry_data.assigned_to, Render.instance.tm.active_entry_data.due_date, Render.instance.tm.active_entry_data.due_time, order, Render.instance.tm.active_entry_data.resource, Render.instance.tm.active_entry_links, [], Render.instance.tm.active_entry_data.create_easy2schedule_event, Render.instance.tm.active_entry_data.tags, Render.instance.tm.active_entry_data.todos)
                }

            }

            if (mode == "update") {
                Fnon.Wait.Circle('', {
                    svgSize: {w: '50px', h: '50px'}, svgColor: '#3a22fa',
                })
                Render.instance.tm.saveChangesOnQueueEntry().then(function() {
                    callback()
                })
            }

            Render.instance.toggleEditmode(false)
            Render.instance.renderTaskListViewWithCookies()

        } else {
            Render.instance.toggleEditmode(true)
            Render.instance.toggleActivityMenu(true, "history")

        }
    }

    renderURLfromCookie(reload = false) {
        if (getCookie(Render.instance.cookie_queue_id)) {
            let url = window.location.href
            //let shortURL =  window.location.protocol + "//" + window.location.host + "/task-manager/";
            let shortURL = new URL(location.pathname, location.href).href

            let list = getCookie(Render.instance.cookie_queue_id)
            let task = getCookie(Render.instance.cookie_task_id)

            if (list && task) {
                Render.instance.cookie_url = `?queue_id=${list}&task_id=${task}`
            } else if (list) {
                Render.instance.cookie_url = `?queue_id=${list}`
            }

            let newURL = shortURL + Render.instance.cookie_url

            if (url !== newURL) {

                if (reload == false) {
                    window.history.pushState(null, "Serviceblick Taskmanager", newURL);
                } else if (reload == true) {
                    window.location.assign(newURL)
                }
            }
        }
    }

    determineDueDate() {
        let due_date = $('#select-task-due-date').val()
        if ($("#select-task-due-time-hour").val() != "" && $("#select-task-due-time-minute").val() != "") {
            const date = new Date(due_date)
            const hours = Number($("#select-task-due-time-hour").val())
            const minutes = Number($("#select-task-due-time-minute").val())
            date.setHours(hours, minutes)
            due_date = date
        } else {
            const date = new Date(due_date)
            // If no time is set, the default time is set to CONFIG.DEFAULT_TASK_DUEDATE_TIME_HOUR
            // Beware!!!
            date.setHours(CONFIG.DEFAULT_TASK_DUEDATE_TIME_HOUR, 0)
            due_date = date
        }
        return due_date
    }

    setRelativeDueDateSelectOptions(select_selector) {
        const options = {
            "days": "d",
            "months": "m",
            "years": "y"
        }
        $(select_selector).html("")

        let translator = new pbI18n()
        for (let [key, value] of Object.entries(options)) {
            $("<option>").val(value).html(translator.translate(key)).appendTo(select_selector)
        }
    }

    stopAlarm() {
        if (Render.instance.alarm_interval) {
            clearInterval(Render.instance.alarm_interval)
        }
        Render.instance.alarm_interval = undefined
    }

    startAlarm() {
        if (!Render.instance.tm.active_entry_data && !Render.instance.alarm_interval) {
            let audio = new Audio('/task-manager/sounds/new_task_alarm.mp3');
            audio.play();
            Render.instance.alarm_interval = setInterval(() => {
                audio.play();
            }, 3000)
            return true
        }
        return false
    }

    initListeners() {
        document.addEventListener("pbTagsListChanged", function _(event) {
            if (!Render.instance.handledTaskIds.includes(event.data.args.task_id)) {
                Render.instance.handledTaskIds.push(event.data.args.task_id)
                Render.instance.renderTagList(event.data.args.tags)
            }
        })

        // checking for change in window to update search field placement
        $(window).resize(function () {
            if (window.innerWidth > 576) {
                $('#task-search').show()
                $('#button-toggle-search').attr('data-selector', 'off');
                $('#listViewHeader').css("padding-bottom", "20px")
            } else {
                $('#task-search').hide()
                $('#button-toggle-search').attr('data-selector', 'off');
                $('#listViewHeader').css("padding-bottom", "20px")
            }
        });


        $(document).on('click', '*[data-name=quickaction_inprogress]', function (e) {
            Render.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                Render.instance.tm.active_entry_data.status = "pending"
                Render.instance.tm.saveChangesOnQueueEntry()
            })

            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_done]', function (e) {
            Render.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                Render.instance.tm.active_entry_data.status = "closed"
                Render.instance.tm.saveChangesOnQueueEntry()
            })
            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_canceled]', function (e) {
            Render.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                Render.instance.tm.active_entry_data.status = "canceled"
                Render.instance.tm.saveChangesOnQueueEntry()
            })
            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_onhold]', function (e) {
            Render.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                Render.instance.tm.active_entry_data.status = "onhold"
                Render.instance.tm.saveChangesOnQueueEntry()
            })
            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_planned]', function (e) {
            Render.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                Render.instance.tm.active_entry_data.status = "planned"
                Render.instance.tm.saveChangesOnQueueEntry()
            })
            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_assigntome]', function (e) {
            Render.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                Render.instance.tm.active_entry_data.assigned_to = `user.${getCookie("username")}`
                Render.instance.tm.saveChangesOnQueueEntry()
            })
            e.preventDefault()
            e.stopPropagation()
        })


        $(document).on('click', '#button-edit-task, #btn-group-task-save', function (e) {
            Render.instance.saveQueueEntry(window.close)
        })

        $(document).on('click', '#button-cancel', function (e) {
            window.close()
        })

        $(document).on('click', '#select-save-as-template', function (e) {
            e.preventDefault()
            e.stopPropagation()

            Render.instance.setRelativeDueDateSelectOptions("#select_relative_due_date_unit")

            $('#relative_due_date_value').val(undefined)
            $('#select_relative_due_date_unit').val(undefined)

            if (isValidDate(Render.instance.determineDueDate())) {
                let due_date = Render.instance.determineDueDate()
                let diff = dateDiff(due_date)
                let value = 1
                let unit = "d"

                if (diff["days"] > 0 && diff["days"] % 365 == 0) {
                    value = diff["days"] / 365
                    unit = "y"
                } else if (diff["days"] >= 30) {
                    value = diff["days"] / 30
                    unit = "m"
                } else {
                    // Make sure to add one day to calculated days if the diff at least 1 seconds bigger than a day
                    value = diff["days"] + ((diff["diff_in_seconds"] - (diff["days"] * 24 * 3600 + 12 * 3600)) > 0 ? 1 : 0)
                    unit = "d"
                }

                $('#relative_due_date_value').val(Math.round(value))
                $('#select_relative_due_date_unit').val(unit)

                $('#relative_due_date_section').removeClass("hidden")
            } else {
                $('#relative_due_date_section').addClass("hidden")
            }

            $('#modal_save_template').modal('toggle');
            $('#template_nameError').hide()
        })

        $('#convert_due_date_to_relative_checkbox').click(function (e) {
            if ($('#convert_due_date_to_relative_checkbox').is(':checked')) {
                $('#relative_due_date_form_wrapper').removeClass("hidden")
            } else {
                $('#relative_due_date_form_wrapper').addClass("hidden")
            }
        })

        $('#button_save_template').click(function (e) {
            e.preventDefault()
            let queue_id = $('#select-list-name-dropdown option').filter(':selected').val()
            Render.instance.tm.setActiveQueueById(queue_id)
            const closeModalandResetView = () => {
                Render.instance.toggleEditmode(false)
                Render.instance.renderTaskListViewWithCookies()

                $('#template_name').val("")
                $('#modal_save_template').modal('toggle')

            }
            let title = $('#task-title').val()
            let name = $('#template_name').val()
            let description = ""
            let descriptionHTML = Render.instance.task_description.container.firstChild.innerHTML
            if (descriptionHTML == "<p><br></p>") {
                Render.instance.clearQuillClipboard()
                description = ""
            } else {
                description = descriptionHTML
            }

            let resource = $('#select-task-resource option').filter(':selected').val()
            let assigned_to = $('#select-task-responsible').val() ? $('#select-task-responsible').val() : []

            let due_date = Render.instance.determineDueDate()

            if ($("#convert_due_date_to_relative_checkbox")[0].checked) {
                let fixed_time = `${parseInt(("0" + due_date.getHours()).slice(-2)) + new Date().getTimezoneOffset() / 60}:${("0" + due_date.getMinutes()).slice(-2)}`
                let due_date_diff = {"i": 0, "h": 0, "d": 0, "w": 0, "m": 0, "y": 0, "fixed_time": fixed_time}
                due_date_diff[$("#select_relative_due_date_unit").val()] = parseInt($("#relative_due_date_value").val())
                due_date = due_date_diff
            }

            let status = $('#select-task-status option').filter(':selected').val()
            let links = []
            if ($('#select-task-contact option').filter(':selected').val() && $('#select-task-contact option').filter(':selected').val() != "default") {
                links.push({
                    "resolver": "contact-manager",
                    "type": "contact",
                    "id": $('#select-task-contact option').filter(':selected').val()
                })
            }
            if ($('#select-task-appointment option').filter(':selected').val() && $('#select-task-appointment option').filter(':selected').val() != "default") {
                links.push({
                    "resolver": "easy2schedule",
                    "type": "event",
                    "id": $('#select-task-appointment option').filter(':selected').val()
                })
            }
            let data_owners = []

            if ($('#template_name').val() != "") {
                let create_easy2schedule_event = $("#create_easy2schedule_entry_for_due_date")[0].checked
                Render.instance.tm.createNewTaskTemplate(Render.instance.tm.active_queue_list_id, title, description, status, assigned_to, due_date, resource, name, links, data_owners, create_easy2schedule_event)
                if ($('#request_make_task').is(':checked')) {
                    Render.instance.saveQueueEntry()
                }
                closeModalandResetView()
            } else {
                $('#template_nameError').show()
            }
        })

        $(document).on('click', '#button-cancel', function (e) {
            e.preventDefault()

            if ($('[data-group="info"]').prop('disabled') === false) {

                let cancelTitle = new pbI18n().translate("Your changes are not saved yet")
                let cancelQuestion = new pbI18n().translate("Möchten Sie wirklich die Detailansicht verlassen? Alle Änderungen gehen verloren.")

                Fnon.Ask.Warning(cancelQuestion, cancelTitle, new pbI18n().translate("Yes, leave"), new pbI18n().translate("Cancel"), (result) => {
                    if (result == true) {
                        Render.instance.renderTaskListViewWithCookies()
                        Render.instance.clearQuillClipboard()
                        Render.instance.toggleActivityMenu(false)
                    }

                })
            } else {
                Render.instance.renderTaskListViewWithCookies()
                Render.instance.clearQuillClipboard()
                Render.instance.toggleActivityMenu(false)
            }


        })

        $(document).on("change", "#select-task-due-date", () => {
            if ($("#select-task-due-date").val() == "") {
                $("#select-task-due-time").addClass('hidden')
            } else {
                $("#select-task-due-time").removeClass('hidden')
            }
        })
        $(document).on("change", "#select-list-name-dropdown", () => {
            let queue_id = $("#select-list-name-dropdown").val()
            Render.instance.tm.fetchTaskTemplates(queue_id).then(function () {
                Render.instance.renderTaskTemplateDrowdown('detail-view-template-dropdown')
                Render.instance.renderTaskTemplateDrowdown('list-view-template-dropdown')
            })


            $("#select-task-template").show()

        })

        $(document).on("renderQueueList", () => Render.instance.renderQueueList())

        $(document).on("renderQueueEntries", () => Render.instance.renderQueueEntries())


        $(document).on('change', '#task-list-filter-responsible', function (e) {
            Render.instance.renderQueueEntries()
            Render.instance.renderActiveQueueData()

        })

        $(document).on('change', '#task-list-filter-status', function (e) {
            Render.instance.renderQueueEntries()
            Render.instance.renderActiveQueueData()
        })


        $('.clickable_div').click(function (e) {
            //make disabled contact select actionalble
            if (Render.instance.tm.active_entry_contact != undefined) {
                let contact_url = "/contact-manager/?ci=" + Render.instance.tm.active_entry_contact
                window.open(contact_url);
            } else {
                e.preventDefault()
            }
        })

        Render.instance.tm.socket.commandhandlers["refreshQueues"] = function (args) {
            Fnon.Alert.Danger({
                title: new pbI18n().translate('Tasklists has been updated'),
                message: new pbI18n().translate('Please reload this page as soon as possible to prevent data-loss'),
                callback: () => {
                }
            });
        }

        Render.instance.tm.socket.commandhandlers["refreshQueueEntries"] = function (args) {
            Render.instance.tm.fetchQueueEntries(args.queue_id).then(function () {
                Render.instance.renderQueueEntries()
                Fnon.Hint.Success(new pbI18n().translate("Data has been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
            })
        }

        Render.instance.tm.socket.commandhandlers["newQueueEntryCreated"] = function (args) {
            if (args.data.tags.includes("System:Alarm")) {
                Render.instance.stopAlarm()
                if (Render.instance.startAlarm()) {
                    Fnon.Alert.Success('Eine neue Aufgabe mit dem Titel<br/><blockquote class="task_title">' + args.data.title + '</blockquote ><br/> ist ins System gekommen.<br/> <a href="#" data-alarmopen="' + args.queue_id + ':' + args.queue_entry_id + '">Aufgabe in neuem Fenster öffnen</a><br/><a href="#" data-alarmStop="true">Alarm beenden</a>', "Eine neue Aufgabe ist eingegangen", "Ok", function (params) {
                        Render.instance.stopAlarm()
                    });
                }
            }
        }

        Render.instance.tm.socket.commandhandlers["refreshTemplates"] = function (args) {
            Render.instance.tm.fetchTaskTemplates(args.queue_id).then(function () {
                Render.instance.renderTaskTemplateDrowdown('detail-view-template-dropdown')
                Render.instance.renderTaskTemplateDrowdown('list-view-template-dropdown')
                Fnon.Hint.Success(new pbI18n().translate("Templates have been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
            })

        }

        Render.instance.tm.socket.commandhandlers["refreshTaskManagerResources"] = function (args) {
            Render.instance.tm.fetchTaskManagerResources().then(function () {
                Fnon.Hint.Success(new pbI18n().translate("Resources have been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
            })

        }

        Render.instance.tm.socket.commandhandlers["newComment"] = function (args) {
            let task_id = args.data.target_id.split("|").pop()
            if (task_id == Render.instance.tm.getActiveQueueEntryId()) {
                TaskManager.instance.fetchCommentsByTaskId(task_id).then(() => {
                    Render.instance.showQueueEntryComments(TaskManager.instance.getComments())
                })
            }
        }

        Render.instance.tm.socket.commandhandlers["deletedComment"] = function (args) {
            let task_id = args.target_id.split("|").pop()
            if (task_id == Render.instance.tm.getActiveQueueEntryId()) {
                TaskManager.instance.fetchCommentsByTaskId(TaskManager.instance.getActiveQueueEntryId()).then(() => {
                    Render.instance.showQueueEntryComments(TaskManager.instance.getComments())
                })
            }
        }

        $('#button-toggle-filter, #task-filter-close').click(function (e) {
            if ($('#button-toggle-filter').attr('data-selector') === 'off') {
                $('#task-list-filter').removeClass('hidden')
                $('#button-toggle-filter').attr('data-selector', 'on');
            } else {
                $('#task-list-filter').addClass('hidden')
                $('#button-toggle-filter').attr('data-selector', 'off');
            }
        })

        $('#button-toggle-search').click(function (e) {
            if ($('#button-toggle-search').attr('data-selector') === 'off') {
                $('#task-search').show()
                $('#listViewHeader').css("padding-bottom", "60px")
                $('#button-toggle-search').attr('data-selector', 'on');
            } else {
                $('#task-search').hide()
                $('#button-toggle-search').attr('data-selector', 'off');
                $('#listViewHeader').css("padding-bottom", "20px")
            }
        })

        $("#task-search").focusin(function () {
            $("#search-hint").show()
        });

        $("#task-search").focusout(function () {
            $("#search-hint").hide()
        });

        $("#no_filter").on("click", function () {
            $("#task-search").focus()
            Render.instance.renderTaskListView()
        })
        $("#no_filter", "#filter-checkbox").on("mousedown", function (event) {
            event.preventDefault()
        })


        $("#search_input").keyup(function (e) {
            e.preventDefault()
            Render.instance.renderTaskListView()
            Render.instance.renderQueueEntries()
        })

        $(document).on('click', '[data-alarmStop]', function () {
            Render.instance.stopAlarm()
            $('[data-alarmStop]').remove()
        });
        $(document).on('click', '[data-alarmopen]', function (e) {
            e.preventDefault()
            e.stopPropagation()
            Render.instance.stopAlarm()
            let data = $('[data-alarmopen]').data('alarmopen')
            let queue_id = data.split(":")[0]
            let queue_entry_id = data.split(":")[1]
            window.open('/task-manager/?queue_id=' + queue_id + '&task_id=' + queue_entry_id)
        })
    }
}
