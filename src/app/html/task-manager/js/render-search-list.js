import {TaskManager} from "./task-manager.js?v=[cs_version]"
import {
    getCookie, getQueryParam
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"


export class RenderSearchList {
    constructor() {
        if (!RenderSearchList.instance) {
            RenderSearchList.instance = this
            RenderSearchList.instance.tm = new TaskManager()
            RenderSearchList.instance.notifications = new PbNotifications()
            RenderSearchList.instance.initListeners()
        }

        return RenderSearchList.instance
    }

    initial() {
        if(getQueryParam("search")) {
            let search = getQueryParam("search")
            $("#search_value").val(search)
            RenderSearchList.instance.search()
        }
    }

    search() {
        $("#enter_search_term_hint").hide()
        $("#enter_search_term_hint").hide()
        let url = CONFIG.API_BASE_URL + "/tasks/search"
        let search_values = $("#search_value").val().split(" ")

        let filters = []
        for(let filter_value of search_values) {
            let complex_filter = filter_value.split(":", 2)
            if(complex_filter.length > 1) {
                if(complex_filter[0] == "links") {
                    filters.push({"field": "links", "value": complex_filter[1]})
                } else {
                    filters.push({"field": "data." + complex_filter[0], "value": complex_filter[1]})
                }
            }
            else {
                filters.push({"field": "data", "value": filter_value})
            }
        }


        const settings = {
            "url": url,
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey"),
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "filters": filters
            }),
        };

        RenderSearchList.instance.notifications.blockForLoading("#contentwrapper")
        $.ajax(settings).done(function (response) {
            RenderSearchList.instance.current_tasks = response.tasks
            PbTpl.instance.renderInto('task-manager/templates/tasklist-entries.tpl', {"entries": response.tasks}, '#task-list-container', "overwrite")
            RenderSearchList.instance.notifications.unblock("#contentwrapper")
        })
    }

    initListeners() {
        RenderSearchList.instance.tm.socket.commandhandlers["refreshQueueEntries"] = function (args) {
            RenderSearchList.instance.search()
        }

        $("#search_button").click(function () {
            RenderSearchList.instance.search()
        })

        $('#search_value').keyup(function (e) {
            if (e.keyCode == 13) {
                RenderSearchList.instance.search()
            }
        })

        $(document).on('click', '*[data-name=quickaction_inprogress_search]', function (e) {
            //RenderSearchList.instance.notifications.blockForLoading("#contentwrapper")
            for (let task of RenderSearchList.instance.current_tasks) {
                if (task.entry_id == e.currentTarget.dataset.id) {
                    RenderSearchList.instance.tm.init(task.entry_data.queue_id).then(function () {
                        RenderSearchList.instance.tm.setActiveQueueById(task.entry_data.queue_id)
                        RenderSearchList.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                            RenderSearchList.instance.tm.active_entry_data.status = "pending"
                            RenderSearchList.instance.tm.saveChangesOnQueueEntry().then(function () {
                                //RenderSearchList.instance.notifications.unblockAll()
                            })
                        })
                    })
                }
            }


            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_done_search]', function (e) {
            //RenderSearchList.instance.notifications.blockForLoading("#contentwrapper")
            for (let task of RenderSearchList.instance.current_tasks) {
                if (task.entry_id == e.currentTarget.dataset.id) {
                    RenderSearchList.instance.tm.init(task.entry_data.queue_id).then(function () {
                        RenderSearchList.instance.tm.setActiveQueueById(task.entry_data.queue_id)
                        RenderSearchList.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                            RenderSearchList.instance.tm.active_entry_data.status = "closed"
                            RenderSearchList.instance.tm.saveChangesOnQueueEntry().then(function () {
                                //RenderSearchList.instance.notifications.unblockAll()
                            })
                        })
                    })
                }
            }
            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_canceled_search]', function (e) {
            //RenderSearchList.instance.notifications.blockForLoading("#contentwrapper")
            for (let task of RenderSearchList.instance.current_tasks) {
                if (task.entry_id == e.currentTarget.dataset.id) {
                    RenderSearchList.instance.tm.init(task.entry_data.queue_id).then(function () {
                        RenderSearchList.instance.tm.setActiveQueueById(task.entry_data.queue_id)
                        RenderSearchList.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                            RenderSearchList.instance.tm.active_entry_data.status = "canceled"
                            RenderSearchList.instance.tm.saveChangesOnQueueEntry().then(function () {
                                //RenderSearchList.instance.notifications.unblockAll()
                            })
                        })
                    })
                }
            }
            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_assigntome_search]', function (e) {
            //RenderSearchList.instance.notifications.blockForLoading("#contentwrapper")
            for (let task of RenderSearchList.instance.current_tasks) {
                if (task.entry_id == e.currentTarget.dataset.id) {
                    RenderSearchList.instance.tm.init(task.entry_data.queue_id).then(function () {
                        RenderSearchList.instance.tm.setActiveQueueById(task.entry_data.queue_id)
                        RenderSearchList.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                            RenderSearchList.instance.tm.active_entry_data.assigned_to = `user.${getCookie("username")}`
                            RenderSearchList.instance.tm.saveChangesOnQueueEntry().then(function () {
                                //RenderSearchList.instance.notifications.unblockAll()
                            })
                        })
                    })
                }
            }

            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_planned_search]', function (e) {
            //RenderSearchList.instance.notifications.blockForLoading("#contentwrapper")
            for (let task of RenderSearchList.instance.current_tasks) {
                if (task.entry_id == e.currentTarget.dataset.id) {
                    RenderSearchList.instance.tm.init(task.entry_data.queue_id).then(function () {
                        RenderSearchList.instance.tm.setActiveQueueById(task.entry_data.queue_id)
                        RenderSearchList.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                            RenderSearchList.instance.tm.active_entry_data.status = "planned"
                            RenderSearchList.instance.tm.saveChangesOnQueueEntry().then(function () {
                                //RenderSearchList.instance.notifications.unblockAll()
                            })
                        })
                    })
                }
            }

            e.preventDefault()
            e.stopPropagation()
        })

        $(document).on('click', '*[data-name=quickaction_pending_search]', function (e) {
            //RenderSearchList.instance.notifications.blockForLoading("#contentwrapper")
            for (let task of RenderSearchList.instance.current_tasks) {
                if (task.entry_id == e.currentTarget.dataset.id) {
                    RenderSearchList.instance.tm.init(task.entry_data.queue_id).then(function () {
                        RenderSearchList.instance.tm.setActiveQueueById(task.entry_data.queue_id)
                        RenderSearchList.instance.tm.setActiveQueueEntryById(e.currentTarget.dataset.id).then(() => {
                            RenderSearchList.instance.tm.active_entry_data.status = "onhold"
                            RenderSearchList.instance.tm.saveChangesOnQueueEntry().then(function () {
                                //RenderSearchList.instance.notifications.unblockAll()
                            })
                        })
                    })
                }
            }

            e.preventDefault()
            e.stopPropagation()
        })
    }
}

