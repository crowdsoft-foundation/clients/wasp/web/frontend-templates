import {TaskManager} from "./task-manager.js?v=[cs_version]"
import {Render} from "./render.js?v=[cs_version]"
import {getCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"


export class RenderResourceSettings {
  modal_mode ="new" 
  types = [] 
  resourceTable

    constructor() {
        if (!RenderResourceSettings.instance) {
            RenderResourceSettings.instance = this
            RenderResourceSettings.instance.tm = new TaskManager()
            RenderResourceSettings.instance.initListeners()
        }

        return RenderResourceSettings.instance
    }

    initial() {
        RenderResourceSettings.instance.tm.fetchTaskManagerResources().then(function () {
            RenderResourceSettings.instance.renderTable(RenderResourceSettings.instance.tm.resources)
            RenderResourceSettings.instance.fetchTypes()
        })
    }

    renderModal() {
      RenderResourceSettings.instance.setModaltoDefaultValues()
      RenderResourceSettings.instance.renderTypesDropdown()
      if (RenderResourceSettings.instance.modal_mode=="update") {
        const {
          type,
          data: { name, tags},
        } = RenderResourceSettings.instance.tm.getActiveResource()
        let tagString = Object.values(tags).toString().replaceAll(",", ", ")
        $('#resource_name').val(name);
        $('#resource_tag').val(tagString)
        $('#resource_type').val(type);
  
        
  
      }
      /* else if(RenderResourceSettings.instance.modal_mode=="new"){
      } */
    }

    renderTypesDropdown() {
      let typeDropdown = $('#resource_type')
      if (typeDropdown != null) {
          typeDropdown.innerHTML = ''
          let option = document.createElement('option')
          option.text = new pbI18n().translate("No resource type chosen")
          option.value = 'default' 
          typeDropdown.append(option)
          if(RenderResourceSettings.instance.types.length > 0){
              $('#resource_type').prop('disabled', false)
              for (let item of RenderResourceSettings.instance.types) {
                  let option = document.createElement('option')
                  option.text =  new pbI18n().translate(item)
                  option.value = item
                  typeDropdown.append(option);
              }
          }
          else {
              $('#resource_type').prop('disabled', true)
          }
          
      }
    }

    setModaltoDefaultValues() {
      $('#resource_name').val('');
      $('#resource_tag').val('');
      $('#resource_type').find('option[value]').remove();
    }

    renderTable(data) {
        if(RenderResourceSettings.instance.resourceTable) {
          RenderResourceSettings.instance.resourceTable.destroy()
        }
        RenderResourceSettings.instance.resourceTable =  $('#resource-table').DataTable({
            pageLength: 100,
            retrieve: true,
            data: data,
            columns: [
              { data: 'data.name' },
              {data: 'data.tags',
              render: function (data) {
                const tags = Object.values(data)
                let tags_pills_tmp = tags.map(tag => {
                  return  `<span class="badge badge-primary">${tag}</span>`
                })
                let tags_pills = tags_pills_tmp.toString().replaceAll(",", " ")
                return tags_pills
                }  
              },
              {
                data: 'type',
                render: function (ressource_type) {
                  return new pbI18n().translate(ressource_type)
                }
              },
              {
                data: 'resource_id',
                render: function (data) {
                  return `<div class="btn-icon-list"><a href="" data-toggle="modal" data-target="#modalResourceDetails"><button class="btn btn-indigo btn-icon mg-r-10" data-toggle="tooltip" data-original-title="bearbeiten" data-selector="btn-resource-edit" data-update-resource-id=${data}><i class="typcn typcn-edit mg-l-5"></i></button></a>
                            <button class="btn btn-outline-indigo btn-icon" data-toggle="tooltip" data-original-title="löschen" data-selector="btn-resource-delete" data-delete-resource-id="${data}"><i class="typcn typcn-trash"></i></button> </div>`;
                },
              },
            ],
            responsive: true,
            pagingType: 'first_last_numbers',
            lengthChange: false,
            language: {
              searchPlaceholder: new pbI18n().translate('Search...'),
              sSearch: '',
              lengthMenu: '_MENU_ items/page',
              loadingRecords: new pbI18n().translate('Loading resources...'),
              zeroRecords: new pbI18n().translate('No resources saved yet.'),
              info: new pbI18n().translate('_PAGE_ of _PAGES_'),
              infoEmpty: new pbI18n().translate('Showing 0 to 0 of 0 entries'),
              paginate: {
                'first': new pbI18n().translate('first'),
                'last': new pbI18n().translate('last'),
                'next': new pbI18n().translate('next'), 
                'previous': new pbI18n().translate('previous')
              }
            },

          });   
    }

    setTypes(value) {
        RenderResourceSettings.instance.types = value
    }

    getTypes() {
        return RenderResourceSettings.instance.types
    }

    fetchTypes() {
      return new Promise((resolve, reject) => {
          let types = ["room", "building", "others"]
          RenderResourceSettings.instance.setTypes(types)
          console.log("Types fetched successfully")
          resolve()
      })
    }

    saveChanges() {
      let name = $('#resource_name').val()
      let type = $('#resource_type').val()
      let tags_tmp = $('#resource_tag').val()
      let new_tags = tags_tmp.split(',')
      new_tags = new_tags.map(tag => tag.trim())
      let tags = Object.assign({}, new_tags)
      if(RenderResourceSettings.instance.modal_mode =="new") {
        RenderResourceSettings.instance.tm.createNewTaskManagerResource(name, tags, type)
      }
      else {
        let id = RenderResourceSettings.instance.tm.getActiveResource().resource_id
        RenderResourceSettings.instance.tm.updateTaskManagerResource(id, name, tags, type)
      }
      $('#modalResourceDetails').modal('hide')
    }
  

    initListeners() {


        RenderResourceSettings.instance.tm.socket.commandhandlers["refreshTaskManagerResources"] = function (args) {
            RenderResourceSettings.instance.tm.fetchTaskManagerResources().then(function () {
                RenderResourceSettings.instance.renderTable(RenderResourceSettings.instance.tm.resources)
                Fnon.Hint.Success(new pbI18n().translate("Resources have been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
            })
        }

        $(document).on('click', 'button[data-selector="btn-resource-delete"]', function () {
          let id = $(this).attr('data-delete-resource-id')
          let deleteTitle = new pbI18n().translate("Delete")
          let deleteQuestion = new pbI18n().translate("Are you sure you want to delete the resource?")
          Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
              if (result == true) {
                RenderResourceSettings.instance.tm.deleteTaskManagerResource(id)
                  Fnon.Hint.Success('Ressource gelöscht')
              }
          })
        })


        
    }               


}

 