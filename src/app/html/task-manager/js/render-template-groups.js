import {TaskManager} from "./task-manager.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie, calculateInterval,  interpretInterval} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {fadeIn} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"

export class RenderTemplateGroups {
    mode = "new"
    active_group_id = undefined
    active_queue_id = undefined
    template_state = []
    active_group_start_date = null
    active_group_scheduling_interval = null

    constructor() {
        if (!RenderTemplateGroups.instance) {
            RenderTemplateGroups.instance = this
            RenderTemplateGroups.instance.tm = new TaskManager()
            RenderTemplateGroups.instance.initListeners()
        }

        return RenderTemplateGroups.instance
    }

    initial() {
        let arr = []
        RenderTemplateGroups.instance.tm.fetchTaskManagerTemplateGroups().then(function () {
            RenderTemplateGroups.instance.renderTemplateGroupList()
        })

    }

    renderTemplateGroupList() {
        let shown_entries = 0
        $('#groups_element_list').html("")
        let list = $('#groups_element_list')
        let list_entry = $('#group_item').html()
        let group_list = RenderTemplateGroups.instance.tm.getTaskManagerTemplateGroups()
        group_list.forEach(element => {
            shown_entries++
            const new_entry = list_entry.replaceAll("[{group_id}]", element.group_id).replace("[{group-name-value}]", element.data.name)
            list.append(new_entry)
        })
        if (shown_entries == 0) {
            $("#template_element_list").html(new pbI18n().translate("Nothing to see here"))
        }

    }

    renderListView() {
        let translationTitle = new pbI18n().translate("#template-group-title-list")
        $("#page-title").html(translationTitle)
        let translationIntro = new pbI18n().translate("#template-group-intro-list")
        $("#page-intro").html(translationIntro)

        // render list items with data values

        $("#detail-view").hide()
        $("#list-view").show()
    }

    renderDetailView() {
        $('#group_nameError').hide()
        RenderTemplateGroups.instance.renderQueueList()
        $('#temp_edit_schedule').html(new pbI18n().translate("No scheduling"))
        if (RenderTemplateGroups.instance.active_group_id == undefined) {
            let translationTitle = new pbI18n().translate("#template-group-title-detail-new")
            $("#page-title").html(translationTitle)
            $('#group_name').val("")
            // render first list and its templates as default

        } else {
            let translationTitle = new pbI18n().translate("#template-group-title-detail-edit")
            $("#page-title").html(translationTitle)
            $('#group_name').val(RenderTemplateGroups.instance.tm.getActiveTemplateGroup()["data"]["name"])
            if(typeof RenderTemplateGroups.instance.active_group_scheduling_interval == "number" && RenderTemplateGroups.instance.active_group_scheduling_interval > 0){
                let intervalObject = interpretInterval(RenderTemplateGroups.instance.tm.getActiveTemplateGroup()['scheduling_interval'])
                let iunit = Object.keys(intervalObject)[0]
                let ivalue = Object.values(intervalObject)[0]
                if (RenderTemplateGroups.instance.active_group_start_date != null) {
                    iunit = new pbI18n().translate(iunit)
                    $('#temp_edit_schedule').html(`${RenderTemplateGroups.instance.active_group_start_date} Jede(n)/Alle ${ivalue} ${iunit}`)
                }

                console.log('unit plus value', ivalue, iunit)

            }
            else {

                if(RenderTemplateGroups.instance.active_group_start_date != null) {
                    let start_date = new Date(RenderTemplateGroups.instance.active_group_start_date)
                    let local_start_date =  moment.utc(start_date).format("YYYY-MM-DDTHH:mm")

                    $('#temp_edit_schedule').html(`Einmalig am ${local_start_date}`)
                } else {
                    $('#temp_edit_schedule').html(new pbI18n().translate("No scheduling"))
                }


            }
            // render first list and its templates as default
            // prefill view with active item data values
        }
        let translationIntro = new pbI18n().translate("#template-group-intro-detail")
        $("#page-intro").html(translationIntro)

        $("#detail-view").show()
        $("#list-view").hide()
    }

    renderQueueList() {
        $("#task-list-select").html("")
        let counter = 0
        for (let [key, value] of Object.entries(RenderTemplateGroups.instance.tm.queue_list)) {
            if (counter == 0) {
                RenderTemplateGroups.instance.setActiveQueueID(key)
                RenderTemplateGroups.instance.tm.fetchTaskTemplates(key).then(function () {
                    RenderTemplateGroups.instance.renderTemplateList()
                })
            }
            $('#task-list-select').append($('<option>', {
                value: key, text: value.queue_data.queue_name
            }))
            counter++
        }
    }

    renderTemplateList() {
        $('#check-list-template').html("")
        let list = $('#check-list-template')
        let list_entry = $('#template_item').html()
        for (let [key, value] of Object.entries(RenderTemplateGroups.instance.tm.task_templates)) {
            const new_entry = list_entry.replaceAll("[{template_id}]", key).replaceAll("[{template_name}]", value.entry_data.name)
            list.append(new_entry)
        }
        let index = (RenderTemplateGroups.instance.template_state.findIndex(element => element.queue_id == RenderTemplateGroups.instance.active_queue_id))
        if (index != -1) {
            let template_ids = RenderTemplateGroups.instance.template_state[index].template_ids.map(element => element.id)
            template_ids.forEach(element => {
                $(`#${element}`).prop("checked", true)
            })
        }
    }


    renderOverviewSection() {
        $('#overview_list').html("")
        let list = $('#overview_list')
        let heading = $('#overview_header').html()
        let entry = $('#overview_entry').html()
        RenderTemplateGroups.instance.template_state.forEach(element => {
            if(element.template_ids.length != 0){
                let queue_name = RenderTemplateGroups.instance.tm.queue_list[element.queue_id].queue_data.queue_name
            let new_heading = heading.replaceAll("[{queue_id}]", element.queue_id).replace("[{queue_name}]", queue_name)
            list.append(new_heading)
            element.template_ids.forEach(element => {
                let new_entry = entry.replaceAll("[{template_id}]", element.id).replace("[{template_name}]", element.name)
                list.append(new_entry)
            })
            }

        })
    }


    setTemplateState() {
        let template_ids_checkbox_values = $(".rule-check-box:checkbox:checked")
        let template_ids = []
        for (let [key, value] of Object.entries(template_ids_checkbox_values)) {
            if (value.id != undefined) {
                let template_object = {}
                template_object["id"] = value.id
                template_object["name"] = value.getAttribute("data-entry-name")
                template_ids.push(template_object)
            }
        }
        let template_object = {}
        template_object["queue_id"] = RenderTemplateGroups.instance.active_queue_id
        template_object["template_ids"] = template_ids
        if (RenderTemplateGroups.instance.template_state.some(element => element["queue_id"] == RenderTemplateGroups.instance.active_queue_id)) {
            const object_to_update = RenderTemplateGroups.instance.template_state.find(element => element["queue_id"] == RenderTemplateGroups.instance.active_queue_id)
            object_to_update["template_ids"] = template_ids
        } else {
            RenderTemplateGroups.instance.template_state.push(template_object)
        }
    }

    setActiveGroupStartDate(id) {
        if(id) {
            const active_group = RenderTemplateGroups.instance.tm.getActiveTemplateGroup()
            RenderTemplateGroups.instance.active_group_start_date= active_group["start_date"]
        }
        else {
            RenderTemplateGroups.instance.active_group_start_date = null
        }
    }

    getActiveGroupStartDate() {
        return RenderTemplateGroups.instance.active_group_start_date
    }

    setActiveQueueID(id) {
        RenderTemplateGroups.instance.active_queue_id = id
    }

    resetScheduleModal() {
        $('#start_date').val(null)
        $('#scheduling-interval-value').val(0)
        $('#scheduling-interval-unit option[value="default"]').prop('selected', true)
        $('#scheduling-interval-value').prop('disabled', true)
        $('#scheduling-interval-unit').prop('disabled', true)
        $('#no_end').prop('checked', true)
    }

    saveChanges() {
        let name = $('#group_name').val()

        if (name == "") {
            $('#group_nameError').show()
        }
        else if(RenderTemplateGroups.instance.template_state.length == 0){

            Fnon.Hint.Danger(new pbI18n().translate("The template group needs templates!"), {displayDuration: 2000})
        }
        else {
            let start_date =RenderTemplateGroups.instance.active_group_start_date != null ? RenderTemplateGroups.instance.active_group_start_date.slice(0, 10) + ' ' + RenderTemplateGroups.instance.active_group_start_date.slice(11) + ':00' : null
            if (RenderTemplateGroups.instance.active_group_id != undefined) {
                RenderTemplateGroups.instance.tm.updateTaskManagerTemplateGroup(RenderTemplateGroups.instance.active_group_id, name, start_date, RenderTemplateGroups.instance.template_state, RenderTemplateGroups.instance.active_group_scheduling_interval)
            } else {
                RenderTemplateGroups.instance.tm.createNewTaskManagerTemplateGroup(name, start_date, RenderTemplateGroups.instance.template_state, RenderTemplateGroups.instance.active_group_scheduling_interval)
            }
            RenderTemplateGroups.instance.renderListView()
        }

    }

    initListeners() {

        RenderTemplateGroups.instance.tm.socket.commandhandlers["refreshTaskManagerTemplateGroups"] = function (args) {
            RenderTemplateGroups.instance.tm.fetchTaskManagerTemplateGroups().then(function () {
                RenderTemplateGroups.instance.renderTemplateGroupList()
                Fnon.Hint.Success(new pbI18n().translate("Templates groups have been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
            })

        }

        $(document).on('click', 'button[data-selector="btn-group-delete"]', function () {
            let id = $(this).attr('data-delete-id')
            let deleteTitle = new pbI18n().translate("Delete template group")
            let deleteQuestion = new pbI18n().translate("Are you sure you want to delete the template group?")
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    RenderTemplateGroups.instance.tm.deleteTaskManagerTemplateGroup(id)
                    Fnon.Hint.Success('Template Group gelöscht')
                }
            })

        })

        $("#new_group_button").on("click", function () {
            RenderTemplateGroups.instance.mode = "new"
            RenderTemplateGroups.instance.active_group_id = undefined
            RenderTemplateGroups.instance.template_state = []
            RenderTemplateGroups.instance.active_group_scheduling_interval = null
            RenderTemplateGroups.instance.renderDetailView()
            RenderTemplateGroups.instance.renderOverviewSection()
            RenderTemplateGroups.instance.setActiveGroupStartDate()

        })

        $('#save_group_button').on("click", (e) => {
            e.preventDefault()
            e.stopPropagation()
            RenderTemplateGroups.instance.saveChanges()
            }
        )

        $("#cancel_group_button").on("click", function () {
            let deleteTitle = new pbI18n().translate("Cancel")
            let deleteQuestion = new pbI18n().translate("All your changes for the template group will be lost!")
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    RenderTemplateGroups.instance.renderListView()
                    RenderTemplateGroups.instance.active_group_scheduling_interval = null
                }
            })
        })

        $("#task-list-select").on("change", () => {
            let id = $("#task-list-select").val()
            RenderTemplateGroups.instance.setActiveQueueID(id)
            RenderTemplateGroups.instance.tm.fetchTaskTemplates(id).then(function () {
                RenderTemplateGroups.instance.renderTemplateList()
            })
        })

        $('#check-list-template').on("change", () => {
            RenderTemplateGroups.instance.setTemplateState()
            RenderTemplateGroups.instance.renderOverviewSection()
        })

        $(document).on('click', 'button[data-selector="btn-template-delete"]', e => {
            let id = e.currentTarget.getAttribute('data-delete-id')
            RenderTemplateGroups.instance.template_state.forEach(element => {
                element.template_ids = element.template_ids.filter(element => element.id != id)
            })
            RenderTemplateGroups.instance.renderOverviewSection()
            RenderTemplateGroups.instance.renderTemplateList()

        })

        $(document).on('click', 'button[data-selector = "btn-group-edit"]', e => {
            let id = e.currentTarget.getAttribute('data-edit-id')
            RenderTemplateGroups.instance.active_group_id = id
            RenderTemplateGroups.instance.tm.setActiveTemplateGroup(id)
            let new_state = RenderTemplateGroups.instance.tm.getActiveTemplateGroup()["data"]["templates"]
            RenderTemplateGroups.instance.active_group_start_date = RenderTemplateGroups.instance.tm.getActiveTemplateGroup()["start_date"]
            RenderTemplateGroups.instance.active_group_scheduling_interval = RenderTemplateGroups.instance.tm.getActiveTemplateGroup()["scheduling_interval"]
            RenderTemplateGroups.instance.template_state = new_state
            RenderTemplateGroups.instance.renderDetailView()
            RenderTemplateGroups.instance.renderOverviewSection()
        })

        $(document).on('click', '#edit_schedule_icon_button', e => {
            $("#modal-schedule").modal('show')
            RenderTemplateGroups.instance.resetScheduleModal()
            let iunit = "default"
            let ivalue = 0
            if(RenderTemplateGroups.instance.active_group_scheduling_interval == null || RenderTemplateGroups.instance.active_group_scheduling_interval == 0){
                iunit = "default"
                ivalue = 0
            }
            else if(RenderTemplateGroups.instance.active_group_scheduling_interval != null && RenderTemplateGroups.instance.active_group_scheduling_interval == 0){
                $('#scheduling-interval-unit').prop('disabled', false)
                $('#scheduling-interval-value').prop('disabled', false)
                let intervalObject = interpretInterval(RenderTemplateGroups.instance.active_group_scheduling_interval)
                iunit = Object.keys(intervalObject)[0]
                ivalue = Object.values(intervalObject)[0]
            }
            else if(RenderTemplateGroups.instance.tm.getActiveTemplateGroup() != undefined && RenderTemplateGroups.instance.tm.getActiveTemplateGroup()['scheduling_interval'] != null) {
                $('#scheduling-interval-unit').prop('disabled', false)
                $('#scheduling-interval-value').prop('disabled', false)
                let intervalObject = interpretInterval(RenderTemplateGroups.instance.tm.getActiveTemplateGroup()['scheduling_interval'])
                iunit = Object.keys(intervalObject)[0]
                ivalue = Object.values(intervalObject)[0]
            }

            $('#scheduling-interval-unit').val(iunit)
            $('#scheduling-interval-value').val(ivalue)

            let today = new Date()
            if(RenderTemplateGroups.instance.active_group_start_date == null){
                $('#no_end').prop('checked', true)
                $('#start_date').prop('disabled', true)
                const dateControl = document.querySelector('input[type="datetime-local"]')
                dateControl.value = moment.utc(today).local().format("YYYY-MM-DDTHH:mm")
                dateControl.setAttribute("min", dateControl.value)

            }
            else {
                $('#yes_end').prop('checked', true)
                $('#start_date').prop('disabled', false)
                $('#scheduling-interval-unit').prop('disabled', false)
                $('#scheduling-interval-value').prop('disabled', false)
                let start_date = new Date(RenderTemplateGroups.instance.active_group_start_date)
                const dateControl = document.querySelector('input[type="datetime-local"]')
                dateControl.value = moment(start_date).format("YYYY-MM-DDTHH:mm")
                dateControl.setAttribute("min", today.toISOString().slice(0, 16))
            }

        })

        $(document).on('click', '#save_schedule_button', e => {
            e.preventDefault()
            let start_date = null
            let interval_unit = $('#scheduling-interval-unit').val()
            let interval_value = $('#scheduling-interval-value').val()
            document.getElementById('modal-error-msg').innerHTML = ""

            if($('#yes_end').prop('checked')){

                let local_start_date = new Date(document.querySelector('input[type="datetime-local"]').value)
                start_date = moment.utc(local_start_date).format("YYYY-MM-DDTHH:mm")
                RenderTemplateGroups.instance.active_group_start_date = start_date

                if (isNaN(local_start_date)) {
                     document.getElementById('modal-error-msg').innerHTML = "<div class='alert alert-danger error_message' data-i18n='Please enter a valid date.'>Please enter a valid date.</div>"
                     fadeIn("modal-error-msg")
                }
                else if ( interval_value > 0 && interval_unit == "default" || interval_value == 0 && interval_unit != "default" ) {
                    document.getElementById('modal-error-msg').innerHTML = "<div class='alert alert-danger error_message' data-i18n='Please select an interval'>Please select an interval</div>"
                    fadeIn("modal-error-msg")
                } else {
                    RenderTemplateGroups.instance.active_group_scheduling_interval = calculateInterval(interval_value, interval_unit)
                    RenderTemplateGroups.instance.active_group_start_date = start_date
                    $("#modal-schedule").modal('hide')

                    if (interval_value != 0) {
                        interval_unit = new pbI18n().translate(interval_unit)
                        $('#temp_edit_schedule').html(`${start_date} Jede(n)/Alle  ${interval_value} ${interval_unit}`)
                    } else {
                        $('#temp_edit_schedule').html(`Einmalig am ${start_date}`)
                    }
                }
            }

            else {
                RenderTemplateGroups.instance.active_group_start_date = start_date
                $("#modal-schedule").modal('hide');
                $('#temp_edit_schedule').html( new pbI18n().translate("No scheduling"))
            }



        })

        $(document).on('click', 'cancel_schedule_button', e => {
            let deleteTitle = new pbI18n().translate("Cancel")
            let deleteQuestion = new pbI18n().translate("Do you want to cancel? All your changes will be lost.")
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    $("#modal-schedule").modal('hide')
                }
            })
        })

        $('#date-fieldset').on('change', () => {
            if(($('#no_end').prop('checked'))) {
                $('#start_date').prop('disabled', true)
                $('#scheduling-interval-unit option[value="default"]').prop('selected', true)
                $('#scheduling-interval-unit').prop('disabled', true)
                $("#scheduling-interval-value").val(0).prop('disabled', true)
            }
            else {
                $('#start_date').prop('disabled', false)
                $('#scheduling-interval-unit').prop('disabled', false)
                $("#scheduling-interval-value").prop('disabled', false)
            }

        })

        $(document).on('click', 'button[data-selector="btn-group-run"]', function (e) {


            let id = $(this).attr('data-run-id')

            let deleteTitle = new pbI18n().translate("Creating tasks from template_groups")
            let deleteQuestion = new pbI18n().translate("Are you sure you want to create tasks based on this template_group now?")
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result) {
                    let storeNewQueueEntryCreatedHandler = undefined
                    if (RenderTemplateGroups.instance.tm.socket.commandhandlers["newQueueEntryCreated"]) {
                        storeNewQueueEntryCreatedHandler = RenderTemplateGroups.instance.tm.socket.commandhandlers["newQueueEntryCreated"]
                    }
                    let storeRefreshQueueEntries = undefined
                    if (RenderTemplateGroups.instance.tm.socket.commandhandlers["refreshQueueEntries"]) {
                        storeNewQueueEntryCreatedHandler = RenderTemplateGroups.instance.tm.socket.commandhandlers["refreshQueueEntries"]
                        RenderTemplateGroups.instance.tm.socket.commandhandlers["refreshQueueEntries"] = ()=>""
                    }

                    RenderTemplateGroups.instance.tm.socket.commandhandlers["newQueueEntryCreated"] = function (args) {

                        Fnon.Hint.Success(new pbI18n().translate('Task "{title}" has been created').replace("{title}", args.data.title))
                    }

                    Fnon.Wait.Circle('', {
                        svgSize: {w: '50px', h: '50px'},
                        svgColor: '#3a22fa',
                    })

                    var settings = {
                        "url": CONFIG.API_BASE_URL + "/create_tasks_of_template_group/" + id,
                        "method": "GET",
                        "timeout": 0,
                        "headers": {
                            "apikey": getCookie("apikey")
                        },
                        "success": () => {
                            Fnon.Hint.Success(new pbI18n().translate("Aufgaben werden erstellt..."))
                            Fnon.Wait.Remove(500)
                            if(storeNewQueueEntryCreatedHandler) {
                                window.setTimeout(()=> {
                                    RenderTemplateGroups.instance.tm.socket.commandhandlers["newQueueEntryCreated"]=storeNewQueueEntryCreatedHandler
                                }, 3000)
                            }
                            if (storeRefreshQueueEntries) {
                                window.setTimeout(()=> {
                                    RenderTemplateGroups.instance.tm.socket.commandhandlers["refreshQueueEntries"] = storeRefreshQueueEntries
                                }, 3000)
                            }
                        },
                        "error": () => {
                            Fnon.Hint.Danger(new pbI18n().translate("Aufgaben konnten nicht erstellt werden"))
                            Fnon.Wait.Remove(500)
                            if(storeNewQueueEntryCreatedHandler) {
                                window.setTimeout(()=> {
                                    RenderTemplateGroups.instance.tm.socket.commandhandlers["newQueueEntryCreated"]=storeNewQueueEntryCreatedHandler
                                }, 3000)
                            }
                            if (storeRefreshQueueEntries) {
                                window.setTimeout(()=> {
                                    RenderTemplateGroups.instance.tm.socket.commandhandlers["refreshQueueEntries"] = storeRefreshQueueEntries
                                }, 3000)
                            }
                        }
                    };
                    $.ajax(settings)

                }
            })

        })

    }
}