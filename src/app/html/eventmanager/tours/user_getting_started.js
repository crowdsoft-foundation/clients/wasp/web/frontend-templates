window.tour_config = {
    header: "Touren im Veranstaltungsplaners",
    intro: "Schnellstarthilfe für jeden Bereich Ihres Veranstaltungsplaners.",
    tours: {
        firstSteps: {
            id: "firstSteps",
            title: "Erste Schritte im in Ihrem Veranstaltungsplaners",
            desc: "Sind Sie neu hier? Dies ist ein guter Ausgangspunkt.",
            startPage: "/eventmanager/index.html",
            startStep: 1,
            prevTour: null,
            nextTour: "manageEvent",
            permission: "eventmanager"
        },
        viewEvent: {
            id: "manageEvent",
            title: "Teilnehmer verwalten",
            desc: "Wie du Teilnehmer zu deiner Veranstaltung einladen und verwalte kannst",
            startPage: "/eventmanager/index.html",
            startStep: 11,
            prevTour: "firstSteps",
            nextTour: null,
            permission: "eventmanager"
        }
    },
    tourSteps: [
        {//1
            title: 'Willkommen',
            disableInteraction: true,
            intro: 'Willkommen im <strong>Veranstaltungsmanager</strong>.<br/> Diese Tour führt dich wie du Veranstaltungen erstellen, bearbeiten und verwalten kannst.<br/>Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.<br/>',
            'myBeforeChangeFunction': function (PbGuide) {
                $("#btn-cancel").click()
            },
        },
        {//2
            title: 'Neue Veranstaltung Anlegen',
            element: '#button-new-event',
            disableInteraction: false,
            intro: 'Beginnen wir damit, eine neue Veranstaltung zu erstellen. Klicken Sie auf die Schaltfläche "Neue Veranstaltung".',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()

                let object = document.querySelector('#button-new-event');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            },
        },
        {//3
            title: 'Titel',
            disableInteraction: false,
            element: '#event_title',
            intro: 'Der <b>Titel</b> ist Pflichtfeld für deine Veranstaltung.',
            'myBeforeChangeFunction': function (PbGuide) {

            },
        },
        {//4
            title: 'Beschreibung',
            disableInteraction: false,
            element: '#event_description',
            intro: 'Verwende diesen Rich-Text-Editor, um deine Veranstaltungsbeschreibung zu formatieren.',
            'myBeforeChangeFunction': function (PbGuide) {
                let object = document.querySelector('#event_title');
                if (object.value == "") {
                    object.value = "OFFENER TANZKREIS für Erwachene"
                }
            },
        },
        {//5
            title: 'Datum und die Uhrzeit auswählen',
            disableInteraction: false,
            element: '#event_date_range, .daterangepicker',
            intro: 'Der <b>Start- und Endpunkt</b> des Ereignisses wird mit der Datums-/Uhrzeitauswahl ausgewählt. Ein Doppelklick wählt ein einzelnes Tag aus.',
            'myBeforeChangeFunction': function (PbGuide) {
                let object = document.querySelector('.ql-editor');
                if (object.innerHTML == "<p><br></p>") {
                    $(".ql-editor").html(
                        "<h4>Tanzen Sie gerne? Treffen Sie gerne nette Leute? Und das alles in ungezwungener Atmosphäre?</h4><p>Dann ist unser Tanzkreis (Kreis- und Gruppentänze) genau das Richtige für Sie. Tanzen bringt den Körper und die Seele in Bewegung. Ob mit oder ohne Tanzpartner, mit viel oder wenig Tanzerfahrung, hier ist jede und jeder herzlich willkommen!</p><p><strong>Vorausetzung: </strong>Eine Schnupperstunde und der Einstieg in den Tanzkreis sind jederzeit möglich.</p><p><strong>Termine:</strong> immer freitags (außer in den Ferien)</p>"
                    )
                }
            },
        },
        {//6
            title: 'Veranstaltungsort eintragen',
            disableInteraction: false,
            element: '#event_location',
            intro: 'Gebe einen <b>Veranstaltungsort</b> ein',
            'myBeforeChangeFunction': function (PbGuide) {

            },
        },
         {//7
            title: 'Weitere Merkmale',
            disableInteraction: false,
            element: '#furtherDetails',
            position: 'top',
            intro: 'Setze weitere spezifische Informationen, wie z. B. die maximale Teilnehmerzahl und das Kategorisierungs-Tagging, welches kann dir bei der Verwaltung deiner Veranstaltung helfen.',
            'myBeforeChangeFunction': function (PbGuide) {
                let object = document.querySelector('#event_location');
                if (object.value == "") {
                    object.value = "Poststraße 81, Deutschland - Aktionsraum (1. OG)"
                }
            },
        },
        {//8
            title: 'Veranstalter setzen',
            disableInteraction: false,
            element: '#specificSettings',
            intro: 'Hier tragst du die Kontaktdaten eines oder mehrerer <b>Veranstalter</b> ein.',
            'myBeforeChangeFunction': function (PbGuide) {
               
            },
        },
        {//9
            title: 'Änderungen speichern',
            disableInteraction: false,
            element: '#btn-save-event',
            intro: 'Klicken Sie auf die Schaltfläche "Speichern", um Ihre Veranstaltung zu speichern.',
            'myBeforeChangeFunction': function (PbGuide) {

                
                let object1 = document.querySelector('#organizer_0');
                let child = object1.querySelectorAll('input');
                if (child[0].value == "") {
                    child[0].value = "Anne Musterfrau"
                }
                if (child[1].value == "") {
                    child[1].value = "anne.musterfrau@mail.de"
                }

                PbGuide.hideNextButton()
                let object = document.querySelector('#btn-save-event');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            },
        },
        {//10
            title: 'Event angelegt',
            disableInteraction: true,
            element: '#event-list',
            intro: 'Sobald das System die Daten erfolgreich gespeichert hat, wird die Liste aktualisiert und deine neue Veranstaltung sollten sichtbar sein. Von den Schnellaktionsmenü kannst du es jetzt <b>bearbeiten oder löschen</b>. um die Freigabeinformationen anzuzeigen und die Details zu verwalten.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.setPrevButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })
                PbGuide.setNextButton("Weiter")
                PbGuide.markTourSeen("eventmanager.firstSteps")   
            },
        },
        
        {//11
            title: 'Veranstaltung verwalten',
            disableInteraction: true,
            // element: '#btn-share-event',
            intro: 'In den Veranstaltungsdetails kannst du auf die Informationen zugreifen, um deine Veranstaltung zu teilen und die Teilnehmer zu verwalten.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hidePreviousButton()
                let object = document.querySelector("#event-list-container .event-element");
                if (!object) { 
                    PbGuide.updateIntro("Bevor wir dir zeigen können, wie du deine Veranstaltungsteilnehmer verwalten kannst, musst du zunächst eine Veranstaltung erstellen.")
                    PbGuide.setNextButton("Weiter", function () {
                        PbGuide.introguide.goToStepNumber(2)
                })
                    } else {
                        $("#event-list-container .event-element").eq(0).click()
                    }
               
            },
        },
        {//12
            title: 'Veranstaltung Teilen ',
            disableInteraction: false,
            element: '#btn-share-event',
            intro: 'Klicke jetzt bitte den <strong>Einladung Teilen</strong>-Button den popup mit den QR-code und Teilen-Link zu offnen.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hideNextButton()

                let object = document.querySelector('#btn-share-event');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
            },
        },
        {//13
            title: 'QR-Code oder Link',
            disableInteraction: true,
            element: '#share_event_modal',
            position: 'top',
            intro: 'Du kannst den QR-Code ausdrucken, damit die Teilnehmer ihn scannen können, um sich zu registrieren, oder den Link in einer Einladungs-E-Mail verwenden.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hidePreviousButton()
         
            },
        },

        {//14
            title: 'Teilnehmer hinzufügen',
            disableInteraction: false,
            element: '#btn-add-participant',
            intro: 'Sollte ein Teilnehmer nicht in der Lage sein, sich selbst für die Veranstaltung anzumelden, kannst du Teilnehmer manuell hinzufügen, indem du auf "Teilnehmer hinzufügen" klickst.',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hidePreviousButton()
                PbGuide.hideNextButton()
                $('#share_event_modal').modal('toggle'); 

                let object = document.querySelector('#btn-add-participant');
                if (object) {
                    object.addEventListener('click', e => {
                            PbGuide.introguide.nextStep()
                        },
                        {once: true}
                    )
                }
               

            },
        },
        {//15
            title: 'Kontaktdaten der Teilnehmer',
            disableInteraction: false,
            intro: 'Der vollständige Name und die E-Mail-Adresse des Teilnehmers sind Pflichtfelder, auch wenn du einen gespeicherten Eintrag aus deinen Kontakten auswählst. ',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hidePreviousButton()
            },
        },
         {//16
            title: 'Teilnehmer speichern',
            disableInteraction: false,
            element: '#btn-confirm-participant',
            position: 'left',
            intro: 'Klick "Speichern", um den Teilnehmer zur Liste hinzuzufügen. ',
            'myBeforeChangeFunction': function (PbGuide) {
                PbGuide.hidePreviousButton()

                let object = document.querySelector('#participant_first_name_value');
                let object1 = document.querySelector('#participant_last_name_value');
                let object2 = document.querySelector('#participant_email_value');
                if (object) {
                    object.value = "Musterman"
                    object1.value = "Hans"
                    object2.value = "hans.musterman@mail.de"
                }


            },
        },
        {//17
           title: 'Teilnehmerliste',
           disableInteraction: false,
           element: '#participant-list',
           intro: 'Sobald das System die Daten erfolgreich gespeichert hat, wird die Teilnehmerliste aktualisiert und deine neue Teilnehmer sollten sichtbar sein. Dort kannst du den Teilnehmern weitere Detailinformationen hinzufügen.',
           'myBeforeChangeFunction': function (PbGuide) {
                $("#btn-confirm-participant").click()

                PbGuide.markTourSeen("eventmanager.manageEvent")
                PbGuide.hidePreviousButton()

                PbGuide.setNextButton("Beenden", function () {
                    PbGuide.introguide.exit()
                })
           },
       }

    ]
}