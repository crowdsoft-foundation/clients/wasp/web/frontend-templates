import {Eventmanager} from "./eventmanager.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {
    multiStringReplace,
    getCookie,
    array_merge_distinct,
    getQueryParam
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {Event} from "./event.js?v=[cs_version]";
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {handleGUIbyidError} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {PbSelect2Tags} from "../../csf-lib/pb-select2-tags.js?v=[cs_version]"
import {PbSelect2Contacts} from "../../csf-lib/pb-select2-contacts.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTag} from "../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"

export class RenderEventmanager {
    detailviewstate = "readonly";
    event_description = undefined
    total_num_participants = undefined

    constructor() {
        if (!RenderEventmanager.instance) {
            RenderEventmanager.instance = this;
            RenderEventmanager.instance.all_list_tags = []
            RenderEventmanager.instance.last_task_ids = []
            let show_outdate_events = getQueryParam("show_outdated") == "true" ? true : false
            RenderEventmanager.instance.em = new Eventmanager(show_outdate_events);
            RenderEventmanager.instance.acc = new PbAccount()
            RenderEventmanager.instance.i18n = new pbI18n();
            RenderEventmanager.instance.participant_select2 = {}
            RenderEventmanager.instance.notifications = new PbNotifications()
            RenderEventmanager.instance.tagListModel = new PbTagsList()
            RenderEventmanager.instance.handledTaskIds = []
            RenderEventmanager.instance.pressedKeys = ""


            document.addEventListener("pbTagsListChanged", function _(event) {
                if (!RenderEventmanager.instance.handledTaskIds.includes(event.data.args.task_id)) {
                    RenderEventmanager.instance.handledTaskIds.push(event.data.args.task_id)
                    RenderEventmanager.instance.renderTagList(event.data.args.tags)
                }
            })
            if (show_outdate_events) {
                $("#button_event_archive").html(RenderEventmanager.instance.i18n.translate("Current"))
                $("#button_event_archive").prop('checked', true)
                $("#button_event_archive").addClass("btn-purple")
            }


            if ($('#event_description').length) {

                var toolbarOptions = [
                    [{'header': [1, 2, 3, 4, 5, 6, false]}],
                    ['bold', 'italic', 'underline'],
                    [{'align': ''}, {'align': 'center'}, {'align': 'right'}],
                    [{'list': 'ordered'}, {'list': 'bullet'}],
                    ['link'],
                    ['clean']
                ]

                RenderEventmanager.instance.event_description = new Quill('#event_description', {
                    modules: {
                        toolbar: toolbarOptions
                    },
                    placeholder: 'Beschreibung...',
                    theme: 'bubble',
                    readOnly: true
                })

            }

            document.addEventListener('keydown', function (event) {
                const key = event.key; // "a", "1", "Shift", etc.
                if (["Control", "AltGraph"].includes(key))
                    return
                if (key == "Enter") {
                    console.log("RECEIVED DATA FROM SCANNER")
                    let [signupId, event_id] = RenderEventmanager.instance.pressedKeys.split("|")
                    // RenderEventmanager.instance.em.setActiveEvent(event_id).then(async function () {
                    //     await RenderEventmanager.instance.renderEventDetail()
                    //     if (event_id == RenderEventmanager.instance.em.active_event.event_id) {
                    //         $("#" + signupId).get(0).scrollIntoView({behavior: 'smooth'})
                    //         $("#" + signupId).css("background-color", "green")
                    //     }
                    // })

                    RenderEventmanager.instance.pressedKeys = ""
                } else {
                    RenderEventmanager.instance.pressedKeys += key
                }
            });

        }
        return RenderEventmanager.instance;
    }


    init() {
        RenderEventmanager.instance.initListeners();
        return RenderEventmanager.instance
    }


    renderTagList(selectedTags = []) {

        RenderEventmanager.instance.notifications.blockForLoading("#tag_container")
        let self = this
        let tags = array_merge_distinct(selectedTags || [], $("#tags").val() || [])
        return new Promise((resolve, reject) => {
            RenderEventmanager.instance.tagListModel.load(false, "eventmanager").then(() => {
                let entries = Object.entries(RenderEventmanager.instance.em.events)
                for (let entry of entries) {
                    entry = entry[1]
                    if (entry.data?.tags) {
                        for (let localTag of entry.data.tags) {
                            let myTag = new PbTag().fromShort(localTag)
                            RenderEventmanager.instance.tagListModel.addTagToList(myTag)
                        }
                    }
                }

                for (let tag of tags) {
                    if (!tag) {
                        continue
                    }
                    if (typeof tag == "string") {
                        tag = new PbTag().fromShort(tag).getData()
                    }

                    tag = new PbTag({"name": tag.id || tag.name, "value": tag.text || tag.value})
                    if (!selectedTags.includes(tag.short())) {
                        selectedTags.push(tag.short())
                    }
                    RenderEventmanager.instance.tagListModel.addTagToList(tag)
                }

                let tagOptions = RenderEventmanager.instance.tagListModel.getSelectOptions(selectedTags)

                PbTpl.instance.renderIntoAsync('csf-lib/pbapi/tagmanager/templates/tag-select2.tpl', {
                    "multiple": true,
                    "addButton": true,
                    "propertyPath": "tags",
                    "options": tagOptions
                }, "#tag_container").then(() => {
                    $('[data-propertyname="tags"]').select2({
                        tags: true,
                        tokenSeparators: [',', ' '],
                        selectOnClose: false
                    })

                    $("#addGlobalTagButton").click(() => {
                        PbNotifications.instance.ask(
                            '<div><input type="text" id="newGlobalTag" placeholder="" class="form-control"/></div>',
                            'New global tag',
                            () => {
                                RenderEventmanager.instance.tagListModel.addTagToList(new PbTag().fromShort($("#newGlobalTag").val()).setUsableFor(["eventmanager"]), true)
                            })
                        $("#newGlobalTag").focus()
                    })
                    if (RenderEventmanager.instance.editMode) {
                        $("#tags").attr("disabled", false)
                        $("#addGlobalTagButton").attr("disabled", false)
                    } else {
                        $("#tags").attr("disabled", true)
                        $("#addGlobalTagButton").attr("disabled", true)
                    }

                    RenderEventmanager.instance.notifications.unblock("#tag_container")
                    resolve()
                })
            })
        })
    }


    renderEventListView() {
        //build list veiw
        $("#tag_container").html("")
        $("#search_input").keyup(() => RenderEventmanager.instance.renderEventList())
        RenderEventmanager.instance.toggleDetailView(false)
        RenderEventmanager.instance.renderEventList()
        $("#button_event_archive").prop("disabled", false)
    }

    renderEventList() {
        //build list
        $("#event-list-container").html("")
        if (RenderEventmanager.instance.em.events.length != 0) {

            Object.entries(RenderEventmanager.instance.em.events).forEach((event) => {
                let id = event[0]
                let data = event[1]['data']
                if (data.tags) {
                    RenderEventmanager.instance.all_list_tags = Array.from(new Set([...RenderEventmanager.instance.all_list_tags, ...data.tags]))
                }

                RenderEventmanager.instance.renderEventListItem(id, data)
            });

        } else {
            let noEventItem = $("#no-active-events").html()
            $('#event-list-container').append(noEventItem);
        }
    }

    renderEventListItem(eventid, eventData) {
        //build list items
        if (Object.keys(eventData).length != 0) {
            let newEventItem = $("#event_item").html()
            let participants_booked = eventData.participants ? Object.values(eventData.participants).filter(item => item.status == 'booked').length : 0

            let tags = ""
            if (eventData.tags) {
                for (let tag of eventData.tags) {
                    tags += `<span class="badge badge-data-tags">${tag}</span>&nbsp;`
                }
            }


            var details = {
                'event-id': eventid,
                'event-title': eventData.name,
                'event-date': moment(eventData.startdatetime).format(CONFIG.DATETIMEWIHTOUTSECONDSFORMAT) + " - " + moment(eventData.enddatetime).format(CONFIG.DATETIMEWIHTOUTSECONDSFORMAT),
                'event-participant-booked': participants_booked,
                'event-participant-max': (eventData.max_participants != 0) ? `/ ${eventData.max_participants}` : "",
                'event-tags': tags,
            }
            let listItemContent = multiStringReplace(details, newEventItem)
            let sub_search_results = []
            if ($("#search_input").val()?.length > 0) {
                let search_values = $("#search_input").val().split(" ")
                for (let search_value of search_values) {
                    if (details["event-id"].toUpperCase().includes(search_value.toUpperCase()) ||
                        details["event-title"].toUpperCase().includes(search_value.toUpperCase()) ||
                        details["event-date"].toUpperCase().includes(search_value.toUpperCase()) ||
                        details["event-tags"].toUpperCase().includes(search_value.toUpperCase())
                    ) {
                        sub_search_results.push(true)
                    } else {
                        sub_search_results.push(false)
                    }
                }
                if (!sub_search_results.includes(false)) {
                    $('#event-list-container').append(listItemContent);
                }
            } else {
                $('#event-list-container').append(listItemContent);
            }
        }
    }

    resetEventDetail() {

        $("#event_title").val("")
        RenderEventmanager.instance.clearQuillClipboard()
        $('#event_location').val("")
        $('#event_max_number_value').val(1)
        $('#event_max_number').prop('checked', false);
        $('#event_max_number_input').hide()
        $("#organizer_form_container").html("")
        $("#participant-list-container").html("")
        $('#organizer_list').html("")
        $('#participant-list-booked').html(0)
        $('#participant-list-reserved').html(0)
        $('#participant-list-cancel').html(0)
        $('#participant-list-participated').html(0)
        $("#additional_text").val("")
        $('#send_participant_reminders').prop('checked', false);

        handleGUIbyidError("event_max_number_value", true)
    }

    async renderEventDetail() {

        return new Promise(async (resolve, reject) => {
            $('#online_event').prop('checked', false)
            RenderEventmanager.instance.resetEventDetail()
            let active_event = RenderEventmanager.instance.em.getActiveEvent()
            RenderEventmanager.instance.renderTagList([...active_event?.tags || []])

            if (RenderEventmanager.instance.em.getActiveEvent()) {
                //build detail view from active event
                RenderEventmanager.instance.toggleDetailEditState(false)

                $("#event_title").val(RenderEventmanager.instance.em.active_event_data.name || "")
                RenderEventmanager.instance.event_description.clipboard.dangerouslyPasteHTML(0, RenderEventmanager.instance.em.active_event_data.desc)
                $('#event_location').val(RenderEventmanager.instance.em.active_event_data.location || "");
                if ($('#event_location').val().includes(RenderEventmanager.instance.acc?.accountProfileData?.jitsi?.hostname) || $('#event_location').val() == "Online") {
                    $('#online_event').prop('checked', true)
                }
                if ($('#online_event').is(':checked') && $('#event_location').val().includes(RenderEventmanager.instance.acc?.accountProfileData?.jitsi?.hostname)) {
                    $('#online_event_start_button').removeClass("hidden")
                } else {
                    $('#online_event_start_button').addClass("hidden")
                }

                if (RenderEventmanager.instance.em.active_event_data.max_participants != 0) {
                    $('#event_max_number').prop('checked', true)
                    $('#event_max_number_input').show()
                } else {
                    $('#event_max_number').prop('checked', false)
                    $('#event_max_number_input').hide()
                }

                $('#event_max_number_value').val(RenderEventmanager.instance.em.active_event_data.max_participants || 0);
                $('#event_date_range_detail').show()
                let startdatetime = moment(RenderEventmanager.instance.em.active_event_data.startdatetime)
                let enddatetime = moment(RenderEventmanager.instance.em.active_event_data.enddatetime)
                RenderEventmanager.instance.eventDateRange(startdatetime, enddatetime)
                RenderEventmanager.instance.renderOrganiser(RenderEventmanager.instance.em.active_event_data.organisers)

                await RenderEventmanager.instance.renderParticipants()
                //$('#participant-list-max').html((RenderEventmanager.instance.em.active_event_data.max_participants != 0) ? `/ ${RenderEventmanager.instance.em.active_event_data.max_participants}` : "")
                /* let total = RenderEventmanager.instance.em.active_event_data.participants ? Object.keys(RenderEventmanager.instance.em.active_event_data.participants).length : 0
                $('#participant-list-booked').html(total) */

                var newLink = $("<a />", {
                    class: "btn g-btn-events ml-2",
                    href: CONFIG.APP_BASE_URL + `/eventmanager/index-guest.html?event_id=${RenderEventmanager.instance.em.active_event.event_id}&custom_css_url=${CONFIG.APP_BASE_URL}/fn.css`,
                    text: pbI18n.instance.translate("Sign up now")
                });
                $("#signup_link_container").html(newLink)

                $("#state_machine_template").val(RenderEventmanager.instance.em.active_event_data.state_machine_template || "")

                if (new PbAccount().hasPermission("frontend.eventmanager.schedule_option") && RenderEventmanager.instance.em.active_event_data.state_machine_template === "default_registration_config") {
                    $("#additional_text_area").removeClass("hidden")
                } else {
                    $("#additional_text_area").addClass("hidden")
                }

                $("#additional_text").val(RenderEventmanager.instance.em.active_event_data.additional_text || "")

                if (RenderEventmanager.instance.em.active_event_data.send_participant_reminders) {
                    $('#send_participant_reminders').prop('checked', true)

                } else {
                    $('#send_participant_reminders').prop('checked', false)
                }
            } else {
                //build new event
                RenderEventmanager.instance.eventDateRange()
                RenderEventmanager.instance.addNewOrganiserForm()
            }
            resolve()
        })
    }


    renderEventDetailEmbed() {
        RenderEventmanager.instance.resetEventDetail()

        if (RenderEventmanager.instance.em.getActiveEvent()) {
            //build detail veiw from active event
            RenderEventmanager.instance.toggleDetailEditState(false)

            $("#event_title").html(RenderEventmanager.instance.em.active_event_data.name || "")
            $("#event_description").html(RenderEventmanager.instance.em.active_event_data.desc || "")
            // RenderEventmanager.instance.event_description.clipboard.dangerouslyPasteHTML(0, RenderEventmanager.instance.em.active_event_data.desc)
            $('#event_location').html(RenderEventmanager.instance.em.active_event_data.location || "");

            let startdatetime = moment(RenderEventmanager.instance.em.active_event_data.startdatetime)
            let enddatetime = moment(RenderEventmanager.instance.em.active_event_data.enddatetime)
            RenderEventmanager.instance.eventDateRange(startdatetime, enddatetime)
            RenderEventmanager.instance.renderOrganiser(RenderEventmanager.instance.em.active_event_data.organisers)

            var newLink = $("<a />", {
                class: "btn g-btn-events text-nowrap ml-2",
                href: CONFIG.APP_BASE_URL + `/eventmanager/index-guest.html?event_id=${RenderEventmanager.instance.em.active_event.event_id}&custom_css_url=${CONFIG.APP_BASE_URL}/fn.css`,
                text: pbI18n.instance.translate("Sign up now")
            });
            $("#signup_link_container").html(newLink)


        }

    }

    getEventDetails() {
        let datestring = $('#event_date_range').text().trim("\n").trim()
        let start = datestring.slice(0, 16)
        start = moment(start, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss')
        let end = datestring.slice(21)
        end = moment(end, 'DD-MM-YYYY HH:mm').format('YYYY-MM-DD HH:mm:ss')
        let details = {
            'data': {},
            'links': [],
            'data_owners': []
        }
        details["data"]['name'] = $("#event_title").val()
        details["data"]['tags'] = $("#tags").val()
        //details["data"]['desc'] = RenderEventmanager.instance.event_description.getText()
        details["data"]['desc'] = RenderEventmanager.instance.event_description.container.firstChild.innerHTML
        details["data"]['location'] = $('#event_location').val() || ""
        details["data"]['status'] = 'planned'
        details["data"]['startdatetime'] = start
        details["data"]['enddatetime'] = end

        if ($('#event_max_number').is(':checked')) {
            details["data"]['max_participants'] = Number($('#event_max_number_value').val())
        } else {
            details["data"]['max_participants'] = 0
        }
        let organisers = $('*[data-organizer-entry]')

        if ($('#send_participant_reminders').is(':checked')) {
            details["data"]['send_participant_reminders'] = true
        } else {
            details["data"]['send_participant_reminders'] = false
        }

        details["data"]["state_machine_template"] = $("#state_machine_template").val() ? $("#state_machine_template").val() : undefined

        details["data"]["additional_text"] = $("#additional_text").val() ? $("#additional_text").val() : undefined

        let api_organiser_entry = []
        for (let organizer_container of organisers) {
            if (organizer_container.id == "[{organizer-id}]") {
                continue
            }
            let organiser_data = $(organizer_container).find("input")
            if (organiser_data[0]) {
                api_organiser_entry.push({
                    "raw": {
                        "name": `${organiser_data[0].value}`,
                        "email": organiser_data[1].value
                    }
                })
            }
        }
        details["data"]['organisers'] = api_organiser_entry

        if (RenderEventmanager.instance.em.active_event && RenderEventmanager.instance.em.active_event['event_id'] != "") {
            details['event_id'] = RenderEventmanager.instance.em.active_event['event_id']
        }
        return details
    }

    toggleDetailEditState(enabled) {
        RenderEventmanager.instance.toggleDetailView(true)
        RenderEventmanager.instance.editMode = enabled
        if (enabled == true) {
            RenderEventmanager.instance.detailviewstate == "edit"
            $('#btn-edit-event').hide()
            $('#btn-save-event').show()
            $('#btn-save-copy-event').show()
            $('#btn-share-event').hide()
            $('[data-group="info"]').prop('disabled', false)
            RenderEventmanager.instance.event_description.enable(true)
            $('#organizer_form').show()
            $('#organizer_list').hide()
            $('#event_date_range_detail').hide()
            $('#event_date_range').show()
            //RenderEventmanager.instance.eventDateRange()
            //$('#participant-list').hide()
            $("#btn-event-scanner").hide()
            $("#tags").attr("disabled", false)
            $("#addGlobalTagButton").attr("disabled", false)
        } else {
            RenderEventmanager.instance.detailviewstate == "readonly"
            $('#btn-edit-event').show()
            $('#btn-save-event').hide()
            $('#btn-save-copy-event').hide()
            $('#btn-share-event').show()
            $('[data-group="info"]').prop('disabled', true)
            RenderEventmanager.instance.event_description.enable(false)
            $('#organizer_form').hide()
            $('#organizer_list').show()
            $('#event_date_range_detail').show()
            $('#event_date_range').hide()
            $('#participant-list').show()
            $("#btn-event-scanner").show()
            $("#tags").attr("disabled", true)
            $("#addGlobalTagButton").attr("disabled", true)
        }
    }

    toggleDetailView(active) {
        if (active == true) {
            $("#list-view").hide();
            $("#detail-view").show();
        } else {
            $("#list-view").show();
            $("#detail-view").hide();
            $('#participant-list').hide()
        }
    }

    eventDateRange(start, end) {

        start = typeof start !== 'undefined' ? start : moment();
        end = typeof end !== 'undefined' ? end : moment().add(1, 'days');

        function cb(start, end) {
            $('#event_date_range span').html(start.format('DD.MM.YYYY HH:mm') + ' bis ' + end.format('DD.MM.YYYY HH:mm'))
            $('#event_date_range_detail').html(start.format('DD.MM.YYYY HH:mm') + ' bis ' + end.format('DD.MM.YYYY HH:mm'))
        }

        $('#event_date_range').daterangepicker({
            startDate: start,
            endDate: end,
            "timePicker": true,
            "timePicker24Hour": true,
            locale: {
                format: "DD.MM.YYYY HH:mm",
                // separator: " . ",
                applyLabel: "anwenden",
                cancelLabel: "abbrechen",
                fromLabel: "von",
                toLabel: "bis",
                customRangeLabel: "Benutzerdefinierte",
                weekLabel: "W",
                daysOfWeek: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
                monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
                "firstDay": 1
            },
            linkedCalendars: true,
            "showCustomRangeLabel": false,
            opens: "center",
            drops: "auto"

        }, cb);

        cb(start, end)
    }

    clearQuillClipboard() {
        RenderEventmanager.instance.event_description.root.innerHTML = ""
    }

    addNewOrganiserForm() {
        let newOrganiserForm = $("#organizer_data_set").html()
        let organizer_id = "organizer_0"
        newOrganiserForm = newOrganiserForm.replaceAll('[{organizer-id}]', organizer_id)
        $('#organizer_form_container').append(newOrganiserForm);
    }

    renderOrganiser(organisers) {
        let organiserForm = $("#organizer_data_set").html()
        let organiserItem = $("#organizer_item").html()

        for (let [key, value] of Object.entries(organisers)) {

            let organizer_id = "organizer_" + key

            for (let [key, data] of Object.entries(value)) {
                if (key == "raw") {

                    var details = {
                        'organizer-id': organizer_id,
                        'organizer-name': data.name,
                        'organizer-email': data.email
                    }

                    //view mode
                    let listItemContent = multiStringReplace(details, organiserItem)
                    $('#organizer_list').append(listItemContent);
                    //edit mode
                    let organiserFormContent = multiStringReplace(details, organiserForm)

                    $('#organizer_form_container').append(organiserFormContent);
                    $('#' + organizer_id + ' > div > input[data-organizer-input="organizer_name_value"]').val(data.name)
                    $('#' + organizer_id + ' > div > input[data-organizer-input="organizer_email_value"]').val(data.email)

                }

            }
        }
    }


    compareParticipantEntriesByStatus(a, b) {
        let data_a = a[1]
        let data_b = b[1]
        let status_value_a = 0
        let status_value_b = 0
        switch (data_a.status) {
            case "booked":
                status_value_a = 0
                break;
            case "reserved":
                status_value_a = 1
                break;
            case "canceled":
                status_value_a = 2
                break;
            case "deleted":
                status_value_a = 3
                break;
        }
        switch (data_b.status) {
            case "booked":
                status_value_b = 0
                break;
            case "reserved":
                status_value_b = 1
                break;
            case "canceled":
                status_value_b = 2
                break;
            case "deleted":
                status_value_b = 3
                break;
        }

        if (status_value_a < status_value_b) {
            return -1;
        }
        if (status_value_b < status_value_a) {
            return 1;
        }
        return 0;
    }

    compareParticipantEntriesByName(a, b) {
        let data_a = a[1].raw.lastname + a[1].raw.firstname
        let data_b = b[1].raw.lastname + a[1].raw.firstname
        if (!data_a)
            data_a = ""
        if (!data_b)
            data_b = ""
        if (data_a.toLowerCase() < data_b.toLowerCase()) {
            return -1;
        }
        if (data_b.toLowerCase() < data_a.toLowerCase()) {
            return 1;
        }
        return 0;

    }

    handleParticipantUpdate(event) {
        let id = event.target.dataset['id']
        let type = event.target.dataset['type']
        event.stopPropagation()
        let new_vals = $(event.target).val()

        let event_details = RenderEventmanager.instance.getEventDetails()
        event_details['data']['participants'] = RenderEventmanager.instance.em.active_event.participants

        if (type == "tags" && JSON.stringify(event_details['data']['participants'][id]["tags"]) != JSON.stringify(new_vals)) {
            event_details['data']['participants'][id]["tags"] = new_vals
            let handledEvent = new Event(event_details["event_id"], event_details)
            handledEvent.update()
            RenderEventmanager.instance.notifications.blockForLoading("#contentwrapper")
        }
        if (type == "notes" && JSON.stringify(event_details['data']['participants'][id]["notes"]) != JSON.stringify(new_vals)) {
            event_details['data']['participants'][id]["notes"] = new_vals
            let handledEvent = new Event(event_details["event_id"], event_details)
            handledEvent.update()
            RenderEventmanager.instance.notifications.blockForLoading("#contentwrapper")
        }
        if (type == "address" && JSON.stringify(event_details['data']['participants'][id]["address"]) != JSON.stringify(new_vals)) {
            event_details['data']['participants'][id]["address"] = new_vals
            let handledEvent = new Event(event_details["event_id"], event_details)
            handledEvent.update()
            RenderEventmanager.instance.notifications.blockForLoading("#contentwrapper")
        }

        RenderEventmanager.instance.lastUpdatedParticipant = id
    }

    async renderParticipants() {
        return new Promise((resolve, reject) => {
            $('#participant-list-container').html("")
            let participantItem = $("#participant_item").html()
            RenderEventmanager.instance.total_num_participants = 0
            let entries = Object.entries(RenderEventmanager.instance.em.active_event_data.participants)
            entries.sort(RenderEventmanager.instance.compareParticipantEntriesByName)
            entries.sort(RenderEventmanager.instance.compareParticipantEntriesByStatus)
            new PbSelect2Tags("#nonexistant").setTagsDropdownFromApi().then((tags_result) => {
                if (participantItem) {

                    for (let [key, value] of entries) {
                        let total_booked = RenderEventmanager.instance.em.active_event_data.participants ? Object.values(RenderEventmanager.instance.em.active_event_data.participants).filter(item => item.status == 'booked').length : 0
                        let total_reserved = RenderEventmanager.instance.em.active_event_data.participants ? Object.values(RenderEventmanager.instance.em.active_event_data.participants).filter(item => item.status == 'reserved').length : 0
                        let total_canceled = RenderEventmanager.instance.em.active_event_data.participants ? Object.values(RenderEventmanager.instance.em.active_event_data.participants).filter(item => item.status == 'canceled').length : 0
                        let total_participated = RenderEventmanager.instance.em.active_event_data.participants ? Object.values(RenderEventmanager.instance.em.active_event_data.participants).filter(item => item.status == 'participated').length : 0

                        let participant_id = key
                        let participant_status = value.status

                        let participant_firstname = value.raw["firstname"] || ""
                        let participant_lasttname = value.raw["lastname"] || ""

                        let selector = "tags_" + participant_id
                        $(document).off('change', "#" + selector)
                        if (value.raw["contact_id"]) {
                            let href = `${CONFIG.APP_BASE_URL}/contact-manager/?ci=${value.raw["contact_id"]}`
                            participant_firstname = $("<a />").html(value.raw["firstname"]).attr("target", "_blank").attr("href", href).addClass("form_control").prop('outerHTML')
                            participant_lasttname = $("<a />").html(value.raw["lastname"]).attr("target", "_blank").attr("href", href).addClass("form_control").prop('outerHTML')
                        }

                        let status_classes = {
                            "booked": "badge-success",
                            "reserved": "badge-warning",
                            "canceled": "badge-danger"
                        }

                        var details = {
                            'participant-id': participant_id,
                            'participant-status': new pbI18n().translate(participant_status),
                            'participant-status-class': status_classes[participant_status] || "badge-info",
                            'participant-firstname': participant_firstname,
                            'participant-lastname': participant_lasttname,
                            'participant-email': (value.raw["email"] != undefined) ? value.raw["email"] : "",
                            'participant-phone': (value.raw["phone"] != undefined) ? value.raw["phone"] : "",
                            'participant-tags-selector': selector,
                            'participant-notes': (value["notes"] != undefined) ? value["notes"] : "",
                            'participant-address': (value["address"] != undefined) ? value["address"] : ""
                        }

                        let listItemContent = multiStringReplace(details, participantItem)

                        $('#participant-list-container').append(listItemContent);
                        $('#participant-list-booked').html(total_booked)
                        $('#participant-list-reserved').html(total_reserved)
                        RenderEventmanager.instance.total_num_participants = total_booked
                        $('#participant-list-cancel').html(total_canceled)
                        $('#participant-list-participated').html(total_participated)
                        $('#participant_download_link').click(() => {
                            $("#participant_download_link").attr("href", CONFIG.API_BASE_URL + "/get_event_participants/" + RenderEventmanager.instance.em.active_event.event_id + "?format=csv&apikey=" + getCookie("apikey"))
                        })


                        let event_details = RenderEventmanager.instance.getEventDetails()
                        event_details['data']['participants'] = RenderEventmanager.instance.em.active_event.participants

                        let selectedValues = event_details.data?.participants[participant_id]["tags"] || []
                        new PbSelect2Tags("#" + selector).addTagToDropdown(RenderEventmanager.instance.i18n.translate("paid"), "paid", true).setTagsDropdown(tags_result, [], false).setSelectedValues(selectedValues)

                        $(document).on('change', "#" + selector, RenderEventmanager.instance.handleParticipantUpdate)
                    }
                }

                resolve()

            })
        })
    }

    resetParticipantModal() {
        $('#participant_first_name_value').val('')
        $('#participant_last_name_value').val('')
        $('#participant_email_value').val('')
    }

    updateParticipantStatus(event_id, participant_id, status) {
        let event = RenderEventmanager.instance.em.getActiveEvent()
        let participant_data = event["participants"][participant_id]


        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/changeSignUpDataForEvent",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey"),
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "event_id": event_id,
                "signup_id": participant_id,
                "data": {
                    "raw": {
                        "firstname": participant_data.raw.firstname,
                        "lastname": participant_data.raw.lastname,
                        "email": participant_data.raw.email,
                        "info": participant_data.raw?.info || {},
                    },
                    "status": status,
                    "tags": participant_data.tags,
                    "success": function (response) {
                        console.log("SUCCESS", response)
                    }
                }
            }),
        };

        $.ajax(settings)
    }

    startStateMachineConfigProcess(firstname, lastname, email) {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/statemachine/create_state_machine_from_template/" + RenderEventmanager.instance.em.active_event.state_machine_template,
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey"),
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "email": email,
                "firstname": firstname,
                "lastname": lastname,
                "redirect_url_success": encodeURIComponent(CONFIG.APP_BASE_URL + "/eventmanager/registration-success.html"),
                "redirect_url_failure": encodeURIComponent(CONFIG.APP_BASE_URL + "/eventmanager/registration-failure.html"),
                "eventdata": {
                    "event_id": RenderEventmanager.instance.em.active_event.event_id,
                    "desc": RenderEventmanager.instance.em.active_event.desc,
                    "enddatetime": RenderEventmanager.instance.em.active_event.end,
                    "name": RenderEventmanager.instance.em.active_event.title,
                    "startdatetime": RenderEventmanager.instance.em.active_event.start
                }
            }),
            "success": function (response) {
                console.log("Response", response);
                RenderEventmanager.instance.moveToFirstStepOfConfig(response.meta.persistent_id)
            }
        };

        $.ajax(settings)
    }

    moveToFirstStepOfConfig(id) {
        var settings = {
            "url": CONFIG.API_BASE_URL + `/statemachine/state_machine_instance/${id}/go_to_next_state`,
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey"),
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({}),
        };

        $.ajax(settings).done(function (response) {
            console.log(response);
        });
    }

    initListeners() {

        $("#button_event_archive").on("click", function () {
            let searchParams = new URLSearchParams(window.location.search);
            let current_state = searchParams.get("show_outdated");
            if (current_state == "true") {
                searchParams.set("show_outdated", false);
                $("#button_event_archive").removeClass("btn-purple")
            } else {
                searchParams.set("show_outdated", true);
                $("#button_event_archive").addClass("btn-purple")
            }
            let newURL = window.location.pathname + '?' + searchParams.toString();
            location.href = newURL;
        });

        $(document).on('click', '#online_event', function () {
            if ($(this).is(":checked")) {
                if (RenderEventmanager.instance.acc?.accountProfileData?.jitsi?.hostname?.length > 0) {
                    $('#event_location').val(RenderEventmanager.instance.acc?.accountProfileData?.jitsi?.hostname + "/" + RenderEventmanager.instance.em.active_event.event_id)
                } else {
                    $('#event_location').val("Online")
                }
            } else {
                if ($('#event_location').val() == "Online" || $('#event_location').val().includes(RenderEventmanager.instance.acc?.accountProfileData?.jitsi?.hostname)) {
                    $('#event_location').val("")
                }
            }
        });

        $("#online_event_start_button").on("click", function () {
            $('#online_event_start_button').hide();
            $('#jitsi_video').show();
            $('#jitsi_video_container').show();


            let config = {
                "apikey": getCookie("apikey"),
                "module": "jitsi",
                "jitsi_server": RenderEventmanager.instance.acc?.accountProfileData?.jitsi?.hostname,
                "target_selector": "#jitsi_video",
                "height": "600px",
                "interfaceConfigOverwrite": {"DEFAULT_BACKGROUND": "#0f6376"},
                "configOverwrite": {},
                "room_id": RenderEventmanager.instance.em.active_event.event_id
            }
            import("/dist/main.js").then(() => {
                document.querySelector(config['target_selector']).innerHTML = "";
                new DistPbs.Pbs(config).then((loadedModuleInstance) => {
                    window.jitsi = loadedModuleInstance
                    let interval = setInterval(() => {
                        try {
                            window.jitsi.api.addListener("readyToClose", function (e) {
                                $('#online_event_start_button').show();
                                $('#jitsi_video').hide();
                                $('#jitsi_video_container').hide();
                            })
                            clearInterval(interval)
                        } catch (e) {

                        }
                    }, 1000)

                    var targetElement = $('#jitsi_video');
                    $('html, body').animate({
                        scrollTop: targetElement.offset().top
                    }, 1000);
                })
            })
        })


        $(document).on('change', '[data-selector-participant-notes]', function (e) {
            RenderEventmanager.instance.handleParticipantUpdate(e)
        })

        $(document).on('change', '[data-selector-participant-address]', function (e) {
            RenderEventmanager.instance.handleParticipantUpdate(e)
        })

        $("#button-new-event").on("click", function () {
            RenderEventmanager.instance.em.setActiveEvent(undefined)
            RenderEventmanager.instance.toggleDetailEditState(true);
            RenderEventmanager.instance.renderEventDetail()
        });


        $(document).on('click', '#btn-save-event', function () {
            if ($("#event_max_number").is(":checked") && Number($("#event_max_number_value").val()) < RenderEventmanager.instance.total_num_participants) {
                handleGUIbyidError("event_max_number_value", false);
            } else {
                RenderEventmanager.instance.toggleDetailEditState(false);
                let details = RenderEventmanager.instance.getEventDetails();

                if (RenderEventmanager.instance.em.active_event) {
                    let handledEvent = new Event(details["event_id"], details);
                    handledEvent.update();
                    RenderEventmanager.instance.renderEventListView();
                } else {
                    let handledEvent = new Event(undefined, details);
                    handledEvent.create();
                    RenderEventmanager.instance.renderEventListView();
                }

            }
        });

        $(document).on('click', '#btn-save-copy-event', function () {
            if ($("#event_max_number").is(":checked") && Number($("#event_max_number_value").val()) < RenderEventmanager.instance.total_num_participants) {
                handleGUIbyidError("event_max_number_value", false);
            } else {
                RenderEventmanager.instance.toggleDetailEditState(false);
                let details = RenderEventmanager.instance.getEventDetails();
                let handledEvent = new Event(undefined, details);
                handledEvent.create();
                RenderEventmanager.instance.renderEventListView();
            }
        });


        $("#btn-edit-event").on("click", function () {
            RenderEventmanager.instance.toggleDetailEditState(true)
        });

        $("#btn-event-scanner").on("click", function () {
            const urlParams = new URLSearchParams(window.location.search);
            let event_id = urlParams.get("event_id")
            location.href = "./scanner.html?event_id=" + event_id
        });

        $("#btn-share-event").on("click", function () {
            $("#share_event_modal").modal("show");
            $('#share_event_link').val("Linkgenerierung...")
            var timeout = setTimeout(() => {
                $('#share_event_link').val("Linkgenerierung fehlgeschlagen.")
            }, 3000)
            RenderEventmanager.instance.em.createInviteCode(RenderEventmanager.instance.em.active_event['event_id']).then((qr_code_id) => {
                clearTimeout(timeout)
                let link = CONFIG.API_BASE_URL + `/sqr/${qr_code_id}`
                $('#share_event_link').val(link)
                let qr_code_url = CONFIG.API_BASE_URL + "/get_qrcode/" + qr_code_id
                $('#share_qr_code').attr("src", qr_code_url)
            }).catch((error) => {
                $('#share_event_link').val("Linkgenerierung fehlgeschlagen.")
            })
        });

        $("#btn-copy-event-link").on("click", function () {
            var copyValue = $("#share_event_link").val();
            navigator.clipboard.writeText(copyValue).then(function() {
                $("#success").text(new pbI18n().translate("Successfully copied")).removeClass('hidden');
                $("#textAndCopy").addClass('hidden')

                setTimeout(function() {
                    $("#share_event_link").val(copyValue);
                    $("#textAndCopy").removeClass('hidden');
                    $("#success").addClass('hidden');
                }, 3000);
            }).catch(function(error) {
                $("#failure").text(new pbI18n().translate("Copy failed")).removeClass('hidden');
                $("#textAndCopy").addClass('hidden');
            });

        });

        $("#btn-new-organizer").on("click", function () {
            RenderEventmanager.instance.addNewOrganiserForm()
        });

        $("#btn-send-reminder-mails").on('click', function () {
            let desc = 'Are you sure that you want to send out reminder mails to all participants with status "booked"?'
            let title = "Reminder email send-out"
            let okay_text = 'Send now'
            let cancel_text = 'Cancel'
            let error_text = "ERROR - Cannot send event reminder"
            let show_results = function (type, api_response) {
                let rows = []
                for (let recipients of Object.values(api_response.sentMails)) {
                    for (let recipient of recipients) {
                        rows.push(recipient)
                    }
                }
                console.log("rows: ", rows)
                PbTpl.instance.renderAsync("eventmanager/templates/email_recipient_list.html", {"rows": rows}, (err, body) => {
                    PbNotifications.instance.showAlert(body, "Versendet an", type)
                    new pbI18n().replaceI18n()
                })


            }
            PbNotifications.instance.ask(desc, title, () => {
                    var settings = {
                        "url": CONFIG.API_BASE_URL + "/sendEventReminderEmails?event_id=" + RenderEventmanager.instance.em.active_event.event_id,
                        "method": "GET",
                        "timeout": 0,
                        "headers": {
                            "apikey": getCookie("apikey")
                        },
                        "success": (response) => show_results("error", response),
                        "error": () => alert(error_text)
                    };
                    $.ajax(settings)
                }, () => {
                    let simulate_only_text = "Would you like to simulate email sending? No emails will be sent, but you will get a display of who the emails would theoretically be sent to."
                    PbNotifications.instance.ask(simulate_only_text, title, () => {
                        var settings = {
                            "url": CONFIG.API_BASE_URL + "/sendEventReminderEmails?simulate_only=1&event_id=" + RenderEventmanager.instance.em.active_event.event_id,
                            "method": "GET",
                            "timeout": 0,
                            "headers": {
                                "apikey": getCookie("apikey")
                            },
                            "success": (response) => show_results("warning", response),
                            "error": () => alert(error_text)
                        };
                        $.ajax(settings)
                    })

                }, "danger", okay_text, cancel_text
            )


        })

        $("#btn-add-participant").on('click', function () {
            let selector = "#contact_to_participant_selection"
            let select = new PbSelect2Contacts(selector)

            select.setContactsDropdownFromApi().then(() => {
                $('#add-participant-modal').modal('show')
            })

            let onchange = function (e) {
                let value = select.getSelectedValues()
                $("#participant_first_name_value").val("")
                $("#participant_last_name_value").val("")
                $("#participant_email_value").val("")
                $("#participant_id_value").val("")


                if (select.contacts[value]) {
                    $("#participant_first_name_value").val(select.contacts[value].data?.firstname || "")
                    $("#participant_last_name_value").val(select.contacts[value].data?.lastname || "")
                    $("#participant_email_value").val(select.contacts[value].data?.email || "")
                    $("#participant_id_value").val(value)
                }
            }
            new PbSelect2Contacts(selector).setOnChange(onchange)
        })

        $(document).on('submit', '#event_participant_add_form', function () {
            if (RenderEventmanager.instance.em.active_event?.state_machine_template && RenderEventmanager.instance.em.active_event?.state_machine_template?.length > 0) {
                RenderEventmanager.instance.notifications.blockForLoading("body")
                RenderEventmanager.instance.startStateMachineConfigProcess($('#participant_first_name_value').val(), $('#participant_last_name_value').val(), $('#participant_email_value').val())

                RenderEventmanager.instance.notifications.showHint("Email with confirmation-link was sent.", "success", () => {
                    $('#add-participant-modal').modal('hide')
                    RenderEventmanager.instance.resetParticipantModal()
                    RenderEventmanager.instance.notifications.unblock("body")
                }, 2000)
                return false;
            } else {
                let modal = document.getElementById('add-participant-modal')
                let payload = {
                    "role": "participants",
                    "personal_data": {
                        "raw": {"info": {}},
                        "status": "reserved"
                    }
                }

                payload.personal_data.raw["firstname"] = $('#participant_first_name_value').val()
                payload.personal_data.raw["lastname"] = $('#participant_last_name_value').val()
                payload.personal_data.raw["email"] = $('#participant_email_value').val()
                payload.personal_data.raw["contact_id"] = $('#participant_id_value').val()
                let event = new Event(RenderEventmanager.instance.em.active_event['event_id'])

                event.signUpParticipant(payload).then(function (response) {
                    RenderEventmanager.instance.last_task_ids.push(response.task_id)
                })
                RenderEventmanager.instance.resetParticipantModal()
                $('#add-participant-modal').modal('hide')
                return false;
            }
        })

        RenderEventmanager.instance.em.socket.commandhandlers["newEventCreatedSuccess"] = function (args) {
            RenderEventmanager.instance.em.getEvents().then(function () {
                RenderEventmanager.instance.renderEventListView()
                Fnon.Hint.Success(new pbI18n().translate("Event has been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
            })
        }

        RenderEventmanager.instance.em.socket.commandhandlers["signUpDataForEventChangedSuccessfully"] = async function (args) {
            RenderEventmanager.instance.em.getEvents().then(function () {
                //RenderEventmanager.instance.renderParticipants()
                //RenderEventmanager.instance.renderEventListView()
                if (RenderEventmanager.instance.em.active_event?.event_id == args.event_id) {
                    RenderEventmanager.instance.em.setActiveEvent(args.event_id).then(async () => {
                        await RenderEventmanager.instance.renderParticipants()
                        $("#" + args.signup_id)[0]?.scrollIntoView();
                        Fnon.Hint.Success(new pbI18n().translate("Eventparticipant has been updated"), {displayDuration: 2000})
                        Fnon.Wait.Remove(500)
                        // await RenderEventmanager.instance.renderEventDetail();

                    })
                }


            })
        }

        RenderEventmanager.instance.em.socket.commandhandlers["eventUpdatedSuccess"] = function (args) {
            RenderEventmanager.instance.em.getEvents().then(function () {
                RenderEventmanager.instance.renderEventListView()
                if (RenderEventmanager.instance.em.active_event?.event_id == args.event_id) {
                    RenderEventmanager.instance.em.setActiveEvent(args.event_id).then(RenderEventmanager.instance.renderEventDetail)
                }

                if (RenderEventmanager.instance.lastUpdatedParticipant) {
                    let id = RenderEventmanager.instance.lastUpdatedParticipant
                    window.setTimeout(() => {
                        //$("#" + id)[0].scrollIntoView()
                        RenderEventmanager.instance.notifications.unblock("#contentwrapper")
                    }, 300)


                    RenderEventmanager.instance.lastUpdatedParticipant = undefined
                }
                Fnon.Hint.Success(new pbI18n().translate("Event has been updated"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
            })
        }

        RenderEventmanager.instance.em.socket.commandhandlers["eventDeletedSuccess"] = function (args) {
            RenderEventmanager.instance.em.getEvents().then(function () {
                RenderEventmanager.instance.renderEventListView()
                Fnon.Hint.Success(new pbI18n().translate("The event has been deleted"), {displayDuration: 2000})
                Fnon.Wait.Remove(500)
            })
        }

        RenderEventmanager.instance.em.socket.commandhandlers["signedUpForEventSuccessfull"] = function (args) {
            RenderEventmanager.instance.em.getEvents().then(function () {
                if (RenderEventmanager.instance.last_task_ids.includes(args.task_id)) {
                    Fnon.Hint.Success(new pbI18n().translate('Signed up sucessfully'), {displayDuration: 2000})
                    RenderEventmanager.instance.em.setActiveEvent(args.event_id).then(RenderEventmanager.instance.renderEventDetail)
                }
            })
        }

        RenderEventmanager.instance.em.socket.commandhandlers["signedUpForEventFailed"] = function (args) {
            RenderEventmanager.instance.em.getEvents().then(function () {
                if (RenderEventmanager.instance.last_task_ids.includes(args.task_id)) {
                    Fnon.Hint.Danger(new pbI18n().translate(args.reason), {displayDuration: 2000})
                }
            })
        }


        $("#btn-cancel").on("click", function () {
            const urlParams = new URLSearchParams(window.location.search);
            urlParams.delete("event_id")
            const newUrl = `${window.location.pathname}?${urlParams.toString()}`;
            history.pushState(null, null, newUrl);
            RenderEventmanager.instance.renderEventListView()
        });

        $('#event_max_number').on('change', function () {
            if ($(this).is(':checked')) {
                $('#event_max_number_input').show()
                $('#event_max_number_value').val(1)
            } else {
                $('#event_max_number_input').hide()
            }
        })

        $(document).on('click', 'li[data-selector="list-entry"]', function () {
            let id = $(this).attr('id')
            const urlParams = new URLSearchParams(window.location.search);
            urlParams.set("event_id", id);
            const newUrl = `${window.location.pathname}?${urlParams.toString()}`;
            history.pushState(null, null, newUrl);
            RenderEventmanager.instance.em.setActiveEvent(id).then(RenderEventmanager.instance.renderEventDetail)

        })

        $(document).on('click', 'button[data-selector="list-entry"]', function () {
            RenderEventmanager.instance.em.setActiveEvent(id).then(RenderEventmanager.instance.renderEventDetailEmbed)
        })

        $(document).on('click', 'a[data-name="quickaction_cancelparticipant"]', function (event) {
            event.preventDefault()
            event.stopPropagation()

            let cancelQuestion = new pbI18n().translate("Are you sure you want to cancel this booking?")
            let cancelTitle = new pbI18n().translate("Cancel booking")
            Fnon.Ask.Danger(cancelQuestion, cancelTitle, 'Okay', 'Abbrechen', (result) => {
                    if (result == true) {
                        let id = event.target.dataset['id']
                        let active_event = RenderEventmanager.instance.em.getActiveEvent()
                        RenderEventmanager.instance.updateParticipantStatus(active_event.event_id, id, "canceled")
                    }
                }
            )


            // Fnon.Ask.Danger(cancelQuestion, cancelTitle, 'Okay', 'Abbrechen', (result) => {
            //     if (result == true) {
            //         let id = event.target.dataset['id']
            //         Object.values(RenderEventmanager.instance.em.active_event.participants).forEach(item => {
            //             if (item.raw.email == id) {
            //                 item.status = 'canceled'
            //             }
            //         })
            //         let details = RenderEventmanager.instance.getEventDetails()
            //         details['data']['participants'] = RenderEventmanager.instance.em.active_event.participants
            //         let handledEvent = new Event(details["event_id"], details)
            //         handledEvent.update()
            //     }
            // })
        })

        $(document).on('click', 'a[data-name="quickaction_reservedparticipant"]', function (event) {
            event.preventDefault()
            event.stopPropagation()
            let id = event.target.dataset['id']
            let active_event = RenderEventmanager.instance.em.getActiveEvent()
            RenderEventmanager.instance.updateParticipantStatus(active_event.event_id, id, "reserved")
            //
            // let id = event.target.dataset['id']
            // Object.values(RenderEventmanager.instance.em.active_event.participants).forEach(item => {
            //     if (item.raw.email == id) {
            //         item.status = 'reserved'
            //     }
            // })
            // let details = RenderEventmanager.instance.getEventDetails()
            // details['data']['participants'] = RenderEventmanager.instance.em.active_event.participants
            // let handledEvent = new Event(details["event_id"], details)
            // handledEvent.update()
        })

        $(document).on('click', 'a[data-name="quickaction_bookedparticipant"]', function (event) {
            event.preventDefault()
            event.stopPropagation()
            let id = event.target.dataset['id']
            let active_event = RenderEventmanager.instance.em.getActiveEvent()
            RenderEventmanager.instance.updateParticipantStatus(active_event.event_id, id, "booked")

            // let id = event.target.dataset['id']
            // Object.values(RenderEventmanager.instance.em.active_event.participants).forEach(item => {
            //     if (item.raw.email == id) {
            //         item.status = 'booked'
            //     }
            // })
            // let details = RenderEventmanager.instance.getEventDetails()
            // details['data']['participants'] = RenderEventmanager.instance.em.active_event.participants
            // let handledEvent = new Event(details["event_id"], details)
            // handledEvent.update()
        })

        $(document).on('click', 'a[data-name="quickaction_participatedparticipant"]', function (event) {
            event.preventDefault()
            event.stopPropagation()
            let id = event.target.dataset['id']
            let active_event = RenderEventmanager.instance.em.getActiveEvent()
            RenderEventmanager.instance.updateParticipantStatus(active_event.event_id, id, "participated")
            // let id = event.target.dataset['id']
            // Object.values(RenderEventmanager.instance.em.active_event.participants).forEach(item => {
            //     if (item.raw.email == id) {
            //         item.status = 'participated'
            //     }
            // })
            // let details = RenderEventmanager.instance.getEventDetails()
            // details['data']['participants'] = RenderEventmanager.instance.em.active_event.participants
            // let handledEvent = new Event(details["event_id"], details)
            // handledEvent.update()
        })

        $(document).on('click', 'a[data-name="quickaction_deleteparticipant"]', function (event) {
            event.preventDefault()
            event.stopPropagation()

            let id = event.target.dataset['id']
            let active_event = RenderEventmanager.instance.em.getActiveEvent()

            let deleteParticipantQuestion = new pbI18n().translate("Are you sure you want to delete this booking?")
            let deleteParticipantTitle = new pbI18n().translate("Delete booking")
            Fnon.Ask.Danger(deleteParticipantQuestion, deleteParticipantTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    RenderEventmanager.instance.updateParticipantStatus(active_event.event_id, id, "deleted")
                }
            })

        })

        $(document).on('click', 'a[data-name="quickaction_delete"]', function (event) {
            event.preventDefault()
            event.stopPropagation()
            let deleteQuestion = new pbI18n().translate("Are you sure you want to delete the event?")
            let deleteTitle = new pbI18n().translate("Delete event")
            let id = event.target.dataset['id']
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    new Event(id).delete().catch((data) => {
                        alert("Error");
                        console.error("ERRORDATA", data)
                    })

                }
            })

        })

        $(document).on('newEventCreatedSuccess', function () {
            RenderEventmanager.instance.renderEventListView()
        })

        $(document).on('eventUpdatedSuccess', function () {
            RenderEventmanager.instance.renderEventListView()

        })

        $(document).on('eventDeletedSuccess', function () {
            RenderEventmanager.instance.renderEventListView()
        })

        $(document).on('click', '#state_machine_template', function (event) {
            // Define all state machine templates that are allowed to use additional text:
            let state_machine_templates = ["default_registration_config"];

            let stateMachineVal = $("#state_machine_template").val();

            if ($.inArray(stateMachineVal, state_machine_templates) !== -1) {
                $("#additional_text_area").removeClass("hidden")
            } else {
                $("#additional_text_area").addClass("hidden")
            }
        });
    }
}