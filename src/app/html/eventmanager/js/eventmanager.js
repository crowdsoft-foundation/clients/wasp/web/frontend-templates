import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import { Event } from "./event.js?v=[cs_version]"


export class Eventmanager {
    events = []
    active_event = undefined
    active_event_data = undefined
    show_outdated = false
    
    constructor(show_outdated=false) {
        if (!Eventmanager.instance) {
            Eventmanager.instance = this
            Eventmanager.instance.socket = new socketio()
        }
        Eventmanager.instance.show_outdated = show_outdated
        return Eventmanager.instance
    }

    init() {
        return new Promise(function (resolve, reject) {
            Eventmanager.instance.getEvents().then(()=> {
                resolve()
            })
        })
    }

    getEvents() {
        let show_outdated = "?show_outdated=true"
        if(this.show_outdated == false) {
            show_outdated = ""
        }
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/get_events" + show_outdated,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (response) => { Eventmanager.instance.setEvents(response); resolve()},
                "error": reject,
            };

            $.ajax(settings)
        })
    }



    getEventById(event_id, refresh=false) {
        if(refresh) {
            return new Promise(function (resolve, reject) {
                var settings = {
                    "url": CONFIG.API_BASE_URL + "/get_events/" + event_id,
                    "method": "GET",
                    "timeout": 0,
                    "headers": {
                        "apikey": getCookie("apikey")
                    },
                    "success": (response) => {
                        Eventmanager.instance.setEvents(response)
                        resolve(Eventmanager.instance.events[event_id] || {})
                    },
                    "error": reject,
                };

                $.ajax(settings)
            })
        } else {
            let event_data = Eventmanager.instance.events[event_id] || {}
            return new Event(event_id, event_data)
        }
    }

    addEvent(addPayload) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createNewEvent",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(addPayload),
                "success": (response) => resolve(response),
                "error": reject,
            };

            $.ajax(settings)
        })
    }

    setEvents(response) {
        if(response) {
            Eventmanager.instance.events = response["events"]
        }        
    }

    updateEvent(payload) {
        if(Eventmanager.instance.active_event){
            payload['event_id'] = Eventmanager.instance.active_event?.event_id
        }
        console.log("Update payload",payload)
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateEvent",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => resolve(response),
                "error": reject,
            };

            $.ajax(settings)
        })
    }
    

    deleteEvent(external_id, event_id=undefined) {
        let payload = {
            "event_id": "optional",
            "external_id": external_id
          }
        if(event_id){
            payload = {
                "event_id": event_id,
                "external_id": "optional"
              }
        }
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "es/cmd/deleteEvent",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify(payload),
                "success": (response) => resolve(response),
                "error": reject,
            };

            $.ajax(settings)
        })
    }

    signUpOrganiser() {
        return new Promise(function (resolve, reject) {
            
            })
    }
    
    setActiveEvent(event_id) {
        return new Promise(function (resolve, reject) {
            Eventmanager.instance.active_event = undefined
            if (!event_id) {
                Eventmanager.instance.active_event = undefined
            } else {

            let active_event = Eventmanager.instance.getEventById(event_id)
            let data = active_event.getDataFormattedForApiCall()["data"]
            Eventmanager.instance.active_event = active_event
            Eventmanager.instance.active_event_data = data
            resolve()

            }
        })
    }

    getActiveEvent() {
        return Eventmanager.instance.active_event
    }

    createInviteCode(id) {
        return new Promise (function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/createQRCode",
                "method": "POST",
                "timeout": 0,
                "headers": {
                  "apikey": getCookie("apikey"),
                  "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                  "name": "Contact-Deep-Link",
                  "data": {
                    "payload": {
                      "type": "url",
                      "guest_login": true,
                      "value": CONFIG.APP_BASE_URL + '/eventmanager/index-guest.html?event_id='+ id
                    },
                    "error_correct": "H",
                    "kind": "default"                    
                  }
                }),
                "success": (response) => {
                    Eventmanager.instance.socket.commandhandlers["qrCodeCreated"] = function (args) {
                        if(args.task_id == response.task_id) {
                            resolve(args.qr_code_id)
                        }
                    }
                    
                },
                "error": reject,
            };

            $.ajax(settings)
          })
    }
}