import {Eventmanager} from "./eventmanager.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {multiStringReplace, getCookie, array_merge_distinct} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {Event} from "./event.js?v=[cs_version]";
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {handleGUIbyidError} from "../../csf-lib/pb-formchecks.js?v=[cs_version]"
import {PbSelect2Tags} from "../../csf-lib/pb-select2-tags.js?v=[cs_version]"
import {PbSelect2Contacts} from "../../csf-lib/pb-select2-contacts.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTag} from "../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"


export class RenderEventmanagerScanner {
    detailviewstate = "readonly";
    event_description = undefined
    total_num_participants = undefined

    constructor() {
        if (!RenderEventmanagerScanner.instance) {
            RenderEventmanagerScanner.instance = this;
            RenderEventmanagerScanner.instance.all_list_tags = []
            RenderEventmanagerScanner.instance.last_task_ids = []
            RenderEventmanagerScanner.instance.em = new Eventmanager();
            RenderEventmanagerScanner.instance.handledTaskIds = []
            RenderEventmanagerScanner.instance.pressedKeys = ""
            RenderEventmanagerScanner.instance.PbTpl = new PbTpl()
            RenderEventmanagerScanner.instance.notifications = new PbNotifications()
            RenderEventmanagerScanner.instance.initListeners();

            document.addEventListener('keydown', function (event) {
                const key = event.key; // "a", "1", "Shift", etc.
                if (["Control", "AltGraph", "Alt"].includes(key)) return
                if (key == "Enter") {
                    console.log("RECEIVED DATA FROM SCANNER", RenderEventmanagerScanner.instance.pressedKeys, RenderEventmanagerScanner.instance.pressedKeys.replace("Shift°", "|").replace("'", "|"))
                    let [signupId, event_id] = RenderEventmanagerScanner.instance.pressedKeys.replace("Shift°", "|").replace("„", "|").split("|")
                    RenderEventmanagerScanner.instance.em.getEventById(event_id, true).then(function() {
                        RenderEventmanagerScanner.instance.em.setActiveEvent(event_id).then(function() {
                            console.log("SignupID", signupId, "Event ID", event_id, "Active Event", RenderEventmanagerScanner.instance.em.active_event)

                            if (event_id == RenderEventmanagerScanner.instance.em.active_event.event_id && $(`[data-participant_id=${signupId}]`)?.get(0)) {

                                $(`[data-participant_id=${signupId}]`)?.get(0)?.scrollIntoView({behavior: 'smooth'})
                                $(`[data-participant_id=${signupId}]`)?.css("background-color", "#99ff99")
                                //$("#" + signupId).get(0).scrollIntoView({behavior: 'smooth'})
                                //$("#" + signupId).css("background-color", "#99ff99")
                                let status = $("#desired_status").val() || "participated"
                                RenderEventmanagerScanner.instance.updateParticipantStatus(event_id, signupId, status)
                                RenderEventmanagerScanner.instance.pressedKeys = ""
                            } else {
                                RenderEventmanagerScanner.instance.notifications.showHint("Teilnehmer nicht gefunden!", "error")
                            }
                        })
                    })


                } else {
                    RenderEventmanagerScanner.instance.pressedKeys += key
                }
            });


        }
        return RenderEventmanagerScanner.instance;
    }


    init() {
        return new Promise((resolve, reject) => {
            const urlParams = new URLSearchParams(window.location.search);
            let event_id = urlParams.get("event_id")
            RenderEventmanagerScanner.instance.em.getEventById(event_id, true).then(async function (event) {
                await RenderEventmanagerScanner.instance.em.setActiveEvent(event_id)
                $("#event_title").html(RenderEventmanagerScanner.instance.em.getActiveEvent().title)
                await RenderEventmanagerScanner.instance.renderParticipantList()
                resolve()

            })
        })
    }

    renderParticipantList() {
        let status_classes = {
            "booked": "badge-success", "reserved": "badge-warning", "canceled": "badge-danger"
        }
        return new Promise((resolve, reject) => {
            RenderEventmanagerScanner.instance.PbTpl.renderIntoAsync('eventmanager/templates/participant_list.html', {
                "event_data": RenderEventmanagerScanner.instance.em.getActiveEvent(), "status_classes": status_classes
            }, "#participant-list-container").then(function () {
                resolve()
            })
        })
    }

    toggleDetailView(active) {
        if (active == true) {
            $("#list-view").hide();
            $("#detail-view").show();
        } else {
            $("#list-view").show();
            $("#detail-view").hide();
            $('#participant-list').hide()
        }
    }


    compareParticipantEntriesByStatus(a, b) {
        let data_a = a[1]
        let data_b = b[1]
        let status_value_a = 0
        let status_value_b = 0
        switch (data_a.status) {
            case "booked":
                status_value_a = 0
                break;
            case "reserved":
                status_value_a = 1
                break;
            case "canceled":
                status_value_a = 2
                break;
            case "deleted":
                status_value_a = 3
                break;
        }
        switch (data_b.status) {
            case "booked":
                status_value_b = 0
                break;
            case "reserved":
                status_value_b = 1
                break;
            case "canceled":
                status_value_b = 2
                break;
            case "deleted":
                status_value_b = 3
                break;
        }

        if (status_value_a < status_value_b) {
            return -1;
        }
        if (status_value_b < status_value_a) {
            return 1;
        }
        return 0;
    }

    compareParticipantEntriesByName(a, b) {
        let data_a = a[1].raw.lastname + a[1].raw.firstname
        let data_b = b[1].raw.lastname + a[1].raw.firstname
        if (!data_a) data_a = ""
        if (!data_b) data_b = ""
        if (data_a.toLowerCase() < data_b.toLowerCase()) {
            return -1;
        }
        if (data_b.toLowerCase() < data_a.toLowerCase()) {
            return 1;
        }
        return 0;

    }

    async renderParticipants() {
        return new Promise((resolve, reject) => {
            alert("RENDERING LIST")
        })
    }

    updateParticipantStatus(event_id, participant_id, status) {
        console.log("updateParticipantStatusFunction", RenderEventmanagerScanner.instance.em)
        let event = RenderEventmanagerScanner.instance.em.getActiveEvent()
        let participant_data = event["participants"][participant_id]


        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/changeSignUpDataForEvent",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "apikey": getCookie("apikey"),
                "Content-Type": "application/json"
            },
            "data": JSON.stringify({
                "event_id": event_id,
                "signup_id": participant_id,
                "data": {
                    "raw": {
                        "firstname": participant_data.raw.firstname,
                        "lastname": participant_data.raw.lastname,
                        "email": participant_data.raw.email,
                        "info": participant_data.raw?.info || {},
                    },
                    "status": status,
                    "tags": participant_data.tags,
                    "success": function(response) {
                        console.log("SUCCESS", response)
                    }
                }
            }),
        };

        $.ajax(settings)
    }


    initListeners() {
        RenderEventmanagerScanner.instance.em.socket.commandhandlers["eventUpdatedSuccess"] = async function (args) {
            await RenderEventmanagerScanner.instance.init()
        }

        RenderEventmanagerScanner.instance.em.socket.commandhandlers["signUpDataForEventChangedSuccessfully"] = async function (args) {
            await RenderEventmanagerScanner.instance.init()
            let signupId = args.signup_id
            $(`[data-participant_id=${signupId}]`)?.get(0)?.scrollIntoView({behavior: 'smooth'})
            $(`[data-participant_id=${signupId}]`)?.css("background-color", "#99ff99")
            // $("#" + signupId).get(0).scrollIntoView({behavior: 'smooth'})
            // $("#" + signupId).css("background-color", "#99ff99")
        }


        $(document).on('click', 'a[data-name="quickaction_cancelparticipant"]', function (event) {
            event.stopPropagation()
            let id = event.target.dataset['id']
            let active_event = RenderEventmanagerScanner.instance.em.getActiveEvent()
            RenderEventmanagerScanner.instance.updateParticipantStatus(active_event.event_id, id, "canceled")
        })

        $(document).on('click', 'a[data-name="quickaction_reservedparticipant"]', function (event) {
            event.stopPropagation()
            let id = event.target.dataset['id']
            let active_event = RenderEventmanagerScanner.instance.em.getActiveEvent()
            RenderEventmanagerScanner.instance.updateParticipantStatus(active_event.event_id, id, "reserved")
        })

        $(document).on('click', 'a[data-name="quickaction_bookedparticipant"]', function (event) {
            event.stopPropagation()
            let id = event.target.dataset['id']
            let active_event = RenderEventmanagerScanner.instance.em.getActiveEvent()
            RenderEventmanagerScanner.instance.updateParticipantStatus(active_event.event_id, id, "booked")
        })

        $(document).on('click', 'a[data-name="quickaction_participatedparticipant"]', function (event) {
            event.stopPropagation()
            let id = event.target.dataset['id']
            let active_event = RenderEventmanagerScanner.instance.em.getActiveEvent()
            RenderEventmanagerScanner.instance.updateParticipantStatus(active_event.event_id, id, "participated")
        })

        $(document).on('click', 'a[data-name="quickaction_deleteparticipant"]', function (event) {
            event.stopPropagation()
            let id = event.target.dataset['id']
            let active_event = RenderEventmanagerScanner.instance.em.getActiveEvent()
            let deleteQuestion = new pbI18n().translate("Are you sure you want to delete the participant? This action cannot be reverted!")
            let deleteTitle = new pbI18n().translate("Delete participant")
            Fnon.Ask.Danger(deleteQuestion, deleteTitle, 'Okay', 'Abbrechen', (result) => {
                if (result == true) {
                    RenderEventmanagerScanner.instance.updateParticipantStatus(active_event.event_id, id, "deleted")
                }
            })
        })
    }
}