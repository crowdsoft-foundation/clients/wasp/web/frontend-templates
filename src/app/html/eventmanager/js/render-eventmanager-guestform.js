import { Event } from "./event.js?v=[cs_version]";
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import { multiStringReplace, getCookie } from "../../csf-lib/pb-functions.js?v=[cs_version]"
import { Eventmanager } from "./eventmanager.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"

export class RenderGuestForm {
    invite_event= undefined
    task_id = undefined
    constructor() {
		if (!RenderGuestForm.instance) {
			RenderGuestForm.instance = this;
			RenderGuestForm.instance.em = new Eventmanager();
			RenderGuestForm.instance.initListeners();
		}
		return RenderGuestForm.instance;
	}
    
    setInviteEvent(event) {
        RenderGuestForm.instance.invite_event = event
    }

    renderFormDetails() {
        if(getCookie("consumer_id") == "34febea3-51a7-4a48-aaaa-6f96bb7fadbb") {
            $("#agb_checkbox_wrapper").show()
        } else {
            $("#agb_checkbox_wrapper").remove()
        }

        let target= $("#event-form").html()
        let details = {
                'event-title': RenderGuestForm.instance.invite_event.title ,
                'event-startdate':  moment(RenderGuestForm.instance.invite_event.start).format('DD.MM.YYYY HH:mm'),
                'event-enddate':  moment(RenderGuestForm.instance.invite_event.end).format('DD.MM.YYYY HH:mm'),
                'event-description': RenderGuestForm.instance.invite_event.desc || "",
                'event-location': RenderGuestForm.instance.invite_event.location || "",
                'event-participant-booked': Object.entries(RenderGuestForm.instance.invite_event.participants).length,     
            }
        $("#event-form").html(`${multiStringReplace(details, target)}`)

        if(RenderGuestForm.instance.invite_event.state_machine_template == "default_registration_config") {
            $("#form_phone_element").hide()
        }


        RenderGuestForm.instance.renderOrganiser(RenderGuestForm.instance.invite_event.organisers)
    }

    renderOrganiser(organisers) {
        let organiserItem = $("#organizer_item").html()
        for (let [key, value] of Object.entries(organisers)) {
            
            let organizer_id = "organizer_" + key

            for (let [key, data] of Object.entries(value)) {
                if(key == "raw") {
                    
                    var details = {
                        'organizer-id':  organizer_id,
                        'organizer-name': data.name,
                        'organizer-email':  data.email
                    }

                     //view mode
                    let listItemContent = multiStringReplace(details, organiserItem)
                    $('#organizer_list').append(listItemContent);

                }
     
            }                
        }
    }

    checkEventTime() {
        let now = new moment()
        let event_ongoing = moment(RenderGuestForm.instance.invite_event.end).isAfter(now)
        console.log("event still going", event_ongoing)
        return event_ongoing
    }

    showEventOver() {
        $('#event-over-display').prop('hidden', false)
        $('#form-wrapper').hide()
    }

    checkInputFields(guestDetails) {
        $('#data_privacy_checkboxLabel').css("color", "initial")
        $('#agb_checkboxLabel').css("color", "initial")
        $('#firstnameError').hide()
        $('#lastnameError').hide()
        $('#emailError').hide()
        let valid = true
        if(guestDetails["personal_data"]["raw"]["firstname"] == ""){
            valid = false
            $('#firstnameError').show()
        }
        if(guestDetails["personal_data"]["raw"]["lastname"] == ""){
            valid = false
            $('#lastnameError').show()
        }
        if(guestDetails["personal_data"]["raw"]["email"] == ""){
            valid = false
            $('#emailError').show()
        }
        if(!$("#data_privacy_checkbox").is(':checked')){
            valid = false
            $('#data_privacy_checkboxLabel').css("color", "red")
        }

        if ($("#agb_checkbox").length != 0 && !$("#agb_checkbox").is(':checked')) {
            valid = false
            $('#agb_checkboxLabel').css("color", "red")
        }

        return valid
    }
    
    resetInputForm() {
        $('#firstname').val('')
        $('#lastname').val('')
        $('#phone').val('')
        $('#email').val('')
        $('#firstnameError').hide()
        $('#lastnameError').hide()
        $('#emailError').hide()
    }

    getGuestDetails() {
        let guestDetails = { "role": "participants",
                            "personal_data": {
                                "raw": {
                                "firstname": "",
                                "lastname": "",
                                "email": "",
                                "info": {
                                    "phone": ""
                                }
                                },
                                "status": "booked"
                            }
    }
        guestDetails["personal_data"]["raw"]["firstname"] =$('#firstname').val()
        guestDetails["personal_data"]["raw"]["lastname"] =$('#lastname').val()
        guestDetails["personal_data"]["raw"]["email"] =$('#email').val()
        guestDetails["personal_data"]["raw"]["phone"]  =$('#phone').val()
        return guestDetails
    }


    updateEventData(id) {
        console.log("Updating data in guest sign up")
        let data = undefined
        const setData = (input) => {
                data = input
        }
        RenderGuestForm.instance.invite_event.getEventById(id).then(response => {
            setData(Object.entries(response.events))
            let guest_event = new Event(data[0][0], data[0][1])
            RenderGuestForm.instance.setInviteEvent(guest_event)
            console.log("Stiil updating")
            RenderGuestForm.instance.renderFormDetails()
            
        })
    }

    initListeners() {  
        RenderGuestForm.instance.em.socket.commandhandlers["eventUpdatedSuccess"] = function (args) {
            
            // check if event_id iin args is active event
            //update data in Render and rerender
            if(args.event_id ==  RenderGuestForm.instance.invite_event.event_id){
                RenderGuestForm.instance.updateEventData(args.event_id)
                Fnon.Hint.Success("Events have been updated", {displayDuration: 2000})
            }
        }

        $(document).on('click', '#btn-submit-guest', (e) => {
            RenderGuestForm.instance.render_sucess = true
            e.preventDefault()
            RenderGuestForm.instance.checkEventTime()
            let details = RenderGuestForm.instance.getGuestDetails()
            let valid = RenderGuestForm.instance.checkInputFields(details)
            if(valid) {
                $("#btn-submit-guest").prop("disabled", true)
                RenderGuestForm.instance.invite_event.signUpParticipant(details, "participants").then(response => {
                    RenderGuestForm.instance.task_id = response.task_id
                    $('#success-display').prop('hidden', false)

                    if(response.task_id == undefined && response.last_condition_check_result) {
                        let message = new pbI18n().translate("We have received your registration. You will receive a confirmation email shortly.")
                        $('#success-display').html(message)
                    }

                    $("#btn-submit-guest").prop("disabled", false)
                })
               }            
        })

        // Check task_id
        RenderGuestForm.instance.em.socket.commandhandlers["signedUpForEventSuccessfull"] = function (args) {
            if(args.event_id ==  RenderGuestForm.instance.invite_event.event_id && RenderGuestForm.instance.task_id == args.task_id){
                $('#success-display').prop('hidden', false)
                RenderGuestForm.instance.resetInputForm()
                $("#btn-submit-guest").prop("disabled", false)
            }
        }

        RenderGuestForm.instance.em.socket.commandhandlers["signedUpForEventFailed"] = function (args) {
            if(args.reason == 'maxparticipants already reached' && RenderGuestForm.instance.task_id == args.task_id){
                $('#event-full-display').prop('hidden', false)
                $('#btn-submit-guest').prop('disabled', true)
            }
            
        }

        
        

    }
}