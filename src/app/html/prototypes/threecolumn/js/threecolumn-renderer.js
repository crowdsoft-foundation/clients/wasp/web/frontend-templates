import {PbTpl} from "../../../csf-lib/pb-tpl.js?v=[cs_version]"

export class ThreecolumnRenderer {

    constructor() {
        if (!ThreecolumnRenderer.instance) {

            this.tpl = new PbTpl()
            ThreecolumnRenderer.instance = this
            ThreecolumnRenderer.instance.handledTaskIds = []
        }

        return ThreecolumnRenderer.instance
    }

    init() {
        if ($("#loader-overlay")) {
            $("#loader-overlay").hide()
            $("#contentwrapper").show()
        }

        window.recalculateColumnWidths();
        window.updateActionMenuPosition();
    }
}