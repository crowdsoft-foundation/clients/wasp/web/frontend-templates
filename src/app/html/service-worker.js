const CACHE_NAME = "pbs-cache";
const toCache = [

];

self.addEventListener("install", function(event) {
    console.log("INSTALL");
    event.waitUntil(
        caches
            .open(CACHE_NAME)
            .then(function(cache) {
                return cache.addAll(toCache);
            })
            .then(self.skipWaiting())
    );
});

// self.addEventListener("fetch", function(event) {
//     console.log("FETCH");
//     event.respondWith(
//         fetch(event.request).catch(() => {
//             return caches.open(CACHE_NAME).then(cache => {
//                 return cache.match(event.request);
//             });
//         })
//     );
// });

self.addEventListener("activate", function(event) {
    console.log("ACTIVATE");
    event.waitUntil(
        caches
            .keys()
            .then(keyList => {
                return Promise.all(
                    keyList.map(key => {
                        if (key !== CACHE_NAME) {
                            console.log("[ServiceWorker] Removing old cache", key);
                            return caches.delete(key);
                        }
                    })
                );
            })
            .then(() => self.clients.claim())
    );
});
