import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]";

export class RenderConsumerPage {
    render_params = {}

    constructor() {
        if (!RenderConsumerPage.instance) {
            RenderConsumerPage.instance = this
        }

        return RenderConsumerPage.instance
    }

    init() {
        let self = this
        this.fetchConsumersFromApi().then(function () {
            PbTpl.instance.renderIntoAsync('snippets/sysadmin-dashboard-panel.tpl', self.render_params, "#portlet-card-list").then(() => {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            })
        })
    }

    fetchConsumersFromApi() {
        let self = this
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": CONFIG.API_BASE_URL + "/system/get_consumers",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey")
                },
                "success": (response) => {
                        self.handleFetchConsumersResponse(response)
                        resolve(response)
                },
                "error": () => alert("Oooops, something went wrong. Sorry! please try again.")
            };

            $.ajax(settings)
        })
    }

    handleFetchConsumersResponse(response) {
        this.render_params = {}
        this.render_params["panels"] = []

        for(let consumer of response) {
            let login_list = []
            for(let login of consumer.logins) {
                let login_entry = {
                        "id": `${login.id}`,
                        "title": `${login.username} (${moment.unix(login.created_at).format(CONFIG.DATEFORMAT)})<br>LL: ${ login.last_login ? moment(login.last_login).format(CONFIG.DATETIMEFORMAT): "never"}`,
                        "href": `./user_rights.html?u=user.${login.username}&cid=${consumer.custom_id}`,
                        "permission": "",
                        "iconclasses": "fa fa-user"
                    }
                login_list.push(login_entry)
            }


            let consumer_panel = {
                "title": `${consumer.username} <br/><a data-consumeroptionlink="clear" id="clearData_${consumer.id}_${consumer.custom_id}" href="#" style="padding: 3px;font-size:12px;border: 1px solid red;">Clear Data</a><br/>
<a data-consumeroptionlink="delete" id="deleteConsumer_${consumer.id}_${consumer.custom_id}" style="padding: 3px;font-size:12px;border: 1px solid red;" href="#">Delete Consumer</a>`,
                "subtitle": `ID: ${consumer.custom_id}<br/>Created at: ${moment.unix(consumer.created_at).format(CONFIG.DATEFORMAT)} <br/> Created by: ${consumer.registration_email}`,
                "icon_src": "/style-global/img/icons/contact.png",
                "links": login_list
            }
            this.render_params.panels.push(consumer_panel)
        }
    }
}
