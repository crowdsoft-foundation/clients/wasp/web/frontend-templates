import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"

export class RenderSysadminPage {
    render_params = {
        "panels": [
            {
                "title": "Consumers",
                "subtitle": "Show all consumers and corresponding logins",
                "icon_src": "/style-global/img/icons/calendar.png",
                "links": [
                    {
                        "id": "dummy_id",
                        "title": "Management",
                        "href": "./consumers.html",
                        "permission": "",
                        "iconclasses": "fa fa-users"
                    }
                ]
            }
        ]
    }

    constructor() {
        if (!RenderSysadminPage.instance) {
            RenderSysadminPage.instance = this
        }

        return RenderSysadminPage.instance
    }

    init() {
        PbTpl.instance.renderIntoAsync('snippets/sysadmin-dashboard-panel.tpl', this.render_params, "#portlet-card-list").then(()=>{
            $("#loader-overlay").hide()
            $("#contentwrapper").show()
        })
    }
}
