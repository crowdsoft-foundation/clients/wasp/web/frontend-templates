import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {
    getCookie,
    setCookie,
    fireEvent,
    getQueryParam,
    makeId,
    localspeech,
    speech,
    waitFor
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {PbFroala} from "./pb-froala.js?v=[cs_version]"
import {PbJitsi} from "./pb-jitsi.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {pbShoppingSession} from "../../csf-lib/pb-shoppingsession.js?v=[cs_version]"
import {PbVideoconference} from "../../csf-lib/pb-videoconference.js?v=[cs_version]"


export class Easy2GetherModule {
    actions = {}
    socketio = undefined
    agent_is_available_interval = undefined
    agent_is_available = true
    guest_room_id = ""

    constructor() {
        if (!Easy2GetherModule.instance) {

            this.guest_room_id = this.generateTmpRoomId()
            this.socketio = new socketio()
            Easy2GetherModule.instance = this
            window.setTimeout( function() {
                Easy2GetherModule.instance.socketio.joinRoom("tmp_easy2gether")
                Easy2GetherModule.instance.socketio.joinRoom(Easy2GetherModule.instance.guest_room_id)
            }, 1000)

            this.initListeners()
            this.initActions()

            $.unblockUI()

        }

        return Easy2GetherModule.instance
    }

    generateTmpRoomId() {
        return "tmp_" + makeId(20)
    }

    closeCall() {
        if(PbJitsi.instance) {
            PbJitsi.instance.closeCall()
        }
        Easy2GetherModule.instance.agent_is_available = true

    }

    initActions() {
        this.actions["easy2getherInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                $("#meetName").val(getCookie("username"))
            })
            callback(myevent)
        }

        this.actions["easy2getherGuestModeInitialised"] = function (myevent, callback = () => "") {
            let data = {"room_id": this.guest_room_id}
            Easy2GetherModule.instance.agent_is_available_interval = window.setInterval(function () {
                fireEvent("easy2getherIsAgentAvailable", data, "tmp_easy2gether")
            }, 1000)
            callback(myevent)
        }

        this.actions["easy2getherIsAgentAvailable"] = function (myevent, callback = () => "") {
            console.log("Sending agent is available")
            if(Easy2GetherModule.instance.agent_is_available == true) {
                fireEvent("easy2getherAgentIsAvailable", {}, "tmp_easy2gether")
            }
            callback(myevent)
        }

        this.actions["easy2getherAgentIsAvailable"] = function (myevent, callback = () => "") {
            console.log("Agent is available")
            fireEvent("easy2getherNewGuest", {"room": Easy2GetherModule.instance.guest_room_id}, "tmp_easy2gether")
            $("#overlay_message").html("Aktiver Gesprächspartner wurde gefunden. Bitte warte bis dein Gesprächspartner dem Gespräch beitritt, du wirst dann automatisch zu diesem verbunden. In der Zwischenzeit kannst du den Videochat einrichten und bereits betreten.")
            window.clearInterval(Easy2GetherModule.instance.agent_is_available_interval)

            let username = prompt("Verrate uns doch bitte wie wir dich ansprechen dürfen:", "Benutzername");
            setCookie("username", username)

            window.setTimeout(function () {
                new PbAccount().init().then(() => {
                    new PbFroala().init("#editor", Easy2GetherModule.instance.guest_room_id)
                    new PbJitsi().init("#meet", username, Easy2GetherModule.instance.guest_room_id)

                    $('#meetName').val(username)
                    PbJitsi.instance.setDisplayName($('#meetName').val())

                    $("#loader-overlay").hide()
                    $("#contentwrapper").show()
                })
            }, 1000)

            callback(myevent)
        }

        this.actions["easy2getherNewGuest"] = function (myevent, callback = () => "") {
            console.log("GOT EVENT easy2getherNewGuest", myevent.data)
            let editor = $('#editor')[0]['data-froala.editor']

            if (editor) {
                editor.html.set("")
                editor.edit.on()
                editor.destroy()
            }
            Easy2GetherModule.instance.agent_is_available = false
            localspeech("Neuer EasyToGether Gast wartet.", true)
            new PbFroala(true).init("#editor", myevent.data.room)
            new PbJitsi().init("#meet", $("#meetName").val(), myevent.data.room)
            callback(myevent)
        }

        this.actions["easy2getherProtocolUpdate"] = function (myevent, callback = () => "") {
            let editor = $('#editor')[0]['data-froala.editor']

            if (editor) {
                if (myevent.data.editorName != getCookie("username")) {
                    $("#editorTypeStatus").html(myevent.data.editorName + " gibt gerade etwas ein...")
                    editor.html.set(myevent.data.protocolContent);
                    editor.edit.off()
                    if (this.typeTimeout) {
                        clearTimeout(this.typeTimeout)
                    }
                    this.typeTimeout = setTimeout(function () {
                        editor.edit.on()
                        $("#editorTypeStatus").html("&nbsp;")
                    }, 2000)

                }
            }
            callback(myevent)
        }
    }

    initListeners() {
        $("#closeCallButton").click(function(){
            new Easy2GetherModule().closeCall()
        })

        $('#meetName').keyup(function (e) {
            setCookie("username", $('#meetName').val())
            PbJitsi.instance.setDisplayName($('#meetName').val())
        });

        $('#lobbyModeButton').click(function () {
            if (!$('#lobbyModeButton').data("lockedState") || $('#lobbyModeButton').data("lockedState") == "false") {
                PbJitsi.instance.setLobbyMode(true)
                $('#lobbyModeButton').data("lockedState", "true")
                $('#lobbyModeButton').html(new pbI18n().translate("Unlock room"))
            } else {
                PbJitsi.instance.setLobbyMode(false)
                $('#lobbyModeButton').data("lockedState", "false")
                $('#lobbyModeButton').html(new pbI18n().translate("Lock room"))
            }
        })
    }

}



export class Easy2GetherProModule {
    actions = {}
    socketio = undefined
    agent_is_available_interval = undefined

    constructor() {

        if (!Easy2GetherProModule.instance) {
            this.socketio = new socketio()
            this.socketio.joinRoom("tmp_easy2gether")
            this.initListeners()
            this.initActions()

            $.unblockUI()
            Easy2GetherProModule.instance = this
        }

        return Easy2GetherProModule.instance
    }

    initActions() {
        this.actions["init"] = function (myevent, callback = () => "") {
            console.log("INIT")

            new pbI18n().init()
            $("#language_change_test").on("click", function (event) {
                new pbI18n().getTranslations("en")
            })

            $("#language_change_en").on("click", function (event) {
                new pbI18n().getTranslations("en")
            })
            $("#language_change_de").on("click", function (event) {
                new pbI18n().getTranslations("de")
            })


            $('*[data-dynamicvalue="username"]').html(getCookie("username"))

            if (getQueryParam("mid")) {
                $(window).off('beforeunload');
                $(window).bind('beforeunload', function () {
                    new pbShoppingSession().aboutToLeave()
                    new PbVideoconference().hangup()
                    var myInterval = window.setInterval(function () {
                        new pbShoppingSession().reconnectToConference()
                        clearInterval(myInterval)
                    }, 5000)
                    //return 'Willst Du die Seite wirklich verlassen?';
                });

                $("#sessionTopicModal").modal()

                let agentOnlineTimer = setTimeout(function () {
                    fireEvent("agentIsOffline", {"timeout_id": agentOnlineTimer}, getCookie("consumer_name"))
                }, 3000)

                fireEvent("isAgentOnline", {"timeout_id": agentOnlineTimer}, getCookie("consumer_name"))
            }

            callback(myevent)
        }

        this.actions["initGuest"] = function (myevent, callback = () => "") {
            if (false && !new PbAccount().hasPermission("simplycollect.merchant_view")) {
                $("#overlay").show()
                $("#contentwrapper").hide()
                new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
            } else {
                new PbAccount().handlePermissionAttributes()
                $("#overlay").hide()
                $("#contentwrapper").show()
                callback(myevent)
            }

            setInterval(function () {
                fireEvent("agentStatus", {
                    "online": true,
                    "queue_length": $('#queue_length').html()
                }, getCookie("consumer_name"))
            }, 5000)
        }

        this.actions["initAgent"] = function (myevent, callback = () => "") {
            if (false && !new PbAccount().hasPermission("simplycollect.merchant_view")) {
                $("#overlay").show()
                $("#contentwrapper").hide()
                new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
            } else {
                new PbAccount().handlePermissionAttributes()
                $("#overlay").hide()
                $("#contentwrapper").show()
                callback(myevent)
            }

            setInterval(function () {
                fireEvent("agentStatus", {
                    "online": true,
                    "queue_length": $('#queue_length').html()
                }, getCookie("consumer_name"))
            }, 5000)
        }

        this.actions["agentStatus"] = function (myevent, callback = () => "") {
            $('#queueEntries').html(myevent.data.queue_length)
        }

        this.actions["isAgentOnline"] = function (myevent, callback = () => "") {
            fireEvent("agentIsOnline", myevent.data, getCookie("consumer_name"))
        }

        this.actions["agentIsOnline"] = function (myevent, callback = () => "") {
            alert("Agent is online")
            clearTimeout(myevent.data.timeout_id)
            $("#waitingForSessionMessage").show()
            $("#merchantNotOnlineMessage").hide()
            let merchant = getCookie("consumer_name")
            let customerName = getCookie("username")
            let customerInterest = ""
            var interval = window.setInterval(function () {
                customerInterest = $("#session_topic").val()
                new pbShoppingSession().request(merchant, customerName, customerInterest, interval)
            }, 5000)

            new pbShoppingSession().request(merchant, customerName, customerInterest, interval)

        }

        this.actions["customerInRoomIsLeaving"] = function (myevent, callback = () => "") {
            $('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]').css("background-color", "#ff0000")
            $('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]').fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100)
            window.setTimeout(function () {
                $('.last_status[data-srid="' + myevent.data.room_id + '"]').html("vermutlich gegangen")
                $('.queueEntryCloseButton[data-srid="' + myevent.data.room_id + '"]').show()
                new pbShoppingSession().cleanUp(false, false)
            }, 15000)
            callback(myevent)
        }

        this.actions["customerInRoomHasLeft"] = function (myevent, callback = () => "") {
            $('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]').css("background-color", "#ff0000")
            $('.queueEntryContainer[data-srid="' + myevent.data.room_id + '"]').fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100)
            window.setTimeout(function () {
                $('.last_status[data-srid="' + myevent.data.room_id + '"]').html("gegangen")
            }, 5000)
            callback(myevent)
        }
    }

    initListeners() {
        $('#customer_queue_wrapper').bind('DOMSubtreeModified', function () {
            $('#queue_length').html(($('.queueEntryContainer').length - 1)) // Subtract one to account for the template
            $('#queue_length_tab').html(($('.queueEntryContainer').length - 1))// Place the value on the tab too
            $('.queueEntryAcceptButton').off('click')
            $('.queueEntryAcceptButton').on('click', function (event) {
                event.stopPropagation();
                event.stopImmediatePropagation();
                jQuery('#bell').prop("muted", true);
                let srid = event.target.getAttribute("data-srid")
                new pbShoppingSession().acceptedBy(srid, getCookie("username"))
                new pbShoppingSession().connectToConference(srid)
                $(event.target).next().show()
                // $('.queueEntryContainer[data-srid="' + srid + '"]').css("background-color", "#3bb001")
                $('.queueEntryContainer[data-srid="' + srid + '"]').removeClass('new')
                $('.queueEntryContainer[data-srid="' + srid + '"]').addClass('selected')


            });

            $('.queueEntryCloseButton').off('click')
            $('.queueEntryCloseButton').on('click', function (event) {
                event.stopPropagation();
                event.stopImmediatePropagation();
                jQuery('#bell').prop("muted", false);
                new pbShoppingSession().quit("Abgelehnt", event.target.getAttribute("data-srid"))
                $(event.target).hide()
            });
        });
    }

}