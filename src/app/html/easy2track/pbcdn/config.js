export const CONFIG = {}
if (document.location.href.indexOf("http://localhost") == 0) {
    console.log("Using DEV-Config")
    CONFIG.API_BASE_URL = "http://localhost:8000"
    CONFIG.APP_BASE_URL = "http://localhost:7000"
    CONFIG.APP_STARTPAGE = "/login/"
    CONFIG.SOCKET_SERVER = "http://localhost:8000"
    CONFIG.COOKIE_DOMAIN = "localhost"
    CONFIG.DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.SYSTEM_MESSAGE = ""
    CONFIG.JS_CACHE_ACTIVE = false
    CONFIG.SEND_CONSOLE_LOG = false
    CONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT = 5000
    CONFIG.FILE_LOAD_FAILED_MAX_RETRIES = 10
    CONFIG.DB_DATEFORMAT = "YYYY-MM-DD HH:mm:ss"
    CONFIG.ENFORCE_SSL = false
} else if (document.location.href.indexOf("testblick.de") != -1) {
    console.log("Using NEW LIVE-Config")
    CONFIG.API_BASE_URL = "https://odin.planblick.com"
    CONFIG.APP_BASE_URL = "https://testblick.de/"
    CONFIG.APP_STARTPAGE = "/login/"
    CONFIG.SOCKET_SERVER = "https://odin.planblick.com"
    CONFIG.COOKIE_DOMAIN = "odin.planblick.de"
    CONFIG.DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.SYSTEM_MESSAGE = ""
    CONFIG.JS_CACHE_ACTIVE = false
    CONFIG.SEND_CONSOLE_LOG = false
    CONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT = 3000
    CONFIG.FILE_LOAD_FAILED_MAX_RETRIES = 10
    CONFIG.DB_DATEFORMAT = "YYYY-MM-DD HH:mm:ss"
    CONFIG.ENFORCE_SSL = false
} else {
    console.log("Using LIVE-Config")
    CONFIG.API_BASE_URL = "https://thor.planblick.com"
    CONFIG.APP_BASE_URL = "https://www.serviceblick.com"
    CONFIG.APP_STARTPAGE = "/login/"
    CONFIG.SOCKET_SERVER = "https://thor.planblick.com"
    CONFIG.COOKIE_DOMAIN = "www.serviceblick.com"
    CONFIG.DATEFORMAT = "dd.MM.yyyy HH:mm:ss"
    CONFIG.DEFAULT_LOGIN_REDIRECT = "/dashboard/"
    CONFIG.SYSTEM_MESSAGE = ""
    CONFIG.JS_CACHE_ACTIVE = false
    CONFIG.SEND_CONSOLE_LOG = false
    CONFIG.FILE_LOAD_FAILED_RETRY_TIMEOUT = 3000
    CONFIG.FILE_LOAD_FAILED_MAX_RETRIES = 10
    CONFIG.DB_DATEFORMAT = "YYYY-MM-DD HH:mm:ss"
    CONFIG.ENFORCE_SSL = true
}
