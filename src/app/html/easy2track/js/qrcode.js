import {fireEvent, getCookie, encrypt, decrypt} from "../pbcdn/functions.js?v=[cs_version]"
import {socketio} from "./sockets.js?v=[cs_version]";
import {CONFIG} from '../pbcdn/config.js?v=[cs_version]';

export class qrcode {
    constructor() {
        if (qrcode.instance) {
            return qrcode.instance
        }
        self.qrcode = null
        qrcode.instance = this
        return qrcode.instance

    }

    init() {
        let params = new URLSearchParams(window.location.search);
        let showclicklink = params.get('showclicklink')
        if(showclicklink !=null && showclicklink == 1) {
            $("#current_value_link").show()
        }
        qrcode.instance.qrcode = new QRCode(document.getElementById("qrcode"), {
            text: "",
            width: 256,
            height: 256,
            colorDark : "#000000",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.L
        })

        fireEvent('qrcode_initialized')
        new socketio().fireEvent(decodeURIComponent(getCookie("consumer_name")), "qr_code_expired")
        window.setInterval(function() {new socketio().fireEvent(decodeURIComponent(getCookie("consumer_name")), "qr_code_expired")}, 30000)
    }

    new_qr_code(value=null) {
        let params = new URLSearchParams(window.location.search);
        var pathParts = window.location.pathname.split('/');
        var path = window.location.pathname.replace(pathParts[pathParts.length-1],'');
        let consumer = params.has('consumer') ? params.get("consumer"): getCookie("consumer_name")
        let login = params.has('login') ? params.get("login"): getCookie("username")
        let page_url = params.has('page_url') ? params.get("page_url"): path + "contact.html"
        let gak = params.has('gak') ? params.get("gak"): getCookie("guestapikey")
        let image_url = params.has('image') ? params.get("image"): "./images/logo.png"

        if(value == null) {
            let uid = params.get('uid')

            if(uid !=null && uid.length > 0) {
                value = uid
            } else {
                value = qrcode.instance.make_id(50)
            }
        }
        let data = {
            rootConsumername: consumer,
            rootUsername: login,
            gak: gak,
            uid: value
        }
        data = encrypt(JSON.stringify(data), value)
        console.log("DECRYPTED", decrypt(data,value))
        let qrCodeContent = CONFIG.APP_BASE_URL + page_url + "?uid=" + value + "&data=" + encodeURIComponent(data)

        //let params = new URLSearchParams(window.location.search);
        let redirect = params.get('r')
        if(redirect) {
            qrCodeContent += "&r=" + redirect
        }

        qrcode.instance.qrcode.clear(); // clear the code.
        $("#current_value").html(qrCodeContent)
        $("#current_value_link").attr("href", qrCodeContent)

        qrcode.instance.qrcode.makeCode(qrCodeContent); // make another code


        if(qrcode.instance.qrcode) {
            qrcode.instance.qrcode.clear()
        }
        console.log($("#transparent").is(':checked'))
        qrcode.instance.qrcode = new QRCode(document.getElementById("qrcode"), {
            text: qrCodeContent,
            //width: 380,
            //height: 380,
            colorDark : "#000000",
            colorLight : "#ffffff",
            correctLevel : QRCode.CorrectLevel.H,
            logo: image_url,
            logoWidth: 50,
            logoHeight: 50,
            logoBackgroundColor: "#ffffff",
            logoBackgroundTransparent: true
        });
        $("#qrcode").appendTo($("#current_value_link"))
    }

    make_id(length) {
       var result           = '';
       var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
       var charactersLength = characters.length;
       for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
       }

       return result;
    }


    static saveQRCode() {
        document.getElementById("qr_image_link").click()
        }

    
    static ImagetoPrint(source) {
        return "<html><head><scri" + "pt>function step1(){\n" +
            "setTimeout('step2()', 10);}\n" +
            "function step2(){window.print();window.close()}\n" +
            "</scri" + "pt></head><body onload='step1()'>\n" +
            "<img width='250px' src='" + source + "' /></body></html>";
        }
    
    static openQRForPrint(src_image_id) {
        var source = document.getElementById(src_image_id).src
        var Pagelink = "about:blank";
        var pwa = window.open(Pagelink, "_new");
        pwa.document.open();
        pwa.document.write(qrcode.ImagetoPrint(source));
        pwa.document.close();
        }
    
    static correctQR(selector) {
        $("#save_image_button").hide()
        $("#print_image_button").hide()
        $("#correct_image_button").hide()
        $("#qr_image").hide("slow", function () {
            let pos = $(selector).offset().top
            $('html, body').animate({
                scrollTop: pos - 200
            }, 'slow');
        })
    
        }



}