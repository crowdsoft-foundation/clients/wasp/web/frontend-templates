import {CONFIG} from '../pbcdn/config.js?v=[cs_version]';
import {decrypt} from '../pbcdn/functions.js?v=[cs_version]';
import {getCookie, create_UUID} from "../pbcdn/functions.js?v=[cs_version]";


export function setTemporaryGuestKey(key) {
    $.cookie("guestapikey", key, {path: '/'});
}

export function setApiKey() {
    let params = new URLSearchParams(window.location.search);
    let uid = params.get('uid')

    try {
        let data = JSON.parse(decrypt(params.get('data'), uid));
        $.cookie("current_consumer", data["rootConsumername"], {path: '/'});
        //console.log(data)
        if (data["uid"] == uid) {
            $.cookie("apikey", data["gak"], {path: '/'});
        } else {
            alert("Sorry da ist etwas schief gelaufen, versuch es bitte noch einmal.")
            console.error("Cannot decrypt access-data")
            throw "Cannot decrypt access-data"

        }
    } catch (err) {
        console.error(err)
        return false
    }
}

export function getDataToAddContact() {
    let params = new URLSearchParams(window.location.search);
    let uid = params.get('uid')

    try {
        let data = JSON.parse(decrypt(params.get('data'), uid));
        console.log(data)
        if (data["uid"] == uid) {
            return data
        } else {
            console.error("Cannot decrypt access-data")
            throw "Cannot decrypt access-data"

        }
    } catch (err) {
        return false
    }
}

export function createQRCode() {
    let form_data = {}
    let formcheck_error = false
    $("input[class=form-control]").each(function (index) {
        form_data[this.id] = this.value
        if ((this.id == 'lastname' | this.id == 'firstname' | this.id == 'phone' | this.id == 'zipcode') && this.value.length < 1) {
            formcheck_error = true
        }
    })

    if (formcheck_error) {
        alert("Bitte füllen Sie alle Pflichteingabefelder aus.")
        return
    }
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');

    jQuery.ajax({
        type: 'POST',
        url: CONFIG.API_BASE_URL + "/easy2track/qr",
        data: JSON.stringify({data: form_data}),
        cache: false,
        headers: {
            "Content-Type": ["application/json", "application/json"]
        },
        xhr: function () {// Seems like the only way to get access to the xhr object
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'blob'
            return xhr;
        },
        success: function (data) {
            var img = document.getElementById('qr_image');
            var link = document.getElementById('qr_image_link');
            var url = window.URL || window.webkitURL;
            let imageUrl = url.createObjectURL(data);
            img.src = imageUrl
            img.onload = function () {
                $("#qr_image").show("slow", function () {
                    $("#save_image_button").show()
                    $("#print_image_button").show()
                    $("#correct_image_button").show()
                });
            };
            link.href = imageUrl
            link.download = "meinQRCode.png"
            // document.querySelector("body").scrollIntoView({
            //       behavior: 'smooth'
            //     });


            $("#qrcode_generate_button").html("QR-Code neu erstellen")


        },
        error: function () {

        }
    });

}

export function saveFromQr(data, mode = "add") {
    let form_data = data
    console.log("FORMDATA", form_data)
    let payload = {}
    var url = ""

    if (mode == "add") {
        if (form_data["hash"] !== undefined) {
            url = CONFIG.API_BASE_URL + "/es/cmd/addContactV2"
            payload = JSON.stringify({
                "consumer": getCookie("consumer_name"),
                "login": getCookie("username"),
                "external_id": form_data["hash"],
                "hash": form_data["hash"],
                "id": form_data["id"],
                "addedtime": moment().tz("Europe/Berlin").format(CONFIG.DB_DATEFORMAT)
            })
        } else {
            url = CONFIG.API_BASE_URL + "/es/cmd/addContact"
            payload = JSON.stringify({
                "consumer": getCookie("consumer_name"),
                "login": getCookie("username"),
                "external_id": create_UUID(),
                "email": "",
                "form_data": form_data,
                "addedtime": moment().tz("Europe/Berlin").format(CONFIG.DB_DATEFORMAT)
            })
        }
    } else if (mode == "remove") {
        url = CONFIG.API_BASE_URL + "/es/cmd/removeContact"
        if (form_data["hash"] !== undefined) {
            payload = JSON.stringify({
                "consumer": getCookie("consumer_name"),
                "login": getCookie("username"),
                "external_id": form_data["hash"],
                "hash": form_data["hash"],
                "id": form_data["id"],
                "deletedtime": moment().tz("Europe/Berlin").format(CONFIG.DB_DATEFORMAT)
            })
        } else {
            payload = JSON.stringify({
                "consumer": getCookie("consumer_name"),
                "login": getCookie("username"),
                "external_id": create_UUID(),
                "email": "",
                "form_data": form_data,
                "deletedtime": moment().tz("Europe/Berlin").format(CONFIG.DB_DATEFORMAT)
            })
        }
    }

    if (form_data != false) {
        var settings = {
            "url": url,
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": ["application/json", "application/json"],
                "apikey": getCookie("apikey")
            },
            "data": payload
        };

        $.ajax(settings).success(function (response) {
            let params = new URLSearchParams(window.location.search);
            let redirect = params.get('r')
            if (redirect) {
                location.href = redirect;
                return
            }
            //$.blockUI({message: '<div class="responseMessage">Vielen Dank für deine Kontaktdaten.</div>'})
            //setTimeout($.unblockUI, 3000)
        });
    } else {
        $.blockUI({message: '<div class="responseMessage">Sorry da ist etwas schief gelaufen, versuch es bitte noch einmal.</div>'})
        setTimeout($.unblockUI, 3000)
    }
}

export function saveContact() {
    let access_data = getDataToAddContact()
    let form_data = {}
    $("input").each(function (index) {
        form_data[this.id] = this.value
    })

    if (access_data != false) {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/addContact",
            "method": "POST",
            "timeout": 0,
            "async": false,
            "headers": {
                "Content-Type": ["application/json", "application/json"],
                "apikey": access_data["gak"]
            },
            "data": JSON.stringify({
                "consumer": access_data["rootConsumername"],
                "login": access_data["rootUsername"],
                "external_id": access_data["uid"],
                "email": $("#contact_email").val(),
                "form_data": form_data,
                "addedtime": moment().tz("Europe/Berlin").format(CONFIG.DB_DATEFORMAT)
            }),
            "success": function (response) {
                var expire_date = new Date();
                expire_date.setHours(23, 59, 59, 999);
                $.cookie("last_consumer_checkin", access_data["rootConsumername"], {path: '/', expires: expire_date})
                $.cookie("last_taskid_checkin", response.task_id, {path: '/', expires: expire_date})

                let params = new URLSearchParams(window.location.search);
                let redirect = params.get('r')
                if (redirect) {
                    $.blockUI({message: '<div class="responseMessage">Vielen Dank für deine Kontaktdaten. Wir haben diese jetzt verschlüsselt gespeichert.<br/>Bitte melde dich hier wieder ab wenn Du gehst.</div>'})
                    window.setTimeout(function () {
                        //location.href = redirect;
                        window.open(redirect);
                        window.setTimeout(function () {
                            location.reload(), 4000
                        })
                    }, 1000)
                    return
                } else {
                    $.blockUI({message: '<div class="responseMessage">Vielen Dank für deine Kontaktdaten. Wir haben diese jetzt verschlüsselt gespeichert.<br/>Bitte melde dich hier wieder ab wenn Du gehst.</div>'})
                    window.setTimeout(function () {
                        location.reload()
                    }, 5000)
                }

            }
        };

        $.ajax(settings).done();
    } else {
        $.blockUI({message: '<div class="responseMessage">Sorry da ist etwas schief gelaufen, versuch es bitte noch einmal.</div>'})
        setTimeout($.unblockUI, 3000)
    }
}

export function removeContact() {
    let access_data = getDataToAddContact()
    let form_data = {}
    $("input").each(function (index) {
        form_data[this.id] = this.value
    })

    if (access_data != false) {
        var settings = {
            "url": CONFIG.API_BASE_URL + "/es/cmd/removeContact",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Content-Type": ["application/json", "application/json"],
                "apikey": access_data["gak"]
            },
            "data": JSON.stringify({
                "consumer": access_data["rootConsumername"],
                "login": access_data["rootUsername"],
                "external_id": getCookie("last_taskid_checkin"),
                "email": $("#contact_email").val(),
                "form_data": form_data,
                "deletedtime": moment().tz("Europe/Berlin").format(CONFIG.DB_DATEFORMAT)
            }),
            "success": function (response) {
                $.removeCookie("last_consumer_checkin", {path: '/'})
                $.removeCookie("last_taskid_checkin", {path: '/'})
                let params = new URLSearchParams(window.location.search);
                let redirect = params.get('r')
                if (redirect) {
                    //location.href=redirect;
                    $.blockUI({message: '<div class="responseMessage">Vielen Dank fürs abmelden. <br/>Du kannst diese Seite jetzt schließen.</div>'})
                    window.setTimeout(function () {
                        location.reload()
                    }, 5000)
                    return
                } else {
                    $.blockUI({message: '<div class="responseMessage">Vielen Dank fürs abmelden. <br/>Du kannst diese Seite jetzt schließen.</div>'})
                    window.setTimeout(function () {
                        location.reload()
                    }, 5000)
                }

            }
        };

        $.ajax(settings).done();
    } else {
        $.blockUI({message: '<div class="responseMessage">Sorry da ist etwas schief gelaufen, versuch es bitte noch einmal.</div>'})
        setTimeout($.unblockUI, 3000)
    }
}


export function abort_scan() {
    $("#check-in-button").prop("onclick", null).off("click")
    $("#check-out-button").prop("onclick", null).off("click")
    $("#data-preview").html("")
    $("#data-preview").hide()
    $("#check-in-button").removeClass( "suggest")
    $("#check-out-button").removeClass( "suggest")
    $("#preview").show()
    $("#abort-scan-button").hide()
}

export function check_in(data) {

    if(data.data !== undefined) {
        saveFromQr(data.data, "add")
    } else {
        saveFromQr(data, "add")
    }
    $("#check-in-button").prop("onclick", null).off("click");
    $("#check-out-button").prop("onclick", null).off("click");
    $("#data-preview").html("")
    $("#data-preview").hide()
    $("#check-in-button").removeClass( "suggest")
    $("#check-out-button").removeClass( "suggest")
    $("#preview").show()
    $("#abort-scan-button").hide()
}

export function check_out(data) {

    if(data.data !== undefined) {
        saveFromQr(data.data, "remove")
    } else {
        saveFromQr(data, "remove")
    }
    $("#check-in-button").prop("onclick", null).off("click");
    $("#check-out-button").prop("onclick", null).off("click");
    $("#data-preview").html("")
    $("#data-preview").hide()
    $("#check-in-button").removeClass( "suggest")
    $("#check-out-button").removeClass( "suggest")
    $("#preview").show()
    $("#abort-scan-button").hide()
}

var automatic_mode_timeout=null
 export  function automatic_mode_toggle(event) {
            event.preventDefault();
            if($("#automatic-mode-button").hasClass("active")) {
                $("#automatic-mode-button").removeClass("active")
                $("#check-in-button").removeClass("suggest")
                $("#check-out-button").removeClass("suggest")
                $("#automatic-mode-button").html("Automatik aktivieren")
                if(automatic_mode_timeout != null) {
                    clearInterval(automatic_mode_timeout);
                }
            } else {
                $("#automatic-mode-button").addClass("active")
                $("#automatic-mode-button").html("Automatik deaktivieren")
                automatic_mode_timeout = setInterval(function () {
                                                                    if($("#check-in-button").hasClass("suggest")) {
                                                                        $("#check-in-button").click()
                                                                    }
                                                                    if($("#check-out-button").hasClass("suggest")) {
                                                                        $("#check-out-button").click()
                                                                    }
                                                                }, 1000);
            }
        }

var isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
//external variable for alternative camera selection in non firefox browsers
export function initScanner() {
    
    let scanner = new Instascan.Scanner({
        continuous: true,
        video: document.getElementById('preview'),
        mirror: true,
        backgroundScan: true,
        refractoryPeriod: 5000,
        scanPeriod: 1
    });
    scanner.addListener('scan', function (content) {
        console.log(content);

        let data = JSON.parse(decodeURIComponent(escape(content)))
        if(data["hash"] !== undefined) {
            is_checked_in(data["hash"])
        } else {
            is_checked_in(data)
        }
        $("#check-in-button").click(function() { check_in(data) })
        $("#check-out-button").click(function() { check_out(data) })

        let display = ""
        for (let key in data.check) {
            display += key + ": " + data.check[key] + "<br/>"
        }
        $("#abort-scan-button").show()
        $("#data-preview").html(display)
        $("#preview").hide()
        $("#data-preview").show()
        
        $("#abort-scan-button").click(function () {
            abort_scan()
        })

    });
    Instascan.Camera.getCameras().then(function (cameras) {
        console.log("CAMERAS", cameras)
        let i = 0
        let items = []
        for(var camera of cameras) {
            console.log(camera)
            if(camera.name != null)
                items.push({name: camera.name, value: i})
            i++;
        }
        console.log("ITEMS", items)
        $('#camera-selection-wrapper').dropdown({
            values: items
        });

        $("#camera_index").change(function() {
            scanner.stop()
            scanner.start(cameras[$("#camera_index").val()]);
        })

        if(!isFirefox) {
            $("#camera-selection-wrapper").show()
        }

        if (cameras.length > 0) {
            if(cameras[0].name != null) {
                $("#camera-selection-default-text").html(cameras[0].name)
            }

            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });
}

export function is_checked_in(data) {
    console.log("DATA", data)
    let payload = ""
    if(data.data === undefined) {
        payload = {"hash": data}
    } else {
        payload = {"hash": md5(data.data)}
    }
    console.log("PAYLOAD", payload)
    payload = JSON.stringify(payload)
    console.log("PAYLOAD", payload)
    let url = CONFIG.API_BASE_URL + "/easy2track/is_checkedin"
    var settings = {
        "url": url,
        "method": "POST",
        "timeout": 0,
        "headers": {
            "Content-Type": ["application/json", "application/json"],
            "apikey": getCookie("apikey")
        },
        "data": payload,
    };

    $.ajax(settings).success(function (response) {
        console.log("CHECKIN RESULT:", response)
        if(response.result == true) {
            $("#check-out-button").addClass("suggest")
        } else {
            $("#check-in-button").addClass("suggest")
        }
    });
}

//  A formatted version of a popular md5 implementation.
       //  Original copyright (c) Paul Johnston & Greg Holt.
       //  The function itself is now 42 lines long.

export function md5(inputString) {
        var hc="0123456789abcdef";
        function rh(n) {var j,s="";for(j=0;j<=3;j++) s+=hc.charAt((n>>(j*8+4))&0x0F)+hc.charAt((n>>(j*8))&0x0F);return s;}
        function ad(x,y) {var l=(x&0xFFFF)+(y&0xFFFF);var m=(x>>16)+(y>>16)+(l>>16);return (m<<16)|(l&0xFFFF);}
        function rl(n,c)            {return (n<<c)|(n>>>(32-c));}
        function cm(q,a,b,x,s,t)    {return ad(rl(ad(ad(a,q),ad(x,t)),s),b);}
        function ff(a,b,c,d,x,s,t)  {return cm((b&c)|((~b)&d),a,b,x,s,t);}
        function gg(a,b,c,d,x,s,t)  {return cm((b&d)|(c&(~d)),a,b,x,s,t);}
        function hh(a,b,c,d,x,s,t)  {return cm(b^c^d,a,b,x,s,t);}
        function ii(a,b,c,d,x,s,t)  {return cm(c^(b|(~d)),a,b,x,s,t);}
        function sb(x) {
            var i;var nblk=((x.length+8)>>6)+1;var blks=new Array(nblk*16);for(i=0;i<nblk*16;i++) blks[i]=0;
            for(i=0;i<x.length;i++) blks[i>>2]|=x.charCodeAt(i)<<((i%4)*8);
            blks[i>>2]|=0x80<<((i%4)*8);blks[nblk*16-2]=x.length*8;return blks;
        }
        var i,x=sb(inputString),a=1732584193,b=-271733879,c=-1732584194,d=271733878,olda,oldb,oldc,oldd;
        for(i=0;i<x.length;i+=16) {olda=a;oldb=b;oldc=c;oldd=d;
            a=ff(a,b,c,d,x[i+ 0], 7, -680876936);d=ff(d,a,b,c,x[i+ 1],12, -389564586);c=ff(c,d,a,b,x[i+ 2],17,  606105819);
            b=ff(b,c,d,a,x[i+ 3],22,-1044525330);a=ff(a,b,c,d,x[i+ 4], 7, -176418897);d=ff(d,a,b,c,x[i+ 5],12, 1200080426);
            c=ff(c,d,a,b,x[i+ 6],17,-1473231341);b=ff(b,c,d,a,x[i+ 7],22,  -45705983);a=ff(a,b,c,d,x[i+ 8], 7, 1770035416);
            d=ff(d,a,b,c,x[i+ 9],12,-1958414417);c=ff(c,d,a,b,x[i+10],17,     -42063);b=ff(b,c,d,a,x[i+11],22,-1990404162);
            a=ff(a,b,c,d,x[i+12], 7, 1804603682);d=ff(d,a,b,c,x[i+13],12,  -40341101);c=ff(c,d,a,b,x[i+14],17,-1502002290);
            b=ff(b,c,d,a,x[i+15],22, 1236535329);a=gg(a,b,c,d,x[i+ 1], 5, -165796510);d=gg(d,a,b,c,x[i+ 6], 9,-1069501632);
            c=gg(c,d,a,b,x[i+11],14,  643717713);b=gg(b,c,d,a,x[i+ 0],20, -373897302);a=gg(a,b,c,d,x[i+ 5], 5, -701558691);
            d=gg(d,a,b,c,x[i+10], 9,   38016083);c=gg(c,d,a,b,x[i+15],14, -660478335);b=gg(b,c,d,a,x[i+ 4],20, -405537848);
            a=gg(a,b,c,d,x[i+ 9], 5,  568446438);d=gg(d,a,b,c,x[i+14], 9,-1019803690);c=gg(c,d,a,b,x[i+ 3],14, -187363961);
            b=gg(b,c,d,a,x[i+ 8],20, 1163531501);a=gg(a,b,c,d,x[i+13], 5,-1444681467);d=gg(d,a,b,c,x[i+ 2], 9,  -51403784);
            c=gg(c,d,a,b,x[i+ 7],14, 1735328473);b=gg(b,c,d,a,x[i+12],20,-1926607734);a=hh(a,b,c,d,x[i+ 5], 4,    -378558);
            d=hh(d,a,b,c,x[i+ 8],11,-2022574463);c=hh(c,d,a,b,x[i+11],16, 1839030562);b=hh(b,c,d,a,x[i+14],23,  -35309556);
            a=hh(a,b,c,d,x[i+ 1], 4,-1530992060);d=hh(d,a,b,c,x[i+ 4],11, 1272893353);c=hh(c,d,a,b,x[i+ 7],16, -155497632);
            b=hh(b,c,d,a,x[i+10],23,-1094730640);a=hh(a,b,c,d,x[i+13], 4,  681279174);d=hh(d,a,b,c,x[i+ 0],11, -358537222);
            c=hh(c,d,a,b,x[i+ 3],16, -722521979);b=hh(b,c,d,a,x[i+ 6],23,   76029189);a=hh(a,b,c,d,x[i+ 9], 4, -640364487);
            d=hh(d,a,b,c,x[i+12],11, -421815835);c=hh(c,d,a,b,x[i+15],16,  530742520);b=hh(b,c,d,a,x[i+ 2],23, -995338651);
            a=ii(a,b,c,d,x[i+ 0], 6, -198630844);d=ii(d,a,b,c,x[i+ 7],10, 1126891415);c=ii(c,d,a,b,x[i+14],15,-1416354905);
            b=ii(b,c,d,a,x[i+ 5],21,  -57434055);a=ii(a,b,c,d,x[i+12], 6, 1700485571);d=ii(d,a,b,c,x[i+ 3],10,-1894986606);
            c=ii(c,d,a,b,x[i+10],15,   -1051523);b=ii(b,c,d,a,x[i+ 1],21,-2054922799);a=ii(a,b,c,d,x[i+ 8], 6, 1873313359);
            d=ii(d,a,b,c,x[i+15],10,  -30611744);c=ii(c,d,a,b,x[i+ 6],15,-1560198380);b=ii(b,c,d,a,x[i+13],21, 1309151649);
            a=ii(a,b,c,d,x[i+ 4], 6, -145523070);d=ii(d,a,b,c,x[i+11],10,-1120210379);c=ii(c,d,a,b,x[i+ 2],15,  718787259);
            b=ii(b,c,d,a,x[i+ 9],21, -343485551);a=ad(a,olda);b=ad(b,oldb);c=ad(c,oldc);d=ad(d,oldd);
        }
        return rh(a)+rh(b)+rh(c)+rh(d);
    }


export function loadAds() {
    let images = [
        "./images/ad-example1.png",
        "./images/ad-example2.png",
        "./images/ad-example3.png",
    ]

    $(".ad_image").each(function( index ) {
        $(this).fadeOut(500, function() {
            $(this).attr("src",images[Math.floor(Math.random()*images.length)]);
            $(this).fadeIn(500);
        });
    })

    window.setInterval(loadAds, 20000)

}


export function save() {
    if (save_data == true) {
        let form_data = {}
        $("input[class=contact_form_input]").each(function (index) {
            form_data[this.id] = this.value
        })
        console.log(JSON.stringify(form_data))
        $.cookie("contact_data", JSON.stringify(form_data), { path: '/' })
    } else {
        $.removeCookie('contact_data', { path: '/' })
    }

    if ($("#lastname").val().length <= 2) {
        alert($("#form_check_lastname_error").html())
        return
    }

    if ($("#firstname").val().length <= 2) {
        alert($("#form_check_firstname_error").html())
        return
    }

    if ($("#phone").val().length <= 2) {
        alert($("#form_check_phone_error").html())
        return
    }

    /*******
        let guest_amount = parseInt($("#guest_amount").val())
    
        if(Number.isNaN(guest_amount) || guest_amount < 1) {
            alert($("#form_check_guestamount_error").html())
            return
        }
    ****/
    saveContact()
}

export function remove() {
    if (save_data == true) {
        let form_data = {}
        $("input[class=contact_form_input]").each(function (index) {
            form_data[this.id] = this.value
        })
        console.log(JSON.stringify(form_data))
        $.cookie("contact_data", JSON.stringify(form_data), { path: '/' })
    } else {
        $.removeCookie('contact_data', { path: '/' })
    }
    removeContact()
}


