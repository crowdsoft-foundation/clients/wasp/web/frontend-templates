import {CONFIG} from '../pbcdn/config.js?v=[cs_version]';
import {getCookie, fireEvent} from '../pbcdn/functions.js?v=[cs_version]';

var socketio_never_connected=true
export class socketio {
    constructor() {
        if (socketio.instance) {
            return socketio.instance;
        }
        socketio.instance = this;
        socketio.instance.namespace = "/"
        socketio.instance.init()
        socketio.instance.cust_id = socketio.instance.make_id(15)

        socketio.instance.commandhandlers = {
            fireEvent: function (args) {
                fireEvent(args.eventname, args.eventdata)
            },
            fireBackendEvent: function (args) {
                fireEvent(args.eventname, args.eventdata)
            },
            showAlert: function (args) {
                alert(args.text)
            },
            forcereload: function (args) {
                console.log(args)
                let reason = args.reason
                alert("Die Seite muss wegen " + reason + " aktualisiert werden.\nNach bestätigen dieser Meldung erledigen wir das automatisch für Sie.")
                location.reload();
            }
        }

        return socketio.instance
    }

    init() {
        console.log("Listening for messages on:" + CONFIG.SOCKET_SERVER + socketio.instance.namespace)
        socketio.instance.socket = io(CONFIG.SOCKET_SERVER + socketio.instance.namespace, {
            path: '/socket/socket.io',
            query: {apikey: getCookie("apikey")}
        });

        socketio.instance.socket.on('connect', function () {
            console.log("Connected")
            socketio.instance.socket.emit('join', {room: "tmp_global"});
            socketio.instance.socket.emit('join', {room: decodeURIComponent(getCookie("consumer_name"))});
            socketio.instance.socket.emit('join', {room: decodeURIComponent(getCookie("username"))});
            console.log("Sending pageload", socketio.instance.cust_id)
            socketio.instance.socket.emit("analytics", JSON.stringify({
                        category: "pageload",
                        cust_id: socketio.instance.cust_id,
                        data: {easy2track: location.href, date: new Date().toJSON()},
                        room: "tmp_global"
                    }))
            if(socketio_never_connected) {
                socketio_never_connected = false
                window.setInterval(function () {
                    // console.log("Sending stillonsite", socketio.instance.cust_id)
                    // socketio.instance.socket.emit("analytics", JSON.stringify({
                    //     category: "stillonsite",
                    //     cust_id: socketio.instance.cust_id,
                    //     data: {easy2track: location.href, date: new Date().toJSON()},
                    //     room: "tmp_global"
                    // }))
                }, 3000)
            }
        });

        socketio.instance.socket.on('command', function (msg) {
            console.log("Received command:")
            console.log(msg)
            if (typeof (msg) == "string") {
                msg = JSON.parse(msg)
            }

            var payload = null;
            if (typeof (msg.data) == "undefined" && msg["data"]) {
                payload = JSON.parse(msg["data"])
            } else {
                payload = msg.data;
            }
            if (!payload) {
                payload = msg
            }
            console.log("PAYLOAD", payload.args)
            socketio.instance.commandhandlers[payload.command](payload.args)
        });

        socketio.instance.socket.on('notification', function (msg) {
            console.log("Received notification:")
            //console.log(msg)
        });

        socketio.instance.socket.on('my_response', function (msg) {
            console.log("Received my_response:")
            console.log(msg)
        });

        return self
    }

    fireEvent(room, eventname, eventdata) {
        socketio.instance.socket.emit("command", {
            command: "fireEvent",
            args: {eventname: eventname, eventdata: eventdata},
            room: room
        })
    }

    joinRoom(room) {
         socketio.instance.socket.emit('join', {room: decodeURIComponent(room)});
    }

    make_id(length) {
       var result           = '';
       var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
       var charactersLength = characters.length;
       for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
       }

       return result;
    }


}
