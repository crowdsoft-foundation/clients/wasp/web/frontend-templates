import {fireEvent} from "../pbcdn/functions.js?v=[cs_version]";

export class pb_keys {
    constructor() {
        self = this
    }

    init() {
        document.addEventListener("keydown", self.handleKeyPress, false);
        fireEvent('pb_keys_eventlistener_set')
    }

    get_key_handlers() {
        return {
            "Delete": function () {
                alert("Delete something")
            },
            "n": function () {
                alert("Create something")
            },
            "Escape": function () {
                if ($("#overlay").is(":visible")) {
                    $("#overlay").toggle()
                }
            },
            "F1": function () {
                if ($("#overlay").is(":visible")) {
                    showHelp()
                    $('#legend_wrapper').hide("slide")
                } else {
                    $('#legend_wrapper').toggle("slide");
                }
            }
        }
    }
    handleKeyPress(e) {
        var evtobj = e
        var keyCode = e.key;
        console.log("Key pressed", evtobj.keyCode, keyCode)
        if(evtobj.altKey) {
            if (self.get_key_handlers()[evtobj.keyCode]) {
                self.get_key_handlers()[evtobj.keyCode]()
            }
            if (self.get_key_handlers()[keyCode]) {
                self.get_key_handlers()[keyCode]()
            }
        }
    }
}