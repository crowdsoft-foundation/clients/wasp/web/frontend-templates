import {CONFIG} from "../../../pb-config.js?v=[cs_version]"
import {PbTpl} from "../../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../../csf-lib/pb-notifications.js?v=[cs_version]"
import {pbI18n} from "../../../csf-lib/pb-i18n.js?v=[cs_version]";
import {ContactManager} from "../../../contact-manager/js/contact-manager.js?v=[cs_version]";

export class BaseLayouts {
    layouts = {
        threecolumn: {
            "template_path": "app_layouts/threecolumn/layout.html",
            "content_selectors": ["#column1-content", "#column2-content", "#column3-content"],
            "additional_css": ["/app_layouts/threecolumn/css/custom.css?v=[cs_version]"],
            "additional_js": ["/app_layouts/threecolumn/js/functions.js?v=[cs_version]"]
        },
        twocolumn: {
            "template_path": "app_layouts/twocolumn/layout.html",
            "content_selectors": ["#column1-content", "#column2-content"],
            "additional_css": ["/app_layouts/twocolumn/css/custom.css?v=[cs_version]"],
            "additional_js": ["/app_layouts/twocolumn/js/functions.js?v=[cs_version]"]
        },
        singlecolumn: {
            "template_path": "app_layouts/singlecolumn/layout.html",
            "content_selectors": ["#column1-content"],
            "additional_css": ["/app_layouts/singlecolumn/css/custom.css?v=[cs_version]"],
            "additional_js": ["/app_layouts/singlecolumn/js/functions.js?v=[cs_version]"]
        }
    }

    constructor(layout, target_node_selector) {
        if (!BaseLayouts.instance) {
            this.tpl = new PbTpl()
            BaseLayouts.instance = this
        }

        BaseLayouts.instance.layout = layout
        BaseLayouts.instance.target_node_selector = target_node_selector
        return BaseLayouts.instance
    }

    render() {
        if (this.layouts[this.layout]?.additional_css) {
            this.layouts[this.layout].additional_css.forEach(css => {
                this.lazyLoadCSS(css)
            })
        }
        if (this.layouts[this.layout]?.additional_js) {
            this.layouts[this.layout].additional_js.forEach(js => {
                this.lazyLoadJS(js)
            })
        }
        let layout_render_promise = this.tpl.renderIntoAsync(this.layouts[this.layout]?.template_path, {}, this.target_node_selector).then(function() {
            this.fire_layout_rendered_event()
        }.bind(this))


        return layout_render_promise
    }

    fire_layout_rendered_event() {
        let layout_render_finished = new CustomEvent('layout_render_finished', {
            detail: {
                message: 'layout_render_finished',
                time: new Date(),
            }
        });
        document.dispatchEvent(layout_render_finished);
    }

    ready_for_user() {
        if ($("#loader-overlay")) {
            $("#loader-overlay").hide()
            $("#contentwrapper").show()
        }
    }

    lazyLoadCSS(href) {
        var link = $("<link>");
        link.attr({
            rel: "stylesheet",
            type: "text/css",
            href: href
        });
        $("head").append(link);
    }

    lazyLoadJS(src, callback = () => "") {
        var script = $("<script>");
        script.attr({
            type: "text/javascript",
            src: src
        });

        // If a callback is provided, execute it once the script is loaded
        if (typeof callback === "function") {
            script.on("load", callback);
        }

        $("head").append(script);
    }
}