import {PbRessources} from "../../csf-lib/pbapi/calendar/pb-ressources.js?v=[cs_version]"
import {PbColumns} from "../../csf-lib/pbapi/calendar/pb-columns.js?v=[cs_version]"
import {PbEvents} from "../../csf-lib/pbapi/calendar/pb-events.js?v=[cs_version]"
import {PbEventReminders} from "../../csf-lib/pbapi/calendar/pb-event-reminders.js?v=[cs_version]"
import {PbExtendedProperties} from "../../csf-lib/pbapi/calendar/pb-extended-properties.js?v=[cs_version]"
import {EventForm} from "./view-renderer/event-form/eventForm.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {PbContacts} from "../../csf-lib/pbapi/contacts/pb-contacts.js?v=[cs_version]"
import {PbGroups} from "../../csf-lib/pbapi/accountmanager/pb-groups.js?v=[cs_version]"
import {PbLogin} from "../../csf-lib/pb-login.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"
import {PbEventStatus} from "../../csf-lib/pbapi/calendar/pb-event-status.js?v=[cs_version]"
import {PbEventTypes} from "../../csf-lib/pbapi/calendar/pb-event-types.js?v=[cs_version]"
import {getCookie, setCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"



export class RenderEventDialogPage {
    #eventId

    constructor(socket) {
        if (!RenderEventDialogPage.instance) {
            RenderEventDialogPage.instance = this
            RenderEventDialogPage.instance.socket = socket
        }

        RenderEventDialogPage.instance.dirtyData = false

        RenderEventDialogPage.instance.PbRessources = new PbRessources(RenderEventDialogPage.instance.socket, () => this.render(true))
        RenderEventDialogPage.instance.PbColumns = new PbColumns()
        RenderEventDialogPage.instance.PbContacts = new PbContacts()
        RenderEventDialogPage.instance.PbEvents = new PbEvents(RenderEventDialogPage.instance.socket, () => "")
        RenderEventDialogPage.instance.PbExtendedProperties = new PbExtendedProperties()

        RenderEventDialogPage.instance.EventForm = new EventForm("#content", RenderEventDialogPage.instance.submitForm.bind(RenderEventDialogPage.instance), () => {
            RenderEventDialogPage.instance.dirtyData = true
        })

        RenderEventDialogPage.instance.PbNotifications = new PbNotifications()
        RenderEventDialogPage.instance.PbLogin = new PbLogin()
        RenderEventDialogPage.instance.PbEventReminders = new PbEventReminders(RenderEventDialogPage.instance.socket, () => RenderEventDialogPage.instance.EventForm.renderReminders())
        RenderEventDialogPage.instance.PbGroups = new PbGroups()
        RenderEventDialogPage.instance.PbTagsList = new PbTagsList(RenderEventDialogPage.instance.socket, (data) => {
            RenderEventDialogPage.instance.EventForm.setTagsListModel(RenderEventDialogPage.instance.PbTagsList).renderTagsList(data?.args?.tags)
        })
        RenderEventDialogPage.instance.PbEventStatus = new PbEventStatus(RenderEventDialogPage.instance.socket)
        RenderEventDialogPage.instance.PbEventTypes = new PbEventTypes()

        return RenderEventDialogPage.instance
    }

    init(eventId) {
        this.#eventId = eventId
        $("#content").hide()
        let self = this
        let dataLoadFunc = () => {
            return self.PbEvents
        }
        if (eventId) {
            dataLoadFunc = () => {
                return self.PbEvents.getEventDataFromApi(eventId)
            }
        }
        return new Promise((resolve, reject) => {
            let promises = [
                self.PbRessources.loadFromApi(),
                self.PbColumns.loadFromApi(),
                self.PbExtendedProperties.loadFromApi(),
                dataLoadFunc(),
                self.PbContacts.loadFromApi(),
                self.PbLogin.fetchProfileData(getCookie("username")),
                self.PbEventReminders.loadFromApi(eventId),
                self.PbGroups.loadFromApi(true),
                self.PbTagsList.load(),
                self.PbEventStatus.load(),
                self.PbEventTypes.loadFromApi()
            ]
            Promise.all(promises).then(function (results) {
                let [resModel, columnsModel, propsModel, eventsModel, contactsModel, loginData, eventRemindersModel, groupsModel, tagsModel, eventStatusModel, appointmentTypeModel] = results
                self.PbRessources = resModel
                self.PbColumns = columnsModel
                self.PbEvents = eventsModel
                self.PbExtendedProperties = propsModel
                self.PbContacts = contactsModel
                self.loginData = loginData
                self.PbEventReminders = eventRemindersModel
                self.PbGroups = groupsModel
                self.loginData["username"] = getCookie("username")
                self.PbTagsList = tagsModel
                self.PbEventStatus = eventStatusModel
                self.PbEventTypes = appointmentTypeModel
                self.render().then(() => resolve(this))
            })
        })
    }

    submitForm(data) {
        let self = this
        self.PbNotifications.blockForLoading("#body")
        return new Promise((resolve, reject) => {
            self.PbEvents.saveActiveEvent(data).then(() => {
                self.dirtyData = false
                self.PbNotifications.showHint("Saved", "success", () => {
                    window.location.href = "/easy2schedule/list-view.html"
                }, 1000)
                resolve()
            })
        })
    }

    render(isUpdate = false, force = false) {
        let self = this
        self.#eventId = self.PbEvents.getActiveEventData().event_id

        if (self.dirtyData && isUpdate && !force) {
            self.PbNotifications.blockForLoading('#content')
            self.PbNotifications.ask(new pbI18n()
                    .translate("Data changed in the background, do you want to reload the form?"),
                new pbI18n().translate("Attention..."),
                () => {
                    //Reload
                    self.render(true, true)
                },
                () => {
                    //Cancel
                    self.PbNotifications.unblock('#content')
                })
        } else {
            return new Promise((resolve, reject) => {
                self.EventForm
                    .setEvents(self.PbEvents)
                    .setRessources(self.PbRessources)
                    .setColumns(self.PbColumns)
                    .setExtendedProperties(self.PbExtendedProperties.getData())
                    .setContacts(self.PbContacts)
                    .setLogin(self.loginData)
                    .setEventReminders(self.PbEventReminders)
                    .setNotifications(self.PbNotifications)
                    .setGroupModel(self.PbGroups)
                    .setTagsListModel(self.PbTagsList)
                    .setEventStatusModel(self.PbEventStatus)
                    .setAppointmentTypeModel(self.PbEventTypes)
                    .render()
                    .then(() => {
                        if (isUpdate) {
                            self.PbNotifications.unblock('#content')
                        } else {
                            $("#loader-overlay").fadeOut()
                            $("#content").fadeIn()
                        }
                        resolve()
                    })
            })
        }
    }
}
