import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {Controller} from "./controller.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"

export class CalendarModule {
    actions = {}

    constructor() {
        if (!CalendarModule.instance) {
            this.initListeners()
            this.initActions()
            CalendarModule.instance = this
            CalendarModule.instance.tpl = PbTpl.instance
            CalendarModule.instance.acc = new PbAccount()
            CalendarModule.instance.socket =  new socketio()
        }

        return CalendarModule.instance
    }

    initActions() {
        let self = this
        this.actions["calendarModuleInitialised"] = function (myevent, callback = () => "") {
            self.acc.init().then(() => {
                if (!self.acc.hasPermission("easy2schedule")) {
                    self.acc.logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new Controller(CalendarModule.instance.socket).init()
                    callback()
                }
            })
        }
    }

    initListeners() {}
}