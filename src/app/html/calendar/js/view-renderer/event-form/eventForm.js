import {PbTpl} from "../../../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "/csf-lib/pb-notifications.js?v=[cs_version]"
import {CONFIG} from "../../../../pb-config.js?v=[cs_version]"
import {makeId, fixDateRangeIfNeeded, array_merge_distinct} from "../../../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbTag} from "../../../../csf-lib/pbapi/tagmanager/pb-tag.js?v=[cs_version]"
import {getCookie} from "../../../../csf-lib/pb-functions.js?v=[cs_version]"

export class EventForm {
    #events
    #ressources
    #columns
    #tagModel
    #extendedProperties
    #contacts
    #login
    #eventRemindersModel
    #notifications
    #groupModel
    #eventStatusModel
    #appointmentTypeModel


    constructor(target, onSave = () => "", onChanges = () => "") {
        this.onSave = onSave
        this.onChanges = onChanges
        this.applyButtonListeners()
        this.initBackendListeners()
        this.target = target
        this.notifications = new PbNotifications()

    }

    initBackendListeners() {
        let self = this
        document.addEventListener("command", function _(event) {
            if (event.data.data.command == "refreshContacts") {
                self.renderContacts()
            }
        })
    }

    setTagsListModel(data) {
        this.#tagModel = data
        return this
    }

    setGroupModel(data) {
        this.#groupModel = data
        return this
    }

    setNotifications(data) {
        this.#notifications = data
        return this
    }

    setEvents(data) {
        this.#events = data
        return this
    }

    setLogin(data) {
        this.#login = data
        return this
    }

    setRessources(data) {
        this.#ressources = data
        return this
    }

    setColumns(data) {
        this.#columns = data
        return this
    }

    setExtendedProperties(data) {
        this.#extendedProperties = data
        return this
    }

    setContacts(data) {
        this.#contacts = data
        return this
    }

    setEventReminders(model) {
        this.#eventRemindersModel = model
        return this
    }

    setEventStatusModel(model) {
        this.#eventStatusModel = model
        return this
    }

    setAppointmentTypeModel(model) {
        this.#appointmentTypeModel = model
        return this
    }

    render(part = undefined) {
        let self = this
        let renderData = {
            eventData: this.#events?.getActiveEventData(),
            ressources: this.#ressources,
            extendedProperties: this.#extendedProperties
        }

        return new Promise((resolve, reject) => {
            PbTpl.instance.renderIntoAsync(
                'calendar/js/view-renderer/event-form/eventForm.tpl',
                {"data": renderData},
                self.target
            ).then(() => {
                let promises = []
                if (renderData.eventData?.event_id) {
                    $("#pilllink_documents").show()
                    $("#pilllink_documents").attr("href", `/documentation/index.html?sid=easy2schedule|event|${renderData.eventData?.event_id}`)
                } else {

                    $("#pilllink_documents").hide()
                }

                let attachedRessources = renderData.eventData?.attachedRessources
                let ressource_options = this.#ressources.getSelectOptions(attachedRessources)
                let column_options = this.#columns.getSelectOptions([renderData.eventData?.resourceId])
                let event_status_options = this.#eventStatusModel.getSelectOptions(renderData.eventData?.data?.event_status)

                let event_type_options = this.#appointmentTypeModel.getSelectOptions(renderData.eventData?.data?.appointment_type)

                let groupOptions = this.#groupModel.getSelectOptions(renderData.eventData?.data_owners)

                if (getCookie("username") && !groupOptions.find(
                    (element) => element.id == "user." + getCookie("username")
                )) {
                    groupOptions.push({id: "user." + getCookie("username"), name: "user." + getCookie("username"), selected: ""})
                }
                let default_tags = []
                if (!renderData.eventData?.event_id) {
                    if (getCookie("data_owners")) {
                        let owners = JSON.parse(decodeURIComponent(getCookie("data_owners")))
                        for (let owner of owners) {
                            groupOptions.find(
                                (element) => element.id == owner
                            ).selected = "selected"

                        }
                    } else {
                        for (let idx in groupOptions) {
                            if (groupOptions[idx].id.startsWith("group.")) {
                                groupOptions[idx].selected = "selected"
                                break;
                            }
                        }
                    }

                    if (getCookie("last_event_types_selection")) {
                        let preselected = JSON.parse(decodeURIComponent(getCookie("last_event_types_selection")))
                        for (let option of event_type_options) {
                            if (preselected.includes(option.id)) {
                                option.selected = "selected"
                            }
                        }
                    }

                    if (getCookie("last_event_status_selection")) {
                        let preselected = decodeURIComponent(getCookie("last_event_status_selection"))
                        for (let option of event_status_options) {
                            if (option.id == preselected) {
                                option.selected = "selected"
                            } else {
                                option.selected = ""
                            }
                        }
                    }

                    if (getCookie("last_event_members_selection")) {
                        let values = JSON.parse(decodeURIComponent(getCookie("last_event_members_selection")))
                        for (let option of values) {
                            option = JSON.parse(option)
                            for (let entry of ressource_options) {
                                let parsed = JSON.parse(entry.id)
                                if (parsed.id == option.id) {
                                    entry.selected = "selected"
                                }
                            }
                        }
                    }

                    if (getCookie("last_event_tag_selection")) {
                        let values = JSON.parse(decodeURIComponent(getCookie("last_event_tag_selection")));
                        for (let value of values) {
                            let tag = {"name": value.id || value.name, "value": value.text || value.value}
                            default_tags.push(tag)
                        }
                    }
                }

                if (!part) {
                    promises.push(PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/title.tpl', {"title": renderData.eventData?.title}, "#title_container"))
                    promises.push(PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/date-selects.tpl', {"start": renderData.eventData?.starttime, "end": renderData.eventData?.endtime}, "#date_selects_container"))
                    promises.push(PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/resource-selects.tpl', {"multiple": false, "propertyPath": "resourceId", "options": column_options}, "#ressources_1st_dimension"))
                    promises.push(PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/resource-selects.tpl', {"multiple": true, "propertyPath": "attachedRessources", "options": ressource_options}, "#ressources_2nd_dimension"))
                    promises.push(PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/comment.tpl', {"comment": renderData.eventData?.extended_props?.comment}, "#comment_container"))
                    promises.push(self.renderContacts())
                    promises.push(self.renderTagsList(default_tags))
                    promises.push(PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/resource-selects.tpl', {"multiple": true, "propertyPath": "data_owners", "options": groupOptions}, "#dataOwnersContainer"))
                    promises.push(PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/resource-selects.tpl', {"multiple": false, "propertyPath": "data.event_status", "options": event_status_options}, "#status_container"))
                    promises.push(PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/resource-selects.tpl', {"multiple": true, "propertyPath": "data.appointment_type", "options": event_type_options}, "#type_container"))
                    promises.push(self.renderCustomFields(renderData))

                    if (renderData.eventData?.event_id) {
                        promises.push(self.#eventRemindersModel.loadFromApi(renderData.eventData.event_id))
                    } else {
                        switch (part) {
                            case "tags":
                                promises.push(PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/resource-selects.tpl', {"multiple": true, "propertyPath": "tags", "options": tagOptions}, "#tag_container"))
                                break;
                        }
                    }
                }

                Promise.all(promises).then(() => {

                    $("#new_contact_button").attr("href", '/contact-manager/index.html?new').attr("target", "_blank")
                    //$("#new_contact_button").attr("href", '/contact-manager/index.html?new&r=' + location.href).attr("target", "_self")

                    $('[data-select2-selector]').select2()
                    $('[data-propertyname="tags"]').select2({
                        tags: true,
                        tokenSeparators: [',', ' '],
                        selectOnClose: false
                    })
                    $('[data-propertyname="attachedContacts"]').select2({
                        templateSelection: this.formatSelectLink
                    })

                    $("#allDayCheckbox").prop("checked", renderData.eventData?.allDay).attr("checked", renderData.eventData?.allDay)
                    $("#allDayCheckbox").change((event) => {
                        if ($("#allDayCheckbox").prop("checked")) {
                            self.updateDatePickerFormat(CONFIG.DATEFORMAT)
                        } else {
                            self.updateDatePickerFormat(CONFIG.DATETIMEWIHTOUTSECONDSFORMAT)
                        }
                    })
                    self.initDatePickers(renderData.eventData?.allDay)

                    let starttime = moment.utc(renderData.eventData?.starttime)
                    if (!starttime.isValid()) {
                        starttime = moment.utc()
                    }
                    $("#starttime").data("DateTimePicker").date(starttime)
                    let endtime = moment.utc(renderData.eventData?.endtime)
                    if (!endtime.isValid()) {
                        endtime = starttime.add(1, "hours")
                    }
                    $("#endtime").data("DateTimePicker").date(endtime)

                    $("input").focus(this.onChanges)
                    $("textarea").focus(this.onChanges)
                    $("select").focus(this.onChanges)
                    $("#eventForm").submit((e) => {
                        if ($("#eventForm")[0].reportValidity()) {
                            self.submit()
                        } else {

                        }
                        return false;
                    })

                    self.renderReminders()

                    $("#new_reminder_button").click(() => {
                        self.addReminder()
                    })
                    resolve(self)
                })
            })
        })
    }

    renderTagsList(selectedTags = $('[data-propertyname="tags"]').val() || []) {
        let self = this
        return new Promise((resolve, reject) => {
            let tags = self.#events?.getActiveEventData().tags || []
            let alreadySelectedTags = []
            if ($('[data-propertyname="tags"]') > 0) {
                for (let alreadySelected of $('[data-propertyname="tags"]').val()) {
                    alreadySelectedTags.push(new PbTag().fromShort(alreadySelected).getData())
                }
            }
            tags = array_merge_distinct(alreadySelectedTags, selectedTags)

            for (let tag of tags) {
                tag = new PbTag({"name": tag.id || tag.name, "value": tag.text || tag.value})
                selectedTags.push(tag.short())
                self.#tagModel.addTagToList(tag)
            }
            
            let tagOptions = self.#tagModel.getSelectOptions(selectedTags)

            PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/resource-selects.tpl', {"multiple": true, "addButton": true, "propertyPath": "tags", "options": tagOptions}, "#tag_container").then(() => {
                $('[data-propertyname="tags"]').select2({
                    tags: true,
                    tokenSeparators: [',', ' '],
                    selectOnClose: false
                })
                $("#addGlobalTagButton").click(() => {
                    PbNotifications.instance.ask(
                        '<div><input type="text" id="newGlobalTag" placeholder="Enter new global Tag" class="form-control"/></div>',
                        'new Global Tag',
                        () => {
                            self.#tagModel.addTagToList(new PbTag().fromShort($("#newGlobalTag").val()).setUsableFor(["easy2schedule"]), true)
                        })
                    $("#newGlobalTag").focus()
                })
                resolve()
            })
        })
    }

    renderContacts() {
        let self = this
        return new Promise((resolve, reject) => {
            self.notifications.blockForLoading("#contact_wrapper")
            let attachedContacts = array_merge_distinct(self.#events?.getActiveEventData()?.attachedContacts, $('[data-propertyname="attachedContacts"]').val() || [])
            self.#contacts.loadFromApi().then(() => {
                let contact_options = self.#contacts.getSelectOptions(attachedContacts)

                PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/contact-select.html', {"multiple": true, "propertyPath": "attachedContacts", "options": contact_options}, "#contactsContainer").then(() => {
                    $('[data-propertyname="attachedContacts"]').select2({
                        templateSelection: self.formatSelectLink
                    })
                    self.notifications.unblock("#contact_wrapper")
                    resolve()
                })
            })
        })
    }

    initDatePickers(allDay = false) {
        let format = CONFIG.DATETIMEWIHTOUTSECONDSFORMAT
        if (allDay) {
            format = CONFIG.DATEFORMAT
        }
        $(".datepicker").datetimepicker({
            format: format,
            locale: 'de',
            sideBySide: true,
            showClose: true,
            ignoreReadonly: true,
            debug: false
        }).on('dp.change', function (e) {
            let startDate = moment($("#starttime").val(), CONFIG.DATETIMEWIHTOUTSECONDSFORMAT)
            let endDate = moment($("#endtime").val(), CONFIG.DATETIMEWIHTOUTSECONDSFORMAT)

            if ($(e.target).attr("id") == "starttime" && endDate.isBefore(startDate)) {
                endDate = fixDateRangeIfNeeded(startDate, endDate, 1, "hours", "end")
                $("#endtime").data("DateTimePicker").date(endDate)
            }
            if ($(e.target).attr("id") == "endtime" && startDate.isAfter(endDate)) {
                let alLDay = $("#allDayCheckbox").is(":checked")
                startDate = fixDateRangeIfNeeded(startDate, endDate, alLDay ? 0 : -1, "hours", "start")
                $("#starttime").data("DateTimePicker").date(startDate)
            }
        })
    }

    updateDatePickerFormat(format) {
        $(".datepicker").each((idx, el) => {
            $(el).datetimepicker().data('DateTimePicker').format(format)
        })
    }


    formatSelectLink(option) {
        if (!option.id) {
            return option.text;
        }
        var baseUrl = "/contact-manager/?ci=";
        var $option = $(
            '<span><a target="_blank"><i class="typcn typcn-arrow-forward-outline pd-r-10 tx-10"></i></a><span></span></span>'
        );
        $option.find("span").text(option.text);
        $option.find("a").attr("href", baseUrl + option.element.value.toLowerCase());

        return $option;
    };

    renderReminders() {
        let self = this
        $("#reminder_data_set").html("")
        if (self.#eventRemindersModel.getData().length > 0) {
            for (let reminder of self.#eventRemindersModel.getData()) {
                self.addReminder(reminder.reminder_id, reminder.seconds_in_advance, reminder.type)
            }
        }
    }

    addReminder(id = undefined, value = undefined, type = undefined) {
        let self = this
        let days = value / 60 / 60 / 24
        let hours = value / 60 / 60
        let minutes = value / 60

        let unit = "minutes"
        value = minutes

        if (hours % 1 == 0) {
            value = hours
            unit = "hours"
        }
        if (days % 1 == 0) {
            value = days
            unit = "days"
        }

        let reminderData = {
            "id": id || "new_" + makeId(15),
            "value": value || 10,
            "unit": unit || "minutes"
        }
        PbTpl.instance
            .renderIntoAsync('calendar/js/view-renderer/event-form/reminder-entry.tpl', {"reminder": reminderData}, "#reminder_data_set", "append")
            .then(() => {
                $(`*[data-reminder-delete-button]`).click((e) => {
                    e.preventDefault()
                    let reminderId = $(e.currentTarget).data("reminderDeleteButton")
                    self.deleteReminder(reminderId)
                })

                $(`#reminder_unit_select_${reminderData.id} option[value=${unit}]`).attr("selected", "selected")
                $(`#reminder_mode_select_${reminderData.id} option[value=${type}]`).attr("selected", "selected")
            })
    }

    deleteReminder(reminderId) {
        let self = this
        this.#eventRemindersModel.deleteFromApi(reminderId).then(() => {
        })
    }

    renderCustomFields(renderData) {
        return new Promise((resolve, reject) => {
            let displayData = renderData.eventData?.extended_props
            if (displayData?.creator_login) {
                delete displayData["creator_login"]
            }
            if (displayData && Object.entries(displayData).length > 0) {
                PbTpl.instance.renderIntoAsync('calendar/js/view-renderer/event-form/custom-fields.tpl', {"extended_props": renderData.eventData.extended_props}, "#custom_fields_container").then(() => {
                    resolve()
                })
            } else {
                resolve()
            }
        })
    }

    submit() {
        let self = this
        let formData = {}
        this.notifications.blockForLoading("#content")

        $("[data-propertyname]").each((idx, ele) => {
            let value = $(ele).val()
            if ($(ele).data("propertyname") == "starttime" || $(ele).data("propertyname") == "endtime") {
                value = moment($(ele).val(), CONFIG.DATETIMEWIHTOUTSECONDSFORMAT).format(CONFIG.DB_DATEFORMAT)
                if ($("#allDayCheckbox").is(":checked") && $(ele).data("propertyname") == "starttime") {
                    value = moment($(ele).val(), CONFIG.DATEFORMAT).startOf("day").format(CONFIG.DB_DATEFORMAT)
                }
                if ($("#allDayCheckbox").is(":checked") && $(ele).data("propertyname") == "endtime") {
                    value = moment($(ele).val(), CONFIG.DATEFORMAT).endOf("day").format(CONFIG.DB_DATEFORMAT)
                }
            } else if ($(ele).data("propertyname") == "tags") {
                const processedTags = value.map(element => {
                    let newvalue = {"id": element, "text": element}
                    return newvalue
                })
                value = processedTags
            } else if ($(ele).data("propertyname") == "allDay") {
                value = $(ele).is(":checked")
            } else {
                value = $(ele).val()
            }
            formData[$(ele).data("propertyname")] = value
        })
        console.log('Form data',)

        let reminders = {}
        let reminderData = $("[data-reminder-data]")
        reminderData.each((idx, item) => {
            let id = $(item).data("reminderData")
            console.log("reminderData", id, $(item).attr("name"), $(item).val())
            if (!reminders[id]) {
                reminders[id] = {}
            }
            reminders[id][$(item).attr("name")] = $(item).val()
        })
        let formReminders = []
        for (let [id, data] of Object.entries(reminders)) {
            let seconds_in_advance = 0
            switch (data.unit) {
                case "minutes":
                    seconds_in_advance = data.value * 60
                    break;
                case "hours":
                    seconds_in_advance = data.value * 60 * 60
                    break;
                case "days":
                    seconds_in_advance = data.value * 60 * 60 * 24
                    break;
            }
            let reminder = {
                "type": data.mode,
                "seconds_in_advance": seconds_in_advance
            }

            if (!id.includes("new_")) {
                reminder["reminder_id"] = id
            }

            if (reminder.type == "email") {
                reminder["recipients"] = [self.#login.email]
            }
            if (reminder.type == "gui") {
                reminder["recipients"] = [self.#login.username]
            }
            formReminders.push(reminder)
        }

        formData["reminders"] = formReminders
        self.onSave(formData).then(() => {
            this.notifications.unblock("#content")
        })
    }

    applyButtonListeners() {
        let self = this


        $("#page_menu_button_1").click(() => {
            $("#eventForm").submit()
        })

        $("#page_menu_button_2").click(() => {
            window.location.href = "/easy2schedule/list-view.html"
        })


    }

}
