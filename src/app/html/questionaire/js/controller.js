export class Controller {
    constructor(socket) {
        if (!Controller.instance) {
            Controller.instance = this
            Controller.instance.socket = socket
        }

        return Controller.instance
    }

    init(QuestionaireModuleInstance) {
        switch (window.location.pathname) {
            case "/questionaire/questionaire_editor.html":
                import("./render-administration.js?v=[cs_version]").then((Module) => {
                    new Module.RenderAdministration(Controller.instance.socket, QuestionaireModuleInstance).init()
                })
                break;
            case "/questionaire/questionaire_export.html":
                import("./render-export.js?v=[cs_version]").then((Module) => {
                    new Module.RenderExport(Controller.instance.socket, QuestionaireModuleInstance).init()
                })
                break;
            case "/questionaire/questionaire_statistics.html":
                import("./render-statistic.js?v=[cs_version]").then((Module) => {
                    new Module.RenderStatistic(Controller.instance.socket, QuestionaireModuleInstance).init()
                })
                break;
            case "/questionaire/Test.html":
                import("./render-test.js?v=[cs_version]").then((Module) => {
                    new Module.RenderTest(Controller.instance.socket, QuestionaireModuleInstance).init()
                })
                break;
        }
    }
}
