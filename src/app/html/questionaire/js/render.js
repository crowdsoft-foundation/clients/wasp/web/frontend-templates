import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {
    getQueryParam,
    setQueryParam,
    fireEvent,
    getCookie
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbQuestionaires} from "./questionaires.js?v=[cs_version]"
import {QuestionaireForm} from "./questionaire_form.js?v=[cs_version]"
import {PbBrowserDb} from "../../csf-lib/pb/indexeddb/PbBrowserDb.js?v=[cs_version]"
import {PbApiRequest} from "../../csf-lib/pbapi/pb-api-request.js?v=[cs_version]"

export class QuestionaireRenderer {
    constructor() {
        if (!QuestionaireRenderer.instance) {
            this.tpl = new PbTpl()
            this.questionaires = new PbQuestionaires()
            QuestionaireRenderer.instance = this
        }

        return QuestionaireRenderer.instance
    }

    init() {
        let promises = [this.questionaires.getQuestionairesById(getQueryParam("questionaire_id") || "")]

        Promise.all(promises).then(function () {
            this.renderQuestionaire()
            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }

        }.bind(this))
    }

    renderQuestionaire() {
        let question_answer_mapping = Object.values(this.questionaires.getQuestionAnswerMapping())
        this.tpl.renderIntoAsync("questionaire/templates/questionaire.html", {"data": question_answer_mapping}, "#main_body_content")
    }
}

export class QuestionaireFormRenderer {
    constructor() {
        if (!QuestionaireFormRenderer.instance) {
            this.tpl = new PbTpl()
            this.questionaires = new PbQuestionaires()
            this.notifications = new PbNotifications()
            let tmpDbQestionaireId = "tmp"
            if (getQueryParam("questionaire_id") !== false) {
                tmpDbQestionaireId = getQueryParam("questionaire_id")
            }
            let dbStores = {
                questionaire: "[questionaireID+questionID+answerID],[questionaireID+questionID], [questionaireID+answerID], data"
            }
            this.db = new PbBrowserDb("questionaire_form_" + tmpDbQestionaireId, dbStores, 2)
            QuestionaireFormRenderer.instance = this
        }

        return QuestionaireFormRenderer.instance
    }

    init() {
        document.addEventListener("command", function _(event) {
            if (event.data.data.event == "questionaireAnswersAdded") {
                this.resetQuestionaireForm()
                this.notifications.unblockAll()
                this.notifications.showHint("Saved")
                if (getQueryParam("ref_code")) {
                    let ref_code = getQueryParam("ref_code")
                    let ref_code_parts = ref_code.split("_")
                    if (ref_code_parts[0] && ref_code_parts[0] == "contact") {
                        let questionaire_config_id = event.data.data.args.data[0].value.questionaireID
                        let questionaire_id = event.data.data.args.questionaire_id
                        this.updateContactInfo(ref_code_parts[1], questionaire_config_id, questionaire_id)
                    }
                }
                setQueryParam("questionaire_id", event.data.data.args.questionaire_id)
                if ($("#loader-overlay")) {
                    $("#loader-overlay").hide()
                    $("#contentwrapper").show()
                }
                this.initial_load()

            } else if (event.data.data.event == "questionaireAnswersAddedFailed") {
                this.notifications.unblockAll()
                this.notifications.showHint("Saving failed, please try again", "error")
                this.initial_load()
            }
        }.bind(this))
        this.initial_load()
    }

    updateContactInfo(contact_id, questionaire_config_id, questionaire_id) {
        new Promise(function (resolve, reject) {
            let attachments = {}
            attachments["questionaire_" + questionaire_config_id + "_" + questionaire_id] = {
                "type": "questionaire",
                "config": questionaire_config_id,
                "questionaire_id": questionaire_id
            }
            var settings = {
                "url": CONFIG.API_BASE_URL + "/es/cmd/updateContact",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": getCookie("apikey"),
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "contact_id": contact_id,
                    "data": {
                        "attachments": attachments
                    }
                }),
                "success": function (data) {
                    //window.location.href = "/contact-manager/?ci=" + contact_id
                    this.notifications.showHint("You can close this window now")
                    resolve()
                }.bind(this)
            };

            $.ajax(settings)
        }.bind(this))
    }

    initial_load() {
        let promises = [this.questionaires.getQuestionaireConfigById(getQueryParam("questionaire_config_id") || ""), this.db.init()]
        if (getQueryParam("questionaire_id")) {
            promises.push(this.questionaires.getQuestionairesById(getQueryParam("questionaire_id")))
        }

        Promise.all(promises).then(function (return_values) {
            this.renderQuestionaire(return_values[0], return_values[2] || [])
            if ($("#loader-overlay")) {
                $("#loader-overlay").hide()
                $("#contentwrapper").show()
            }

        }.bind(this))
    }

    resetQuestionaireForm() {
        $("#questionaire_form")[0].reset()
        $('input').prop('checked', false)
        this.db.clear("questionaire")
        $("[data-questionli]").addClass("missing")
        QuestionaireFormRenderer.instance.tpl.removeStickyBanner()
    }

    renderQuestionaire(questionaireConfig, answers) {
        new QuestionaireForm(this.tpl, questionaireConfig, answers).renderInto("#main_body_content", function () {
                this.postProcessQuestionaire(questionaireConfig, answers)
            }.bind(this),
            (question) => {
                for (let answerInput of question.answer.data.inputs) {
                    this.db.delete("questionaire", {
                        "questionaireID": questionaireConfig.id,
                        "answerID": "answer_value_" + answerInput.id
                    })
                }
            })
            .then(function () {
                this.postProcessQuestionaire(questionaireConfig, answers)
            }.bind(this))
    }

    async postProcessQuestionaire(questionaireConfig, answers) {
        for (let answer of answers) {
            await QuestionaireFormRenderer.instance.db.bulkPut("questionaire", [
                {
                    "questionaireID": questionaireConfig.id,
                    "questionID": answer.data.value.questionID,
                    "answerID": answer.data.value.answerID,
                    "data": answer.data.value.data
                }
            ])
        }

        //$('[data-answer-value-element]').unbind()
        $('[data-answer-value-element]').click(async function (event) {
            if (event.currentTarget.type == "radio" | event.currentTarget.type == "checkbox") {
                $("#question_" + event.currentTarget.dataset.question_id).removeClass("missing")
            }
        })

        //$('[data-answer-value-element]').unbind()
        $('[data-answer-value-element]').change(async function (event) {
            let val = $(event.currentTarget).val()
            if (event.currentTarget.type == "radio") {
                QuestionaireFormRenderer.instance.db.delete("questionaire", {
                    "questionaireID": questionaireConfig.id,
                    "questionID": event.currentTarget.dataset.question_id
                }).then(function (data) {
                    QuestionaireFormRenderer.instance.db.bulkPut("questionaire", [
                        {
                            "questionaireID": questionaireConfig.id,
                            "questionID": event.currentTarget.dataset.question_id,
                            "answerID": event.currentTarget.id,
                            "data": val
                        }
                    ])
                })
            } else if (event.currentTarget.type == "checkbox") {
                if (event.currentTarget.checked) {
                    QuestionaireFormRenderer.instance.db.bulkPut("questionaire", [
                        {
                            "questionaireID": questionaireConfig.id,
                            "questionID": event.currentTarget.dataset.question_id,
                            "answerID": event.currentTarget.id,
                            "data": val
                        }
                    ])
                } else {
                    QuestionaireFormRenderer.instance.db.delete("questionaire", {
                        "questionaireID": questionaireConfig.id,
                        "answerID": event.currentTarget.id
                    })
                }
            } else {
                QuestionaireFormRenderer.instance.db.bulkPut("questionaire", [
                    {
                        "questionaireID": questionaireConfig.id,
                        "questionID": event.currentTarget.dataset.question_id,
                        "answerID": event.currentTarget.id,
                        "data": val
                    }
                ])
            }
        })

        QuestionaireFormRenderer.instance.db.read("questionaire", {"questionaireID": questionaireConfig.id}).then((data) => {
            QuestionaireFormRenderer.instance.questionaireDirty = data.length > 0
            for (let row of data) {
                if ($("#" + row.answerID).attr('type') == "checkbox") {
                    if (!$(`#${row.answerID}`).is(":checked"))
                        $(`#${row.answerID}`).click()
                } else if ($("#" + row.answerID).attr('type') == "radio") {
                    if (!$(`input[type=radio][data-question_id=${row.questionID}][value=${row.data}]`).is(":checked")) {
                        $(`input[type=radio][data-question_id=${row.questionID}][value=${row.data}]`).click()
                    }
                } else {
                    $("#" + row.answerID).val(row.data)
                    $(`[data-answer-display="${row.answerID}"]`).html(row.data)
                }
                $("#question_" + row.questionID).removeClass("missing")
            }
        })
        setTimeout(function () {
            $('[data-answer-value-element]').change(async function (event) {
                QuestionaireFormRenderer.instance.questionaireDirty = true
                QuestionaireFormRenderer.instance.tpl.showStickyBanner(QuestionaireFormRenderer.instance.tpl.translate("Unsaved changes"))
            })
        }, 500)

        $("#new_questionaire_button").unbind()
        $("#new_questionaire_button").click(function (event) {
            event.preventDefault()
            QuestionaireFormRenderer.instance.resetQuestionaireForm()
            window.location.reload()
        })

        $("#save_questionaire_button").unbind()
        $("#save_questionaire_button").click(function (event) {
            event.preventDefault()
            if ($("#loader-overlay")) {
                $("#loader-overlay").show()
                $("#contentwrapper").hide()
            }
            this.notifications.blockForLoading("#save_questionaire_button")
            QuestionaireFormRenderer.instance.db.read("questionaire", {"questionaireID": questionaireConfig.id}).then((data) => {
                let answerList = []
                for (let entry of data) {
                    let answerListEntry = {
                        "persistent_id": `${entry.questionaireID}|${entry.questionID}|${entry.answerID}`,
                        "value": entry
                    }

                    if (getQueryParam("ref_code ")) {
                        answerListEntry["ref_code"] = getQueryParam("ref_code")
                    }

                    if (getQueryParam("questionaire_id")) {
                        answerListEntry["questionaire_id"] = getQueryParam("questionaire_id")
                    }
                    answerList.push(answerListEntry)
                }

                let payload = {
                    "ref": getQueryParam("ref"),
                    "data": answerList,
                    "data_owners": []
                }

                return new PbApiRequest("/es/cmd/addQuestionaireAnswers", "POST", payload).execute().then(() => {
                    QuestionaireFormRenderer.instance.resetQuestionaireForm()
                })
            })
        }.bind(this))
    }
}
