import {PbAccount} from "../../csf-lib/pb-account.js?v=[cs_version]"
import {socketio} from "../../csf-lib/pb-socketio.js?v=[cs_version]"
import {getCookie, fireEvent} from "../../csf-lib/pb-functions.js?v=[cs_version]";
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {Controller} from "./controller.js?v=[cs_version]";
import {QuestionaireRenderer, QuestionaireFormRenderer} from "./render.js?v=[cs_version]"

export class QuestionaireModule {
    actions = {}
    config = []

    constructor() {
        if (!QuestionaireModule.instance) {
            this.initListeners()
            this.initActions()
            QuestionaireModule.instance = this;
        }

        return QuestionaireModule.instance;
    }

    initActions() {
        this.actions["questionaireModuleInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(async () => {
                if (!new PbAccount().hasPermission('frontend.questionaire')) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."));
                } else {
                    this.questionaireRenderer = new QuestionaireRenderer()
                    await this.questionaireRenderer.init()
                    if ($("#loader-overlay")) {
                        $("#loader-overlay").hide()
                        $("#contentwrapper").show()
                    }
                    callback(myevent);
                }
            });
        }

        this.actions["questionaireFormModuleInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(async () => {
                if (false && !new PbAccount().hasPermission('frontend.questionaire')) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."));
                } else {
                    this.questionaireFormRenderer = new QuestionaireFormRenderer()
                    await this.questionaireFormRenderer.init()
                    if ($("#loader-overlay")) {
                        $("#loader-overlay").hide()
                        $("#contentwrapper").show()
                    }
                    callback(myevent);
                }
            });
        }

        this.actions["questionaireExportModuleInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("frontend.questionaire")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new Controller(QuestionaireModule.instance.socket).init(QuestionaireModule.instance)
                    callback()
                }
            })
        }

        this.actions["questionaireStatisticModuleInitialised"] = function (myevent, callback = () => "") {
            new PbAccount().init().then(() => {
                if (!new PbAccount().hasPermission("frontend.questionaire")) {
                    new PbAccount().logout(null, new pbI18n().translate("You are not allowed to visit this page. For safety-reasons we've logged you out automatically."))
                } else {
                    new Controller(QuestionaireModule.instance.socket).init(QuestionaireModule.instance)
                    callback()
                }
            })
        }
    }

    initListeners() {
        $("#deleteQuestionaire").click(function (event) {
            event.preventDefault()
        })

        document.addEventListener("InPageEvent", function (args) {
            console.log("GOT InPageEvent WITH ARGS", args)
        })

        fireEvent("InPageEvent", "init")
    }
}