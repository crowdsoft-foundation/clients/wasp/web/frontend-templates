import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbQuestionaires} from "./questionaires.js?v=[cs_version]"
import {PbEditor} from "../../csf-lib/pb-editor.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {PbTaskHandler} from "../../csf-lib/pb-taskhandler.js?v=[cs_version]"

export class RenderAdministration {
    constructor() {
        if (!RenderAdministration.instance) {
            this.tpl = new PbTpl()
            this.questionaires = new PbQuestionaires()
            RenderAdministration.instance = this
            RenderAdministration.instance.taskhandler = new PbTaskHandler()
        }

        return RenderAdministration.instance
    }

    init() {
        RenderAdministration.instance.initListeners();
        let promises = [this.questionaires.getQuestionaireConfigs()]

        Promise.all(promises).then(function (return_values) {
            let res = Object.values(return_values[0]);

            $("#questionaire_dropdown").append($("<option></option>").text(new pbI18n().translate("New questionaire")).val("new"));
            $.each(res, function (index, value) {
                $("#questionaire_dropdown").append($("<option></option>").val(value.id).html(value.title));
            });

            $("#questionaire_dropdown").prepend($("<option></option>").text(new pbI18n().translate("--- Please choose a questionaire ---")).val("").prop("selected", true));
        }.bind(this))

        if ($("#loader-overlay")) {
            $("#loader-overlay").hide()
            $("#contentwrapper").show()
        }

        $(document).on('click', '#showQuestionaire', function (e) {
            let chosenQuestionnaire = $("#chosenQuestionnaire").val();

            if (!chosenQuestionnaire) {
                PbNotifications.instance.showHint(new pbI18n().translate("Either an existing questionnaire or a new one must be selected."), "error")
                $('#questionaire_dropdown_id').addClass('alert-danger');
                $('#questionaire_dropdown_id').removeClass('hidden');
            } else {
                $('#questionaire_dropdown_id').removeClass('alert-danger');
                $('#questionaire_dropdown_id').addClass('hidden');
            }

            // New questionaire entry
            let questionaire_dropdown = $("#questionaire_dropdown").val();

            if (questionaire_dropdown === 'new') {
                let empty_content = {
                    "id": "",
                    "title": "",
                    "description": "",
                    "questions": [
                        {
                            "answer": {
                                "select_options": [
                                    {
                                        "id": "a1_1",
                                        "label": "",
                                        "value": "a1_1"
                                    },
                                    {
                                        "id": "a1_2",
                                        "label": "",
                                        "value": "a1_2"
                                    },
                                    {
                                        "id": "a1_3",
                                        "label": "",
                                        "value": "a1_3"
                                    }
                                ],
                                "type": "select"
                            },
                            "question": {
                                "id": "q1",
                                "next": "q2",
                                "previous": null,
                                "text": ""
                            }
                        }
                    ]
                }

                this.initEditorWithData(empty_content)

                // SAVE BUTTON
                $(document).on('click', '#saveContent', function (e) {
                    this.saveEditorData();
                }.bind(this));
            } else {
                let promises = [this.questionaires.getQuestionaireConfigById(chosenQuestionnaire)]

                Promise.all(promises).then(function (return_values) {
                    new PbNotifications().blockForLoading('#editor_container')

                    this.initEditorWithData(return_values[0])

                    new PbNotifications().unblock('#editor_container')

                    // COPY CONTENT BUTTON.
                    $(document).on('click', '#copyContentButton', function (e) {
                        let originalContentElement = document.getElementById('originalContent').innerHTML;

                        // Create a temporary element to hold the cleaned text.
                        let tempElement = document.createElement('div');
                        tempElement.innerHTML = originalContentElement.innerHTML;

                        navigator.clipboard.writeText(originalContentElement).then(function () {
                            PbNotifications.instance.showHint(new pbI18n().translate("The content has been copied."))
                        }, function () {
                            PbNotifications.instance.showHint(new pbI18n().translate("The content could not be copied."), "error")
                        });
                    });

                    // SAVE BUTTON.
                    $(document).on('click', '#saveContent', function (e) {
                        this.saveEditorData();
                    }.bind(this));
                }.bind(this))
                    .catch(function (error) {
                        PbNotifications.instance.showAlert(new pbI18n().translate("An error has occurred. Please contact the support."));
                    });
            }
        }.bind(this))
    }

    initEditorWithData(contentData) {
        PbTpl.instance.renderInto('questionaire/templates/questionaire_editor.html', {document: contentData,}, "#questionaire_editor");

        new PbEditor(CONFIG.DOCUMENTATION.EDITOR, "#content", undefined, undefined, {
            pastePlain: true,
            heightMax: 550,
            events: {}
        }).then((pbeditor) => {
            pbeditor.init().then((initialisedEditor) => {
                RenderAdministration.instance.editor = initialisedEditor;

                // Set the beautified JSON content in the editor.
                RenderAdministration.instance.editor.setContent(`<pre id="editorContent">${JSON.stringify(contentData, null, 2)}</pre>`);
            });
        });
    }

    decodeHTMLEntities(text) {
        let textArea = document.createElement('textarea');
        textArea.innerHTML = text;
        return textArea.value;
    }

    cleanAndParseJson(jsonString) {
        // Remove any non-printable characters and trim spaces
        jsonString = jsonString.replace(/[\u200B-\u200D\uFEFF]/g, '').trim();

        // Replace HTML entities with their correct equivalents and remove new line characters
        jsonString = jsonString.replace(/&quot;/g, '"')
            .replace(/&amp;/g, '&')
            .replace(/&nbsp;/g, ' ')
            .replace(/<[^>]*>/g, '')  // Remove HTML tags
            .replace(/\n/g, '')
            .replace(/\r/g, '');

        // Decode any remaining HTML entities
        jsonString = this.decodeHTMLEntities(jsonString);
        jsonString = jsonString.trim();

        // Replace multiple spaces with a single space
        jsonString = jsonString.replace(/\s\s+/g, ' ');


        // Use JSON.parse to convert the cleaned string into a JSON object
        try {
            return JSON.parse(jsonString);
        } catch (error) {
            console.error("Failed to parse JSON:", error);
            console.error("Error occurred with JSON string:", jsonString);
            return null;
        }
    }

    saveEditorData() {
        let content = null;

        // Check if the editor instance is available
        if (RenderAdministration.instance.editor) {
            // Get the content from the Froala editor
            let editorContent = RenderAdministration.instance.editor.getContent();

            // Create a temporary element to strip the tags
            let tempElement = document.createElement('div');
            tempElement.innerHTML = editorContent;

            // Extract the inner text which will be the JSON data
            content = tempElement.innerText;

            // Decode HTML entities
            content = this.decodeHTMLEntities(content);
        }

        // Clean and parse the JSON content
        let jsonData = this.cleanAndParseJson(content);
        if (jsonData === null) {
            PbNotifications.instance.showHint("Failed to parse JSON data.", "error");
            return;
        }

        new PbNotifications().ask(
            new pbI18n().translate("Do you want to save the changes?"),
            new pbI18n().translate("Saving..."),
            () => {
                let promises = [this.questionaires.putQuestionaires(jsonData)];

                new PbNotifications().blockForLoading('#contentwrapper');

                Promise.all(promises).then(function (return_values) {
                    let task_id = return_values[0]["task_id"];
                    RenderAdministration.instance.taskhandler.setTaskMethod(task_id, function (event) {
                        if (event.data.data.event == "questionaireConfigPut") {
                            PbNotifications.instance.showHint(new pbI18n().translate("The data has been saved. The page is now reloading."));
                            setTimeout(function () {
                                location.reload();
                            }, 3000);
                        }

                        if (event.data.data.event == "questionaireConfigPutFailed") {
                            let proper_version = event.data.data.args.data;
                            PbNotifications.instance.ask(new pbI18n().translate(`The data wasn't saved. Probably someone else already made changes. Shall we reload the config to get the latest version? ({{version}})`).replace("{{version}}", proper_version), new pbI18n().translate("Data not saved!"), function () {
                                location.reload();
                            }, function () {
                                new PbNotifications().unblock('#contentwrapper');
                            });
                        }
                    });
                }.bind(this));
            },
            () => {
                new PbNotifications().unblock('#detail_view_tpl');
            },
            "info"
        );
    }

    initListeners() {
        document.addEventListener("command", function _(event) {
            RenderAdministration.instance.taskhandler.execute(event.data.data.task_id, event);
        });


        $("#shareButton").on("click", function () {
            let chosenQuestionnaire = $("#chosenQuestionnaire").val();

            if (!chosenQuestionnaire) {
                PbNotifications.instance.showHint(new pbI18n().translate("Either an existing questionnaire or a new one must be selected."), "error");
                $('#questionaire_dropdown_id').addClass('alert-danger');
                $('#questionaire_dropdown_id').removeClass('hidden');
            } else {
                $('#questionaire_dropdown_id').removeClass('alert-danger');
                $('#questionaire_dropdown_id').addClass('hidden');

                $("#shareEventModal").modal("show");
                $('#shareEventLink').val("Linkgenerierung...");

                let timeout = setTimeout(() => {
                    $('#shareEventLink').val("Linkgenerierung fehlgeschlagen.");
                }, 3000)

                RenderAdministration.instance.questionaires.createInviteCode(chosenQuestionnaire).then((qr_code_id) => {
                    clearTimeout(timeout);

                    let link = CONFIG.API_BASE_URL + `/sqr/${qr_code_id}`;
                    $('#shareEventLink').val(link);

                    let qr_code_url = CONFIG.API_BASE_URL + "/get_qrcode/" + qr_code_id;
                    $('#ShareQrCode').attr("src", qr_code_url);
                }).catch((error) => {
                    $('#shareEventLink').val("Linkgenerierung fehlgeschlagen.");
                })
            }
        });

        $("#btnCopyInviteLink").on("click", function () {
            let copyValue = $("#shareEventLink").val();
            navigator.clipboard.writeText(copyValue).then(function () {
                $("#success").text(new pbI18n().translate("Successfully copied")).removeClass('hidden');
                $("#textAndCopy").addClass('hidden')

                setTimeout(function () {
                    $("#shareEventLink").val(copyValue);
                    $("#textAndCopy").removeClass('hidden');
                    $("#success").addClass('hidden');
                }, 3000);
            }).catch(function (error) {
                $("#failure").text(new pbI18n().translate("Copy failed")).removeClass('hidden');
                $("#textAndCopy").addClass('hidden');
            });
        });
    }
}