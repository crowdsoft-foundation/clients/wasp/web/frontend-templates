import {PbQuestionaire} from "../../csf-lib/pb/questionaire/classes.js";

export class QuestionaireForm extends PbQuestionaire {
    PbTpl;

    constructor(PbTplInstance, questionaireConfig, answers = []) {
        super(questionaireConfig);
        this.answers = answers;
        this.PbTpl = PbTplInstance;
    }

    renderInto(selector, postProcessingCallback, onSubQuestionDelete) {
        return this.PbTpl.renderIntoAsync('questionaire/templates/questionaire_form.html', {
            "title": this.title,
            "questions": this.questions,
            "image_url": this.image_url,
        }, selector).then(function () {
            for (let question of this.questions) {
                if (question.hidden === true) {
                    if (question.options.subquestion_to_id) {
                        $(`[data-answercontainerforid=${question.options.subquestion_to_id}] input`).click(function (event) {
                            if ($(event.target).is(":checked")) {
                                question.hidden = false
                                $(`#question_${question.id}`).remove()
                                this.PbTpl.renderIntoAsync('csf-lib/pb/questionaire/tpl/question.html', {
                                    "question": question,
                                    "class": "subquestion_depth_1"
                                }, `[data-answercontainerforid=${question.options.subquestion_to_id}]`, "append").then(function () {
                                    postProcessingCallback()
                                })
                            } else {
                                onSubQuestionDelete(question)
                                $(`#question_${question.id}`).remove()
                            }
                        }.bind(this))
                    }
                }
            }

            const rangeInputs = document.querySelectorAll('input[type="range"]');
            rangeInputs.forEach(function (rangeInput) {
                rangeInput.addEventListener('input', function (event) {
                    $(`[data-range-display-question-id=${event.target.dataset.question_id}]`).html($(event.target).val())
                });
            });

            const stars = document.querySelectorAll('.star-rating input');
            stars.forEach(star => {
                star.addEventListener('change', () => {
                    const questionId = star.name.split('_')[1];
                    const answerValueElement = document.querySelector(`[data-answer-display="answer_value_${questionId}"]`);

                    if (answerValueElement) {
                        answerValueElement.textContent = star.value;
                    }

                    stars.forEach(s => {
                        if (s.checked) {
                            s.parentElement.classList.add('selected');
                        } else {
                            s.parentElement.classList.remove('selected');
                        }
                    });
                });
            });
        }.bind(this));
    }
}
