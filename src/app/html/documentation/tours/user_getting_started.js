window.tour_config = {
	header: "Touren im Dokumentation",
	intro: "Schnellstarthilfe für Dokumentation",
	tours: {
		firstSteps: {
			id: "firstSteps",
			title: "Erste Schritte im Dokumentation",
			desc: "Lerne die Oberfläche des Dokumentationstools kennen.",
			startPage: "/documentation/index.html",
			startStep: 1,
			prevTour: null,
			nextTour: "newDoc",
			permission: "contacts",
		},
		newDoc: {
			id: "newDoc",
			title: "Erstellen und Löschen neuer Dokumente",
			desc: "Lerne, wie man ein neues Dokument erstellen und löschen kann.",
			startPage: "/documentation/index.html",
			startStep: 7,
			prevTour: "firstSteps",
			nextTour: null,
			permission: "contacts",
		},
	},
	tourSteps: [
		{
			//1
			title: "Willkommen",
			disableInteraction: true,
			intro:
				"Willkommen in der <strong>Dokumentations-App</strong>.<br/> Diese Tour führt dich durch die Oberfläche der Verwaltung von Dokumenten.<br/>Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.<br/>",
		},
		{
			//2
			title: "Dokumentenliste",
			element: ".az-content-left",
			disableInteraction: true,
			intro:
				"Die Verwaltung von Dokumenten hat zwei wesentliche Inhaltselemente. Auf der linken Seite wird dir die Dokumentenliste angezeigt. Die Standard-Dokumentlistenansicht ist ungefiltert und enthält eine Liste aller für dich sichtbaren Dokumente. Diese Liste wird automatisch gefiltert, wenn du von einem verweisenden Element wie einem Kontakt, einem Termin oder einer Aufgabe kommst.",
			myBeforeChangeFunction: function (PbGuide) {},
		},
		{
			//3
			title: "Suche",
			element: "#documents_search_input",
			disableInteraction: true,
			intro:
				"Hier kannst du die Dokumentenliste durchsuchen. Wenn du nach etwas suchst, werden nur noch die Dokumente angezeigt, in denen der Suchbegriff vorkommt. Um wieder alle Einträge angezeigt zu bekommen, leere einfach das Suchfeld wieder. Die Suche startet automatisch bei jeder Änderung, die du am Suchbegriff vornimmst.",
		},
		{
			//4
			title: "Dokument anlegen",
			element: "#button-new-document",
			disableInteraction: true,
			intro:
				"Wenn du dieses Plus-Symbol anklickst, kannst du ein neues Dokument anlegen. Hierfür wird im Detailbereich eine Eingabemaske geöffnet.",
		},
		{
			//5
			title: "Detail-Ansicht",
			element: "#azDocBody",
			disableInteraction: true,
			intro:
				"Auf der rechten Seite der Verwaltung von Dokumenten befindet sich die Detailansicht. Hier werden dir den Inhalt eines ausgewählten Dokuments angezeigt bzw. das Formular zum ändern oder eintragen eines neuen Dokuments.",
			myBeforeChangeFunction: function (PbGuide) {
				$("#button-new-document").click();
			},
		},
		{
			//6
			title: "Tour abgeschlossen",
			intro:
				"Damit hast du unseren Rundgang abgeschlossen. Wenn du möchtest, können wir aber auch gleich mit der nächsten Tour weitermachen. Du kannst diese aber auch später starten, indem du auf das Fragezeichen klickst und die gewünschte Tour auswählst. <br/><strong>Möchtest du mit der nächsten Tour weitermachen?</strong>",
			myBeforeChangeFunction: function (PbGuide) {
				PbGuide.markTourSeen("documentation.firstSteps");
				PbGuide.setPrevButton("Beenden", function () {
					PbGuide.introguide.exit();
				});

				PbGuide.setNextButton("Weiter", function () {
					let nextTour = PbGuide.tourConfig.tours.firstSteps.nextTour;
					let nextStartStep = PbGuide.tourConfig.tours[nextTour].startStep;
					PbGuide.introguide.goToStepNumber(nextStartStep);
				});
			},
		},
		{
			//7
			title: "Neues Dokument",
			intro:
				"Diese Tour zeigt dir interaktiv, wie du ganz einfach einen <strong>Dokument anlegen</strong> und wieder <strong>löschen</strong> kannst.<br/> Klicke einfach auf die Pfeilschaltflächen, um durch die Tour zu navigieren. Klicke auf <strong> x</strong>, um die Tour abzubrechen.<br/>",
			myBeforeChangeFunction: function (PbGuide) {
				let object = document.querySelector("#button-new-document");
				if ($(window).width() < 767) {
					object.click();
					PbGuide.updateIntro(
						"Mit einem Klick auf die Schaltfläche mit dem<strong> +</strong>-Symbol in den Listansicht, gelangst du in die Detailansicht, und kann mit der Erstellung eines neuen Dokuments beginnen."
					);
					PbGuide.setNextButton("Weiter", function () {
						PbGuide.introguide.goToStepNumber(9);
					});
				}
			},
		},
		{
			//8
			title: "Dokument anlegen",
			element: "#button-new-document",
			intro:
				"Um einen neuen Dokument anzulegen, klicke jetzt bitte auf das <strong>+</strong>-Symbol.",
			myBeforeChangeFunction: function (PbGuide) {
				PbGuide.hideNextButton();

				let object = document.querySelector("#button-new-document");
				if (object) {
					object.addEventListener(
						"click",
						(e) => {
							PbGuide.introguide.nextStep();
						},
						{ once: true }
					);
				}
			},
		},
		{
			//9
			title: "Name des Dokuments",
			element: "#azDocBody",
			disableInteraction: false,
			intro:
				"Gib deine Dokument ein <b>Name</b> und klicke dann auf <strong>Weiter</strong>",
			myBeforeChangeFunction: function (PbGuide) {
				PbGuide.hidePreviousButton();
				PbGuide.setNextButton("Weiter", function () {
					object1.focus();
					if (object1.value == "") {
						$("#document_title_input").attr(
							"value",
							"1. Mein erstes Testdokument"
						);
					}
				});

				let object1 = document.querySelector("#document_title_input");
				if (object1) {
					object1.classList.add("invalid");
					object1.addEventListener(
						"focus",
						(e) => {
							object1.classList.remove("invalid");
						},
						{ once: true }
					);
				}
			},
		},
		{
			//10
			title: "Erweiterte Details",
			element: "#azDocBody",
			intro:
				"In dieser Bereich können Dokumenteigenschaften für Informationen wie Statistiken, Recherchen und Querverweise eingegeben werden.",
			myBeforeChangeFunction: function (PbGuide) {
				PbGuide.setNextButton("Weiter");
				let object = document.querySelector("#documentPropertiesToggle");
				if (object.classList.contains("collapsed")) {
					$("#documentPropertiesToggle").click();
				}
			},
		},
		{
			//11
			title: "WYSIWYG-Texteditor",
			element: "#azDocBody",
			intro:
				"Füge Texte ein oder schreibe einfach in den Rich-Text-Editorbereich. Die Toolbar gruppiert alle Formatierungsaktionen nach Bereich in Kategorien. Richte einen Nextcloud-Bereich ein und füge Bilder, Videos und PDFs in deine Dokumente ein.",
			myBeforeChangeFunction: function (PbGuide) {
				let object = document.querySelector(".fr-element");

				PbGuide.setNextButton("Weiter", function () {
					object.focus();
				});

				if (object.innerHTML == "<p><br></p>") {
					$(".fr-element").html(
						"<h6>Schnelle Notizen überall und jederzeit.</h6><h2>Das Tool zum Erstellen<br>und Teilen von Dokumenten und Notizen</h2><p>Erstelle ein neues Dokument oder Notiz. Öffne und bearbeite eine vorhandenen Eintrag aus der Liste. Nach dem Speichern steht die neuen Version allen zugelassenen Benutzern zur Verfügung.</p><ul><li>Ideen, Links, To-Do-Listen oder Klartext festhalten</li><li>Hilft Text sofort zu schreiben und zu bearbeiten</li></ul>"
					);
					object.addEventListener("focus", (e) => {}, { once: true });
				}
			},
		},
		{
			//12
			title: "Dokument speichern",
			element: "#azDocBody",
			intro:
				"Klicke jetzt bitte auf den <strong>Speichern</strong>-Button. Keine Sorge, wie man den Dokument wieder löscht, erklären wir dir auch gleich.",
			myBeforeChangeFunction: function (PbGuide) {
				PbGuide.hideNextButton();

				let object = document.querySelector("#save_document_button");
				if (object) {
					object.classList.add("text-primary");
					object.addEventListener(
						"click",
						(e) => {
							PbGuide.introguide.nextStep();
						},
						{ once: true }
					);
				}
			},
		},
		{
			//13
			title: "Neues Dokument gespeichert",
			disableInteraction: true,
			intro:
                "Dir sollte jetzt eine Erfolgsmeldung angezeigt werden, und das neue Dokument sollte in der Liste angezeigt werden.",
			myBeforeChangeFunction: function (PbGuide) {
				if ($(window).width() < 767) {
					PbGuide.updateIntro(
						"Dir sollte jetzt eine Erfolgsmeldung angezeigt werden, und das neue Dokument wird angelegt und sollte in der Liste angezeigt werden."
					);
				}
			},
		},
		{
			//14
			title: "Dokument auswählen",
			element: ".az-content-left",
			intro:
                "Nun wollen wir das Dokument wieder löschen. Im Detailbereich sollten Sie die Informationen des Dokuments sehen.",
		},
		{
			//15
			title: "Dokument löschen",
			element: "#azDocBody",
			intro:
                "Klick jetzt bitte den <strong>Löschen</strong>-Button und bestätige dass du den Kontakt löschen möchtest.",
			myBeforeChangeFunction: function (PbGuide) {
				PbGuide.hideNextButton();
				let object = document.querySelector("#delete_document_button");
				if (object) {
					object.classList.add("text-primary");
					object.addEventListener(
						"click",
						(e) => {
							PbGuide.introguide.exit();
						},
						{ once: true }
					);
				}
				document.addEventListener(
					"command",
					function (event) {
						PbGuide.introguide.goToStepNumber(15).start();
					},
					{ once: true }
				);
			},
		},
		{
			//16
			title: "Dokument gelöscht",
			element: "body",
			intro:
				"Das Dokument wurde gelöscht und aus der Liste der Dokumente entfernt.",
		},
		{
			//17
			title: "Tour abgeschlossen",
			intro:
				"Sehr gut! Damit hast du die Tour zum Anlegen und Löschen von Dokumenten abgeschlossen.",
			myBeforeChangeFunction: function (PbGuide) {
				PbGuide.markTourSeen("documentation.newDoc");
				PbGuide.setPrevButton("Beenden", function () {
					PbGuide.introguide.exit();
				});
			},
		},
	],
};