<ul class="list-unstyled">
    {% for attachment in document.data.attachments %}
    {% if attachment %}
    {% set parts = attachment.split('/') %}
    {% set subparts = parts[parts.length-1].split('|') %}
    <li class="list">
<!--        <a class="attachment_delete_link" href="#" data-attachmentdeletelink="{{attachment}}" target="_blank"><i class="typcn typcn-trash"></i></a>-->
        <a class="attachment_link" href="{{attachment}}" target="_blank">{{subparts[subparts.length-1]}}</a>
    </li>
    {% endif %}
    {% endfor %}
</ul>