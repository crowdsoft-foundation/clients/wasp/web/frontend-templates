<div id="document_list_container">
    {% for item in documents.documentations %}
        {% if item.show %}
            <div id=`listentry_{{item.document_id}}` data-listentry="{{item.document_id}}" class="az-contact-item pl-1">
                <i class="typcn typcn-document-text tx-22 text-muted pr-2"></i>
                <div class="az-contact-body ml-0">
                    <h6>
                        {% if item.data.title %}
                            {{item.data.title}}
                        {% else %}
                            {{item.document_id}}
                        {% endif %}</h6>
                    <span>{{item.creator}} {{item.create_time | date("D MMM")}}</span><br/>
                    {% for tag in item.data.tags|sort() %}
                        <span class="badge badge-data-tags mr-2 mt-1">{{tag}}</span>
                    {% endfor %}
                </div>
            </div>
        {% endif %}
    {% endfor %}
</div>
