<div class="" style="">
    {% for item in documents.documentations %}
        {% if item.show %}
            <div id="contactItem" class="archived-table-row">
                <div class="archived-table-data-cell">
                    <h6>
                        {% if item.data.title %}
                            {{item.data.title}}
                        {% else %}
                            {{item.document_id}}
                        {% endif %}
                    </h6>
                    <span>{{item.creator}} {{item.create_time | date("D MMM")}}</span><br/>

                    {% for tag in item.data.tags|sort() %}
                        <span class="badge badge-data-tags mr-2 mt-1">{{tag}}</span>
                    {% endfor %}
                </div>
                <div class="archived-table-data-cell">
                    <a data-permission='frontend.documentation.editor.archive' href="#" data-document-recycle="{{item.document_id}}" class="">
                        <i class="fa fa-recycle tx-dark-blue tx-16 mr-2" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        {% endif %}
    {% endfor %}
</div>