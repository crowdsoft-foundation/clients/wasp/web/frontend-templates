import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {
    getQueryParam,
    getCookie,
} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {pbI18n} from "../../csf-lib/pb-i18n.js?v=[cs_version]"
import {ContactManager} from "../../contact-manager/js/contact-manager.js?v=[cs_version]"
import {PbTpl} from "../../csf-lib/pb-tpl.js?v=[cs_version]"
import {pbEventEntries} from "../../easy2schedule/js/pb-event-entries.js?v=[cs_version]"
import {PbNotifications} from "../../csf-lib/pb-notifications.js?v=[cs_version]"
import {PbTagsList} from "../../csf-lib/pbapi/tagmanager/pb-tags-list.js?v=[cs_version]"

export class RenderArchived {
    constructor(socket, DocumentationModuleInstance) {
        if (!RenderArchived.instance) {
            let nj = nunjucks.configure(CONFIG.APP_BASE_URL, {autoescape: false})
            nj.addFilter('date', (data) => {
                let createtime = moment(data)
                return createtime.format(CONFIG.DATETIMEFORMAT)
            })

            RenderArchived.instance = this
            RenderArchived.instance.documentationModuleInstance = DocumentationModuleInstance
            RenderArchived.instance.displayProperties = getCookie("dcp") == "true"
            RenderArchived.instance.source_id = getQueryParam("sid")
            RenderArchived.instance.initViewListener()
            RenderArchived.instance.notifications = new PbNotifications()
            RenderArchived.instance.tagListModel = new PbTagsList()
            RenderArchived.instance.handledTaskIds = []
            RenderArchived.instance.activeDocument = undefined
        }

        return RenderArchived.instance
    }

    init() {
        let contact = new ContactManager()
        let eventEntries = new pbEventEntries()
        let promises = [contact.init(), eventEntries.init()]
        Promise.all(promises).then(function () {
            RenderArchived.instance.contacts_list = contact.contacts_list

            let fetchFunction = RenderArchived.instance.documentationModuleInstance.docs.fetchAllDocuments

            fetchFunction(false, true).then((docs) => {
                RenderArchived.instance.documentationModuleInstance.docs.filter($("#documents_search_input").val())
                RenderArchived.instance.renderDocumentList(docs)
            })
        })

        if ($("#loader-overlay")) {
            $("#loader-overlay").hide()
            $("#contentwrapper").show()
        }
    }

    renderDocumentList(DocumentsInstance) {
        RenderArchived.instance.documentsInstance = DocumentsInstance
        $("#azDocumentListArchived").html('')

        PbTpl.instance.renderIntoAsync('documentation/templates/documents-archive-list.tpl', {documents: DocumentsInstance}, "#azDocumentListArchived").then(() => {
            $('[data-document-recycle]').click((event) => {
                let documentId = $(event.currentTarget).data('document-recycle');

                new PbNotifications().blockForLoading('#list_container')
                new PbNotifications().ask(new pbI18n()
                    .translate("Möchten Sie das Dokument wirklich wiederherstellen?"),
                new pbI18n().translate("Achtung..."),
                () => {
                     RenderArchived.instance.documentationModuleInstance.docs.restoreDocument(documentId).catch(() => {
                    })
                },
                () => {
                    new PbNotifications().unblock('#list_container')
                })
            })

            new PbNotifications().unblock('#list_container')

        })
    }

    initViewListener() {
        let self = this

        document.addEventListener("command", function _(event) {
            if (event.data.data.command == "updatedDocument" || event.data.data.command == "documentArchived") {
                this.init()
            }
        }.bind(this))
    }
}
