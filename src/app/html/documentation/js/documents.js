import {fireEvent, getCookie} from "../../csf-lib/pb-functions.js?v=[cs_version]"
import {PbBrowserDb} from "../../csf-lib/pb/indexeddb/PbBrowserDb.js?v=[cs_version]"
import {CONFIG} from "../../pb-config.js?v=[cs_version]"
import {PbTaskHandler} from "../../csf-lib/pb-taskhandler.js?v=[cs_version]"

export class PbDocuments {
    constructor(apikey, config, socket, onDataReadyFunc = () => "", onBackendUpdateFunc = () => "", onSelfInducedUpdateFunc = () => "") {
        if (!PbDocuments.instance) {
            PbDocuments.instance = this
            PbDocuments.instance.apikey = apikey
            PbDocuments.instance.config = config
            PbDocuments.instance.socket = socket
            PbDocuments.instance.documentations = []
            PbDocuments.instance.taskhandler = new PbTaskHandler()
            PbDocuments.instance.onDataReadyFunc = onDataReadyFunc
            PbDocuments.instance.onBackendUpdateFunc = onBackendUpdateFunc
            PbDocuments.instance.onSelfInducedUpdateFunc = onSelfInducedUpdateFunc
            PbDocuments.instance.initBackendListeners()
            let dbStores = {
                autosaved_documents: "document_id, data"
            }
            PbDocuments.instance.db = new PbBrowserDb("documents", dbStores, 1)
            PbDocuments.instance.db.init()
        }
        return PbDocuments.instance
    }

    filter(searchterm = "") {
        for (let document of this.documentations) {
            document.matches(searchterm)
        }
    }

    setDocumentsFromApiResponse(documentations) {
        this.documentations = []
        for (let document_data of documentations) {
            this.documentations.push(new PbDocument(document_data))
        }
        //Documents.instance.documentations = documentations
    }

    getDocumentById(documentId) {
        let document_index = this.documentations.map(i => i.document_id).indexOf(documentId)
        let document = this.documentations[document_index]

        return document
    }

    fetchDocumentsBySearchString(search) {

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": PbDocuments.instance.config.API_BASE_URL + "/get_documents/" + search,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": PbDocuments.instance.apikey
                },
                "success": (response) => {
                    PbDocuments.instance.setDocuments(response)
                    resolve(PbDocuments.instance)
                },
                "error": () => reject(),
            };

            $.ajax(settings)
        })
    }

    fetchAllDocuments(include_archive= false, only_archived = false) {

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": PbDocuments.instance.config.API_BASE_URL + `/get_documents?include_archived=${include_archive?1:0}&only_archived=${only_archived?1:0}`,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": PbDocuments.instance.apikey
                },
                "success": (response) => {
                    PbDocuments.instance.setDocumentsFromApiResponse(response)
                    resolve(PbDocuments.instance)
                },
                "error": () => reject(),
            };

            $.ajax(settings)
        })
    }

    fetchDocumentsBySourceId(id) {

        return new Promise(function (resolve, reject) {
            var settings = {
                "url": PbDocuments.instance.config.API_BASE_URL + "/ds/get_documents_by_source_id/" + id,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": PbDocuments.instance.apikey
                },
                "success": (response) => {
                    PbDocuments.instance.setDocumentsFromApiResponse(response)
                    resolve(PbDocuments.instance)
                },
                "error": () => reject(),
            };

            $.ajax(settings)
        })
    }

    autosave(documentation) {
        PbDocuments.instance.db.bulkPut("autosaved_documents", [
            {
                "document_id": documentation.document_id,
                "data": documentation
            }
        ])
    }

    deleteAutosave(document_id) {
        return new Promise(function (resolve, reject) {
            PbDocuments.instance.db.delete("autosaved_documents", {"document_id": document_id}).then(function (deleteCount) {
                resolve(deleteCount)
            }.bind(this))
        })
    }

    getAutosavedDocumentById(document_id) {
        return new Promise(function (resolve, reject) {
            PbDocuments.instance.db.read("autosaved_documents", {"document_id": document_id}).then(function (data) {
                if (data.length > 0) {
                    resolve(data[0].data)
                } else {
                    resolve(false)
                }
            }.bind(this))
        })
    }

    save(documentation) {
        return new Promise(function (resolve, reject) {

            let url = PbDocuments.instance.config.API_BASE_URL + "/es/cmd/createNewDocument"

            if (documentation.document_id && documentation.document_id.length > 0) {
                url = PbDocuments.instance.config.API_BASE_URL + "/es/cmd/updateDocument"
            } else {
                delete documentation["document_id"]
                if (!documentation["external_id"] || documentation["external_id"].length == 0) {
                    delete documentation["external_id"]
                }
            }

            let settings = {
                "url": url,
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": PbDocuments.instance.apikey,
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify(documentation),
                "success": function (response) {
                    PbDocuments.instance.taskhandler.setTaskMethod(response.task_id, PbDocuments.instance.onSelfInducedUpdateFunc)
                    PbDocuments.instance.deleteAutosave(response.document_id)
                    resolve(response)

                },
                "error": function (response) {
                    reject(response)
                }
            }

            $.ajax(settings)
        })
    }

    delete(documentation) {
        let external_id = documentation['external_id']
        let document_id = documentation['document_id']
        return new Promise((resolve, reject) => {

            let settings = {
                "url": PbDocuments.instance.config.API_BASE_URL + "/es/cmd/deleteDocument",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": PbDocuments.instance.apikey,
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "external_id": external_id,
                    "document_id": document_id
                }),
                "success": function (response) {
                    resolve(response)

                },
                "error": function (response) {
                    reject(response)
                }
            };

            $.ajax(settings)
        })
    }

    archive(documentation) {
        let document_id = documentation['document_id']
        return new Promise((resolve, reject) => {
            var settings = {
                "url": PbDocuments.instance.config.API_BASE_URL + "/es/cmd/archiveDocument",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": PbDocuments.instance.apikey,
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "document_id": document_id
                }),
                "success": function (response) {
                    resolve(response)

                },
                "error": function (response) {
                    reject(response)
                }
            };

            $.ajax(settings)
        })
    }

    initBackendListeners() {
        this.socket.commandhandlers["newDocument"] = function (args) {
            PbDocuments.instance.taskhandler.execute(args.task_id, args)
            PbDocuments.instance.deleteAutosave("")
            PbDocuments.instance.onBackendUpdateFunc(args)
            fireEvent("newPBHistory", args)
        }

        this.socket.commandhandlers["updatedDocument"] = function (args) {
            PbDocuments.instance.taskhandler.execute(args.task_id, args)
            PbDocuments.instance.onBackendUpdateFunc(args)
            fireEvent("updatePBHistory", args)
        }

        this.socket.commandhandlers["deletedDocument"] = function (args) {
            PbDocuments.instance.onBackendUpdateFunc(args)
        }

        this.socket.commandhandlers["documentArchived"] = function (args) {
            PbDocuments.instance.onBackendUpdateFunc(args)
            PbDocuments.instance.onSelfInducedUpdateFunc(args)
        }

        this.socket.commandhandlers["documentRestored"] = function (args) {
            PbDocuments.instance.onBackendUpdateFunc(args)
            PbDocuments.instance.onSelfInducedUpdateFunc(args)
        }

        this.socket.commandhandlers["toBeDeletedDocumentNotFound"] = function (args) {
            PbDocuments.instance.onBackendUpdateFunc(args)
        }
    }

    restoreDocument(document_id) {
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": PbDocuments.instance.config.API_BASE_URL + "/es/cmd/restoreDocument",
                "method": "POST",
                "timeout": 0,
                "headers": {
                    "apikey": PbDocuments.instance.apikey,
                    "Content-Type": "application/json"
                },
                "data": JSON.stringify({
                    "document_id": document_id
                }),
                "success": resolve,
                "error": reject
            };

            $.ajax(settings)
        })
    }

}

export class PbDocument {
    constructor(data = {}) {
        this.show = true
        this.document_id = data.document_id || ""
        this.external_id = data.external_id || ""
        this.data = data.data || {}
        this.data_owners = data.data_owners || []
        this.links = data.links || []
        this.create_time = data.create_time || []
        this.creator = data.creator || ""
        this.correlation_id = data.correlation_id || ""
    }

    matches(value) {
        this.show = true
        let searchstring = this.creator + " | " + this.data.content + " | " + this.data.title + " | " + moment(this.create_time).format(CONFIG.DATETIMEWIHTOUTSECONDSFORMAT)
        if (this.data.tags) {
            searchstring += " | " + this.data.tags.join(" | ")
        }

        let subsearches = value.split(" ")
        searchstring = searchstring.toLowerCase()
        for (let search of subsearches) {
            if (searchstring.includes(search.toLowerCase())) {
                this.show = this.show && true
            } else {
                this.show = this.show && false
            }
        }
    }

    setDataProperty(name, value) {
        if (name != "documentId") {
            if (name == "time_spent") {
                value = Number(value)
            }
            this.data[name] = value
        }
    }

    setDocumentId(document_id) {
        this.document_id = document_id
        if (!this.external_id || this.external_id.length == 0) {
            this.setExternalId(document_id)
        }
    }

    setExternalId(external_id) {
        this.external_id = external_id
    }

    setSourceId(source_id) {
        this.data.source_id = source_id
    }

    setContent(content) {
        this.data.content = content
    }

    setLinks(links) {
        if (!Array.isArray(links)) {
            throw 'Links must be an array'
        }
        this.links = links
    }

    setDataOwners(owners) {
        if (!Array.isArray(owners)) {
            throw 'Owners must be an array'
        }
        this.data_owners = owners
    }

    setObservers(observers) {

        if (!Array.isArray(observers)) {
            throw 'Observers must be an array'
        }
        this.data["observers"] = observers
        console.log("Observers set", this.data)
    }

    setTags(tags) {
        if (!Array.isArray(tags)) {
            throw 'Tags must be an array'
        }
        this.data.tags = tags
    }

    setCreateTime(time) {
        throw 'CreateTime is readonly'
    }

    setCreator(time) {
        throw 'Creator is readonly'
    }

    setCorrelationId(cid) {
        throw 'CorrelationID is readonly'
    }

    setCustomFields(fields) {
        if (typeof fields != "object") {
            throw 'fields must be an object'
        } else {
            this.data = Object.assign({}, fields, this.data)
        }
    }

    setSourceId(sid) {
        this.data['source_id'] = sid
    }

    fetchDocumentHistoryFromApi(api_base_url, apikey) {
        let document_id = this.document_id

        // TODO this is just copied from contact manager
        let event_types = ["newDocumentCreated", "documentUpdated", "documentDeleted"]
        return new Promise(function (resolve, reject) {
            var settings = {
                "url": `${api_base_url}/es/event/history?search=${document_id}&event_types=${event_types.join(",")}`,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "apikey": apikey
                },
                "success": (response) => {
                    resolve(response)
                }
            }

            $.ajax(settings)
        })
    }

}