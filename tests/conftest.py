from datetime import datetime

import pytest

from tests.fixtures.generics import *
from tests.fixtures.accessrights import *
from py.xml import html

ENVIRONMENTS = {
    "local": {
        "baseurl": "http://localhost:7001"
    },
    "staging": {
        "baseurl": "https://www.testblick.de"
    },
    "live": {
        "baseurl": "https://www.serviceblick.com"
    }

}


def pytest_addoption(parser):
    parser.addoption(
        "--browser_engine",
        action="store",
        default="firefox",
        help="To be used browser-engine, can be firefox or chrome (drivers has to be installed)"
    )
    parser.addoption(
        "--headless",
        action="store",
        default="no",
        help="Whether the test-browser should be called in headless-mode"
    )
    parser.addoption(
        "--size",
        action="store",
        default="max",
        help="The window-size for the test-browser for example 640x480. Use 'max' to maximise window"
    )
    parser.addoption(
        "--environment",
        action="store",
        default="local",
        help="The environment the test shall be executed against. Can be local, staging or live"
    )
    parser.addoption(
        "--test_auth",
        action="store",
        default="local",
        help="Basic auth to be used for authentication to selenium grid"
    )
    parser.addoption(
        "--credentials",
        action="store",
        default="local",
        help='Username and password to use for all login required actions. May not exist. Format: username:passwort'
    )
    parser.addoption(
        "--admin_credentials",
        action="store",
        default="local",
        help='Username and password to use for all admin-login required actions. Has to exist. Format: username:passwort'
    )


def pytest_html_results_table_header(cells):
    cells.insert(2, html.th("Description"))
    cells.insert(1, html.th("Time", class_="sortable time", col="time"))
    cells.pop()


def pytest_html_results_table_row(report, cells):
    cells.insert(2, html.td(report.description))
    cells.insert(1, html.td(datetime.utcnow(), class_="col-time"))
    cells.pop()


@ pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    report.description = str(item.function.__doc__)


