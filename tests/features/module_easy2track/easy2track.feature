@module_easy2track
Feature: Easy2Track
  As a registered user with easy2track license,
  I want to be able to start the easy2schedule application from the dashboard,
  So that I can use the tool.

  Scenario: Try to access easy2track application without permissions
    Given I'm a user without easy2track permissions
    When I visit the easy2track statistics page
    Then I want to be redirected to the login-page

  Scenario: Get to easy2track classic-QR client pages
    Given I'm logging in
    When I press the "zu Ihrem QR-Code" link
    Then I want to be redirected to the easy2track client-page
    Then I want a qrcode to be shown to me
    When I click on the qrcode
    Then I want to be redirected to the Easy2Track Classic form

  Scenario: Get to easy2track form client page
    Given I'm logging in
    When I press the "zum Formular" link
    Then I want to be redirected to the easy2track client-form

  Scenario: Get to easy2track scanner page
    Given I'm logging in
    When I press the "zum Scanner" link
    Then I want to be redirected to the easy2track scanner-page

  Scenario: Get to easy2track statistics page
    Given I'm logging in
    When I press the "zur Statistik" link
    Then I want to be redirected to the easy2track statistics-page

