@module_dashboard_roles
Feature: System Role Management
  A logged in user with role administration rights, I am able to access the role management area and manage role permissions and details.

  Background: I am logged in and have role administration rights
    Given I am logged in and have role administration rights
  
  
  Scenario Outline: As an admin, I want to get to role management page from the dashboard and create, edit and delete a new role
    Given the dashboard page is shown
    And I click the role management link  
    And the page for the role management is displayed 
    And I fill in the form fields in with <name> and <description> for creating a new role 
    When I click the button to create a new role 
    And a role named <name> can be found in the table of roles
    And I click the edit button next to a chosen role <name> in the table of roles
    And the detail page for the chosen role is displayed with <name> and <description>
    Then I "edit" the description with a <newdescription>
    And I "save" the new description <newdescription>
    And I go back to the page for the role management
    And I click the delete button next to a chosen role <name> in the table of roles
    And I confirm to delete the chosen role
    And the chosen role <name> is no longer in the table of roles 

    Examples: Chosen role
      |name|description|newdescription|
      |Test_role_editor|this is a basic role that has editing rights for testing|this is a new basic role that has editing rights for testing|
  
  @main_test_feature
  Scenario Outline: As an admin, I want to add and remove permissions from a role by changing its role membership
    Given the detail page for a chosen role with <name> and <description> is shown 
    And I check a role from the role membership list that I want to add
    And the total permission table contains the name of the membership role in the source column
    When I click the membership role tag in the role membership box that I want to remove
    Then the tag with the name of the membership role is removed
    And the total permission table for role <name> does not contain the name of the membership role in the source column

    Examples: Chosen role
      |name|description|
      |Test_role_editor|this is a basic role that has editing rights for testing|




