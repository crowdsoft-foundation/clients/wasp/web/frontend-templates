@module_dashboard
Feature: Dashboard group management
  As a logged in user user with permission system.groups
  I want to be able to be able to access the group management page,
  So that I can manage groups.

  Scenario: Get to group management page
    Given I'm logged in as admin
    When I press the "Gruppenverwaltung" link
    Then I want to see the group management page

#   Scenario Outline: Adding a new group
#    Given I am on the group management page
#    When I fill in the "<name>" and "<description>" form fields
#    When I click on "Neue Gruppe erstellen" button
#    Then A new group with name "<name>" and description "<description>" will show up in the groups table
#    When I click the "Bearbeiten" button for role with name <name>
#    Then I want to be redirected to the edit group page
#    When I click on the back arrow
#    Then I want to see the group management page
#    When I click on the trashbin button for "<name>"
#    Then I want the group with name "<name>" to be deleted and not shown in the group-table anymore
#
#
#    Examples: Credentials
#    | name            |description          |
#    | bdd_test_group  |this group is for automatic testing only|
