@module_dashboard
Feature: Dashboard display
  As a user visiting the dashboard, I want to get informations depending on my state.

  Scenario: Not logged in user
    Given I'm not logged in
    When I'm entering the url /dashboard/ into the browser-address bar
    Then I want to be redirected to the login-page

  Scenario: Logged in as default user
    Given I'm logged in as a user without any special permissions
    When I'm entering the url /dashboard/ into the browser-address bar
    Then I want to see the headline "Willkommen in Deinem Dashboard"
    Then I want to see the application "Verwaltung"
    Then I don't want to see the application "Easy2Track"
    Then I don't want to see the application "Easy2Schedule"
    Then I don't want to see the application "Simplycollect"

  Scenario: Logged in as default user with permission dashboard.rightsmanagment
    Given I'm logged in as demo_1
    When I'm entering the url /dashboard/ into the browser-address bar
    Then I want to see the headline "Willkommen in Deinem Dashboard"
    Then I want to see the application "Verwaltung"
    Then I want to see a link labeled "Benutzerverwaltung"