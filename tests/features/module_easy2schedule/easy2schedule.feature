@module_easy2schedule
Feature: Easy2Schedule
  As a registered user with easy2schedule license,
  I want to be able to be able to start the easy2schedule application from the dashboard,
  So that I can use my calendar.

  Scenario: Try to access easy2schedule application without permissions
    Given I'm a user without easy2schedule permissions
    When I visit the easy2schedule index page
    Then I want to be redirected to the login-page

  Scenario: Get to easy2schedule application
    Given I'm on the dashboard page
    When I press the "zu Kalendar" link
    Then I want to be redirected to the easy2schedule application

  Scenario Outline: Adding new appointments to the calendar
    Given I'm on the easy2schedule page
    When I click on "Neuer Termin"
    When I fill in the "<title>"
    When I press the "Anlegen" button
    Then I want that an a new appointment with title <title> is created
    When I click the dashboard link in the menu
    #When I click the export-button
    #Then I want the new event with title <title> to be in the export
    When I go back to the calendar
    When I delete the event with <title>
    Then No appointment with title <title> does exist

    Examples: Credentials
      | title            |
      | bdd test appointment 1 |

#  Scenario: Open an existing appointment
#    Given I'm on the easy2schedule page
#    When I doubleclick on an event
#    Then I want to see to edit event form

