@module_login
Feature: Logout
  As a registered user,
  I want to be able to log off from system,
  So that nobody else can use my account.

  Scenario: Logout
    Given I'm on the dashboard page
    When I press the logout button
    Then I want to be redirected to the login-page