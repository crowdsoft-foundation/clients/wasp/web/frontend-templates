@module_contactmanager
Feature: Contactmanager

  Scenario: Try to access contact-manager application without permissions
    Given I'm a user without contact-manager permissions
    When I visit the contact-manager index page
    Then I want to be redirected to the login-page

  Scenario: Add new contact
    Given I'm on the contact-manager index page and i have suitable permission
    When I click on the button "<createContactButton>"
    Then A form should be displayed
    When I enter the name <Test-Kontakt>
    When I click on the <Speichern> Button
    Then I want to see a new entry in the list named "<Test-Kontakt>"

  Scenario: Update existing contact
    Given I'm on the contact-manager index page and i have suitable permission
    When I click on the entry in the list named "<Test-Kontakt>"
    Then A form should be displayed
    When I click on the button "<Editieren>"
    When I enter the name <Test-Kontakt editiert> 
    When I click on the <Speichern> Button
    Then I want to see a updated entry in the list named "<Test-Kontakt editiert>"

  Scenario: Delete existing contact
    Given I'm on the contact-manager index page and i have suitable permission
    When I click on the button "<createContactButton>"
    Then A form should be displayed
    When I enter the name <Test-Kontakt to delete>
    When I click on the <Speichern> Button
    Then I want to see a new entry in the list named "<Test-Kontakt to delete>"
    When I click on the entry in the list named "<Test-Kontakt to delete>"
    Then A form should be displayed
    When I click on the <Loeschen> Button
    Then I don't want to see an entry named "<Test-Kontakt to delete>"anymore
