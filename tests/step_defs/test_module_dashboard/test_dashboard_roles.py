from time import sleep
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from pytest_bdd import scenarios, given, when, then, parsers
from sqlalchemy import false
from tests.helper_functions import make_screenshot

scenarios("../../features/module_dashboard/dashboard_roles.feature")

@given("I am logged in and have role administration rights")
def step_impl(browser, admin_login):    
    pass


@given('the dashboard page is shown')
def step_impl(browser, no_js_error):
    browser.is_text_present("Willkommen") 
    no_js_error()

@given('I click the role management link')
def step_impl(browser):
    element = browser.links.find_by_href('./roles.html').last
    sleep(1)
    make_screenshot(browser, "2.1.1 Access role management", "Permissions - Role Management", None,
                    description="Login with your administrator credentials</br></br>On the dashboard, the account management area will be shown in the applications area</br></br>Click the link to the role management page.",
                    next="2.1.2 The role management page")
    element.click()

@given('the page for the role management is displayed')
def step_impl(browser, no_js_error):
    browser.is_text_present("Neue Rolle erstellen")
    make_screenshot(browser, "2.1.2 The role management page", "Permissions - Role Management", None,
                    description="On the role management page, you have the possiblity to create and edit and delete the roles in your account",
                    previous="2.1.1 Access role management",
                    next="2.2.1 Create a new role")
    no_js_error()

@given(parsers.cfparse('I fill in the form fields in with {name} and {description} for creating a new role'))
def step_impl(browser, name, description):
    sleep(1)
    browser.fill('newrolename', name)
    browser.fill('newroledescription', description)
    make_screenshot(browser, "2.2.1 Create a new role", "Permissions - Role Management", None,
                    description="On the role management page, fill in the form with a role name the does not already exist, using onle numbers,letters and underscores.</br></br>Enter a helpful description for the new role in the description field</br></br>Click the create new role button. </br></br>NOTE:If a new role is created with the name of a previously deleted role it will not restore the previous role and its permissions but instead, be reset with default permissions",
                    previous="2.1.2 The role management page",
                    next="2.2.2 New role added to role table")

@when('I click the button to create a new role')
def step_impl(browser):
    button = browser.find_by_id("createRoleButton")
    button.click()

@when(parsers.cfparse('a role named {name} can be found in the table of roles'))
def step_impl(browser, name):
    element = 'rolesEditButtonBootstrap_role.' + name
    sleep(1)
    make_screenshot(browser, "2.2.2 New role added to role table", "Permissions - Role Management", None,
                    description="The new role will shown alphabetically in the list of roles",
                    previous="2.2.1 Create a new role",
                    next="2.3.1 Access role details")
    assert browser.is_text_present(name) is True

@when(parsers.cfparse('I click the edit button next to a chosen role {name} in the table of roles'))
def step_impl(browser, name):
    button = browser.find_by_id("rolesEditButtonBootstrap_role." + name)
    sleep(1)
    make_screenshot(browser, "2.3.1 Access role details", "Permissions - Role Management", None,
                    description="On the role management page, select the edit button next to a role in the table of roles. This will take you to the detail page for the selected role. There you have the possiblity to edit the role's description and permissions.",
                    previous="2.2.2 New role added to role table",
                    next="2.4.1 Edit a role description")
    button.click()

@when(parsers.cfparse('the detail page for the chosen role is displayed with {name} and {description}'))
def step_impl(browser, name, description, no_js_error):
    browser.is_text_present(name)
    browser.is_text_present(description)

@then(parsers.cfparse('I "edit" the description with a {newdescription}'))
def step_impl(browser, newdescription):
    button = browser.find_by_id("details-edit-link")
    button.click()
    make_screenshot(browser, "2.4.1 Edit a role description", "Permissions - Role Management", None,
                    description="From the role's detail page, you can edit the description by click the edit button next to the role description in the details area",
                    previous="2.3.1 Access role details",
                    next="2.4.2 Save a role description")
    sleep(1)
    browser.fill('fname', newdescription)
    
@then(parsers.cfparse('I "save" the new description {newdescription}'))
def step_impl(browser, newdescription):
    button = browser.find_by_id("details-edit-link")
    button.click()
    make_screenshot(browser, "2.4.2 Save a role description", "Permissions - Role Management", None,
                    description="Click the save button after you have entered the new description in the text field, to confirm your changes.",
                    previous="2.4.1 Edit a role description",
                    next="2.5.1 Delete a role")
    assert browser.is_text_present(newdescription)

@then('I go back to the page for the role management')
def step_impl(browser):
    links_found = browser.find_by_id('back-link')
    links_found.click()

@then(parsers.cfparse('I click the delete button next to a chosen role {name} in the table of roles'))
def step_impl(browser, name):
    sleep(1)
    button = browser.find_by_id("deleteRolesButtonBootstrap_role." + name)
    button.click()
    make_screenshot(browser, "2.5.1 Delete a role", "Permissions - Role Management", None,
                    description="Delete a role on the role management page, by clicking the button with the trash can icon next to a role in the table of roles.",
                    previous="2.4.2 Save a role description",
                    next="2.5.2 Confirmation to delete a role")

@then('I confirm to delete the chosen role')
def step_impl(browser):
    assert browser.is_text_present("wirklich löschen?") is True
    sleep(1)
    make_screenshot(browser, "2.5.2 Confirmation to delete a role", "Permissions - Role Management", None,
                    description="Confirm you wish to delete the selected role and it will be removed from the table of roles.</br></br>NOTE: All the permissions that were set by the deleted role, will be removed from the users and roles who are members of it. The role membership will not automatically be removed.",
                    previous="2.5.1 Delete a role",
                    next="2.6.1 Add a role membership to a role")
    browser.find_by_text('Okay').click()

@then(parsers.cfparse('the chosen role {name} is no longer in the table of roles'))
def step_impl(browser, name):
    sleep(2)
    assert browser.is_element_not_present_by_id("deleteRolesButtonBootstrap_role." + name)

@given(parsers.cfparse('the detail page for a chosen role with {name} and {description} is shown'))
def step_impl(browser, environment, name, description):
    url = environment.get("baseurl") + "/dashboard/roles.html"
    sleep(1)
    browser.visit(url)
    sleep(1)
    browser.fill('newrolename', name)
    browser.fill('newroledescription', description)
    button = browser.find_by_id("createRoleButton")
    button.click()
    button = browser.find_by_id("rolesEditButtonBootstrap_role." + name)
    button.click()

@given('I check a role from the role membership list that I want to add')
def step_impl(browser):
    role = browser.find_by_id('rolelistentry_role.easy2schedule_user')
    role.click()
    make_screenshot(browser, "2.6.1 Add a role membership to a role", "Permissions - Role Management", None,
                    description="On the role detail page, Click the checkbox next to a role from the role membership list that you wish to add",
                    previous="2.5.2 Confirmation to delete a role",
                    next="2.6.2 New permissions are updated")
    sleep(1)
    browser.reload()
    sleep(1)

@given('the total permission table contains the name of the membership role in the source column')
def step_impl(browser):
    browser.execute_script("window.scrollBy(0,1200)", "")
    make_screenshot(browser, "2.6.2 New permissions are updated", "Permissions - Role Management", None,
                    description="The permissions from the new role membership are inherited immediately. Their weight and action will be calculated and displayed in the total permission table.",
                    previous="2.6.1 Add a role membership to a role",
                    next="2.6.3 Remove a role membership from a role")
    table_entry = browser.find_by_css('.force-wrap')
    entries = table_entry.find_by_text('role.easy2schedule_user')
    len(entries) < 0
    
@when('I click the membership role tag in the role membership box that I want to remove')
def step_impl(browser):
    browser.execute_script("window.scrollTo(0, 0);")
    roletag = browser.find_by_id('roletag_role.easy2schedule_user')
    make_screenshot(browser, "2.6.3 Remove a role membership from a role", "Permissions - Role Management", None,
                    description="On the role detail page, click a membership role tag in the box, to remove it. Alternatively, you may uncheck it from the role membership list",
                    previous="2.6.2 New permissions are updated",
                    next="2.6.4 Role membership updated")
    roletag.click()
    sleep(1)
    browser.reload()
    sleep(1)

@then('the tag with the name of the membership role is removed')
def step_impl(browser):
    browser.is_element_not_present_by_id('roletag_role.easy2schedule_user')
    make_screenshot(browser, "2.6.4 Role membership updated", "Permissions - Role Management", None,
                    description="The membership tag is removed from the role membership box and only current role memberships are shown.",
                    previous="2.6.3 Remove a role membership from a role",
                    next="2.6.5 Inherited permissions are removed")
    
    
@then(parsers.cfparse('the total permission table for role {name} does not contain the name of the membership role in the source column'))
def step_impl(browser, name):
    table_entry = browser.find_by_css('.force-wrap')
    entries = table_entry.find_by_text('role.easy2schedule_user')
    len(entries) > 0
    browser.execute_script("window.scrollBy(0,1200)", "")
    make_screenshot(browser, "2.6.5 Inherited permissions are removed", "Permissions - Role Management", None,
                    description="The permissions that were given by the role membership, are removed immediately. The effects of thier removal will be calculated and shown in the total permission table.",
                    previous="2.6.4 Role membership updated",
)
    
    links_found = browser.find_by_id('back-link')
    links_found.click()
    button = browser.find_by_id("deleteRolesButtonBootstrap_role." + name)
    button.click()
    browser.find_by_text('Okay').click()


