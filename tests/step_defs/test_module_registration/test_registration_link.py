from time import sleep

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pytest_bdd import scenarios, given, when, then, parsers

from tests.helper_functions import make_screenshot

scenarios("../../features/module_login/registration.feature")


@given("I'm on the login page")
def step_impl(browser, environment):
    url = environment.get("baseurl") + "/"
    browser.visit(url)
    sleep(0.2)
    pass


@when("I press the register button")
def step_impl(browser):
    link = browser.links.find_by_partial_text('Jetzt registrieren')
    make_screenshot(browser, "Registration process", "System", "register_now_button",
                    description="Click on the register link to get on the registration page.")
    link.click()


@then("I want to be redirected to the registration-page")
def step_impl(browser):
    assert browser.is_text_present('Registrierung') is True
    assert browser.is_text_present('Erstellen Sie Ihr Konto') is True
    assert browser.is_text_present('E-Mail') is True
    assert browser.is_text_present('Passwort') is True
    assert browser.is_text_present('Passwort wiederholen') is True
    assert browser.is_text_present('Ich habe die Datenschutzerklärung gelesen und bin mit dieser Einverstanden') is True


@given("I'm on the registration page")
def step_impl(browser, environment):
    url = environment.get("baseurl") + "/registration/"
    browser.visit(url)
    sleep(0.2)


@when(parsers.cfparse('I fill in my "{username}", "{email}" and "{password}"'))
def step_impl(browser, username, email, password):
    browser.fill('login', username)
    browser.fill('email', email)
    browser.fill('password', password)
    browser.fill('password_confirmation', password)


@when("I checked the data-privacy-checkbox")
def step_impl(browser):
    checkbox = browser.find_by_id("privacy_agreement")
    checkbox.click()
    make_screenshot(browser, "Filling out the registration-form", "System", "privacy_agreement",
                    description="Enter your email-address as well as your desired password. Enter the same password in the password-repeat-field and accept the data-privacy-agreement by checking the checkbox")



@when("I press the form submit button")
def step_impl(browser):
    button = browser.find_by_id("register_button")
    make_screenshot(browser, "Submit the registration-form", "System", "register_button",
                    description="After entering your data and accepting the data-privacy-aggreement, submit the form by clicking the submit-button.")
    button.click()


@then("I want that an account with the given data is created")
def step_impl(browser):
    assert browser.is_text_present('Du bekommst gleich eine Email von uns') is True
    # Making screenshot fails here because of the opened alert-window. Need to rework the alertbox into a normal html-popup first
    #make_screenshot(browser, "Registration successfull", "System", "",
    #                description="Your registration has been submitted successfully if a popup shows up. Please check your emails and click on the confirmation-link we sent you.")
    #alert.accept()



@when('I click on the "Anmelden" link')
def step_impl(browser):
    link = browser.links.find_by_partial_text('Anmelden')
    link.click()


@then("I want to be redirected to the login-screen")
def step_impl(browser):
    assert browser.is_text_present('Anmelden') is True
