from pytest import fixture
import requests
import json
from time import sleep


@fixture()
def default_user(request, environment, user_apikey):
    url = f"{environment.get('apiurl')}/es/cmd/addRightsRole"

    payload = json.dumps({
        "name": f"user.{environment.get('default_login')}",
        "description": "my own rule22",
        "definition": {
            "import": [
                "role.default_user"
            ],
            "rules": {}
        }
    })
    headers = {
        'apikey': f'{user_apikey}',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print("SET ROLE TO", payload)
    return response.text


@fixture()
def easy2schedule_user(request, environment, user_apikey):
    url = f"{environment.get('apiurl')}/es/cmd/addRightsRole"

    payload = json.dumps({
        "name": f"user.{environment.get('default_login')}",
        "description": "my own rule22",
        "definition": {
            "import": [
                "role.default_user",
                "role.easy2schedule_user"
            ],
            "rules": {}
        }
    })
    headers = {
        'apikey': f'{user_apikey}',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print("SET ROLE TO", payload)
    return response.text


@fixture()
def easy2schedule_user_admin(request, environment, user_apikey):
    url = f"{environment.get('apiurl')}/es/cmd/addRightsRole"

    payload = json.dumps({
        "name": f"user.{environment.get('default_login')}",
        "description": "my own rule22",
        "definition": {
            "import": [
                "role.default_user",
                "role.easy2schedule_user",
                "role.easy2schedule_user_admin"
            ],
            "rules": {}
        }
    })
    headers = {
        'apikey': f'{user_apikey}',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print("SET ROLE TO", payload)
    return response.text


@fixture()
def easy2track_user(request, environment, user_apikey):
    url = f"{environment.get('apiurl')}/es/cmd/addRightsRole"

    payload = json.dumps({
        "name": f"user.{environment.get('default_login')}",
        "description": "my own rule22",
        "definition": {
            "import": [
                "role.default_user",
                "role.easy2track_user"
            ],
            "rules": {}
        }
    })
    headers = {
        'apikey': f'{user_apikey}',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print("SET ROLE TO", payload)
    return response.text


@fixture()
def simplycollect_user(request, environment, user_apikey):
    url = f"{environment.get('apiurl')}/es/cmd/addRightsRole"

    payload = json.dumps({
        "name": f"user.{environment.get('default_login')}",
        "description": "my own rule22",
        "definition": {
            "import": [
                "role.default_user",
                "role.simplycollect_user"
            ],
            "rules": {}
        }
    })
    headers = {
        'apikey': f'{user_apikey}',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print("SET ROLE TO", payload)
    return response.text


@fixture()
def system_groups_admin(request, environment, user_apikey):
    url = f"{environment.get('apiurl')}/es/cmd/addRightsRole"

    payload = json.dumps({
        "name": f"user.{environment.get('default_login')}",
        "description": "BDD Created",
        "definition": {
            "import": [
                "role.default_user"
            ],
            "rules": {
                "system.groups": {"action": "forceallow", "weight": 1000}
            }
        }
    })
    headers = {
        'apikey': f'{user_apikey}',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    print("SET ROLE TO", payload)
    return response.text
