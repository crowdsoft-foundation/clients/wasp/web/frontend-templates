from time import sleep
from tests.helper_functions import make_screenshot, set_viewport_size
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from splinter import Browser
from pytest import fixture
import warnings
import json
import time
import requests
import json
from pytest_bdd import scenarios, given, when, then, parsers

@fixture()
def browser_engine(request):
    return request.config.getoption("--browser_engine")


@fixture()
def splinter_remote_url(address):
    return "http://localhost:4444"

@fixture()
def headless(request):
    return request.config.getoption("--headless")

@fixture()
def credentials(request):
    data = {"username": request.config.getoption("--credentials").split(":")[0], "password": request.config.getoption("--credentials").split(":")[1]}
    return data

@fixture()
def admin_credentials(request):
    data = {"username": request.config.getoption("--admin_credentials").split(":")[0], "password": request.config.getoption("--admin_credentials").split(":")[1]}
    return data

@fixture()
def test_auth(request):
    return request.config.getoption("--test_auth")

@fixture()
def size(request):
    return request.config.getoption("--size")


@fixture()
def window_size(browser):
    size = browser.driver.execute_script("return [window.innerWidth, window.innerHeight];")
    return {"width": size[0], "height": size[1]}


@fixture()
def environment(request, credentials, admin_credentials):
    with open('./environments.json') as json_file:
        ENVIRONMENTS = json.load(json_file)

    if ENVIRONMENTS.get(request.config.getoption("--environment")) is None:
        raise Exception("Environment unknown")
    else:
        data = ENVIRONMENTS.get(request.config.getoption("--environment"))
        if credentials.get("username") is not None and credentials.get("password") is not None:
            data["default_login"] = credentials.get("username")
            data["default_password"] = credentials.get("password")
        if admin_credentials.get("username") is not None and admin_credentials.get("password") is not None:
            data["admin_login"] = admin_credentials.get("username")
            data["admin_password"] = admin_credentials.get("password")
        return data


@fixture()
def login(registered_account, browser, environment):
    url = environment.get("baseurl")
    browser.visit(url)
    sleep(1)
    browser.fill('login', environment.get("default_login"))
    browser.fill('password', environment.get("default_password"))

    button = browser.find_by_id("register_button")
    button.click()
    end_time = time.time() + 1
    while True:
        cookies = browser.driver.get_cookies()
        for cookie in cookies:
            if cookie.get("name","") == "apikey" and cookie.get("value","") != "":
                return True
        try:
            button.click()
        except:
            pass

        if time.time() > end_time:
            break
    sleep(0.1)

@fixture()
def admin_login(browser, environment, credentials):
    url = environment.get("baseurl")
    browser.visit(url)
    sleep(0.5)
    browser.fill('login', environment.get("admin_login"))
    browser.fill('password', environment.get("admin_password"))
    button = browser.find_by_id("register_button")
    button.click()
    end_time = time.time() + 30
    while True:
        cookies = browser.driver.get_cookies()
        
        for cookie in cookies:
            if cookie.get("name","") == "apikey" and cookie.get("value","") != "":
                return True
        if time.time() > end_time:
            break

@fixture()
def admin_apikey(environment):
    url = f"{environment.get('apiurl')}/login"

    response = requests.get(url, auth=requests.auth.HTTPBasicAuth(environment.get("admin_login"), environment.get("admin_password")))

    return response.json().get("apikey")

@fixture()
def user_apikey(browser, environment):
    cookies = browser.driver.get_cookies()
    for cookie in cookies:
        if cookie.get("name", "") == "apikey" and cookie.get("value", "") != "":
            return cookie.get("value", "")

    url = f"{environment.get('apiurl')}/login"

    response = requests.get(url, auth=requests.auth.HTTPBasicAuth(environment.get("default_login"), environment.get("default_password")))

    return response.json().get("apikey")

@fixture()
def registered_account(browser, environment):
    apiurl = environment.get("apiurl")

    url = f"{apiurl}/registration"

    payload = json.dumps({
        "username": environment.get("default_login"),
        "password": environment.get("default_password")
    })
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    taskid = response.json().get("taskid")

    confirm_url = f"{apiurl}/confirm_registration?k={taskid}"
    payload = {}
    headers = {}

    response = requests.request("GET", confirm_url, headers=headers, data=payload)
    print("REGISTRATION CONFIRMATION RESULT", response.text)


@fixture()
def browser(browser_engine, headless, size, test_auth):
    maximize = False
    if size != "max" and len(size.split('x')) == 2:
        window_width, window_height = size.split('x', 1)
    else:
        window_width, window_height = (0, 0)
        maximize = True

    headless_mode = False
    headless = headless.lower()

    if headless == "yes" or headless == "1" or headless == "true":
        headless_mode = True

    if browser_engine == "firefox":
        browser = Browser('firefox', headless=headless_mode)
        if maximize:
            browser.driver.maximize_window()
        else:
            # browser.driver.set_window_size(window_width, window_height)
            set_viewport_size(browser.driver, window_width, window_height)
    elif browser_engine == "chrome":
        browser = Browser('chrome', headless=headless_mode)
        if maximize:
            browser.driver.maximize_window()
        else:
            # browser.driver.set_window_size(window_width, window_height)
            set_viewport_size(browser.driver, window_width, window_height)
    elif browser_engine == "remote":
        browser = Browser(driver_name="remote",
        browser='chrome',
        command_executor=f"https://{test_auth}odin.planblick.com/selenium")
        if maximize:
            browser.driver.maximize_window()
        else:
            # browser.driver.set_window_size(window_width, window_height)
            set_viewport_size(browser.driver, window_width, window_height)
    else:
        raise Exception("Given browser_engine is unknown")
    browser.wait_time = 10
    yield browser
    browser.quit()

@fixture()
def no_js_error(browser):
    def internal_no_js_error():
        browserlogs = browser.driver.get_log('browser')
        errors = []
        for entry in browserlogs:
            if entry['level'] == 'SEVERE' and ("Cannot access video stream" not in entry.get("message")) and ("i18n" not in entry.get("message")):
                errors.append(entry)
                #warnings.warn(UserWarning("Severe JS-error occured: " + json.dumps(entry)))
            elif entry['level'] == 'SEVERE':
                warnings.warn(UserWarning("Severe JS-error occured: " + json.dumps(entry)))

        if len(errors) > 0:
            print("Javascript errors:", errors)
            raise Exception("Javascript-Errors found in log: " + json.dumps(errors))

        return errors
    return internal_no_js_error

@then("I want to be redirected to the login-page")
def step_impl(browser):
    assert browser.is_text_present('Melden Sie sich an') is True

@given("I'm not logged in")
def step_impl():
    pass

@given("I'm logged in")
def step_impl(login):
    pass