FROM planblick.registry.jetbrains.space/p/planblick/images/python-base-image:production

# install pip modules (own filesystem layer)
COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt --no-cache-dir

# copy our app
COPY src /src


# some info for the callers
EXPOSE 8000
#VOLUME ["/src"]
WORKDIR "/src/app"

# run our service
RUN chmod 777 /src/runApp.sh && chmod +x /src/runApp.sh
CMD ["/src/runApp.sh"]


